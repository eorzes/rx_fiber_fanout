// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Fri Mar 17 16:34:57 2023
// Host        : PCPHESE71 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top PCS_PMA -prefix
//               PCS_PMA_ PCS_PMA_sim_netlist.v
// Design      : PCS_PMA
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_8,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module PCS_PMA
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire \<const1> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire NLW_U0_mmcm_locked_out_UNCONNECTED;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign mmcm_locked_out = \<const1> ;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  PCS_PMA_PCS_PMA_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(NLW_U0_mmcm_locked_out_UNCONNECTED),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
  VCC VCC
       (.P(\<const1> ));
endmodule

module PCS_PMA_PCS_PMA_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    pma_reset_out,
    signal_detect,
    userclk2,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  input pma_reset_out;
  input signal_detect;
  input userclk2;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [2:0]configuration_vector;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_out;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire pma_reset_out;
  wire powerdown;
  wire resetdone;
  wire resetdone_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire userclk2;
  wire NLW_PCS_PMA_core_an_enable_UNCONNECTED;
  wire NLW_PCS_PMA_core_an_interrupt_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_den_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_dwe_UNCONNECTED;
  wire NLW_PCS_PMA_core_drp_req_UNCONNECTED;
  wire NLW_PCS_PMA_core_en_cdet_UNCONNECTED;
  wire NLW_PCS_PMA_core_ewrap_UNCONNECTED;
  wire NLW_PCS_PMA_core_loc_ref_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_out_UNCONNECTED;
  wire NLW_PCS_PMA_core_mdio_tri_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_PCS_PMA_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_PCS_PMA_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b1001010000" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "PCS_PMA" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "zynquplus" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  PCS_PMA_gig_ethernet_pcs_pma_v16_2_8 PCS_PMA_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_PCS_PMA_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_PCS_PMA_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(1'b1),
        .drp_daddr(NLW_PCS_PMA_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_PCS_PMA_core_drp_den_UNCONNECTED),
        .drp_di(NLW_PCS_PMA_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_PCS_PMA_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_PCS_PMA_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_PCS_PMA_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_PCS_PMA_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_PCS_PMA_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_PCS_PMA_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_PCS_PMA_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(pma_reset_out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_PCS_PMA_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_PCS_PMA_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_PCS_PMA_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_PCS_PMA_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_PCS_PMA_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_PCS_PMA_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_PCS_PMA_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_PCS_PMA_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_PCS_PMA_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_PCS_PMA_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_PCS_PMA_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_PCS_PMA_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_PCS_PMA_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_PCS_PMA_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(userclk2));
  PCS_PMA_PCS_PMA_sync_block sync_block_reset_done
       (.data_in(resetdone_i),
        .resetdone(resetdone),
        .userclk2(userclk2));
  PCS_PMA_PCS_PMA_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispval),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(resetdone_i),
        .enablealign(enablealign),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .mgt_tx_reset(mgt_tx_reset),
        .pma_reset_out(pma_reset_out),
        .powerdown(powerdown),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk_out(rxoutclk_out),
        .rxp(rxp),
        .txbuferr(txbuferr),
        .txchardispmode_reg_reg_0(txchardispmode),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk_out(txoutclk_out),
        .txp(txp),
        .userclk2(userclk2));
endmodule

module PCS_PMA_PCS_PMA_clocking
   (gtrefclk_out,
    userclk2,
    userclk,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    rxoutclk,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output gtrefclk_out;
  output userclk2;
  output userclk;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input rxoutclk;
  output lopt;
  output lopt_1;
  input lopt_2;
  input lopt_3;
  input lopt_4;
  input lopt_5;

  wire \<const0> ;
  wire \<const1> ;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire \^lopt ;
  wire \^lopt_1 ;
  wire \^lopt_2 ;
  wire \^lopt_3 ;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;

  assign \^lopt  = lopt_2;
  assign \^lopt_1  = lopt_3;
  assign \^lopt_2  = lopt_4;
  assign \^lopt_3  = lopt_5;
  assign lopt = \<const1> ;
  assign lopt_1 = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH(1'b0),
    .REFCLK_HROW_CK_SEL(2'b00),
    .REFCLK_ICNTL_RX(2'b00)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    rxrecclk_bufg_inst
       (.CE(\^lopt ),
        .CEMASK(1'b1),
        .CLR(\^lopt_1 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(rxoutclk),
        .O(rxuserclk2_out));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk2_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(txoutclk),
        .O(userclk2));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    usrclk_bufg_inst
       (.CE(\^lopt_2 ),
        .CEMASK(1'b1),
        .CLR(\^lopt_3 ),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b1}),
        .I(txoutclk),
        .O(userclk));
endmodule

(* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
module PCS_PMA_PCS_PMA_gt
   (gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    cpllrefclksel_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drpwe_in,
    eyescanreset_in,
    eyescantrigger_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    gtrefclk1_in,
    loopback_in,
    pcsrsvdin_in,
    rx8b10ben_in,
    rxbufreset_in,
    rxcdrhold_in,
    rxcommadeten_in,
    rxdfelpmreset_in,
    rxlpmen_in,
    rxmcommaalignen_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxrate_in,
    rxusrclk_in,
    rxusrclk2_in,
    tx8b10ben_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdiffctrl_in,
    txelecidle_in,
    txinhibit_in,
    txpcsreset_in,
    txpd_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txusrclk_in,
    txusrclk2_in,
    cplllock_out,
    dmonitorout_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxclkcorcnt_out,
    rxcommadet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxoutclk_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxresetdone_out,
    txbufstatus_out,
    txoutclk_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txresetdone_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [2:0]cpllrefclksel_in;
  input [9:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drpwe_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [2:0]loopback_in;
  input [15:0]pcsrsvdin_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcommadeten_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [2:0]rxrate_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]tx8b10ben_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [4:0]txdiffctrl_in;
  input [0:0]txelecidle_in;
  input [0:0]txinhibit_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]cplllock_out;
  output [15:0]dmonitorout_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcommadet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxresetdone_out;
  output [1:0]txbufstatus_out;
  output [0:0]txoutclk_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txresetdone_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]NLW_inst_bufgtce_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtcemask_out_UNCONNECTED;
  wire [8:0]NLW_inst_bufgtdiv_out_UNCONNECTED;
  wire [0:0]NLW_inst_bufgtreset_out_UNCONNECTED;
  wire [2:0]NLW_inst_bufgtrstmask_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllfbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_cplllock_out_UNCONNECTED;
  wire [0:0]NLW_inst_cpllrefclklost_out_UNCONNECTED;
  wire [15:0]NLW_inst_dmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_dmonitoroutclk_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_common_out_UNCONNECTED;
  wire [15:0]NLW_inst_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_common_out_UNCONNECTED;
  wire [0:0]NLW_inst_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtrefclkmonitor_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxn_out_UNCONNECTED;
  wire [0:0]NLW_inst_gtytxp_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierategen3_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcierateidle_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllpd_out_UNCONNECTED;
  wire [1:0]NLW_inst_pcierateqpllreset_out_UNCONNECTED;
  wire [0:0]NLW_inst_pciesynctxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieusergen3rdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserphystatusrst_out_UNCONNECTED;
  wire [0:0]NLW_inst_pcieuserratestart_out_UNCONNECTED;
  wire [15:0]NLW_inst_pcsrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_phystatus_out_UNCONNECTED;
  wire [15:0]NLW_inst_pinrsrvdas_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout0_out_UNCONNECTED;
  wire [7:0]NLW_inst_pmarsvdout1_out_UNCONNECTED;
  wire [0:0]NLW_inst_powerpresent_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll0refclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1fbclklost_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1lock_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1outrefclk_out_UNCONNECTED;
  wire [0:0]NLW_inst_qpll1refclklost_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor0_out_UNCONNECTED;
  wire [7:0]NLW_inst_qplldmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor0_out_UNCONNECTED;
  wire [0:0]NLW_inst_refclkoutmonitor1_out_UNCONNECTED;
  wire [0:0]NLW_inst_resetexception_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrlock_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcdrphdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanbondseq_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanisaligned_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxchanrealign_out_UNCONNECTED;
  wire [4:0]NLW_inst_rxchbondo_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxckcaldone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcominitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcommadet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomsasdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxcomwakedet_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_inst_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_inst_rxctrl3_out_UNCONNECTED;
  wire [127:0]NLW_inst_rxdata_out_UNCONNECTED;
  wire [7:0]NLW_inst_rxdataextendrsvd_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxdatavalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxelecidle_out_UNCONNECTED;
  wire [5:0]NLW_inst_rxheader_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxheadervalid_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpstresetdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED;
  wire [7:0]NLW_inst_rxmonitorout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxosintstrobestarted_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxphalignerr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprbslocked_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxratedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk0_sel_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk0sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclk1_sel_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxrecclk1sel_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxrecclkout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsliderdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslipoutclkrdy_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxslippmardy_out_UNCONNECTED;
  wire [1:0]NLW_inst_rxstartofseq_out_UNCONNECTED;
  wire [2:0]NLW_inst_rxstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_rxvalid_out_UNCONNECTED;
  wire [3:0]NLW_inst_sdm0finalout_out_UNCONNECTED;
  wire [14:0]NLW_inst_sdm0testdata_out_UNCONNECTED;
  wire [3:0]NLW_inst_sdm1finalout_out_UNCONNECTED;
  wire [14:0]NLW_inst_sdm1testdata_out_UNCONNECTED;
  wire [9:0]NLW_inst_tcongpo_out_UNCONNECTED;
  wire [0:0]NLW_inst_tconrsvdout0_out_UNCONNECTED;
  wire [0:0]NLW_inst_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_inst_txcomfinish_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdccdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txdlysresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkfabric_out_UNCONNECTED;
  wire [0:0]NLW_inst_txoutclkpcs_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphaligndone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txphinitdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenn_out_UNCONNECTED;
  wire [0:0]NLW_inst_txqpisenp_out_UNCONNECTED;
  wire [0:0]NLW_inst_txratedone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txresetdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncdone_out_UNCONNECTED;
  wire [0:0]NLW_inst_txsyncout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdaddr_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubden_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdi_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubdwe_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubmdmtdo_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubrsvdout_out_UNCONNECTED;
  wire [0:0]NLW_inst_ubtxuart_out_UNCONNECTED;

  assign cplllock_out[0] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_COMMON_SCALING_FACTOR = "1" *) 
  (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
  (* C_ENABLE_COMMON_USRCLK = "0" *) 
  (* C_FORCE_COMMONS = "0" *) 
  (* C_FREERUN_FREQUENCY = "62.500000" *) 
  (* C_GT_REV = "57" *) 
  (* C_GT_TYPE = "2" *) 
  (* C_INCLUDE_CPLL_CAL = "2" *) 
  (* C_LOCATE_COMMON = "0" *) 
  (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) 
  (* C_LOCATE_RESET_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_RX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
  (* C_LOCATE_TX_USER_CLOCKING = "1" *) 
  (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) 
  (* C_PCIE_CORECLK_FREQ = "250" *) 
  (* C_PCIE_ENABLE = "0" *) 
  (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) 
  (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
  (* C_RX_BUFFBYPASS_MODE = "0" *) 
  (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_RX_BUFFER_MODE = "1" *) 
  (* C_RX_CB_DISP = "8'b00000000" *) 
  (* C_RX_CB_K = "8'b00000000" *) 
  (* C_RX_CB_LEN_SEQ = "1" *) 
  (* C_RX_CB_MAX_LEVEL = "1" *) 
  (* C_RX_CB_NUM_SEQ = "0" *) 
  (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_CC_DISP = "8'b00000000" *) 
  (* C_RX_CC_ENABLE = "1" *) 
  (* C_RX_CC_K = "8'b00010001" *) 
  (* C_RX_CC_LEN_SEQ = "2" *) 
  (* C_RX_CC_NUM_SEQ = "2" *) 
  (* C_RX_CC_PERIODICITY = "5000" *) 
  (* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) 
  (* C_RX_COMMA_M_ENABLE = "1" *) 
  (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
  (* C_RX_COMMA_P_ENABLE = "1" *) 
  (* C_RX_COMMA_P_VAL = "10'b0101111100" *) 
  (* C_RX_DATA_DECODING = "1" *) 
  (* C_RX_ENABLE = "1" *) 
  (* C_RX_INT_DATA_WIDTH = "20" *) 
  (* C_RX_LINE_RATE = "1.250000" *) 
  (* C_RX_MASTER_CHANNEL_IDX = "109" *) 
  (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) 
  (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_RX_OUTCLK_SOURCE = "1" *) 
  (* C_RX_PLL_TYPE = "2" *) 
  (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
  (* C_RX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_RX_SLIDE_MODE = "0" *) 
  (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_RX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_RX_USER_DATA_WIDTH = "16" *) 
  (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_RX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_SECONDARY_QPLL_ENABLE = "0" *) 
  (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
  (* C_SIM_CPLL_CAL_BYPASS = "1" *) 
  (* C_TOTAL_NUM_CHANNELS = "1" *) 
  (* C_TOTAL_NUM_COMMONS = "0" *) 
  (* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) 
  (* C_TXPROGDIV_FREQ_ENABLE = "1" *) 
  (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
  (* C_TXPROGDIV_FREQ_VAL = "125.000000" *) 
  (* C_TX_BUFFBYPASS_MODE = "0" *) 
  (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
  (* C_TX_BUFFER_MODE = "1" *) 
  (* C_TX_DATA_ENCODING = "1" *) 
  (* C_TX_ENABLE = "1" *) 
  (* C_TX_INT_DATA_WIDTH = "20" *) 
  (* C_TX_LINE_RATE = "1.250000" *) 
  (* C_TX_MASTER_CHANNEL_IDX = "109" *) 
  (* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) 
  (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) 
  (* C_TX_OUTCLK_SOURCE = "4" *) 
  (* C_TX_PLL_TYPE = "2" *) 
  (* C_TX_REFCLK_FREQUENCY = "156.250000" *) 
  (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
  (* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) 
  (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) 
  (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
  (* C_TX_USER_CLOCKING_SOURCE = "0" *) 
  (* C_TX_USER_DATA_WIDTH = "16" *) 
  (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
  (* C_TX_USRCLK_FREQUENCY = "62.500000" *) 
  (* C_USER_GTPOWERGOOD_DELAY_EN = "1" *) 
  PCS_PMA_PCS_PMA_gt_gtwizard_top inst
       (.bgbypassb_in(1'b1),
        .bgmonitorenb_in(1'b1),
        .bgpdb_in(1'b1),
        .bgrcalovrd_in({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .bgrcalovrdenb_in(1'b1),
        .bufgtce_out(NLW_inst_bufgtce_out_UNCONNECTED[0]),
        .bufgtcemask_out(NLW_inst_bufgtcemask_out_UNCONNECTED[2:0]),
        .bufgtdiv_out(NLW_inst_bufgtdiv_out_UNCONNECTED[8:0]),
        .bufgtreset_out(NLW_inst_bufgtreset_out_UNCONNECTED[0]),
        .bufgtrstmask_out(NLW_inst_bufgtrstmask_out_UNCONNECTED[2:0]),
        .cdrstepdir_in(1'b0),
        .cdrstepsq_in(1'b0),
        .cdrstepsx_in(1'b0),
        .cfgreset_in(1'b0),
        .clkrsvd0_in(1'b0),
        .clkrsvd1_in(1'b0),
        .cpllfbclklost_out(NLW_inst_cpllfbclklost_out_UNCONNECTED[0]),
        .cpllfreqlock_in(1'b0),
        .cplllock_out(NLW_inst_cplllock_out_UNCONNECTED[0]),
        .cplllockdetclk_in(1'b0),
        .cplllocken_in(1'b1),
        .cpllpd_in(1'b0),
        .cpllrefclklost_out(NLW_inst_cpllrefclklost_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .cpllreset_in(1'b0),
        .dmonfiforeset_in(1'b0),
        .dmonitorclk_in(1'b0),
        .dmonitorout_out(NLW_inst_dmonitorout_out_UNCONNECTED[15:0]),
        .dmonitoroutclk_out(NLW_inst_dmonitoroutclk_out_UNCONNECTED[0]),
        .drpaddr_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_common_in(1'b0),
        .drpclk_in(drpclk_in),
        .drpdi_common_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_common_out(NLW_inst_drpdo_common_out_UNCONNECTED[15:0]),
        .drpdo_out(NLW_inst_drpdo_out_UNCONNECTED[15:0]),
        .drpen_common_in(1'b0),
        .drpen_in(1'b0),
        .drprdy_common_out(NLW_inst_drprdy_common_out_UNCONNECTED[0]),
        .drprdy_out(NLW_inst_drprdy_out_UNCONNECTED[0]),
        .drprst_in(1'b0),
        .drpwe_common_in(1'b0),
        .drpwe_in(1'b0),
        .elpcaldvorwren_in(1'b0),
        .elpcalpaorwren_in(1'b0),
        .evoddphicaldone_in(1'b0),
        .evoddphicalstart_in(1'b0),
        .evoddphidrden_in(1'b0),
        .evoddphidwren_in(1'b0),
        .evoddphixrden_in(1'b0),
        .evoddphixwren_in(1'b0),
        .eyescandataerror_out(NLW_inst_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanmode_in(1'b0),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .freqos_in(1'b0),
        .gtgrefclk0_in(1'b0),
        .gtgrefclk1_in(1'b0),
        .gtgrefclk_in(1'b0),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtnorthrefclk00_in(1'b0),
        .gtnorthrefclk01_in(1'b0),
        .gtnorthrefclk0_in(1'b0),
        .gtnorthrefclk10_in(1'b0),
        .gtnorthrefclk11_in(1'b0),
        .gtnorthrefclk1_in(1'b0),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk00_in(1'b0),
        .gtrefclk01_in(1'b0),
        .gtrefclk0_in(gtrefclk0_in),
        .gtrefclk10_in(1'b0),
        .gtrefclk11_in(1'b0),
        .gtrefclk1_in(1'b0),
        .gtrefclkmonitor_out(NLW_inst_gtrefclkmonitor_out_UNCONNECTED[0]),
        .gtresetsel_in(1'b0),
        .gtrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtrxreset_in(1'b0),
        .gtrxresetsel_in(1'b0),
        .gtsouthrefclk00_in(1'b0),
        .gtsouthrefclk01_in(1'b0),
        .gtsouthrefclk0_in(1'b0),
        .gtsouthrefclk10_in(1'b0),
        .gtsouthrefclk11_in(1'b0),
        .gtsouthrefclk1_in(1'b0),
        .gttxreset_in(1'b0),
        .gttxresetsel_in(1'b0),
        .gtwiz_buffbypass_rx_done_out(NLW_inst_gtwiz_buffbypass_rx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_error_out(NLW_inst_gtwiz_buffbypass_rx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_rx_reset_in(1'b0),
        .gtwiz_buffbypass_rx_start_user_in(1'b0),
        .gtwiz_buffbypass_tx_done_out(NLW_inst_gtwiz_buffbypass_tx_done_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_error_out(NLW_inst_gtwiz_buffbypass_tx_error_out_UNCONNECTED[0]),
        .gtwiz_buffbypass_tx_reset_in(1'b0),
        .gtwiz_buffbypass_tx_start_user_in(1'b0),
        .gtwiz_gthe3_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe3_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe3_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gthe4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gthe4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_bufg_ce_in(1'b0),
        .gtwiz_gtye4_cpll_cal_cnt_tol_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_gtye4_cpll_cal_txoutclk_period_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_qpll0lock_in(1'b0),
        .gtwiz_reset_qpll0reset_out(NLW_inst_gtwiz_reset_qpll0reset_out_UNCONNECTED[0]),
        .gtwiz_reset_qpll1lock_in(1'b0),
        .gtwiz_reset_qpll1reset_out(NLW_inst_gtwiz_reset_qpll1reset_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_cdr_stable_out(NLW_inst_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_in(1'b0),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_in(1'b0),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_rx_active_out(NLW_inst_gtwiz_userclk_rx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_reset_in(1'b0),
        .gtwiz_userclk_rx_srcclk_out(NLW_inst_gtwiz_userclk_rx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk2_out(NLW_inst_gtwiz_userclk_rx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_rx_usrclk_out(NLW_inst_gtwiz_userclk_rx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userclk_tx_active_out(NLW_inst_gtwiz_userclk_tx_active_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_reset_in(1'b0),
        .gtwiz_userclk_tx_srcclk_out(NLW_inst_gtwiz_userclk_tx_srcclk_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk2_out(NLW_inst_gtwiz_userclk_tx_usrclk2_out_UNCONNECTED[0]),
        .gtwiz_userclk_tx_usrclk_out(NLW_inst_gtwiz_userclk_tx_usrclk_out_UNCONNECTED[0]),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .gtyrxn_in(1'b0),
        .gtyrxp_in(1'b0),
        .gtytxn_out(NLW_inst_gtytxn_out_UNCONNECTED[0]),
        .gtytxp_out(NLW_inst_gtytxp_out_UNCONNECTED[0]),
        .incpctrl_in(1'b0),
        .loopback_in({1'b0,1'b0,1'b0}),
        .looprsvd_in(1'b0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .lpbkrxtxseren_in(1'b0),
        .lpbktxrxseren_in(1'b0),
        .pcieeqrxeqadaptdone_in(1'b0),
        .pcierategen3_out(NLW_inst_pcierategen3_out_UNCONNECTED[0]),
        .pcierateidle_out(NLW_inst_pcierateidle_out_UNCONNECTED[0]),
        .pcierateqpll0_in({1'b0,1'b0,1'b0}),
        .pcierateqpll1_in({1'b0,1'b0,1'b0}),
        .pcierateqpllpd_out(NLW_inst_pcierateqpllpd_out_UNCONNECTED[1:0]),
        .pcierateqpllreset_out(NLW_inst_pcierateqpllreset_out_UNCONNECTED[1:0]),
        .pcierstidle_in(1'b0),
        .pciersttxsyncstart_in(1'b0),
        .pciesynctxsyncdone_out(NLW_inst_pciesynctxsyncdone_out_UNCONNECTED[0]),
        .pcieusergen3rdy_out(NLW_inst_pcieusergen3rdy_out_UNCONNECTED[0]),
        .pcieuserphystatusrst_out(NLW_inst_pcieuserphystatusrst_out_UNCONNECTED[0]),
        .pcieuserratedone_in(1'b0),
        .pcieuserratestart_out(NLW_inst_pcieuserratestart_out_UNCONNECTED[0]),
        .pcsrsvdin2_in(1'b0),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcsrsvdout_out(NLW_inst_pcsrsvdout_out_UNCONNECTED[15:0]),
        .phystatus_out(NLW_inst_phystatus_out_UNCONNECTED[0]),
        .pinrsrvdas_out(NLW_inst_pinrsrvdas_out_UNCONNECTED[15:0]),
        .pmarsvd0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pmarsvdin_in(1'b0),
        .pmarsvdout0_out(NLW_inst_pmarsvdout0_out_UNCONNECTED[7:0]),
        .pmarsvdout1_out(NLW_inst_pmarsvdout1_out_UNCONNECTED[7:0]),
        .powerpresent_out(NLW_inst_powerpresent_out_UNCONNECTED[0]),
        .qpll0clk_in(1'b0),
        .qpll0clkrsvd0_in(1'b0),
        .qpll0clkrsvd1_in(1'b0),
        .qpll0fbclklost_out(NLW_inst_qpll0fbclklost_out_UNCONNECTED[0]),
        .qpll0fbdiv_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpll0freqlock_in(1'b0),
        .qpll0lock_out(NLW_inst_qpll0lock_out_UNCONNECTED[0]),
        .qpll0lockdetclk_in(1'b0),
        .qpll0locken_in(1'b0),
        .qpll0outclk_out(NLW_inst_qpll0outclk_out_UNCONNECTED[0]),
        .qpll0outrefclk_out(NLW_inst_qpll0outrefclk_out_UNCONNECTED[0]),
        .qpll0pd_in(1'b1),
        .qpll0refclk_in(1'b0),
        .qpll0refclklost_out(NLW_inst_qpll0refclklost_out_UNCONNECTED[0]),
        .qpll0refclksel_in({1'b0,1'b0,1'b1}),
        .qpll0reset_in(1'b1),
        .qpll1clk_in(1'b0),
        .qpll1clkrsvd0_in(1'b0),
        .qpll1clkrsvd1_in(1'b0),
        .qpll1fbclklost_out(NLW_inst_qpll1fbclklost_out_UNCONNECTED[0]),
        .qpll1fbdiv_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpll1freqlock_in(1'b0),
        .qpll1lock_out(NLW_inst_qpll1lock_out_UNCONNECTED[0]),
        .qpll1lockdetclk_in(1'b0),
        .qpll1locken_in(1'b0),
        .qpll1outclk_out(NLW_inst_qpll1outclk_out_UNCONNECTED[0]),
        .qpll1outrefclk_out(NLW_inst_qpll1outrefclk_out_UNCONNECTED[0]),
        .qpll1pd_in(1'b1),
        .qpll1refclk_in(1'b0),
        .qpll1refclklost_out(NLW_inst_qpll1refclklost_out_UNCONNECTED[0]),
        .qpll1refclksel_in({1'b0,1'b0,1'b1}),
        .qpll1reset_in(1'b1),
        .qplldmonitor0_out(NLW_inst_qplldmonitor0_out_UNCONNECTED[7:0]),
        .qplldmonitor1_out(NLW_inst_qplldmonitor1_out_UNCONNECTED[7:0]),
        .qpllrsvd1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd2_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd3_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .qpllrsvd4_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rcalenb_in(1'b1),
        .refclkoutmonitor0_out(NLW_inst_refclkoutmonitor0_out_UNCONNECTED[0]),
        .refclkoutmonitor1_out(NLW_inst_refclkoutmonitor1_out_UNCONNECTED[0]),
        .resetexception_out(NLW_inst_resetexception_out_UNCONNECTED[0]),
        .resetovrd_in(1'b0),
        .rstclkentx_in(1'b0),
        .rx8b10ben_in(1'b1),
        .rxafecfoken_in(1'b1),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({\^rxbufstatus_out ,NLW_inst_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_inst_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_inst_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrfreqreset_in(1'b0),
        .rxcdrhold_in(1'b0),
        .rxcdrlock_out(NLW_inst_rxcdrlock_out_UNCONNECTED[0]),
        .rxcdrovrden_in(1'b0),
        .rxcdrphdone_out(NLW_inst_rxcdrphdone_out_UNCONNECTED[0]),
        .rxcdrreset_in(1'b0),
        .rxcdrresetrsv_in(1'b0),
        .rxchanbondseq_out(NLW_inst_rxchanbondseq_out_UNCONNECTED[0]),
        .rxchanisaligned_out(NLW_inst_rxchanisaligned_out_UNCONNECTED[0]),
        .rxchanrealign_out(NLW_inst_rxchanrealign_out_UNCONNECTED[0]),
        .rxchbonden_in(1'b0),
        .rxchbondi_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rxchbondlevel_in({1'b0,1'b0,1'b0}),
        .rxchbondmaster_in(1'b0),
        .rxchbondo_out(NLW_inst_rxchbondo_out_UNCONNECTED[4:0]),
        .rxchbondslave_in(1'b0),
        .rxckcaldone_out(NLW_inst_rxckcaldone_out_UNCONNECTED[0]),
        .rxckcalreset_in(1'b0),
        .rxckcalstart_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxcominitdet_out(NLW_inst_rxcominitdet_out_UNCONNECTED[0]),
        .rxcommadet_out(NLW_inst_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxcomsasdet_out(NLW_inst_rxcomsasdet_out_UNCONNECTED[0]),
        .rxcomwakedet_out(NLW_inst_rxcomwakedet_out_UNCONNECTED[0]),
        .rxctrl0_out({NLW_inst_rxctrl0_out_UNCONNECTED[15:2],\^rxctrl0_out }),
        .rxctrl1_out({NLW_inst_rxctrl1_out_UNCONNECTED[15:2],\^rxctrl1_out }),
        .rxctrl2_out({NLW_inst_rxctrl2_out_UNCONNECTED[7:2],\^rxctrl2_out }),
        .rxctrl3_out({NLW_inst_rxctrl3_out_UNCONNECTED[7:2],\^rxctrl3_out }),
        .rxdata_out(NLW_inst_rxdata_out_UNCONNECTED[127:0]),
        .rxdataextendrsvd_out(NLW_inst_rxdataextendrsvd_out_UNCONNECTED[7:0]),
        .rxdatavalid_out(NLW_inst_rxdatavalid_out_UNCONNECTED[1:0]),
        .rxdccforcestart_in(1'b0),
        .rxdfeagcctrl_in({1'b0,1'b1}),
        .rxdfeagchold_in(1'b0),
        .rxdfeagcovrden_in(1'b0),
        .rxdfecfokfcnum_in({1'b1,1'b1,1'b0,1'b1}),
        .rxdfecfokfen_in(1'b0),
        .rxdfecfokfpulse_in(1'b0),
        .rxdfecfokhold_in(1'b0),
        .rxdfecfokovren_in(1'b0),
        .rxdfekhhold_in(1'b0),
        .rxdfekhovrden_in(1'b0),
        .rxdfelfhold_in(1'b0),
        .rxdfelfovrden_in(1'b0),
        .rxdfelpmreset_in(1'b0),
        .rxdfetap10hold_in(1'b0),
        .rxdfetap10ovrden_in(1'b0),
        .rxdfetap11hold_in(1'b0),
        .rxdfetap11ovrden_in(1'b0),
        .rxdfetap12hold_in(1'b0),
        .rxdfetap12ovrden_in(1'b0),
        .rxdfetap13hold_in(1'b0),
        .rxdfetap13ovrden_in(1'b0),
        .rxdfetap14hold_in(1'b0),
        .rxdfetap14ovrden_in(1'b0),
        .rxdfetap15hold_in(1'b0),
        .rxdfetap15ovrden_in(1'b0),
        .rxdfetap2hold_in(1'b0),
        .rxdfetap2ovrden_in(1'b0),
        .rxdfetap3hold_in(1'b0),
        .rxdfetap3ovrden_in(1'b0),
        .rxdfetap4hold_in(1'b0),
        .rxdfetap4ovrden_in(1'b0),
        .rxdfetap5hold_in(1'b0),
        .rxdfetap5ovrden_in(1'b0),
        .rxdfetap6hold_in(1'b0),
        .rxdfetap6ovrden_in(1'b0),
        .rxdfetap7hold_in(1'b0),
        .rxdfetap7ovrden_in(1'b0),
        .rxdfetap8hold_in(1'b0),
        .rxdfetap8ovrden_in(1'b0),
        .rxdfetap9hold_in(1'b0),
        .rxdfetap9ovrden_in(1'b0),
        .rxdfeuthold_in(1'b0),
        .rxdfeutovrden_in(1'b0),
        .rxdfevphold_in(1'b0),
        .rxdfevpovrden_in(1'b0),
        .rxdfevsen_in(1'b0),
        .rxdfexyden_in(1'b1),
        .rxdlybypass_in(1'b1),
        .rxdlyen_in(1'b0),
        .rxdlyovrden_in(1'b0),
        .rxdlysreset_in(1'b0),
        .rxdlysresetdone_out(NLW_inst_rxdlysresetdone_out_UNCONNECTED[0]),
        .rxelecidle_out(NLW_inst_rxelecidle_out_UNCONNECTED[0]),
        .rxelecidlemode_in({1'b1,1'b1}),
        .rxeqtraining_in(1'b0),
        .rxgearboxslip_in(1'b0),
        .rxheader_out(NLW_inst_rxheader_out_UNCONNECTED[5:0]),
        .rxheadervalid_out(NLW_inst_rxheadervalid_out_UNCONNECTED[1:0]),
        .rxlatclk_in(1'b0),
        .rxlfpstresetdet_out(NLW_inst_rxlfpstresetdet_out_UNCONNECTED[0]),
        .rxlfpsu2lpexitdet_out(NLW_inst_rxlfpsu2lpexitdet_out_UNCONNECTED[0]),
        .rxlfpsu3wakedet_out(NLW_inst_rxlfpsu3wakedet_out_UNCONNECTED[0]),
        .rxlpmen_in(1'b1),
        .rxlpmgchold_in(1'b0),
        .rxlpmgcovrden_in(1'b0),
        .rxlpmhfhold_in(1'b0),
        .rxlpmhfovrden_in(1'b0),
        .rxlpmlfhold_in(1'b0),
        .rxlpmlfklovrden_in(1'b0),
        .rxlpmoshold_in(1'b0),
        .rxlpmosovrden_in(1'b0),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxmonitorout_out(NLW_inst_rxmonitorout_out_UNCONNECTED[7:0]),
        .rxmonitorsel_in({1'b0,1'b0}),
        .rxoobreset_in(1'b0),
        .rxoscalreset_in(1'b0),
        .rxoshold_in(1'b0),
        .rxosintcfg_in(1'b0),
        .rxosintdone_out(NLW_inst_rxosintdone_out_UNCONNECTED[0]),
        .rxosinten_in(1'b0),
        .rxosinthold_in(1'b0),
        .rxosintovrden_in(1'b0),
        .rxosintstarted_out(NLW_inst_rxosintstarted_out_UNCONNECTED[0]),
        .rxosintstrobe_in(1'b0),
        .rxosintstrobedone_out(NLW_inst_rxosintstrobedone_out_UNCONNECTED[0]),
        .rxosintstrobestarted_out(NLW_inst_rxosintstrobestarted_out_UNCONNECTED[0]),
        .rxosinttestovrden_in(1'b0),
        .rxosovrden_in(1'b0),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkfabric_out(NLW_inst_rxoutclkfabric_out_UNCONNECTED[0]),
        .rxoutclkpcs_out(NLW_inst_rxoutclkpcs_out_UNCONNECTED[0]),
        .rxoutclksel_in({1'b0,1'b1,1'b0}),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpd_in[1],1'b0}),
        .rxphalign_in(1'b0),
        .rxphaligndone_out(NLW_inst_rxphaligndone_out_UNCONNECTED[0]),
        .rxphalignen_in(1'b0),
        .rxphalignerr_out(NLW_inst_rxphalignerr_out_UNCONNECTED[0]),
        .rxphdlypd_in(1'b1),
        .rxphdlyreset_in(1'b0),
        .rxphovrden_in(1'b0),
        .rxpllclksel_in({1'b0,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_inst_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_inst_rxprbserr_out_UNCONNECTED[0]),
        .rxprbslocked_out(NLW_inst_rxprbslocked_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxprgdivresetdone_out(NLW_inst_rxprgdivresetdone_out_UNCONNECTED[0]),
        .rxprogdivreset_in(1'b0),
        .rxqpien_in(1'b0),
        .rxqpisenn_out(NLW_inst_rxqpisenn_out_UNCONNECTED[0]),
        .rxqpisenp_out(NLW_inst_rxqpisenp_out_UNCONNECTED[0]),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxratedone_out(NLW_inst_rxratedone_out_UNCONNECTED[0]),
        .rxratemode_in(1'b0),
        .rxrecclk0_sel_out(NLW_inst_rxrecclk0_sel_out_UNCONNECTED[0]),
        .rxrecclk0sel_out(NLW_inst_rxrecclk0sel_out_UNCONNECTED[1:0]),
        .rxrecclk1_sel_out(NLW_inst_rxrecclk1_sel_out_UNCONNECTED[0]),
        .rxrecclk1sel_out(NLW_inst_rxrecclk1sel_out_UNCONNECTED[1:0]),
        .rxrecclkout_out(NLW_inst_rxrecclkout_out_UNCONNECTED[0]),
        .rxresetdone_out(NLW_inst_rxresetdone_out_UNCONNECTED[0]),
        .rxslide_in(1'b0),
        .rxsliderdy_out(NLW_inst_rxsliderdy_out_UNCONNECTED[0]),
        .rxslipdone_out(NLW_inst_rxslipdone_out_UNCONNECTED[0]),
        .rxslipoutclk_in(1'b0),
        .rxslipoutclkrdy_out(NLW_inst_rxslipoutclkrdy_out_UNCONNECTED[0]),
        .rxslippma_in(1'b0),
        .rxslippmardy_out(NLW_inst_rxslippmardy_out_UNCONNECTED[0]),
        .rxstartofseq_out(NLW_inst_rxstartofseq_out_UNCONNECTED[1:0]),
        .rxstatus_out(NLW_inst_rxstatus_out_UNCONNECTED[2:0]),
        .rxsyncallin_in(1'b0),
        .rxsyncdone_out(NLW_inst_rxsyncdone_out_UNCONNECTED[0]),
        .rxsyncin_in(1'b0),
        .rxsyncmode_in(1'b0),
        .rxsyncout_out(NLW_inst_rxsyncout_out_UNCONNECTED[0]),
        .rxsysclksel_in({1'b0,1'b0}),
        .rxtermination_in(1'b0),
        .rxuserrdy_in(1'b1),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(rxusrclk_in),
        .rxvalid_out(NLW_inst_rxvalid_out_UNCONNECTED[0]),
        .sdm0data_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .sdm0finalout_out(NLW_inst_sdm0finalout_out_UNCONNECTED[3:0]),
        .sdm0reset_in(1'b0),
        .sdm0testdata_out(NLW_inst_sdm0testdata_out_UNCONNECTED[14:0]),
        .sdm0toggle_in(1'b0),
        .sdm0width_in({1'b0,1'b0}),
        .sdm1data_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .sdm1finalout_out(NLW_inst_sdm1finalout_out_UNCONNECTED[3:0]),
        .sdm1reset_in(1'b0),
        .sdm1testdata_out(NLW_inst_sdm1testdata_out_UNCONNECTED[14:0]),
        .sdm1toggle_in(1'b0),
        .sdm1width_in({1'b0,1'b0}),
        .sigvalidclk_in(1'b0),
        .tcongpi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tcongpo_out(NLW_inst_tcongpo_out_UNCONNECTED[9:0]),
        .tconpowerup_in(1'b0),
        .tconreset_in({1'b0,1'b0}),
        .tconrsvdin1_in({1'b0,1'b0}),
        .tconrsvdout0_out(NLW_inst_tconrsvdout0_out_UNCONNECTED[0]),
        .tstin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10bbypass_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx8b10ben_in(1'b1),
        .txbufdiffctrl_in(1'b0),
        .txbufstatus_out({\^txbufstatus_out ,NLW_inst_txbufstatus_out_UNCONNECTED[0]}),
        .txcomfinish_out(NLW_inst_txcomfinish_out_UNCONNECTED[0]),
        .txcominit_in(1'b0),
        .txcomsas_in(1'b0),
        .txcomwake_in(1'b0),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in[1:0]}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in[1:0]}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in[1:0]}),
        .txdata_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdataextendrsvd_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txdccdone_out(NLW_inst_txdccdone_out_UNCONNECTED[0]),
        .txdccforcestart_in(1'b0),
        .txdccreset_in(1'b0),
        .txdeemph_in({1'b0,1'b0}),
        .txdetectrx_in(1'b0),
        .txdiffctrl_in({1'b1,1'b1,1'b0,1'b0,1'b0}),
        .txdiffpd_in(1'b0),
        .txdlybypass_in(1'b1),
        .txdlyen_in(1'b0),
        .txdlyhold_in(1'b0),
        .txdlyovrden_in(1'b0),
        .txdlysreset_in(1'b0),
        .txdlysresetdone_out(NLW_inst_txdlysresetdone_out_UNCONNECTED[0]),
        .txdlyupdown_in(1'b0),
        .txelecidle_in(txelecidle_in),
        .txelforcestart_in(1'b0),
        .txheader_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txinhibit_in(1'b0),
        .txlatclk_in(1'b0),
        .txlfpstreset_in(1'b0),
        .txlfpsu2lpexit_in(1'b0),
        .txlfpsu3wake_in(1'b0),
        .txmaincursor_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txmargin_in({1'b0,1'b0,1'b0}),
        .txmuxdcdexhold_in(1'b0),
        .txmuxdcdorwren_in(1'b0),
        .txoneszeros_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txoutclkfabric_out(NLW_inst_txoutclkfabric_out_UNCONNECTED[0]),
        .txoutclkpcs_out(NLW_inst_txoutclkpcs_out_UNCONNECTED[0]),
        .txoutclksel_in({1'b1,1'b0,1'b1}),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpdelecidlemode_in(1'b0),
        .txphalign_in(1'b0),
        .txphaligndone_out(NLW_inst_txphaligndone_out_UNCONNECTED[0]),
        .txphalignen_in(1'b0),
        .txphdlypd_in(1'b1),
        .txphdlyreset_in(1'b0),
        .txphdlytstclk_in(1'b0),
        .txphinit_in(1'b0),
        .txphinitdone_out(NLW_inst_txphinitdone_out_UNCONNECTED[0]),
        .txphovrden_in(1'b0),
        .txpippmen_in(1'b0),
        .txpippmovrden_in(1'b0),
        .txpippmpd_in(1'b0),
        .txpippmsel_in(1'b0),
        .txpippmstepsize_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpisopd_in(1'b0),
        .txpllclksel_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_inst_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txpostcursorinv_in(1'b0),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprecursorinv_in(1'b0),
        .txprgdivresetdone_out(NLW_inst_txprgdivresetdone_out_UNCONNECTED[0]),
        .txprogdivreset_in(1'b0),
        .txqpibiasen_in(1'b0),
        .txqpisenn_out(NLW_inst_txqpisenn_out_UNCONNECTED[0]),
        .txqpisenp_out(NLW_inst_txqpisenp_out_UNCONNECTED[0]),
        .txqpistrongpdown_in(1'b0),
        .txqpiweakpup_in(1'b0),
        .txrate_in({1'b0,1'b0,1'b0}),
        .txratedone_out(NLW_inst_txratedone_out_UNCONNECTED[0]),
        .txratemode_in(1'b0),
        .txresetdone_out(NLW_inst_txresetdone_out_UNCONNECTED[0]),
        .txsequence_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txswing_in(1'b0),
        .txsyncallin_in(1'b0),
        .txsyncdone_out(NLW_inst_txsyncdone_out_UNCONNECTED[0]),
        .txsyncin_in(1'b0),
        .txsyncmode_in(1'b0),
        .txsyncout_out(NLW_inst_txsyncout_out_UNCONNECTED[0]),
        .txsysclksel_in({1'b0,1'b0}),
        .txuserrdy_in(1'b1),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0),
        .ubcfgstreamen_in(1'b0),
        .ubdaddr_out(NLW_inst_ubdaddr_out_UNCONNECTED[0]),
        .ubden_out(NLW_inst_ubden_out_UNCONNECTED[0]),
        .ubdi_out(NLW_inst_ubdi_out_UNCONNECTED[0]),
        .ubdo_in(1'b0),
        .ubdrdy_in(1'b0),
        .ubdwe_out(NLW_inst_ubdwe_out_UNCONNECTED[0]),
        .ubenable_in(1'b0),
        .ubgpi_in(1'b0),
        .ubintr_in(1'b0),
        .ubiolmbrst_in(1'b0),
        .ubmbrst_in(1'b0),
        .ubmdmcapture_in(1'b0),
        .ubmdmdbgrst_in(1'b0),
        .ubmdmdbgupdate_in(1'b0),
        .ubmdmregen_in(1'b0),
        .ubmdmshift_in(1'b0),
        .ubmdmsysrst_in(1'b0),
        .ubmdmtck_in(1'b0),
        .ubmdmtdi_in(1'b0),
        .ubmdmtdo_out(NLW_inst_ubmdmtdo_out_UNCONNECTED[0]),
        .ubrsvdout_out(NLW_inst_ubrsvdout_out_UNCONNECTED[0]),
        .ubtxuart_out(NLW_inst_ubtxuart_out_UNCONNECTED[0]));
endmodule

module PCS_PMA_PCS_PMA_gt_gthe4_channel_wrapper
   (in0,
    \gen_gtwizard_gthe4.drprdy_int ,
    gthtxn_out,
    gthtxp_out,
    \gen_gtwizard_gthe4.gtpowergood_int ,
    rxcdrlock_out,
    rxoutclk_out,
    rxoutclkpcs_out,
    gtwiz_userclk_rx_active_out,
    rxresetdone_out,
    txoutclk_out,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST ,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    D,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ,
    drpclk_in,
    DEN_O,
    DWE_O,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ,
    \gen_gtwizard_gthe4.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe4.rxprogdivreset_int ,
    RXRATE,
    \gen_gtwizard_gthe4.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe4.txprogdivreset_ch_int ,
    \gen_gtwizard_gthe4.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    Q,
    txctrl0_in,
    txctrl1_in,
    RXPD,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ,
    txctrl2_in,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output in0;
  output \gen_gtwizard_gthe4.drprdy_int ;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output \gen_gtwizard_gthe4.gtpowergood_int ;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST ;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [15:0]D;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ;
  input \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ;
  input \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ;
  input [0:0]drpclk_in;
  input DEN_O;
  input DWE_O;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  input \gen_gtwizard_gthe4.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe4.rxprogdivreset_int ;
  input [0:0]RXRATE;
  input \gen_gtwizard_gthe4.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  input \gen_gtwizard_gthe4.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [15:0]Q;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]RXPD;
  input [2:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ;
  input [1:0]txctrl2_in;
  input [6:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [15:0]D;
  wire DEN_O;
  wire DWE_O;
  wire [15:0]Q;
  wire [0:0]RXPD;
  wire [0:0]RXRATE;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  wire \gen_gtwizard_gthe4.drprdy_int ;
  wire \gen_gtwizard_gthe4.gtpowergood_int ;
  wire \gen_gtwizard_gthe4.gttxreset_int ;
  wire \gen_gtwizard_gthe4.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe4.rxuserrdy_int ;
  wire \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  wire \gen_gtwizard_gthe4.txuserrdy_int ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ;
  wire [2:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ;
  wire [6:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_userclk_rx_active_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire in0;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxoutclkpcs_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;

  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_channel channel_inst
       (.D(D),
        .DEN_O(DEN_O),
        .DWE_O(DWE_O),
        .Q(Q),
        .RXPD(RXPD),
        .RXRATE(RXRATE),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int (\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ),
        .\gen_gtwizard_gthe4.drprdy_int (\gen_gtwizard_gthe4.drprdy_int ),
        .\gen_gtwizard_gthe4.gtpowergood_int (\gen_gtwizard_gthe4.gtpowergood_int ),
        .\gen_gtwizard_gthe4.gttxreset_int (\gen_gtwizard_gthe4.gttxreset_int ),
        .\gen_gtwizard_gthe4.rxprogdivreset_int (\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .\gen_gtwizard_gthe4.rxuserrdy_int (\gen_gtwizard_gthe4.rxuserrdy_int ),
        .\gen_gtwizard_gthe4.txprogdivreset_ch_int (\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .\gen_gtwizard_gthe4.txuserrdy_int (\gen_gtwizard_gthe4.txuserrdy_int ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 (\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userclk_rx_active_out(gtwiz_userclk_rx_active_out),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .in0(in0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(rxcdrlock_out),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkpcs_out(rxoutclkpcs_out),
        .rxresetdone_out(rxresetdone_out),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(txresetdone_out));
endmodule

module PCS_PMA_PCS_PMA_gt_gtwizard_gthe4
   (gtpowergood_out,
    gthtxn_out,
    gthtxp_out,
    rxoutclk_out,
    txoutclk_out,
    gtwiz_userdata_rx_out,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    rxpd_in,
    drpclk_in,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    rxmcommaalignen_in,
    rxusrclk_in,
    txelecidle_in,
    gtwiz_userdata_tx_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_datapath_in,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output [0:0]gtpowergood_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]rxpd_in;
  input [0:0]drpclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input [0:0]rxmcommaalignen_in;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input [15:0]gtwiz_userdata_tx_in;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [1:0]txctrl2_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.cplllock_ch_int ;
  wire \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  wire [15:0]\gen_gtwizard_gthe4.drpdo_int ;
  wire \gen_gtwizard_gthe4.drpen_ch_int ;
  wire \gen_gtwizard_gthe4.drprdy_int ;
  wire \gen_gtwizard_gthe4.drpwe_ch_int ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_12 ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_5 ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_57 ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_7 ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_8 ;
  wire \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_9 ;
  wire \gen_gtwizard_gthe4.gen_cpll_cal_gthe4.cpll_cal_reset_int ;
  wire [7:1]\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpaddr_cpll_cal_int ;
  wire [15:0]\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpdi_cpll_cal_int ;
  wire \gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_1 ;
  wire \gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_2 ;
  wire \gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_3 ;
  wire [2:0]\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.txoutclksel_cpll_cal_int ;
  wire \gen_gtwizard_gthe4.gen_pwrgood_delay_inst[0].delay_powergood_inst_n_1 ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe4.gtpowergood_int ;
  wire \gen_gtwizard_gthe4.gttxreset_int ;
  wire \gen_gtwizard_gthe4.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe4.rxratemode_ch_int ;
  wire \gen_gtwizard_gthe4.rxuserrdy_int ;
  wire \gen_gtwizard_gthe4.txprgdivresetdone_int ;
  wire \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  wire \gen_gtwizard_gthe4.txprogdivreset_int ;
  wire \gen_gtwizard_gthe4.txuserrdy_int ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire rst_in0;
  wire [0:0]rxbufstatus_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  PCS_PMA_PCS_PMA_gt_gthe4_channel_wrapper \gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst 
       (.D(\gen_gtwizard_gthe4.drpdo_int ),
        .DEN_O(\gen_gtwizard_gthe4.drpen_ch_int ),
        .DWE_O(\gen_gtwizard_gthe4.drpwe_ch_int ),
        .Q(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpdi_cpll_cal_int ),
        .RXPD(\gen_gtwizard_gthe4.gen_pwrgood_delay_inst[0].delay_powergood_inst_n_1 ),
        .RXRATE(\gen_gtwizard_gthe4.rxratemode_ch_int ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int (\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ),
        .\gen_gtwizard_gthe4.drprdy_int (\gen_gtwizard_gthe4.drprdy_int ),
        .\gen_gtwizard_gthe4.gtpowergood_int (\gen_gtwizard_gthe4.gtpowergood_int ),
        .\gen_gtwizard_gthe4.gttxreset_int (\gen_gtwizard_gthe4.gttxreset_int ),
        .\gen_gtwizard_gthe4.rxprogdivreset_int (\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .\gen_gtwizard_gthe4.rxuserrdy_int (\gen_gtwizard_gthe4.rxuserrdy_int ),
        .\gen_gtwizard_gthe4.txprogdivreset_ch_int (\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .\gen_gtwizard_gthe4.txuserrdy_int (\gen_gtwizard_gthe4.txuserrdy_int ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST (\gen_gtwizard_gthe4.txprgdivresetdone_int ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 (\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_57 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 (\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_1 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 (\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_2 ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 (\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.txoutclksel_cpll_cal_int ),
        .\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 (\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpaddr_cpll_cal_int ),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_userclk_rx_active_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_8 ),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .in0(\gen_gtwizard_gthe4.cplllock_ch_int ),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(rxbufstatus_out),
        .rxcdrlock_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_5 ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(rxctrl0_out),
        .rxctrl1_out(rxctrl1_out),
        .rxctrl2_out(rxctrl2_out),
        .rxctrl3_out(rxctrl3_out),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxoutclkpcs_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_7 ),
        .rxresetdone_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_9 ),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(txbufstatus_out),
        .txctrl0_in(txctrl0_in),
        .txctrl1_in(txctrl1_in),
        .txctrl2_in(txctrl2_in),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out),
        .txresetdone_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_12 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal \gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst 
       (.DADDR_O(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpaddr_cpll_cal_int ),
        .DEN_O(\gen_gtwizard_gthe4.drpen_ch_int ),
        .DI_O(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.drpdi_cpll_cal_int ),
        .DO_I(\gen_gtwizard_gthe4.drpdo_int ),
        .DWE_O(\gen_gtwizard_gthe4.drpwe_ch_int ),
        .Q(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.txoutclksel_cpll_cal_int ),
        .RESET_IN(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.cpll_cal_reset_int ),
        .USER_CPLLLOCK_OUT_reg(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_3 ),
        .cpllpd_int_reg(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_1 ),
        .cpllreset_int_reg(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_2 ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.drprdy_int (\gen_gtwizard_gthe4.drprdy_int ),
        .\gen_gtwizard_gthe4.txprogdivreset_ch_int (\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .i_in_meta_reg(\gen_gtwizard_gthe4.txprogdivreset_int ),
        .i_in_meta_reg_0(\gen_gtwizard_gthe4.txprgdivresetdone_int ),
        .in0(\gen_gtwizard_gthe4.cplllock_ch_int ),
        .lopt(lopt_4),
        .lopt_1(lopt_5),
        .rst_in0(rst_in0),
        .txoutclk_out(txoutclk_out));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_delay_powergood \gen_gtwizard_gthe4.gen_pwrgood_delay_inst[0].delay_powergood_inst 
       (.RXPD(\gen_gtwizard_gthe4.gen_pwrgood_delay_inst[0].delay_powergood_inst_n_1 ),
        .RXRATE(\gen_gtwizard_gthe4.rxratemode_ch_int ),
        .\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 (\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_57 ),
        .out(gtpowergood_out),
        .rxoutclkpcs_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_7 ),
        .rxpd_in(rxpd_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_rxresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .rxresetdone_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_9 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0 \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_txresetdone_inst 
       (.drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .txresetdone_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_12 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gtwiz_reset \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst 
       (.RESET_IN(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.cpll_cal_reset_int ),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int (\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .\gen_gtwizard_gthe4.gtpowergood_int (\gen_gtwizard_gthe4.gtpowergood_int ),
        .\gen_gtwizard_gthe4.gttxreset_int (\gen_gtwizard_gthe4.gttxreset_int ),
        .\gen_gtwizard_gthe4.rxprogdivreset_int (\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .\gen_gtwizard_gthe4.rxuserrdy_int (\gen_gtwizard_gthe4.rxuserrdy_int ),
        .\gen_gtwizard_gthe4.txuserrdy_int (\gen_gtwizard_gthe4.txuserrdy_int ),
        .gtpowergood_out(gtpowergood_out),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_userclk_rx_active_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_8 ),
        .i_in_meta_reg(\gen_gtwizard_gthe4.gen_cpll_cal_gthe4.gen_cpll_cal_inst[0].gen_inst_cpll_cal.gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_inst_n_3 ),
        .rst_in0(rst_in0),
        .rst_in_out_reg(\gen_gtwizard_gthe4.txprogdivreset_int ),
        .rxcdrlock_out(\gen_gtwizard_gthe4.gen_channel_container[27].gen_enabled_channel.gthe4_channel_wrapper_inst_n_5 ),
        .rxusrclk_in(rxusrclk_in));
endmodule

(* C_CHANNEL_ENABLE = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) (* C_COMMON_SCALING_FACTOR = "1" *) (* C_CPLL_VCO_FREQUENCY = "2500.000000" *) 
(* C_ENABLE_COMMON_USRCLK = "0" *) (* C_FORCE_COMMONS = "0" *) (* C_FREERUN_FREQUENCY = "62.500000" *) 
(* C_GT_REV = "57" *) (* C_GT_TYPE = "2" *) (* C_INCLUDE_CPLL_CAL = "2" *) 
(* C_LOCATE_COMMON = "0" *) (* C_LOCATE_IN_SYSTEM_IBERT_CORE = "2" *) (* C_LOCATE_RESET_CONTROLLER = "0" *) 
(* C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER = "0" *) (* C_LOCATE_RX_USER_CLOCKING = "1" *) (* C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER = "0" *) 
(* C_LOCATE_TX_USER_CLOCKING = "1" *) (* C_LOCATE_USER_DATA_WIDTH_SIZING = "0" *) (* C_PCIE_CORECLK_FREQ = "250" *) 
(* C_PCIE_ENABLE = "0" *) (* C_RESET_CONTROLLER_INSTANCE_CTRL = "0" *) (* C_RESET_SEQUENCE_INTERVAL = "0" *) 
(* C_RX_BUFFBYPASS_MODE = "0" *) (* C_RX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) (* C_RX_BUFFER_MODE = "1" *) 
(* C_RX_CB_DISP = "8'b00000000" *) (* C_RX_CB_K = "8'b00000000" *) (* C_RX_CB_LEN_SEQ = "1" *) 
(* C_RX_CB_MAX_LEVEL = "1" *) (* C_RX_CB_NUM_SEQ = "0" *) (* C_RX_CB_VAL = "80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_CC_DISP = "8'b00000000" *) (* C_RX_CC_ENABLE = "1" *) (* C_RX_CC_K = "8'b00010001" *) 
(* C_RX_CC_LEN_SEQ = "2" *) (* C_RX_CC_NUM_SEQ = "2" *) (* C_RX_CC_PERIODICITY = "5000" *) 
(* C_RX_CC_VAL = "80'b00000000000000000000001011010100101111000000000000000000000000010100000010111100" *) (* C_RX_COMMA_M_ENABLE = "1" *) (* C_RX_COMMA_M_VAL = "10'b1010000011" *) 
(* C_RX_COMMA_P_ENABLE = "1" *) (* C_RX_COMMA_P_VAL = "10'b0101111100" *) (* C_RX_DATA_DECODING = "1" *) 
(* C_RX_ENABLE = "1" *) (* C_RX_INT_DATA_WIDTH = "20" *) (* C_RX_LINE_RATE = "1.250000" *) 
(* C_RX_MASTER_CHANNEL_IDX = "109" *) (* C_RX_OUTCLK_BUFG_GT_DIV = "1" *) (* C_RX_OUTCLK_FREQUENCY = "62.500000" *) 
(* C_RX_OUTCLK_SOURCE = "1" *) (* C_RX_PLL_TYPE = "2" *) (* C_RX_RECCLK_OUTPUT = "192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" *) 
(* C_RX_REFCLK_FREQUENCY = "156.250000" *) (* C_RX_SLIDE_MODE = "0" *) (* C_RX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_RX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_RX_USER_CLOCKING_SOURCE = "0" *) (* C_RX_USER_DATA_WIDTH = "16" *) (* C_RX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_RX_USRCLK_FREQUENCY = "62.500000" *) (* C_SECONDARY_QPLL_ENABLE = "0" *) (* C_SECONDARY_QPLL_REFCLK_FREQUENCY = "257.812500" *) 
(* C_SIM_CPLL_CAL_BYPASS = "1" *) (* C_TOTAL_NUM_CHANNELS = "1" *) (* C_TOTAL_NUM_COMMONS = "0" *) 
(* C_TOTAL_NUM_COMMONS_EXAMPLE = "0" *) (* C_TXPROGDIV_FREQ_ENABLE = "1" *) (* C_TXPROGDIV_FREQ_SOURCE = "2" *) 
(* C_TXPROGDIV_FREQ_VAL = "125.000000" *) (* C_TX_BUFFBYPASS_MODE = "0" *) (* C_TX_BUFFER_BYPASS_INSTANCE_CTRL = "0" *) 
(* C_TX_BUFFER_MODE = "1" *) (* C_TX_DATA_ENCODING = "1" *) (* C_TX_ENABLE = "1" *) 
(* C_TX_INT_DATA_WIDTH = "20" *) (* C_TX_LINE_RATE = "1.250000" *) (* C_TX_MASTER_CHANNEL_IDX = "109" *) 
(* C_TX_OUTCLK_BUFG_GT_DIV = "2" *) (* C_TX_OUTCLK_FREQUENCY = "62.500000" *) (* C_TX_OUTCLK_SOURCE = "4" *) 
(* C_TX_PLL_TYPE = "2" *) (* C_TX_REFCLK_FREQUENCY = "156.250000" *) (* C_TX_USER_CLOCKING_CONTENTS = "0" *) 
(* C_TX_USER_CLOCKING_INSTANCE_CTRL = "0" *) (* C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK = "1" *) (* C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 = "1" *) 
(* C_TX_USER_CLOCKING_SOURCE = "0" *) (* C_TX_USER_DATA_WIDTH = "16" *) (* C_TX_USRCLK2_FREQUENCY = "62.500000" *) 
(* C_TX_USRCLK_FREQUENCY = "62.500000" *) (* C_USER_GTPOWERGOOD_DELAY_EN = "1" *) 
module PCS_PMA_PCS_PMA_gt_gtwizard_top
   (gtwiz_userclk_tx_reset_in,
    gtwiz_userclk_tx_active_in,
    gtwiz_userclk_tx_srcclk_out,
    gtwiz_userclk_tx_usrclk_out,
    gtwiz_userclk_tx_usrclk2_out,
    gtwiz_userclk_tx_active_out,
    gtwiz_userclk_rx_reset_in,
    gtwiz_userclk_rx_active_in,
    gtwiz_userclk_rx_srcclk_out,
    gtwiz_userclk_rx_usrclk_out,
    gtwiz_userclk_rx_usrclk2_out,
    gtwiz_userclk_rx_active_out,
    gtwiz_buffbypass_tx_reset_in,
    gtwiz_buffbypass_tx_start_user_in,
    gtwiz_buffbypass_tx_done_out,
    gtwiz_buffbypass_tx_error_out,
    gtwiz_buffbypass_rx_reset_in,
    gtwiz_buffbypass_rx_start_user_in,
    gtwiz_buffbypass_rx_done_out,
    gtwiz_buffbypass_rx_error_out,
    gtwiz_reset_clk_freerun_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_pll_and_datapath_in,
    gtwiz_reset_tx_datapath_in,
    gtwiz_reset_rx_pll_and_datapath_in,
    gtwiz_reset_rx_datapath_in,
    gtwiz_reset_tx_done_in,
    gtwiz_reset_rx_done_in,
    gtwiz_reset_qpll0lock_in,
    gtwiz_reset_qpll1lock_in,
    gtwiz_reset_rx_cdr_stable_out,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    gtwiz_reset_qpll0reset_out,
    gtwiz_reset_qpll1reset_out,
    gtwiz_gthe3_cpll_cal_txoutclk_period_in,
    gtwiz_gthe3_cpll_cal_cnt_tol_in,
    gtwiz_gthe3_cpll_cal_bufg_ce_in,
    gtwiz_gthe4_cpll_cal_txoutclk_period_in,
    gtwiz_gthe4_cpll_cal_cnt_tol_in,
    gtwiz_gthe4_cpll_cal_bufg_ce_in,
    gtwiz_gtye4_cpll_cal_txoutclk_period_in,
    gtwiz_gtye4_cpll_cal_cnt_tol_in,
    gtwiz_gtye4_cpll_cal_bufg_ce_in,
    gtwiz_userdata_tx_in,
    gtwiz_userdata_rx_out,
    bgbypassb_in,
    bgmonitorenb_in,
    bgpdb_in,
    bgrcalovrd_in,
    bgrcalovrdenb_in,
    drpaddr_common_in,
    drpclk_common_in,
    drpdi_common_in,
    drpen_common_in,
    drpwe_common_in,
    gtgrefclk0_in,
    gtgrefclk1_in,
    gtnorthrefclk00_in,
    gtnorthrefclk01_in,
    gtnorthrefclk10_in,
    gtnorthrefclk11_in,
    gtrefclk00_in,
    gtrefclk01_in,
    gtrefclk10_in,
    gtrefclk11_in,
    gtsouthrefclk00_in,
    gtsouthrefclk01_in,
    gtsouthrefclk10_in,
    gtsouthrefclk11_in,
    pcierateqpll0_in,
    pcierateqpll1_in,
    pmarsvd0_in,
    pmarsvd1_in,
    qpll0clkrsvd0_in,
    qpll0clkrsvd1_in,
    qpll0fbdiv_in,
    qpll0lockdetclk_in,
    qpll0locken_in,
    qpll0pd_in,
    qpll0refclksel_in,
    qpll0reset_in,
    qpll1clkrsvd0_in,
    qpll1clkrsvd1_in,
    qpll1fbdiv_in,
    qpll1lockdetclk_in,
    qpll1locken_in,
    qpll1pd_in,
    qpll1refclksel_in,
    qpll1reset_in,
    qpllrsvd1_in,
    qpllrsvd2_in,
    qpllrsvd3_in,
    qpllrsvd4_in,
    rcalenb_in,
    sdm0data_in,
    sdm0reset_in,
    sdm0toggle_in,
    sdm0width_in,
    sdm1data_in,
    sdm1reset_in,
    sdm1toggle_in,
    sdm1width_in,
    tcongpi_in,
    tconpowerup_in,
    tconreset_in,
    tconrsvdin1_in,
    ubcfgstreamen_in,
    ubdo_in,
    ubdrdy_in,
    ubenable_in,
    ubgpi_in,
    ubintr_in,
    ubiolmbrst_in,
    ubmbrst_in,
    ubmdmcapture_in,
    ubmdmdbgrst_in,
    ubmdmdbgupdate_in,
    ubmdmregen_in,
    ubmdmshift_in,
    ubmdmsysrst_in,
    ubmdmtck_in,
    ubmdmtdi_in,
    drpdo_common_out,
    drprdy_common_out,
    pmarsvdout0_out,
    pmarsvdout1_out,
    qpll0fbclklost_out,
    qpll0lock_out,
    qpll0outclk_out,
    qpll0outrefclk_out,
    qpll0refclklost_out,
    qpll1fbclklost_out,
    qpll1lock_out,
    qpll1outclk_out,
    qpll1outrefclk_out,
    qpll1refclklost_out,
    qplldmonitor0_out,
    qplldmonitor1_out,
    refclkoutmonitor0_out,
    refclkoutmonitor1_out,
    rxrecclk0_sel_out,
    rxrecclk1_sel_out,
    rxrecclk0sel_out,
    rxrecclk1sel_out,
    sdm0finalout_out,
    sdm0testdata_out,
    sdm1finalout_out,
    sdm1testdata_out,
    tcongpo_out,
    tconrsvdout0_out,
    ubdaddr_out,
    ubden_out,
    ubdi_out,
    ubdwe_out,
    ubmdmtdo_out,
    ubrsvdout_out,
    ubtxuart_out,
    cdrstepdir_in,
    cdrstepsq_in,
    cdrstepsx_in,
    cfgreset_in,
    clkrsvd0_in,
    clkrsvd1_in,
    cpllfreqlock_in,
    cplllockdetclk_in,
    cplllocken_in,
    cpllpd_in,
    cpllrefclksel_in,
    cpllreset_in,
    dmonfiforeset_in,
    dmonitorclk_in,
    drpaddr_in,
    drpclk_in,
    drpdi_in,
    drpen_in,
    drprst_in,
    drpwe_in,
    elpcaldvorwren_in,
    elpcalpaorwren_in,
    evoddphicaldone_in,
    evoddphicalstart_in,
    evoddphidrden_in,
    evoddphidwren_in,
    evoddphixrden_in,
    evoddphixwren_in,
    eyescanmode_in,
    eyescanreset_in,
    eyescantrigger_in,
    freqos_in,
    gtgrefclk_in,
    gthrxn_in,
    gthrxp_in,
    gtnorthrefclk0_in,
    gtnorthrefclk1_in,
    gtrefclk0_in,
    gtrefclk1_in,
    gtresetsel_in,
    gtrsvd_in,
    gtrxreset_in,
    gtrxresetsel_in,
    gtsouthrefclk0_in,
    gtsouthrefclk1_in,
    gttxreset_in,
    gttxresetsel_in,
    incpctrl_in,
    gtyrxn_in,
    gtyrxp_in,
    loopback_in,
    looprsvd_in,
    lpbkrxtxseren_in,
    lpbktxrxseren_in,
    pcieeqrxeqadaptdone_in,
    pcierstidle_in,
    pciersttxsyncstart_in,
    pcieuserratedone_in,
    pcsrsvdin_in,
    pcsrsvdin2_in,
    pmarsvdin_in,
    qpll0clk_in,
    qpll0freqlock_in,
    qpll0refclk_in,
    qpll1clk_in,
    qpll1freqlock_in,
    qpll1refclk_in,
    resetovrd_in,
    rstclkentx_in,
    rx8b10ben_in,
    rxafecfoken_in,
    rxbufreset_in,
    rxcdrfreqreset_in,
    rxcdrhold_in,
    rxcdrovrden_in,
    rxcdrreset_in,
    rxcdrresetrsv_in,
    rxchbonden_in,
    rxchbondi_in,
    rxchbondlevel_in,
    rxchbondmaster_in,
    rxchbondslave_in,
    rxckcalreset_in,
    rxckcalstart_in,
    rxcommadeten_in,
    rxdfeagcctrl_in,
    rxdccforcestart_in,
    rxdfeagchold_in,
    rxdfeagcovrden_in,
    rxdfecfokfcnum_in,
    rxdfecfokfen_in,
    rxdfecfokfpulse_in,
    rxdfecfokhold_in,
    rxdfecfokovren_in,
    rxdfekhhold_in,
    rxdfekhovrden_in,
    rxdfelfhold_in,
    rxdfelfovrden_in,
    rxdfelpmreset_in,
    rxdfetap10hold_in,
    rxdfetap10ovrden_in,
    rxdfetap11hold_in,
    rxdfetap11ovrden_in,
    rxdfetap12hold_in,
    rxdfetap12ovrden_in,
    rxdfetap13hold_in,
    rxdfetap13ovrden_in,
    rxdfetap14hold_in,
    rxdfetap14ovrden_in,
    rxdfetap15hold_in,
    rxdfetap15ovrden_in,
    rxdfetap2hold_in,
    rxdfetap2ovrden_in,
    rxdfetap3hold_in,
    rxdfetap3ovrden_in,
    rxdfetap4hold_in,
    rxdfetap4ovrden_in,
    rxdfetap5hold_in,
    rxdfetap5ovrden_in,
    rxdfetap6hold_in,
    rxdfetap6ovrden_in,
    rxdfetap7hold_in,
    rxdfetap7ovrden_in,
    rxdfetap8hold_in,
    rxdfetap8ovrden_in,
    rxdfetap9hold_in,
    rxdfetap9ovrden_in,
    rxdfeuthold_in,
    rxdfeutovrden_in,
    rxdfevphold_in,
    rxdfevpovrden_in,
    rxdfevsen_in,
    rxdfexyden_in,
    rxdlybypass_in,
    rxdlyen_in,
    rxdlyovrden_in,
    rxdlysreset_in,
    rxelecidlemode_in,
    rxeqtraining_in,
    rxgearboxslip_in,
    rxlatclk_in,
    rxlpmen_in,
    rxlpmgchold_in,
    rxlpmgcovrden_in,
    rxlpmhfhold_in,
    rxlpmhfovrden_in,
    rxlpmlfhold_in,
    rxlpmlfklovrden_in,
    rxlpmoshold_in,
    rxlpmosovrden_in,
    rxmcommaalignen_in,
    rxmonitorsel_in,
    rxoobreset_in,
    rxoscalreset_in,
    rxoshold_in,
    rxosintcfg_in,
    rxosinten_in,
    rxosinthold_in,
    rxosintovrden_in,
    rxosintstrobe_in,
    rxosinttestovrden_in,
    rxosovrden_in,
    rxoutclksel_in,
    rxpcommaalignen_in,
    rxpcsreset_in,
    rxpd_in,
    rxphalign_in,
    rxphalignen_in,
    rxphdlypd_in,
    rxphdlyreset_in,
    rxphovrden_in,
    rxpllclksel_in,
    rxpmareset_in,
    rxpolarity_in,
    rxprbscntreset_in,
    rxprbssel_in,
    rxprogdivreset_in,
    rxqpien_in,
    rxrate_in,
    rxratemode_in,
    rxslide_in,
    rxslipoutclk_in,
    rxslippma_in,
    rxsyncallin_in,
    rxsyncin_in,
    rxsyncmode_in,
    rxsysclksel_in,
    rxtermination_in,
    rxuserrdy_in,
    rxusrclk_in,
    rxusrclk2_in,
    sigvalidclk_in,
    tstin_in,
    tx8b10bbypass_in,
    tx8b10ben_in,
    txbufdiffctrl_in,
    txcominit_in,
    txcomsas_in,
    txcomwake_in,
    txctrl0_in,
    txctrl1_in,
    txctrl2_in,
    txdata_in,
    txdataextendrsvd_in,
    txdccforcestart_in,
    txdccreset_in,
    txdeemph_in,
    txdetectrx_in,
    txdiffctrl_in,
    txdiffpd_in,
    txdlybypass_in,
    txdlyen_in,
    txdlyhold_in,
    txdlyovrden_in,
    txdlysreset_in,
    txdlyupdown_in,
    txelecidle_in,
    txelforcestart_in,
    txheader_in,
    txinhibit_in,
    txlatclk_in,
    txlfpstreset_in,
    txlfpsu2lpexit_in,
    txlfpsu3wake_in,
    txmaincursor_in,
    txmargin_in,
    txmuxdcdexhold_in,
    txmuxdcdorwren_in,
    txoneszeros_in,
    txoutclksel_in,
    txpcsreset_in,
    txpd_in,
    txpdelecidlemode_in,
    txphalign_in,
    txphalignen_in,
    txphdlypd_in,
    txphdlyreset_in,
    txphdlytstclk_in,
    txphinit_in,
    txphovrden_in,
    txpippmen_in,
    txpippmovrden_in,
    txpippmpd_in,
    txpippmsel_in,
    txpippmstepsize_in,
    txpisopd_in,
    txpllclksel_in,
    txpmareset_in,
    txpolarity_in,
    txpostcursor_in,
    txpostcursorinv_in,
    txprbsforceerr_in,
    txprbssel_in,
    txprecursor_in,
    txprecursorinv_in,
    txprogdivreset_in,
    txqpibiasen_in,
    txqpistrongpdown_in,
    txqpiweakpup_in,
    txrate_in,
    txratemode_in,
    txsequence_in,
    txswing_in,
    txsyncallin_in,
    txsyncin_in,
    txsyncmode_in,
    txsysclksel_in,
    txuserrdy_in,
    txusrclk_in,
    txusrclk2_in,
    bufgtce_out,
    bufgtcemask_out,
    bufgtdiv_out,
    bufgtreset_out,
    bufgtrstmask_out,
    cpllfbclklost_out,
    cplllock_out,
    cpllrefclklost_out,
    dmonitorout_out,
    dmonitoroutclk_out,
    drpdo_out,
    drprdy_out,
    eyescandataerror_out,
    gthtxn_out,
    gthtxp_out,
    gtpowergood_out,
    gtrefclkmonitor_out,
    gtytxn_out,
    gtytxp_out,
    pcierategen3_out,
    pcierateidle_out,
    pcierateqpllpd_out,
    pcierateqpllreset_out,
    pciesynctxsyncdone_out,
    pcieusergen3rdy_out,
    pcieuserphystatusrst_out,
    pcieuserratestart_out,
    pcsrsvdout_out,
    phystatus_out,
    pinrsrvdas_out,
    powerpresent_out,
    resetexception_out,
    rxbufstatus_out,
    rxbyteisaligned_out,
    rxbyterealign_out,
    rxcdrlock_out,
    rxcdrphdone_out,
    rxchanbondseq_out,
    rxchanisaligned_out,
    rxchanrealign_out,
    rxchbondo_out,
    rxckcaldone_out,
    rxclkcorcnt_out,
    rxcominitdet_out,
    rxcommadet_out,
    rxcomsasdet_out,
    rxcomwakedet_out,
    rxctrl0_out,
    rxctrl1_out,
    rxctrl2_out,
    rxctrl3_out,
    rxdata_out,
    rxdataextendrsvd_out,
    rxdatavalid_out,
    rxdlysresetdone_out,
    rxelecidle_out,
    rxheader_out,
    rxheadervalid_out,
    rxlfpstresetdet_out,
    rxlfpsu2lpexitdet_out,
    rxlfpsu3wakedet_out,
    rxmonitorout_out,
    rxosintdone_out,
    rxosintstarted_out,
    rxosintstrobedone_out,
    rxosintstrobestarted_out,
    rxoutclk_out,
    rxoutclkfabric_out,
    rxoutclkpcs_out,
    rxphaligndone_out,
    rxphalignerr_out,
    rxpmaresetdone_out,
    rxprbserr_out,
    rxprbslocked_out,
    rxprgdivresetdone_out,
    rxqpisenn_out,
    rxqpisenp_out,
    rxratedone_out,
    rxrecclkout_out,
    rxresetdone_out,
    rxsliderdy_out,
    rxslipdone_out,
    rxslipoutclkrdy_out,
    rxslippmardy_out,
    rxstartofseq_out,
    rxstatus_out,
    rxsyncdone_out,
    rxsyncout_out,
    rxvalid_out,
    txbufstatus_out,
    txcomfinish_out,
    txdccdone_out,
    txdlysresetdone_out,
    txoutclk_out,
    txoutclkfabric_out,
    txoutclkpcs_out,
    txphaligndone_out,
    txphinitdone_out,
    txpmaresetdone_out,
    txprgdivresetdone_out,
    txqpisenn_out,
    txqpisenp_out,
    txratedone_out,
    txresetdone_out,
    txsyncdone_out,
    txsyncout_out,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  input [0:0]gtwiz_userclk_tx_reset_in;
  input [0:0]gtwiz_userclk_tx_active_in;
  output [0:0]gtwiz_userclk_tx_srcclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk_out;
  output [0:0]gtwiz_userclk_tx_usrclk2_out;
  output [0:0]gtwiz_userclk_tx_active_out;
  input [0:0]gtwiz_userclk_rx_reset_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  output [0:0]gtwiz_userclk_rx_srcclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk_out;
  output [0:0]gtwiz_userclk_rx_usrclk2_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]gtwiz_buffbypass_tx_reset_in;
  input [0:0]gtwiz_buffbypass_tx_start_user_in;
  output [0:0]gtwiz_buffbypass_tx_done_out;
  output [0:0]gtwiz_buffbypass_tx_error_out;
  input [0:0]gtwiz_buffbypass_rx_reset_in;
  input [0:0]gtwiz_buffbypass_rx_start_user_in;
  output [0:0]gtwiz_buffbypass_rx_done_out;
  output [0:0]gtwiz_buffbypass_rx_error_out;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input [0:0]gtwiz_reset_tx_done_in;
  input [0:0]gtwiz_reset_rx_done_in;
  input [0:0]gtwiz_reset_qpll0lock_in;
  input [0:0]gtwiz_reset_qpll1lock_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output [0:0]gtwiz_reset_qpll0reset_out;
  output [0:0]gtwiz_reset_qpll1reset_out;
  input [17:0]gtwiz_gthe3_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe3_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe3_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gthe4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gthe4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gthe4_cpll_cal_bufg_ce_in;
  input [17:0]gtwiz_gtye4_cpll_cal_txoutclk_period_in;
  input [17:0]gtwiz_gtye4_cpll_cal_cnt_tol_in;
  input [0:0]gtwiz_gtye4_cpll_cal_bufg_ce_in;
  input [15:0]gtwiz_userdata_tx_in;
  output [15:0]gtwiz_userdata_rx_out;
  input [0:0]bgbypassb_in;
  input [0:0]bgmonitorenb_in;
  input [0:0]bgpdb_in;
  input [4:0]bgrcalovrd_in;
  input [0:0]bgrcalovrdenb_in;
  input [15:0]drpaddr_common_in;
  input [0:0]drpclk_common_in;
  input [15:0]drpdi_common_in;
  input [0:0]drpen_common_in;
  input [0:0]drpwe_common_in;
  input [0:0]gtgrefclk0_in;
  input [0:0]gtgrefclk1_in;
  input [0:0]gtnorthrefclk00_in;
  input [0:0]gtnorthrefclk01_in;
  input [0:0]gtnorthrefclk10_in;
  input [0:0]gtnorthrefclk11_in;
  input [0:0]gtrefclk00_in;
  input [0:0]gtrefclk01_in;
  input [0:0]gtrefclk10_in;
  input [0:0]gtrefclk11_in;
  input [0:0]gtsouthrefclk00_in;
  input [0:0]gtsouthrefclk01_in;
  input [0:0]gtsouthrefclk10_in;
  input [0:0]gtsouthrefclk11_in;
  input [2:0]pcierateqpll0_in;
  input [2:0]pcierateqpll1_in;
  input [7:0]pmarsvd0_in;
  input [7:0]pmarsvd1_in;
  input [0:0]qpll0clkrsvd0_in;
  input [0:0]qpll0clkrsvd1_in;
  input [7:0]qpll0fbdiv_in;
  input [0:0]qpll0lockdetclk_in;
  input [0:0]qpll0locken_in;
  input [0:0]qpll0pd_in;
  input [2:0]qpll0refclksel_in;
  input [0:0]qpll0reset_in;
  input [0:0]qpll1clkrsvd0_in;
  input [0:0]qpll1clkrsvd1_in;
  input [7:0]qpll1fbdiv_in;
  input [0:0]qpll1lockdetclk_in;
  input [0:0]qpll1locken_in;
  input [0:0]qpll1pd_in;
  input [2:0]qpll1refclksel_in;
  input [0:0]qpll1reset_in;
  input [7:0]qpllrsvd1_in;
  input [4:0]qpllrsvd2_in;
  input [4:0]qpllrsvd3_in;
  input [7:0]qpllrsvd4_in;
  input [0:0]rcalenb_in;
  input [24:0]sdm0data_in;
  input [0:0]sdm0reset_in;
  input [0:0]sdm0toggle_in;
  input [1:0]sdm0width_in;
  input [24:0]sdm1data_in;
  input [0:0]sdm1reset_in;
  input [0:0]sdm1toggle_in;
  input [1:0]sdm1width_in;
  input [9:0]tcongpi_in;
  input [0:0]tconpowerup_in;
  input [1:0]tconreset_in;
  input [1:0]tconrsvdin1_in;
  input [0:0]ubcfgstreamen_in;
  input [0:0]ubdo_in;
  input [0:0]ubdrdy_in;
  input [0:0]ubenable_in;
  input [0:0]ubgpi_in;
  input [0:0]ubintr_in;
  input [0:0]ubiolmbrst_in;
  input [0:0]ubmbrst_in;
  input [0:0]ubmdmcapture_in;
  input [0:0]ubmdmdbgrst_in;
  input [0:0]ubmdmdbgupdate_in;
  input [0:0]ubmdmregen_in;
  input [0:0]ubmdmshift_in;
  input [0:0]ubmdmsysrst_in;
  input [0:0]ubmdmtck_in;
  input [0:0]ubmdmtdi_in;
  output [15:0]drpdo_common_out;
  output [0:0]drprdy_common_out;
  output [7:0]pmarsvdout0_out;
  output [7:0]pmarsvdout1_out;
  output [0:0]qpll0fbclklost_out;
  output [0:0]qpll0lock_out;
  output [0:0]qpll0outclk_out;
  output [0:0]qpll0outrefclk_out;
  output [0:0]qpll0refclklost_out;
  output [0:0]qpll1fbclklost_out;
  output [0:0]qpll1lock_out;
  output [0:0]qpll1outclk_out;
  output [0:0]qpll1outrefclk_out;
  output [0:0]qpll1refclklost_out;
  output [7:0]qplldmonitor0_out;
  output [7:0]qplldmonitor1_out;
  output [0:0]refclkoutmonitor0_out;
  output [0:0]refclkoutmonitor1_out;
  output [0:0]rxrecclk0_sel_out;
  output [0:0]rxrecclk1_sel_out;
  output [1:0]rxrecclk0sel_out;
  output [1:0]rxrecclk1sel_out;
  output [3:0]sdm0finalout_out;
  output [14:0]sdm0testdata_out;
  output [3:0]sdm1finalout_out;
  output [14:0]sdm1testdata_out;
  output [9:0]tcongpo_out;
  output [0:0]tconrsvdout0_out;
  output [0:0]ubdaddr_out;
  output [0:0]ubden_out;
  output [0:0]ubdi_out;
  output [0:0]ubdwe_out;
  output [0:0]ubmdmtdo_out;
  output [0:0]ubrsvdout_out;
  output [0:0]ubtxuart_out;
  input [0:0]cdrstepdir_in;
  input [0:0]cdrstepsq_in;
  input [0:0]cdrstepsx_in;
  input [0:0]cfgreset_in;
  input [0:0]clkrsvd0_in;
  input [0:0]clkrsvd1_in;
  input [0:0]cpllfreqlock_in;
  input [0:0]cplllockdetclk_in;
  input [0:0]cplllocken_in;
  input [0:0]cpllpd_in;
  input [2:0]cpllrefclksel_in;
  input [0:0]cpllreset_in;
  input [0:0]dmonfiforeset_in;
  input [0:0]dmonitorclk_in;
  input [9:0]drpaddr_in;
  input [0:0]drpclk_in;
  input [15:0]drpdi_in;
  input [0:0]drpen_in;
  input [0:0]drprst_in;
  input [0:0]drpwe_in;
  input [0:0]elpcaldvorwren_in;
  input [0:0]elpcalpaorwren_in;
  input [0:0]evoddphicaldone_in;
  input [0:0]evoddphicalstart_in;
  input [0:0]evoddphidrden_in;
  input [0:0]evoddphidwren_in;
  input [0:0]evoddphixrden_in;
  input [0:0]evoddphixwren_in;
  input [0:0]eyescanmode_in;
  input [0:0]eyescanreset_in;
  input [0:0]eyescantrigger_in;
  input [0:0]freqos_in;
  input [0:0]gtgrefclk_in;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtnorthrefclk0_in;
  input [0:0]gtnorthrefclk1_in;
  input [0:0]gtrefclk0_in;
  input [0:0]gtrefclk1_in;
  input [0:0]gtresetsel_in;
  input [15:0]gtrsvd_in;
  input [0:0]gtrxreset_in;
  input [0:0]gtrxresetsel_in;
  input [0:0]gtsouthrefclk0_in;
  input [0:0]gtsouthrefclk1_in;
  input [0:0]gttxreset_in;
  input [0:0]gttxresetsel_in;
  input [0:0]incpctrl_in;
  input [0:0]gtyrxn_in;
  input [0:0]gtyrxp_in;
  input [2:0]loopback_in;
  input [0:0]looprsvd_in;
  input [0:0]lpbkrxtxseren_in;
  input [0:0]lpbktxrxseren_in;
  input [0:0]pcieeqrxeqadaptdone_in;
  input [0:0]pcierstidle_in;
  input [0:0]pciersttxsyncstart_in;
  input [0:0]pcieuserratedone_in;
  input [15:0]pcsrsvdin_in;
  input [0:0]pcsrsvdin2_in;
  input [0:0]pmarsvdin_in;
  input [0:0]qpll0clk_in;
  input [0:0]qpll0freqlock_in;
  input [0:0]qpll0refclk_in;
  input [0:0]qpll1clk_in;
  input [0:0]qpll1freqlock_in;
  input [0:0]qpll1refclk_in;
  input [0:0]resetovrd_in;
  input [0:0]rstclkentx_in;
  input [0:0]rx8b10ben_in;
  input [0:0]rxafecfoken_in;
  input [0:0]rxbufreset_in;
  input [0:0]rxcdrfreqreset_in;
  input [0:0]rxcdrhold_in;
  input [0:0]rxcdrovrden_in;
  input [0:0]rxcdrreset_in;
  input [0:0]rxcdrresetrsv_in;
  input [0:0]rxchbonden_in;
  input [4:0]rxchbondi_in;
  input [2:0]rxchbondlevel_in;
  input [0:0]rxchbondmaster_in;
  input [0:0]rxchbondslave_in;
  input [0:0]rxckcalreset_in;
  input [6:0]rxckcalstart_in;
  input [0:0]rxcommadeten_in;
  input [1:0]rxdfeagcctrl_in;
  input [0:0]rxdccforcestart_in;
  input [0:0]rxdfeagchold_in;
  input [0:0]rxdfeagcovrden_in;
  input [3:0]rxdfecfokfcnum_in;
  input [0:0]rxdfecfokfen_in;
  input [0:0]rxdfecfokfpulse_in;
  input [0:0]rxdfecfokhold_in;
  input [0:0]rxdfecfokovren_in;
  input [0:0]rxdfekhhold_in;
  input [0:0]rxdfekhovrden_in;
  input [0:0]rxdfelfhold_in;
  input [0:0]rxdfelfovrden_in;
  input [0:0]rxdfelpmreset_in;
  input [0:0]rxdfetap10hold_in;
  input [0:0]rxdfetap10ovrden_in;
  input [0:0]rxdfetap11hold_in;
  input [0:0]rxdfetap11ovrden_in;
  input [0:0]rxdfetap12hold_in;
  input [0:0]rxdfetap12ovrden_in;
  input [0:0]rxdfetap13hold_in;
  input [0:0]rxdfetap13ovrden_in;
  input [0:0]rxdfetap14hold_in;
  input [0:0]rxdfetap14ovrden_in;
  input [0:0]rxdfetap15hold_in;
  input [0:0]rxdfetap15ovrden_in;
  input [0:0]rxdfetap2hold_in;
  input [0:0]rxdfetap2ovrden_in;
  input [0:0]rxdfetap3hold_in;
  input [0:0]rxdfetap3ovrden_in;
  input [0:0]rxdfetap4hold_in;
  input [0:0]rxdfetap4ovrden_in;
  input [0:0]rxdfetap5hold_in;
  input [0:0]rxdfetap5ovrden_in;
  input [0:0]rxdfetap6hold_in;
  input [0:0]rxdfetap6ovrden_in;
  input [0:0]rxdfetap7hold_in;
  input [0:0]rxdfetap7ovrden_in;
  input [0:0]rxdfetap8hold_in;
  input [0:0]rxdfetap8ovrden_in;
  input [0:0]rxdfetap9hold_in;
  input [0:0]rxdfetap9ovrden_in;
  input [0:0]rxdfeuthold_in;
  input [0:0]rxdfeutovrden_in;
  input [0:0]rxdfevphold_in;
  input [0:0]rxdfevpovrden_in;
  input [0:0]rxdfevsen_in;
  input [0:0]rxdfexyden_in;
  input [0:0]rxdlybypass_in;
  input [0:0]rxdlyen_in;
  input [0:0]rxdlyovrden_in;
  input [0:0]rxdlysreset_in;
  input [1:0]rxelecidlemode_in;
  input [0:0]rxeqtraining_in;
  input [0:0]rxgearboxslip_in;
  input [0:0]rxlatclk_in;
  input [0:0]rxlpmen_in;
  input [0:0]rxlpmgchold_in;
  input [0:0]rxlpmgcovrden_in;
  input [0:0]rxlpmhfhold_in;
  input [0:0]rxlpmhfovrden_in;
  input [0:0]rxlpmlfhold_in;
  input [0:0]rxlpmlfklovrden_in;
  input [0:0]rxlpmoshold_in;
  input [0:0]rxlpmosovrden_in;
  input [0:0]rxmcommaalignen_in;
  input [1:0]rxmonitorsel_in;
  input [0:0]rxoobreset_in;
  input [0:0]rxoscalreset_in;
  input [0:0]rxoshold_in;
  input [0:0]rxosintcfg_in;
  input [0:0]rxosinten_in;
  input [0:0]rxosinthold_in;
  input [0:0]rxosintovrden_in;
  input [0:0]rxosintstrobe_in;
  input [0:0]rxosinttestovrden_in;
  input [0:0]rxosovrden_in;
  input [2:0]rxoutclksel_in;
  input [0:0]rxpcommaalignen_in;
  input [0:0]rxpcsreset_in;
  input [1:0]rxpd_in;
  input [0:0]rxphalign_in;
  input [0:0]rxphalignen_in;
  input [0:0]rxphdlypd_in;
  input [0:0]rxphdlyreset_in;
  input [0:0]rxphovrden_in;
  input [1:0]rxpllclksel_in;
  input [0:0]rxpmareset_in;
  input [0:0]rxpolarity_in;
  input [0:0]rxprbscntreset_in;
  input [3:0]rxprbssel_in;
  input [0:0]rxprogdivreset_in;
  input [0:0]rxqpien_in;
  input [2:0]rxrate_in;
  input [0:0]rxratemode_in;
  input [0:0]rxslide_in;
  input [0:0]rxslipoutclk_in;
  input [0:0]rxslippma_in;
  input [0:0]rxsyncallin_in;
  input [0:0]rxsyncin_in;
  input [0:0]rxsyncmode_in;
  input [1:0]rxsysclksel_in;
  input [0:0]rxtermination_in;
  input [0:0]rxuserrdy_in;
  input [0:0]rxusrclk_in;
  input [0:0]rxusrclk2_in;
  input [0:0]sigvalidclk_in;
  input [19:0]tstin_in;
  input [7:0]tx8b10bbypass_in;
  input [0:0]tx8b10ben_in;
  input [0:0]txbufdiffctrl_in;
  input [0:0]txcominit_in;
  input [0:0]txcomsas_in;
  input [0:0]txcomwake_in;
  input [15:0]txctrl0_in;
  input [15:0]txctrl1_in;
  input [7:0]txctrl2_in;
  input [127:0]txdata_in;
  input [7:0]txdataextendrsvd_in;
  input [0:0]txdccforcestart_in;
  input [0:0]txdccreset_in;
  input [1:0]txdeemph_in;
  input [0:0]txdetectrx_in;
  input [4:0]txdiffctrl_in;
  input [0:0]txdiffpd_in;
  input [0:0]txdlybypass_in;
  input [0:0]txdlyen_in;
  input [0:0]txdlyhold_in;
  input [0:0]txdlyovrden_in;
  input [0:0]txdlysreset_in;
  input [0:0]txdlyupdown_in;
  input [0:0]txelecidle_in;
  input [0:0]txelforcestart_in;
  input [5:0]txheader_in;
  input [0:0]txinhibit_in;
  input [0:0]txlatclk_in;
  input [0:0]txlfpstreset_in;
  input [0:0]txlfpsu2lpexit_in;
  input [0:0]txlfpsu3wake_in;
  input [6:0]txmaincursor_in;
  input [2:0]txmargin_in;
  input [0:0]txmuxdcdexhold_in;
  input [0:0]txmuxdcdorwren_in;
  input [0:0]txoneszeros_in;
  input [2:0]txoutclksel_in;
  input [0:0]txpcsreset_in;
  input [1:0]txpd_in;
  input [0:0]txpdelecidlemode_in;
  input [0:0]txphalign_in;
  input [0:0]txphalignen_in;
  input [0:0]txphdlypd_in;
  input [0:0]txphdlyreset_in;
  input [0:0]txphdlytstclk_in;
  input [0:0]txphinit_in;
  input [0:0]txphovrden_in;
  input [0:0]txpippmen_in;
  input [0:0]txpippmovrden_in;
  input [0:0]txpippmpd_in;
  input [0:0]txpippmsel_in;
  input [4:0]txpippmstepsize_in;
  input [0:0]txpisopd_in;
  input [1:0]txpllclksel_in;
  input [0:0]txpmareset_in;
  input [0:0]txpolarity_in;
  input [4:0]txpostcursor_in;
  input [0:0]txpostcursorinv_in;
  input [0:0]txprbsforceerr_in;
  input [3:0]txprbssel_in;
  input [4:0]txprecursor_in;
  input [0:0]txprecursorinv_in;
  input [0:0]txprogdivreset_in;
  input [0:0]txqpibiasen_in;
  input [0:0]txqpistrongpdown_in;
  input [0:0]txqpiweakpup_in;
  input [2:0]txrate_in;
  input [0:0]txratemode_in;
  input [6:0]txsequence_in;
  input [0:0]txswing_in;
  input [0:0]txsyncallin_in;
  input [0:0]txsyncin_in;
  input [0:0]txsyncmode_in;
  input [1:0]txsysclksel_in;
  input [0:0]txuserrdy_in;
  input [0:0]txusrclk_in;
  input [0:0]txusrclk2_in;
  output [0:0]bufgtce_out;
  output [2:0]bufgtcemask_out;
  output [8:0]bufgtdiv_out;
  output [0:0]bufgtreset_out;
  output [2:0]bufgtrstmask_out;
  output [0:0]cpllfbclklost_out;
  output [0:0]cplllock_out;
  output [0:0]cpllrefclklost_out;
  output [15:0]dmonitorout_out;
  output [0:0]dmonitoroutclk_out;
  output [15:0]drpdo_out;
  output [0:0]drprdy_out;
  output [0:0]eyescandataerror_out;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output [0:0]gtpowergood_out;
  output [0:0]gtrefclkmonitor_out;
  output [0:0]gtytxn_out;
  output [0:0]gtytxp_out;
  output [0:0]pcierategen3_out;
  output [0:0]pcierateidle_out;
  output [1:0]pcierateqpllpd_out;
  output [1:0]pcierateqpllreset_out;
  output [0:0]pciesynctxsyncdone_out;
  output [0:0]pcieusergen3rdy_out;
  output [0:0]pcieuserphystatusrst_out;
  output [0:0]pcieuserratestart_out;
  output [15:0]pcsrsvdout_out;
  output [0:0]phystatus_out;
  output [15:0]pinrsrvdas_out;
  output [0:0]powerpresent_out;
  output [0:0]resetexception_out;
  output [2:0]rxbufstatus_out;
  output [0:0]rxbyteisaligned_out;
  output [0:0]rxbyterealign_out;
  output [0:0]rxcdrlock_out;
  output [0:0]rxcdrphdone_out;
  output [0:0]rxchanbondseq_out;
  output [0:0]rxchanisaligned_out;
  output [0:0]rxchanrealign_out;
  output [4:0]rxchbondo_out;
  output [0:0]rxckcaldone_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]rxcominitdet_out;
  output [0:0]rxcommadet_out;
  output [0:0]rxcomsasdet_out;
  output [0:0]rxcomwakedet_out;
  output [15:0]rxctrl0_out;
  output [15:0]rxctrl1_out;
  output [7:0]rxctrl2_out;
  output [7:0]rxctrl3_out;
  output [127:0]rxdata_out;
  output [7:0]rxdataextendrsvd_out;
  output [1:0]rxdatavalid_out;
  output [0:0]rxdlysresetdone_out;
  output [0:0]rxelecidle_out;
  output [5:0]rxheader_out;
  output [1:0]rxheadervalid_out;
  output [0:0]rxlfpstresetdet_out;
  output [0:0]rxlfpsu2lpexitdet_out;
  output [0:0]rxlfpsu3wakedet_out;
  output [7:0]rxmonitorout_out;
  output [0:0]rxosintdone_out;
  output [0:0]rxosintstarted_out;
  output [0:0]rxosintstrobedone_out;
  output [0:0]rxosintstrobestarted_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkfabric_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]rxphaligndone_out;
  output [0:0]rxphalignerr_out;
  output [0:0]rxpmaresetdone_out;
  output [0:0]rxprbserr_out;
  output [0:0]rxprbslocked_out;
  output [0:0]rxprgdivresetdone_out;
  output [0:0]rxqpisenn_out;
  output [0:0]rxqpisenp_out;
  output [0:0]rxratedone_out;
  output [0:0]rxrecclkout_out;
  output [0:0]rxresetdone_out;
  output [0:0]rxsliderdy_out;
  output [0:0]rxslipdone_out;
  output [0:0]rxslipoutclkrdy_out;
  output [0:0]rxslippmardy_out;
  output [1:0]rxstartofseq_out;
  output [2:0]rxstatus_out;
  output [0:0]rxsyncdone_out;
  output [0:0]rxsyncout_out;
  output [0:0]rxvalid_out;
  output [1:0]txbufstatus_out;
  output [0:0]txcomfinish_out;
  output [0:0]txdccdone_out;
  output [0:0]txdlysresetdone_out;
  output [0:0]txoutclk_out;
  output [0:0]txoutclkfabric_out;
  output [0:0]txoutclkpcs_out;
  output [0:0]txphaligndone_out;
  output [0:0]txphinitdone_out;
  output [0:0]txpmaresetdone_out;
  output [0:0]txprgdivresetdone_out;
  output [0:0]txqpisenn_out;
  output [0:0]txqpisenp_out;
  output [0:0]txratedone_out;
  output [0:0]txresetdone_out;
  output [0:0]txsyncdone_out;
  output [0:0]txsyncout_out;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire \<const0> ;
  wire [0:0]drpclk_in;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_reset_all_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire [2:2]\^rxbufstatus_out ;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]\^rxctrl0_out ;
  wire [1:0]\^rxctrl1_out ;
  wire [1:0]\^rxctrl2_out ;
  wire [1:0]\^rxctrl3_out ;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [1:0]rxpd_in;
  wire [0:0]rxusrclk_in;
  wire [1:1]\^txbufstatus_out ;
  wire [15:0]txctrl0_in;
  wire [15:0]txctrl1_in;
  wire [7:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;

  assign bufgtce_out[0] = \<const0> ;
  assign bufgtcemask_out[2] = \<const0> ;
  assign bufgtcemask_out[1] = \<const0> ;
  assign bufgtcemask_out[0] = \<const0> ;
  assign bufgtdiv_out[8] = \<const0> ;
  assign bufgtdiv_out[7] = \<const0> ;
  assign bufgtdiv_out[6] = \<const0> ;
  assign bufgtdiv_out[5] = \<const0> ;
  assign bufgtdiv_out[4] = \<const0> ;
  assign bufgtdiv_out[3] = \<const0> ;
  assign bufgtdiv_out[2] = \<const0> ;
  assign bufgtdiv_out[1] = \<const0> ;
  assign bufgtdiv_out[0] = \<const0> ;
  assign bufgtreset_out[0] = \<const0> ;
  assign bufgtrstmask_out[2] = \<const0> ;
  assign bufgtrstmask_out[1] = \<const0> ;
  assign bufgtrstmask_out[0] = \<const0> ;
  assign cpllfbclklost_out[0] = \<const0> ;
  assign cplllock_out[0] = \<const0> ;
  assign cpllrefclklost_out[0] = \<const0> ;
  assign dmonitorout_out[15] = \<const0> ;
  assign dmonitorout_out[14] = \<const0> ;
  assign dmonitorout_out[13] = \<const0> ;
  assign dmonitorout_out[12] = \<const0> ;
  assign dmonitorout_out[11] = \<const0> ;
  assign dmonitorout_out[10] = \<const0> ;
  assign dmonitorout_out[9] = \<const0> ;
  assign dmonitorout_out[8] = \<const0> ;
  assign dmonitorout_out[7] = \<const0> ;
  assign dmonitorout_out[6] = \<const0> ;
  assign dmonitorout_out[5] = \<const0> ;
  assign dmonitorout_out[4] = \<const0> ;
  assign dmonitorout_out[3] = \<const0> ;
  assign dmonitorout_out[2] = \<const0> ;
  assign dmonitorout_out[1] = \<const0> ;
  assign dmonitorout_out[0] = \<const0> ;
  assign dmonitoroutclk_out[0] = \<const0> ;
  assign drpdo_common_out[15] = \<const0> ;
  assign drpdo_common_out[14] = \<const0> ;
  assign drpdo_common_out[13] = \<const0> ;
  assign drpdo_common_out[12] = \<const0> ;
  assign drpdo_common_out[11] = \<const0> ;
  assign drpdo_common_out[10] = \<const0> ;
  assign drpdo_common_out[9] = \<const0> ;
  assign drpdo_common_out[8] = \<const0> ;
  assign drpdo_common_out[7] = \<const0> ;
  assign drpdo_common_out[6] = \<const0> ;
  assign drpdo_common_out[5] = \<const0> ;
  assign drpdo_common_out[4] = \<const0> ;
  assign drpdo_common_out[3] = \<const0> ;
  assign drpdo_common_out[2] = \<const0> ;
  assign drpdo_common_out[1] = \<const0> ;
  assign drpdo_common_out[0] = \<const0> ;
  assign drpdo_out[15] = \<const0> ;
  assign drpdo_out[14] = \<const0> ;
  assign drpdo_out[13] = \<const0> ;
  assign drpdo_out[12] = \<const0> ;
  assign drpdo_out[11] = \<const0> ;
  assign drpdo_out[10] = \<const0> ;
  assign drpdo_out[9] = \<const0> ;
  assign drpdo_out[8] = \<const0> ;
  assign drpdo_out[7] = \<const0> ;
  assign drpdo_out[6] = \<const0> ;
  assign drpdo_out[5] = \<const0> ;
  assign drpdo_out[4] = \<const0> ;
  assign drpdo_out[3] = \<const0> ;
  assign drpdo_out[2] = \<const0> ;
  assign drpdo_out[1] = \<const0> ;
  assign drpdo_out[0] = \<const0> ;
  assign drprdy_common_out[0] = \<const0> ;
  assign drprdy_out[0] = \<const0> ;
  assign eyescandataerror_out[0] = \<const0> ;
  assign gtrefclkmonitor_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_rx_error_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_done_out[0] = \<const0> ;
  assign gtwiz_buffbypass_tx_error_out[0] = \<const0> ;
  assign gtwiz_reset_qpll0reset_out[0] = \<const0> ;
  assign gtwiz_reset_qpll1reset_out[0] = \<const0> ;
  assign gtwiz_reset_rx_cdr_stable_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_rx_usrclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_active_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_srcclk_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk2_out[0] = \<const0> ;
  assign gtwiz_userclk_tx_usrclk_out[0] = \<const0> ;
  assign gtytxn_out[0] = \<const0> ;
  assign gtytxp_out[0] = \<const0> ;
  assign pcierategen3_out[0] = \<const0> ;
  assign pcierateidle_out[0] = \<const0> ;
  assign pcierateqpllpd_out[1] = \<const0> ;
  assign pcierateqpllpd_out[0] = \<const0> ;
  assign pcierateqpllreset_out[1] = \<const0> ;
  assign pcierateqpllreset_out[0] = \<const0> ;
  assign pciesynctxsyncdone_out[0] = \<const0> ;
  assign pcieusergen3rdy_out[0] = \<const0> ;
  assign pcieuserphystatusrst_out[0] = \<const0> ;
  assign pcieuserratestart_out[0] = \<const0> ;
  assign pcsrsvdout_out[15] = \<const0> ;
  assign pcsrsvdout_out[14] = \<const0> ;
  assign pcsrsvdout_out[13] = \<const0> ;
  assign pcsrsvdout_out[12] = \<const0> ;
  assign pcsrsvdout_out[11] = \<const0> ;
  assign pcsrsvdout_out[10] = \<const0> ;
  assign pcsrsvdout_out[9] = \<const0> ;
  assign pcsrsvdout_out[8] = \<const0> ;
  assign pcsrsvdout_out[7] = \<const0> ;
  assign pcsrsvdout_out[6] = \<const0> ;
  assign pcsrsvdout_out[5] = \<const0> ;
  assign pcsrsvdout_out[4] = \<const0> ;
  assign pcsrsvdout_out[3] = \<const0> ;
  assign pcsrsvdout_out[2] = \<const0> ;
  assign pcsrsvdout_out[1] = \<const0> ;
  assign pcsrsvdout_out[0] = \<const0> ;
  assign phystatus_out[0] = \<const0> ;
  assign pinrsrvdas_out[15] = \<const0> ;
  assign pinrsrvdas_out[14] = \<const0> ;
  assign pinrsrvdas_out[13] = \<const0> ;
  assign pinrsrvdas_out[12] = \<const0> ;
  assign pinrsrvdas_out[11] = \<const0> ;
  assign pinrsrvdas_out[10] = \<const0> ;
  assign pinrsrvdas_out[9] = \<const0> ;
  assign pinrsrvdas_out[8] = \<const0> ;
  assign pinrsrvdas_out[7] = \<const0> ;
  assign pinrsrvdas_out[6] = \<const0> ;
  assign pinrsrvdas_out[5] = \<const0> ;
  assign pinrsrvdas_out[4] = \<const0> ;
  assign pinrsrvdas_out[3] = \<const0> ;
  assign pinrsrvdas_out[2] = \<const0> ;
  assign pinrsrvdas_out[1] = \<const0> ;
  assign pinrsrvdas_out[0] = \<const0> ;
  assign pmarsvdout0_out[7] = \<const0> ;
  assign pmarsvdout0_out[6] = \<const0> ;
  assign pmarsvdout0_out[5] = \<const0> ;
  assign pmarsvdout0_out[4] = \<const0> ;
  assign pmarsvdout0_out[3] = \<const0> ;
  assign pmarsvdout0_out[2] = \<const0> ;
  assign pmarsvdout0_out[1] = \<const0> ;
  assign pmarsvdout0_out[0] = \<const0> ;
  assign pmarsvdout1_out[7] = \<const0> ;
  assign pmarsvdout1_out[6] = \<const0> ;
  assign pmarsvdout1_out[5] = \<const0> ;
  assign pmarsvdout1_out[4] = \<const0> ;
  assign pmarsvdout1_out[3] = \<const0> ;
  assign pmarsvdout1_out[2] = \<const0> ;
  assign pmarsvdout1_out[1] = \<const0> ;
  assign pmarsvdout1_out[0] = \<const0> ;
  assign powerpresent_out[0] = \<const0> ;
  assign qpll0fbclklost_out[0] = \<const0> ;
  assign qpll0lock_out[0] = \<const0> ;
  assign qpll0outclk_out[0] = \<const0> ;
  assign qpll0outrefclk_out[0] = \<const0> ;
  assign qpll0refclklost_out[0] = \<const0> ;
  assign qpll1fbclklost_out[0] = \<const0> ;
  assign qpll1lock_out[0] = \<const0> ;
  assign qpll1outclk_out[0] = \<const0> ;
  assign qpll1outrefclk_out[0] = \<const0> ;
  assign qpll1refclklost_out[0] = \<const0> ;
  assign qplldmonitor0_out[7] = \<const0> ;
  assign qplldmonitor0_out[6] = \<const0> ;
  assign qplldmonitor0_out[5] = \<const0> ;
  assign qplldmonitor0_out[4] = \<const0> ;
  assign qplldmonitor0_out[3] = \<const0> ;
  assign qplldmonitor0_out[2] = \<const0> ;
  assign qplldmonitor0_out[1] = \<const0> ;
  assign qplldmonitor0_out[0] = \<const0> ;
  assign qplldmonitor1_out[7] = \<const0> ;
  assign qplldmonitor1_out[6] = \<const0> ;
  assign qplldmonitor1_out[5] = \<const0> ;
  assign qplldmonitor1_out[4] = \<const0> ;
  assign qplldmonitor1_out[3] = \<const0> ;
  assign qplldmonitor1_out[2] = \<const0> ;
  assign qplldmonitor1_out[1] = \<const0> ;
  assign qplldmonitor1_out[0] = \<const0> ;
  assign refclkoutmonitor0_out[0] = \<const0> ;
  assign refclkoutmonitor1_out[0] = \<const0> ;
  assign resetexception_out[0] = \<const0> ;
  assign rxbufstatus_out[2] = \^rxbufstatus_out [2];
  assign rxbufstatus_out[1] = \<const0> ;
  assign rxbufstatus_out[0] = \<const0> ;
  assign rxbyteisaligned_out[0] = \<const0> ;
  assign rxbyterealign_out[0] = \<const0> ;
  assign rxcdrlock_out[0] = \<const0> ;
  assign rxcdrphdone_out[0] = \<const0> ;
  assign rxchanbondseq_out[0] = \<const0> ;
  assign rxchanisaligned_out[0] = \<const0> ;
  assign rxchanrealign_out[0] = \<const0> ;
  assign rxchbondo_out[4] = \<const0> ;
  assign rxchbondo_out[3] = \<const0> ;
  assign rxchbondo_out[2] = \<const0> ;
  assign rxchbondo_out[1] = \<const0> ;
  assign rxchbondo_out[0] = \<const0> ;
  assign rxckcaldone_out[0] = \<const0> ;
  assign rxcominitdet_out[0] = \<const0> ;
  assign rxcommadet_out[0] = \<const0> ;
  assign rxcomsasdet_out[0] = \<const0> ;
  assign rxcomwakedet_out[0] = \<const0> ;
  assign rxctrl0_out[15] = \<const0> ;
  assign rxctrl0_out[14] = \<const0> ;
  assign rxctrl0_out[13] = \<const0> ;
  assign rxctrl0_out[12] = \<const0> ;
  assign rxctrl0_out[11] = \<const0> ;
  assign rxctrl0_out[10] = \<const0> ;
  assign rxctrl0_out[9] = \<const0> ;
  assign rxctrl0_out[8] = \<const0> ;
  assign rxctrl0_out[7] = \<const0> ;
  assign rxctrl0_out[6] = \<const0> ;
  assign rxctrl0_out[5] = \<const0> ;
  assign rxctrl0_out[4] = \<const0> ;
  assign rxctrl0_out[3] = \<const0> ;
  assign rxctrl0_out[2] = \<const0> ;
  assign rxctrl0_out[1:0] = \^rxctrl0_out [1:0];
  assign rxctrl1_out[15] = \<const0> ;
  assign rxctrl1_out[14] = \<const0> ;
  assign rxctrl1_out[13] = \<const0> ;
  assign rxctrl1_out[12] = \<const0> ;
  assign rxctrl1_out[11] = \<const0> ;
  assign rxctrl1_out[10] = \<const0> ;
  assign rxctrl1_out[9] = \<const0> ;
  assign rxctrl1_out[8] = \<const0> ;
  assign rxctrl1_out[7] = \<const0> ;
  assign rxctrl1_out[6] = \<const0> ;
  assign rxctrl1_out[5] = \<const0> ;
  assign rxctrl1_out[4] = \<const0> ;
  assign rxctrl1_out[3] = \<const0> ;
  assign rxctrl1_out[2] = \<const0> ;
  assign rxctrl1_out[1:0] = \^rxctrl1_out [1:0];
  assign rxctrl2_out[7] = \<const0> ;
  assign rxctrl2_out[6] = \<const0> ;
  assign rxctrl2_out[5] = \<const0> ;
  assign rxctrl2_out[4] = \<const0> ;
  assign rxctrl2_out[3] = \<const0> ;
  assign rxctrl2_out[2] = \<const0> ;
  assign rxctrl2_out[1:0] = \^rxctrl2_out [1:0];
  assign rxctrl3_out[7] = \<const0> ;
  assign rxctrl3_out[6] = \<const0> ;
  assign rxctrl3_out[5] = \<const0> ;
  assign rxctrl3_out[4] = \<const0> ;
  assign rxctrl3_out[3] = \<const0> ;
  assign rxctrl3_out[2] = \<const0> ;
  assign rxctrl3_out[1:0] = \^rxctrl3_out [1:0];
  assign rxdata_out[127] = \<const0> ;
  assign rxdata_out[126] = \<const0> ;
  assign rxdata_out[125] = \<const0> ;
  assign rxdata_out[124] = \<const0> ;
  assign rxdata_out[123] = \<const0> ;
  assign rxdata_out[122] = \<const0> ;
  assign rxdata_out[121] = \<const0> ;
  assign rxdata_out[120] = \<const0> ;
  assign rxdata_out[119] = \<const0> ;
  assign rxdata_out[118] = \<const0> ;
  assign rxdata_out[117] = \<const0> ;
  assign rxdata_out[116] = \<const0> ;
  assign rxdata_out[115] = \<const0> ;
  assign rxdata_out[114] = \<const0> ;
  assign rxdata_out[113] = \<const0> ;
  assign rxdata_out[112] = \<const0> ;
  assign rxdata_out[111] = \<const0> ;
  assign rxdata_out[110] = \<const0> ;
  assign rxdata_out[109] = \<const0> ;
  assign rxdata_out[108] = \<const0> ;
  assign rxdata_out[107] = \<const0> ;
  assign rxdata_out[106] = \<const0> ;
  assign rxdata_out[105] = \<const0> ;
  assign rxdata_out[104] = \<const0> ;
  assign rxdata_out[103] = \<const0> ;
  assign rxdata_out[102] = \<const0> ;
  assign rxdata_out[101] = \<const0> ;
  assign rxdata_out[100] = \<const0> ;
  assign rxdata_out[99] = \<const0> ;
  assign rxdata_out[98] = \<const0> ;
  assign rxdata_out[97] = \<const0> ;
  assign rxdata_out[96] = \<const0> ;
  assign rxdata_out[95] = \<const0> ;
  assign rxdata_out[94] = \<const0> ;
  assign rxdata_out[93] = \<const0> ;
  assign rxdata_out[92] = \<const0> ;
  assign rxdata_out[91] = \<const0> ;
  assign rxdata_out[90] = \<const0> ;
  assign rxdata_out[89] = \<const0> ;
  assign rxdata_out[88] = \<const0> ;
  assign rxdata_out[87] = \<const0> ;
  assign rxdata_out[86] = \<const0> ;
  assign rxdata_out[85] = \<const0> ;
  assign rxdata_out[84] = \<const0> ;
  assign rxdata_out[83] = \<const0> ;
  assign rxdata_out[82] = \<const0> ;
  assign rxdata_out[81] = \<const0> ;
  assign rxdata_out[80] = \<const0> ;
  assign rxdata_out[79] = \<const0> ;
  assign rxdata_out[78] = \<const0> ;
  assign rxdata_out[77] = \<const0> ;
  assign rxdata_out[76] = \<const0> ;
  assign rxdata_out[75] = \<const0> ;
  assign rxdata_out[74] = \<const0> ;
  assign rxdata_out[73] = \<const0> ;
  assign rxdata_out[72] = \<const0> ;
  assign rxdata_out[71] = \<const0> ;
  assign rxdata_out[70] = \<const0> ;
  assign rxdata_out[69] = \<const0> ;
  assign rxdata_out[68] = \<const0> ;
  assign rxdata_out[67] = \<const0> ;
  assign rxdata_out[66] = \<const0> ;
  assign rxdata_out[65] = \<const0> ;
  assign rxdata_out[64] = \<const0> ;
  assign rxdata_out[63] = \<const0> ;
  assign rxdata_out[62] = \<const0> ;
  assign rxdata_out[61] = \<const0> ;
  assign rxdata_out[60] = \<const0> ;
  assign rxdata_out[59] = \<const0> ;
  assign rxdata_out[58] = \<const0> ;
  assign rxdata_out[57] = \<const0> ;
  assign rxdata_out[56] = \<const0> ;
  assign rxdata_out[55] = \<const0> ;
  assign rxdata_out[54] = \<const0> ;
  assign rxdata_out[53] = \<const0> ;
  assign rxdata_out[52] = \<const0> ;
  assign rxdata_out[51] = \<const0> ;
  assign rxdata_out[50] = \<const0> ;
  assign rxdata_out[49] = \<const0> ;
  assign rxdata_out[48] = \<const0> ;
  assign rxdata_out[47] = \<const0> ;
  assign rxdata_out[46] = \<const0> ;
  assign rxdata_out[45] = \<const0> ;
  assign rxdata_out[44] = \<const0> ;
  assign rxdata_out[43] = \<const0> ;
  assign rxdata_out[42] = \<const0> ;
  assign rxdata_out[41] = \<const0> ;
  assign rxdata_out[40] = \<const0> ;
  assign rxdata_out[39] = \<const0> ;
  assign rxdata_out[38] = \<const0> ;
  assign rxdata_out[37] = \<const0> ;
  assign rxdata_out[36] = \<const0> ;
  assign rxdata_out[35] = \<const0> ;
  assign rxdata_out[34] = \<const0> ;
  assign rxdata_out[33] = \<const0> ;
  assign rxdata_out[32] = \<const0> ;
  assign rxdata_out[31] = \<const0> ;
  assign rxdata_out[30] = \<const0> ;
  assign rxdata_out[29] = \<const0> ;
  assign rxdata_out[28] = \<const0> ;
  assign rxdata_out[27] = \<const0> ;
  assign rxdata_out[26] = \<const0> ;
  assign rxdata_out[25] = \<const0> ;
  assign rxdata_out[24] = \<const0> ;
  assign rxdata_out[23] = \<const0> ;
  assign rxdata_out[22] = \<const0> ;
  assign rxdata_out[21] = \<const0> ;
  assign rxdata_out[20] = \<const0> ;
  assign rxdata_out[19] = \<const0> ;
  assign rxdata_out[18] = \<const0> ;
  assign rxdata_out[17] = \<const0> ;
  assign rxdata_out[16] = \<const0> ;
  assign rxdata_out[15] = \<const0> ;
  assign rxdata_out[14] = \<const0> ;
  assign rxdata_out[13] = \<const0> ;
  assign rxdata_out[12] = \<const0> ;
  assign rxdata_out[11] = \<const0> ;
  assign rxdata_out[10] = \<const0> ;
  assign rxdata_out[9] = \<const0> ;
  assign rxdata_out[8] = \<const0> ;
  assign rxdata_out[7] = \<const0> ;
  assign rxdata_out[6] = \<const0> ;
  assign rxdata_out[5] = \<const0> ;
  assign rxdata_out[4] = \<const0> ;
  assign rxdata_out[3] = \<const0> ;
  assign rxdata_out[2] = \<const0> ;
  assign rxdata_out[1] = \<const0> ;
  assign rxdata_out[0] = \<const0> ;
  assign rxdataextendrsvd_out[7] = \<const0> ;
  assign rxdataextendrsvd_out[6] = \<const0> ;
  assign rxdataextendrsvd_out[5] = \<const0> ;
  assign rxdataextendrsvd_out[4] = \<const0> ;
  assign rxdataextendrsvd_out[3] = \<const0> ;
  assign rxdataextendrsvd_out[2] = \<const0> ;
  assign rxdataextendrsvd_out[1] = \<const0> ;
  assign rxdataextendrsvd_out[0] = \<const0> ;
  assign rxdatavalid_out[1] = \<const0> ;
  assign rxdatavalid_out[0] = \<const0> ;
  assign rxdlysresetdone_out[0] = \<const0> ;
  assign rxelecidle_out[0] = \<const0> ;
  assign rxheader_out[5] = \<const0> ;
  assign rxheader_out[4] = \<const0> ;
  assign rxheader_out[3] = \<const0> ;
  assign rxheader_out[2] = \<const0> ;
  assign rxheader_out[1] = \<const0> ;
  assign rxheader_out[0] = \<const0> ;
  assign rxheadervalid_out[1] = \<const0> ;
  assign rxheadervalid_out[0] = \<const0> ;
  assign rxlfpstresetdet_out[0] = \<const0> ;
  assign rxlfpsu2lpexitdet_out[0] = \<const0> ;
  assign rxlfpsu3wakedet_out[0] = \<const0> ;
  assign rxmonitorout_out[7] = \<const0> ;
  assign rxmonitorout_out[6] = \<const0> ;
  assign rxmonitorout_out[5] = \<const0> ;
  assign rxmonitorout_out[4] = \<const0> ;
  assign rxmonitorout_out[3] = \<const0> ;
  assign rxmonitorout_out[2] = \<const0> ;
  assign rxmonitorout_out[1] = \<const0> ;
  assign rxmonitorout_out[0] = \<const0> ;
  assign rxosintdone_out[0] = \<const0> ;
  assign rxosintstarted_out[0] = \<const0> ;
  assign rxosintstrobedone_out[0] = \<const0> ;
  assign rxosintstrobestarted_out[0] = \<const0> ;
  assign rxoutclkfabric_out[0] = \<const0> ;
  assign rxoutclkpcs_out[0] = \<const0> ;
  assign rxphaligndone_out[0] = \<const0> ;
  assign rxphalignerr_out[0] = \<const0> ;
  assign rxpmaresetdone_out[0] = \<const0> ;
  assign rxprbserr_out[0] = \<const0> ;
  assign rxprbslocked_out[0] = \<const0> ;
  assign rxprgdivresetdone_out[0] = \<const0> ;
  assign rxqpisenn_out[0] = \<const0> ;
  assign rxqpisenp_out[0] = \<const0> ;
  assign rxratedone_out[0] = \<const0> ;
  assign rxrecclk0_sel_out[0] = \<const0> ;
  assign rxrecclk0sel_out[1] = \<const0> ;
  assign rxrecclk0sel_out[0] = \<const0> ;
  assign rxrecclk1_sel_out[0] = \<const0> ;
  assign rxrecclk1sel_out[1] = \<const0> ;
  assign rxrecclk1sel_out[0] = \<const0> ;
  assign rxrecclkout_out[0] = \<const0> ;
  assign rxresetdone_out[0] = \<const0> ;
  assign rxsliderdy_out[0] = \<const0> ;
  assign rxslipdone_out[0] = \<const0> ;
  assign rxslipoutclkrdy_out[0] = \<const0> ;
  assign rxslippmardy_out[0] = \<const0> ;
  assign rxstartofseq_out[1] = \<const0> ;
  assign rxstartofseq_out[0] = \<const0> ;
  assign rxstatus_out[2] = \<const0> ;
  assign rxstatus_out[1] = \<const0> ;
  assign rxstatus_out[0] = \<const0> ;
  assign rxsyncdone_out[0] = \<const0> ;
  assign rxsyncout_out[0] = \<const0> ;
  assign rxvalid_out[0] = \<const0> ;
  assign sdm0finalout_out[3] = \<const0> ;
  assign sdm0finalout_out[2] = \<const0> ;
  assign sdm0finalout_out[1] = \<const0> ;
  assign sdm0finalout_out[0] = \<const0> ;
  assign sdm0testdata_out[14] = \<const0> ;
  assign sdm0testdata_out[13] = \<const0> ;
  assign sdm0testdata_out[12] = \<const0> ;
  assign sdm0testdata_out[11] = \<const0> ;
  assign sdm0testdata_out[10] = \<const0> ;
  assign sdm0testdata_out[9] = \<const0> ;
  assign sdm0testdata_out[8] = \<const0> ;
  assign sdm0testdata_out[7] = \<const0> ;
  assign sdm0testdata_out[6] = \<const0> ;
  assign sdm0testdata_out[5] = \<const0> ;
  assign sdm0testdata_out[4] = \<const0> ;
  assign sdm0testdata_out[3] = \<const0> ;
  assign sdm0testdata_out[2] = \<const0> ;
  assign sdm0testdata_out[1] = \<const0> ;
  assign sdm0testdata_out[0] = \<const0> ;
  assign sdm1finalout_out[3] = \<const0> ;
  assign sdm1finalout_out[2] = \<const0> ;
  assign sdm1finalout_out[1] = \<const0> ;
  assign sdm1finalout_out[0] = \<const0> ;
  assign sdm1testdata_out[14] = \<const0> ;
  assign sdm1testdata_out[13] = \<const0> ;
  assign sdm1testdata_out[12] = \<const0> ;
  assign sdm1testdata_out[11] = \<const0> ;
  assign sdm1testdata_out[10] = \<const0> ;
  assign sdm1testdata_out[9] = \<const0> ;
  assign sdm1testdata_out[8] = \<const0> ;
  assign sdm1testdata_out[7] = \<const0> ;
  assign sdm1testdata_out[6] = \<const0> ;
  assign sdm1testdata_out[5] = \<const0> ;
  assign sdm1testdata_out[4] = \<const0> ;
  assign sdm1testdata_out[3] = \<const0> ;
  assign sdm1testdata_out[2] = \<const0> ;
  assign sdm1testdata_out[1] = \<const0> ;
  assign sdm1testdata_out[0] = \<const0> ;
  assign tcongpo_out[9] = \<const0> ;
  assign tcongpo_out[8] = \<const0> ;
  assign tcongpo_out[7] = \<const0> ;
  assign tcongpo_out[6] = \<const0> ;
  assign tcongpo_out[5] = \<const0> ;
  assign tcongpo_out[4] = \<const0> ;
  assign tcongpo_out[3] = \<const0> ;
  assign tcongpo_out[2] = \<const0> ;
  assign tcongpo_out[1] = \<const0> ;
  assign tcongpo_out[0] = \<const0> ;
  assign tconrsvdout0_out[0] = \<const0> ;
  assign txbufstatus_out[1] = \^txbufstatus_out [1];
  assign txbufstatus_out[0] = \<const0> ;
  assign txcomfinish_out[0] = \<const0> ;
  assign txdccdone_out[0] = \<const0> ;
  assign txdlysresetdone_out[0] = \<const0> ;
  assign txoutclkfabric_out[0] = \<const0> ;
  assign txoutclkpcs_out[0] = \<const0> ;
  assign txphaligndone_out[0] = \<const0> ;
  assign txphinitdone_out[0] = \<const0> ;
  assign txpmaresetdone_out[0] = \<const0> ;
  assign txprgdivresetdone_out[0] = \<const0> ;
  assign txqpisenn_out[0] = \<const0> ;
  assign txqpisenp_out[0] = \<const0> ;
  assign txratedone_out[0] = \<const0> ;
  assign txresetdone_out[0] = \<const0> ;
  assign txsyncdone_out[0] = \<const0> ;
  assign txsyncout_out[0] = \<const0> ;
  assign ubdaddr_out[0] = \<const0> ;
  assign ubden_out[0] = \<const0> ;
  assign ubdi_out[0] = \<const0> ;
  assign ubdwe_out[0] = \<const0> ;
  assign ubmdmtdo_out[0] = \<const0> ;
  assign ubrsvdout_out[0] = \<const0> ;
  assign ubtxuart_out[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  PCS_PMA_PCS_PMA_gt_gtwizard_gthe4 \gen_gtwizard_gthe4_top.PCS_PMA_gt_gtwizard_gthe4_inst 
       (.drpclk_in(drpclk_in),
        .gthrxn_in(gthrxn_in),
        .gthrxp_in(gthrxp_in),
        .gthtxn_out(gthtxn_out),
        .gthtxp_out(gthtxp_out),
        .gtpowergood_out(gtpowergood_out),
        .gtrefclk0_in(gtrefclk0_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .gtwiz_userdata_rx_out(gtwiz_userdata_rx_out),
        .gtwiz_userdata_tx_in(gtwiz_userdata_tx_in),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxbufstatus_out(\^rxbufstatus_out ),
        .rxclkcorcnt_out(rxclkcorcnt_out),
        .rxctrl0_out(\^rxctrl0_out ),
        .rxctrl1_out(\^rxctrl1_out ),
        .rxctrl2_out(\^rxctrl2_out ),
        .rxctrl3_out(\^rxctrl3_out ),
        .rxmcommaalignen_in(rxmcommaalignen_in),
        .rxoutclk_out(rxoutclk_out),
        .rxpd_in(rxpd_in[1]),
        .rxusrclk_in(rxusrclk_in),
        .txbufstatus_out(\^txbufstatus_out ),
        .txctrl0_in(txctrl0_in[1:0]),
        .txctrl1_in(txctrl1_in[1:0]),
        .txctrl2_in(txctrl2_in[1:0]),
        .txelecidle_in(txelecidle_in),
        .txoutclk_out(txoutclk_out));
endmodule

module PCS_PMA_PCS_PMA_reset_sync
   (reset_out,
    userclk2,
    enablealign);
  output reset_out;
  input userclk2;
  input enablealign;

  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(userclk2),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(userclk2),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module PCS_PMA_PCS_PMA_resets
   (pma_reset_out,
    independent_clock_bufg,
    reset);
  output pma_reset_out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign pma_reset_out = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module PCS_PMA_PCS_PMA_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    gtpowergood,
    signal_detect);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  output gtpowergood;
  input signal_detect;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gtpowergood;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign mmcm_locked_out = \<const0> ;
  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  PCS_PMA_PCS_PMA_clocking core_clocking_i
       (.gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  PCS_PMA_PCS_PMA_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .pma_reset_out(pma_reset_out),
        .reset(reset));
  PCS_PMA_PCS_PMA_block pcs_pma_block_i
       (.CLK(userclk_out),
        .configuration_vector(configuration_vector[3:1]),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtpowergood(gtpowergood),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pma_reset_out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk_out(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk_out(txoutclk),
        .txp(txp),
        .userclk2(userclk2_out));
endmodule

module PCS_PMA_PCS_PMA_sync_block
   (resetdone,
    data_in,
    userclk2);
  output resetdone;
  input data_in;
  input userclk2;

  wire data_in;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire userclk2;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(userclk2),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(userclk2),
        .CE(1'b1),
        .D(data_sync5),
        .Q(resetdone),
        .R(1'b0));
endmodule

module PCS_PMA_PCS_PMA_transceiver
   (txn,
    txp,
    gtpowergood,
    rxoutclk_out,
    txoutclk_out,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    Q,
    \rxdata_reg[7]_0 ,
    data_in,
    pma_reset_out,
    independent_clock_bufg,
    rxn,
    rxp,
    gtrefclk_out,
    CLK,
    userclk2,
    SR,
    powerdown,
    mgt_tx_reset,
    D,
    txchardispmode_reg_reg_0,
    txcharisk_reg_reg_0,
    enablealign,
    \txdata_reg_reg[7]_0 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output txn;
  output txp;
  output gtpowergood;
  output [0:0]rxoutclk_out;
  output [0:0]txoutclk_out;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  output data_in;
  input pma_reset_out;
  input independent_clock_bufg;
  input rxn;
  input rxp;
  input gtrefclk_out;
  input CLK;
  input userclk2;
  input [0:0]SR;
  input powerdown;
  input mgt_tx_reset;
  input [0:0]D;
  input [0:0]txchardispmode_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input enablealign;
  input [7:0]\txdata_reg_reg[7]_0 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire CLK;
  wire [0:0]D;
  wire PCS_PMA_gt_i_n_117;
  wire PCS_PMA_gt_i_n_57;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire enablealign;
  wire encommaalign_int;
  wire gtpowergood;
  wire gtrefclk_out;
  wire gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_done_out_int;
  wire gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_done_out_int;
  wire independent_clock_bufg;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire mgt_tx_reset;
  wire p_0_in;
  wire pma_reset_out;
  wire powerdown;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_reg__0;
  wire [0:0]rxoutclk_out;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire toggle;
  wire toggle_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [0:0]txchardispmode_reg_reg_0;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire [0:0]txoutclk_out;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire userclk2;
  wire [0:0]NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED;
  wire [15:0]NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED;
  wire [1:0]NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED;
  wire [15:2]NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED;
  wire [7:2]NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED;
  wire [0:0]NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED;

  (* CHECK_LICENSE_TYPE = "PCS_PMA_gt,PCS_PMA_gt_gtwizard_top,{}" *) 
  (* X_CORE_INFO = "PCS_PMA_gt_gtwizard_top,Vivado 2022.1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  PCS_PMA_PCS_PMA_gt PCS_PMA_gt_i
       (.cplllock_out(NLW_PCS_PMA_gt_i_cplllock_out_UNCONNECTED[0]),
        .cpllrefclksel_in({1'b0,1'b0,1'b1}),
        .dmonitorout_out(NLW_PCS_PMA_gt_i_dmonitorout_out_UNCONNECTED[15:0]),
        .drpaddr_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpclk_in(independent_clock_bufg),
        .drpdi_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drpdo_out(NLW_PCS_PMA_gt_i_drpdo_out_UNCONNECTED[15:0]),
        .drpen_in(1'b0),
        .drprdy_out(NLW_PCS_PMA_gt_i_drprdy_out_UNCONNECTED[0]),
        .drpwe_in(1'b0),
        .eyescandataerror_out(NLW_PCS_PMA_gt_i_eyescandataerror_out_UNCONNECTED[0]),
        .eyescanreset_in(1'b0),
        .eyescantrigger_in(1'b0),
        .gthrxn_in(rxn),
        .gthrxp_in(rxp),
        .gthtxn_out(txn),
        .gthtxp_out(txp),
        .gtpowergood_out(gtpowergood),
        .gtrefclk0_in(gtrefclk_out),
        .gtrefclk1_in(1'b0),
        .gtwiz_reset_all_in(pma_reset_out),
        .gtwiz_reset_clk_freerun_in(1'b0),
        .gtwiz_reset_rx_cdr_stable_out(NLW_PCS_PMA_gt_i_gtwiz_reset_rx_cdr_stable_out_UNCONNECTED[0]),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out_int),
        .gtwiz_reset_rx_pll_and_datapath_in(1'b0),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out_int),
        .gtwiz_reset_tx_pll_and_datapath_in(1'b0),
        .gtwiz_userclk_rx_active_in(1'b0),
        .gtwiz_userclk_tx_active_in(1'b1),
        .gtwiz_userclk_tx_reset_in(1'b0),
        .gtwiz_userdata_rx_out(rxdata_int),
        .gtwiz_userdata_tx_in(txdata_int),
        .loopback_in({1'b0,1'b0,1'b0}),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .pcsrsvdin_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx8b10ben_in(1'b1),
        .rxbufreset_in(1'b0),
        .rxbufstatus_out({PCS_PMA_gt_i_n_57,NLW_PCS_PMA_gt_i_rxbufstatus_out_UNCONNECTED[1:0]}),
        .rxbyteisaligned_out(NLW_PCS_PMA_gt_i_rxbyteisaligned_out_UNCONNECTED[0]),
        .rxbyterealign_out(NLW_PCS_PMA_gt_i_rxbyterealign_out_UNCONNECTED[0]),
        .rxcdrhold_in(1'b0),
        .rxclkcorcnt_out(rxclkcorcnt_int),
        .rxcommadet_out(NLW_PCS_PMA_gt_i_rxcommadet_out_UNCONNECTED[0]),
        .rxcommadeten_in(1'b1),
        .rxctrl0_out({NLW_PCS_PMA_gt_i_rxctrl0_out_UNCONNECTED[15:2],rxctrl0_out}),
        .rxctrl1_out({NLW_PCS_PMA_gt_i_rxctrl1_out_UNCONNECTED[15:2],rxctrl1_out}),
        .rxctrl2_out({NLW_PCS_PMA_gt_i_rxctrl2_out_UNCONNECTED[7:2],rxctrl2_out}),
        .rxctrl3_out({NLW_PCS_PMA_gt_i_rxctrl3_out_UNCONNECTED[7:2],rxctrl3_out}),
        .rxdfelpmreset_in(1'b0),
        .rxlpmen_in(1'b1),
        .rxmcommaalignen_in(encommaalign_int),
        .rxoutclk_out(rxoutclk_out),
        .rxpcommaalignen_in(1'b0),
        .rxpcsreset_in(1'b0),
        .rxpd_in({rxpowerdown,1'b0}),
        .rxpmareset_in(1'b0),
        .rxpmaresetdone_out(NLW_PCS_PMA_gt_i_rxpmaresetdone_out_UNCONNECTED[0]),
        .rxpolarity_in(1'b0),
        .rxprbscntreset_in(1'b0),
        .rxprbserr_out(NLW_PCS_PMA_gt_i_rxprbserr_out_UNCONNECTED[0]),
        .rxprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .rxrate_in({1'b0,1'b0,1'b0}),
        .rxresetdone_out(NLW_PCS_PMA_gt_i_rxresetdone_out_UNCONNECTED[0]),
        .rxusrclk2_in(1'b0),
        .rxusrclk_in(CLK),
        .tx8b10ben_in(1'b1),
        .txbufstatus_out({PCS_PMA_gt_i_n_117,NLW_PCS_PMA_gt_i_txbufstatus_out_UNCONNECTED[0]}),
        .txctrl0_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispval_int}),
        .txctrl1_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txchardispmode_int}),
        .txctrl2_in({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txcharisk_int}),
        .txdiffctrl_in({1'b1,1'b1,1'b0,1'b0,1'b0}),
        .txelecidle_in(txpowerdown),
        .txinhibit_in(1'b0),
        .txoutclk_out(txoutclk_out),
        .txpcsreset_in(1'b0),
        .txpd_in({1'b0,1'b0}),
        .txpmareset_in(1'b0),
        .txpmaresetdone_out(NLW_PCS_PMA_gt_i_txpmaresetdone_out_UNCONNECTED[0]),
        .txpolarity_in(1'b0),
        .txpostcursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprbsforceerr_in(1'b0),
        .txprbssel_in({1'b0,1'b0,1'b0,1'b0}),
        .txprecursor_in({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .txprgdivresetdone_out(NLW_PCS_PMA_gt_i_txprgdivresetdone_out_UNCONNECTED[0]),
        .txresetdone_out(NLW_PCS_PMA_gt_i_txresetdone_out_UNCONNECTED[0]),
        .txusrclk2_in(1'b0),
        .txusrclk_in(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_1
       (.I0(mgt_tx_reset),
        .I1(gtwiz_reset_tx_done_out_int),
        .O(gtwiz_reset_tx_datapath_in));
  LUT2 #(
    .INIT(4'h8)) 
    PCS_PMA_gt_i_i_2
       (.I0(SR),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(gtwiz_reset_rx_datapath_in));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_sync1_i_1
       (.I0(gtwiz_reset_tx_done_out_int),
        .I1(gtwiz_reset_rx_done_out_int),
        .O(data_in));
  PCS_PMA_PCS_PMA_reset_sync reclock_encommaalign
       (.enablealign(enablealign),
        .reset_out(encommaalign_int),
        .userclk2(userclk2));
  FDRE rxbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_57),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl2_out[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl0_out[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl1_out[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(userclk2),
        .CE(toggle),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxctrl3_out[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(userclk2),
        .CE(toggle),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    toggle_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(1'b0));
  FDRE txbuferr_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(PCS_PMA_gt_i_n_117),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispmode_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txchardispmode_reg_reg_0),
        .Q(txchardispmode_reg),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispval_double[1]),
        .R(mgt_tx_reset));
  FDRE \txchardispval_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(D),
        .Q(txchardispval_reg),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(mgt_tx_reset));
  FDRE \txcharisk_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[0] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[10] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[11] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[12] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[13] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[14] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[15] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[1] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[2] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[3] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[4] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[5] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[6] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[7] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[8] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(mgt_tx_reset));
  FDRE \txdata_double_reg[9] 
       (.C(userclk2),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(mgt_tx_reset));
  FDRE \txdata_int_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[1] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[2] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[3] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[4] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[5] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[6] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(mgt_tx_reset));
  FDRE \txdata_reg_reg[7] 
       (.C(userclk2),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(mgt_tx_reset));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(userclk2),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(mgt_tx_reset));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer
   (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    rxresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input [0:0]rxresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_0
   (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    txresetdone_out,
    drpclk_in);
  output \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input [0:0]txresetdone_out;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]txresetdone_out;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(txresetdone_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1
   (E,
    gtpowergood_out,
    drpclk_in,
    \FSM_sequential_sm_reset_all_reg[0] ,
    Q,
    \FSM_sequential_sm_reset_all_reg[0]_0 );
  output [0:0]E;
  input [0:0]gtpowergood_out;
  input [0:0]drpclk_in;
  input \FSM_sequential_sm_reset_all_reg[0] ;
  input [2:0]Q;
  input \FSM_sequential_sm_reset_all_reg[0]_0 ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_all_reg[0] ;
  wire \FSM_sequential_sm_reset_all_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire [0:0]gtpowergood_out;
  wire gtpowergood_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;

  LUT6 #(
    .INIT(64'hAF0FAF00CFFFCFFF)) 
    \FSM_sequential_sm_reset_all[2]_i_1 
       (.I0(gtpowergood_sync),
        .I1(\FSM_sequential_sm_reset_all_reg[0] ),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\FSM_sequential_sm_reset_all_reg[0]_0 ),
        .I5(Q[1]),
        .O(E));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtpowergood_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtpowergood_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10
   (i_in_out_reg_0,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[2] ,
    rxcdrlock_out,
    drpclk_in,
    sm_reset_rx_cdr_to_sat,
    Q,
    sm_reset_rx_cdr_to_clr_reg,
    sm_reset_rx_cdr_to_clr,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe4.rxprogdivreset_int );
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input sm_reset_rx_cdr_to_sat;
  input [2:0]Q;
  input sm_reset_rx_cdr_to_clr_reg;
  input sm_reset_rx_cdr_to_clr;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe4.rxprogdivreset_int ;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.rxprogdivreset_int ;
  wire gtwiz_reset_rx_any_sync;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire [0:0]rxcdrlock_out;
  wire sm_reset_rx_cdr_to_clr;
  wire sm_reset_rx_cdr_to_clr0__0;
  wire sm_reset_rx_cdr_to_clr_reg;
  wire sm_reset_rx_cdr_to_sat;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rxcdrlock_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(i_in_out_reg_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFF700000330)) 
    rxprogdivreset_out_i_1
       (.I0(sm_reset_rx_cdr_to_clr0__0),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  LUT6 #(
    .INIT(64'hFFFF3FFF02023303)) 
    sm_reset_rx_cdr_to_clr_i_1
       (.I0(sm_reset_rx_cdr_to_clr0__0),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(sm_reset_rx_cdr_to_clr_reg),
        .I4(Q[2]),
        .I5(sm_reset_rx_cdr_to_clr),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    sm_reset_rx_cdr_to_clr_i_2
       (.I0(sm_reset_rx_cdr_to_sat),
        .I1(i_in_out_reg_0),
        .O(sm_reset_rx_cdr_to_clr0__0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_19
   (drprst_in_sync,
    drpclk_in);
  output drprst_in_sync;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire drprst_in_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;

  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(drprst_in_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2
   (gtwiz_reset_rx_datapath_dly,
    in0,
    drpclk_in);
  output gtwiz_reset_rx_datapath_dly;
  input in0;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire gtwiz_reset_rx_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_22
   (i_in_out_reg_0,
    \cpll_cal_state_reg[0] ,
    in0,
    drpclk_in,
    Q,
    cal_on_tx_reset_in_sync,
    USER_CPLLLOCK_OUT_reg);
  output i_in_out_reg_0;
  output \cpll_cal_state_reg[0] ;
  input in0;
  input [0:0]drpclk_in;
  input [1:0]Q;
  input cal_on_tx_reset_in_sync;
  input USER_CPLLLOCK_OUT_reg;

  wire [1:0]Q;
  wire USER_CPLLLOCK_OUT_reg;
  wire cal_on_tx_reset_in_sync;
  wire \cpll_cal_state_reg[0] ;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  LUT5 #(
    .INIT(32'h00000004)) 
    USER_CPLLLOCK_OUT_i_1
       (.I0(Q[0]),
        .I1(i_in_out_reg_0),
        .I2(cal_on_tx_reset_in_sync),
        .I3(Q[1]),
        .I4(USER_CPLLLOCK_OUT_reg),
        .O(\cpll_cal_state_reg[0] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(i_in_out_reg_0),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_23
   (D,
    drpclk_in,
    txoutclksel_int,
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] );
  output [0:0]D;
  input [0:0]drpclk_in;
  input [0:0]txoutclksel_int;
  input \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] ;

  wire [0:0]D;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] ;
  wire [0:0]txoutclksel_int;
  wire [0:0]user_txoutclksel_sync;

  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(user_txoutclksel_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT[0]_i_1 
       (.I0(txoutclksel_int),
        .I1(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] ),
        .I2(user_txoutclksel_sync),
        .O(D));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_24
   (D,
    drpclk_in,
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] );
  output [0:0]D;
  input [0:0]drpclk_in;
  input \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] ;

  wire [0:0]D;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] ;
  wire [1:1]user_txoutclksel_sync;

  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b0),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(user_txoutclksel_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT[1]_i_1 
       (.I0(user_txoutclksel_sync),
        .I1(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] ),
        .O(D));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_25
   (D,
    drpclk_in,
    txoutclksel_int,
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] );
  output [0:0]D;
  input [0:0]drpclk_in;
  input [0:0]txoutclksel_int;
  input \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] ;

  wire [0:0]D;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] ;
  wire [0:0]txoutclksel_int;
  wire [2:2]user_txoutclksel_sync;

  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(user_txoutclksel_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT[2]_i_1 
       (.I0(txoutclksel_int),
        .I1(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] ),
        .I2(user_txoutclksel_sync),
        .O(D));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_26
   (\cpll_cal_state_reg[14] ,
    D,
    i_in_meta_reg_0,
    drpclk_in,
    Q,
    cal_on_tx_reset_in_sync,
    freq_counter_rst_reg,
    freq_counter_rst_reg_0,
    \cpll_cal_state_reg[19] ,
    freq_counter_rst_reg_1,
    \cpll_cal_state_reg[20] ,
    cal_fail_store__0);
  output \cpll_cal_state_reg[14] ;
  output [4:0]D;
  input i_in_meta_reg_0;
  input [0:0]drpclk_in;
  input [8:0]Q;
  input cal_on_tx_reset_in_sync;
  input freq_counter_rst_reg;
  input freq_counter_rst_reg_0;
  input \cpll_cal_state_reg[19] ;
  input freq_counter_rst_reg_1;
  input \cpll_cal_state_reg[20] ;
  input cal_fail_store__0;

  wire [4:0]D;
  wire [8:0]Q;
  wire cal_fail_store__0;
  wire cal_on_tx_reset_in_sync;
  wire \cpll_cal_state_reg[14] ;
  wire \cpll_cal_state_reg[19] ;
  wire \cpll_cal_state_reg[20] ;
  wire [0:0]drpclk_in;
  wire freq_counter_rst_i_2_n_0;
  wire freq_counter_rst_reg;
  wire freq_counter_rst_reg_0;
  wire freq_counter_rst_reg_1;
  wire gthe4_txprgdivresetdone_sync;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_meta_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;

  LUT4 #(
    .INIT(16'h4F44)) 
    \cpll_cal_state[19]_i_1 
       (.I0(gthe4_txprgdivresetdone_sync),
        .I1(Q[3]),
        .I2(\cpll_cal_state_reg[19] ),
        .I3(Q[2]),
        .O(D[0]));
  LUT4 #(
    .INIT(16'h8F88)) 
    \cpll_cal_state[20]_i_1 
       (.I0(Q[3]),
        .I1(gthe4_txprgdivresetdone_sync),
        .I2(\cpll_cal_state_reg[20] ),
        .I3(Q[4]),
        .O(D[1]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \cpll_cal_state[29]_i_1 
       (.I0(gthe4_txprgdivresetdone_sync),
        .I1(Q[6]),
        .I2(\cpll_cal_state_reg[19] ),
        .I3(Q[5]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hEAAA)) 
    \cpll_cal_state[30]_i_1 
       (.I0(Q[7]),
        .I1(gthe4_txprgdivresetdone_sync),
        .I2(Q[6]),
        .I3(cal_fail_store__0),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hAAEA)) 
    \cpll_cal_state[31]_i_1 
       (.I0(Q[8]),
        .I1(gthe4_txprgdivresetdone_sync),
        .I2(Q[6]),
        .I3(cal_fail_store__0),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hFFFFFEFE00303232)) 
    freq_counter_rst_i_1
       (.I0(Q[0]),
        .I1(cal_on_tx_reset_in_sync),
        .I2(Q[1]),
        .I3(freq_counter_rst_reg),
        .I4(freq_counter_rst_i_2_n_0),
        .I5(freq_counter_rst_reg_0),
        .O(\cpll_cal_state_reg[14] ));
  LUT4 #(
    .INIT(16'h0BBB)) 
    freq_counter_rst_i_2
       (.I0(freq_counter_rst_reg_1),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(gthe4_txprgdivresetdone_sync),
        .O(freq_counter_rst_i_2_n_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta_reg_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gthe4_txprgdivresetdone_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_27
   (txprogdivreset_int_reg,
    i_in_meta_reg_0,
    drpclk_in,
    txprogdivreset_int,
    \non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg );
  output txprogdivreset_int_reg;
  input i_in_meta_reg_0;
  input [0:0]drpclk_in;
  input txprogdivreset_int;
  input \non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg ;

  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_meta_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire \non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg ;
  wire txprogdivreset_int;
  wire txprogdivreset_int_reg;
  wire user_txprogdivreset_sync;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta_reg_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(user_txprogdivreset_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_i_1 
       (.I0(txprogdivreset_int),
        .I1(\non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg ),
        .I2(user_txprogdivreset_sync),
        .O(txprogdivreset_int_reg));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3
   (D,
    sm_reset_rx_pll_timer_sat_reg,
    in0,
    drpclk_in,
    Q,
    p_0_in11_out__0,
    sm_reset_rx_pll_timer_sat,
    \FSM_sequential_sm_reset_rx[2]_i_3 ,
    gtwiz_reset_rx_datapath_dly);
  output [1:0]D;
  output sm_reset_rx_pll_timer_sat_reg;
  input in0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input p_0_in11_out__0;
  input sm_reset_rx_pll_timer_sat;
  input \FSM_sequential_sm_reset_rx[2]_i_3 ;
  input gtwiz_reset_rx_datapath_dly;

  wire [1:0]D;
  wire \FSM_sequential_sm_reset_rx[2]_i_3 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_rx_datapath_dly;
  wire gtwiz_reset_rx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;
  wire p_0_in11_out__0;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_pll_timer_sat_reg;

  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'hDD769976)) 
    \FSM_sequential_sm_reset_rx[0]_i_1 
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(gtwiz_reset_rx_pll_and_datapath_dly),
        .I3(Q[1]),
        .I4(p_0_in11_out__0),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT5 #(
    .INIT(32'h00FFF511)) 
    \FSM_sequential_sm_reset_rx[1]_i_1 
       (.I0(Q[2]),
        .I1(gtwiz_reset_rx_pll_and_datapath_dly),
        .I2(p_0_in11_out__0),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(D[1]));
  LUT5 #(
    .INIT(32'h2F2F2F20)) 
    \FSM_sequential_sm_reset_rx[2]_i_5 
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(\FSM_sequential_sm_reset_rx[2]_i_3 ),
        .I2(Q[0]),
        .I3(gtwiz_reset_rx_pll_and_datapath_dly),
        .I4(gtwiz_reset_rx_datapath_dly),
        .O(sm_reset_rx_pll_timer_sat_reg));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_rx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4
   (gtwiz_reset_tx_datapath_dly,
    in0,
    drpclk_in);
  output gtwiz_reset_tx_datapath_dly;
  input in0;
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5
   (sm_reset_tx_pll_timer_sat_reg,
    D,
    in0,
    drpclk_in,
    sm_reset_tx_pll_timer_sat,
    \FSM_sequential_sm_reset_tx[2]_i_3 ,
    Q,
    gtwiz_reset_tx_datapath_dly);
  output sm_reset_tx_pll_timer_sat_reg;
  output [1:0]D;
  input in0;
  input [0:0]drpclk_in;
  input sm_reset_tx_pll_timer_sat;
  input \FSM_sequential_sm_reset_tx[2]_i_3 ;
  input [2:0]Q;
  input gtwiz_reset_tx_datapath_dly;

  wire [1:0]D;
  wire \FSM_sequential_sm_reset_tx[2]_i_3 ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire gtwiz_reset_tx_datapath_dly;
  wire gtwiz_reset_tx_pll_and_datapath_dly;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire in0;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_pll_timer_sat_reg;

  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'h0F3E)) 
    \FSM_sequential_sm_reset_tx[0]_i_1 
       (.I0(gtwiz_reset_tx_pll_and_datapath_dly),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT4 #(
    .INIT(16'h55AB)) 
    \FSM_sequential_sm_reset_tx[1]_i_1 
       (.I0(Q[0]),
        .I1(gtwiz_reset_tx_pll_and_datapath_dly),
        .I2(Q[2]),
        .I3(Q[1]),
        .O(D[1]));
  LUT5 #(
    .INIT(32'h2F2F2F20)) 
    \FSM_sequential_sm_reset_tx[2]_i_6 
       (.I0(sm_reset_tx_pll_timer_sat),
        .I1(\FSM_sequential_sm_reset_tx[2]_i_3 ),
        .I2(Q[0]),
        .I3(gtwiz_reset_tx_pll_and_datapath_dly),
        .I4(gtwiz_reset_tx_datapath_dly),
        .O(sm_reset_tx_pll_timer_sat_reg));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(in0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_tx_pll_and_datapath_dly),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6
   (\FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    \FSM_sequential_sm_reset_rx_reg[0]_0 ,
    gtwiz_userclk_rx_active_out,
    drpclk_in,
    p_0_in11_out__0,
    Q,
    sm_reset_rx_cdr_to_sat,
    \FSM_sequential_sm_reset_rx_reg[0]_1 ,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    sm_reset_rx_timer_clr_reg,
    sm_reset_rx_timer_sat,
    sm_reset_rx_timer_clr_reg_0,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe4.rxuserrdy_int );
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[0] ;
  output \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  input [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]drpclk_in;
  input p_0_in11_out__0;
  input [2:0]Q;
  input sm_reset_rx_cdr_to_sat;
  input \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input sm_reset_rx_timer_clr_reg;
  input sm_reset_rx_timer_sat;
  input sm_reset_rx_timer_clr_reg_0;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe4.rxuserrdy_int ;

  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe4.rxuserrdy_int ;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_userclk_rx_active_sync;
  wire [0:0]gtwiz_userclk_rx_active_out;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire p_0_in11_out__0;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_timer_clr0__0;
  wire sm_reset_rx_timer_clr_i_2_n_0;
  wire sm_reset_rx_timer_clr_reg;
  wire sm_reset_rx_timer_clr_reg_0;
  wire sm_reset_rx_timer_sat;

  LUT6 #(
    .INIT(64'h30BB30BB30BB3088)) 
    \FSM_sequential_sm_reset_rx[2]_i_4 
       (.I0(p_0_in11_out__0),
        .I1(Q[1]),
        .I2(sm_reset_rx_timer_clr0__0),
        .I3(Q[0]),
        .I4(sm_reset_rx_cdr_to_sat),
        .I5(\FSM_sequential_sm_reset_rx_reg[0]_1 ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_userclk_rx_active_out),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_rx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFEDED00000800)) 
    rxuserrdy_out_i_1
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(sm_reset_rx_timer_clr0__0),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe4.rxuserrdy_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h40)) 
    rxuserrdy_out_i_2
       (.I0(sm_reset_rx_timer_clr_reg),
        .I1(sm_reset_rx_timer_sat),
        .I2(gtwiz_reset_userclk_rx_active_sync),
        .O(sm_reset_rx_timer_clr0__0));
  LUT6 #(
    .INIT(64'hFAAFCCFF0AA0CC0F)) 
    sm_reset_rx_timer_clr_i_1
       (.I0(sm_reset_rx_timer_clr_i_2_n_0),
        .I1(sm_reset_rx_timer_clr_reg_0),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(sm_reset_rx_timer_clr_reg),
        .O(\FSM_sequential_sm_reset_rx_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT5 #(
    .INIT(32'h0B000800)) 
    sm_reset_rx_timer_clr_i_2
       (.I0(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(Q[1]),
        .I2(sm_reset_rx_timer_clr_reg),
        .I3(sm_reset_rx_timer_sat),
        .I4(gtwiz_reset_userclk_rx_active_sync),
        .O(sm_reset_rx_timer_clr_i_2_n_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7
   (sm_reset_tx_timer_clr0__0,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    \FSM_sequential_sm_reset_tx_reg[2] ,
    drpclk_in,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    Q,
    sm_reset_tx_timer_clr013_out__0,
    sm_reset_tx_timer_clr_reg,
    sm_reset_tx_timer_sat,
    sm_reset_tx_timer_clr_reg_0,
    gtwiz_reset_tx_any_sync,
    \gen_gtwizard_gthe4.txuserrdy_int );
  output sm_reset_tx_timer_clr0__0;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  output \FSM_sequential_sm_reset_tx_reg[2] ;
  input [0:0]drpclk_in;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input [2:0]Q;
  input sm_reset_tx_timer_clr013_out__0;
  input sm_reset_tx_timer_clr_reg;
  input sm_reset_tx_timer_sat;
  input sm_reset_tx_timer_clr_reg_0;
  input gtwiz_reset_tx_any_sync;
  input \gen_gtwizard_gthe4.txuserrdy_int ;

  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire \FSM_sequential_sm_reset_tx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe4.txuserrdy_int ;
  wire gtwiz_reset_tx_any_sync;
  wire gtwiz_reset_userclk_tx_active_sync;
  (* async_reg = "true" *) wire i_in_meta;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire n_0_0;
  wire sm_reset_tx_timer_clr013_out__0;
  wire sm_reset_tx_timer_clr0__0;
  wire sm_reset_tx_timer_clr_i_2_n_0;
  wire sm_reset_tx_timer_clr_reg;
  wire sm_reset_tx_timer_clr_reg_0;
  wire sm_reset_tx_timer_sat;

  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_tx[2]_i_5 
       (.I0(sm_reset_tx_timer_clr_reg),
        .I1(sm_reset_tx_timer_sat),
        .I2(gtwiz_reset_userclk_tx_active_sync),
        .O(sm_reset_tx_timer_clr0__0));
  LUT1 #(
    .INIT(2'h2)) 
    i_0
       (.I0(1'b1),
        .O(n_0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(n_0_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(gtwiz_reset_userclk_tx_active_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFACFFACF0AC00ACF)) 
    sm_reset_tx_timer_clr_i_1
       (.I0(sm_reset_tx_timer_clr_i_2_n_0),
        .I1(sm_reset_tx_timer_clr_reg_0),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(sm_reset_tx_timer_clr_reg),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  LUT6 #(
    .INIT(64'hBABF00008A800000)) 
    sm_reset_tx_timer_clr_i_2
       (.I0(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(sm_reset_tx_timer_clr013_out__0),
        .I5(gtwiz_reset_userclk_tx_active_sync),
        .O(sm_reset_tx_timer_clr_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFCCF00000008)) 
    txuserrdy_out_i_1
       (.I0(sm_reset_tx_timer_clr0__0),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(gtwiz_reset_tx_any_sync),
        .I5(\gen_gtwizard_gthe4.txuserrdy_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[2] ));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8
   (E,
    i_in_out_reg_0,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    \FSM_sequential_sm_reset_rx_reg[2] ,
    \FSM_sequential_sm_reset_rx_reg[1]_0 ,
    i_in_meta_reg_0,
    drpclk_in,
    Q,
    \FSM_sequential_sm_reset_rx_reg[0] ,
    sm_reset_rx_timer_sat,
    \FSM_sequential_sm_reset_rx_reg[0]_0 ,
    \FSM_sequential_sm_reset_rx_reg[0]_1 ,
    p_0_in11_out__0,
    gtwiz_reset_rx_done_int_reg,
    gtwiz_reset_rx_any_sync,
    \gen_gtwizard_gthe4.gtrxreset_int );
  output [0:0]E;
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  output \FSM_sequential_sm_reset_rx_reg[2] ;
  output \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  input i_in_meta_reg_0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input \FSM_sequential_sm_reset_rx_reg[0] ;
  input sm_reset_rx_timer_sat;
  input \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  input p_0_in11_out__0;
  input gtwiz_reset_rx_done_int_reg;
  input gtwiz_reset_rx_any_sync;
  input \gen_gtwizard_gthe4.gtrxreset_int ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_rx[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0] ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire \FSM_sequential_sm_reset_rx_reg[1]_0 ;
  wire \FSM_sequential_sm_reset_rx_reg[2] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gtrxreset_int ;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_rx_done_int_reg;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_meta_reg_0;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire p_0_in11_out__0;
  wire plllock_rx_sync;
  wire sm_reset_rx_timer_sat;

  LUT6 #(
    .INIT(64'h00B0FFFF00B00000)) 
    \FSM_sequential_sm_reset_rx[2]_i_3 
       (.I0(plllock_rx_sync),
        .I1(Q[0]),
        .I2(sm_reset_rx_timer_sat),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .I4(Q[1]),
        .I5(\FSM_sequential_sm_reset_rx_reg[0]_1 ),
        .O(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ));
  MUXF7 \FSM_sequential_sm_reset_rx_reg[2]_i_1 
       (.I0(\FSM_sequential_sm_reset_rx[2]_i_3_n_0 ),
        .I1(\FSM_sequential_sm_reset_rx_reg[0] ),
        .O(E),
        .S(Q[2]));
  LUT6 #(
    .INIT(64'hFFFFFF7F0000003E)) 
    gtrxreset_out_i_1
       (.I0(\FSM_sequential_sm_reset_rx_reg[1] ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(gtwiz_reset_rx_any_sync),
        .I5(\gen_gtwizard_gthe4.gtrxreset_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFFFF77FF00800080)) 
    gtwiz_reset_rx_done_int_i_1
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(p_0_in11_out__0),
        .I3(Q[0]),
        .I4(plllock_rx_sync),
        .I5(gtwiz_reset_rx_done_int_reg),
        .O(\FSM_sequential_sm_reset_rx_reg[2] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta_reg_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_rx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    sm_reset_rx_cdr_to_clr_i_3
       (.I0(Q[1]),
        .I1(plllock_rx_sync),
        .I2(sm_reset_rx_timer_sat),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT4 #(
    .INIT(16'h00B0)) 
    sm_reset_rx_timer_clr_i_3
       (.I0(plllock_rx_sync),
        .I1(Q[0]),
        .I2(sm_reset_rx_timer_sat),
        .I3(\FSM_sequential_sm_reset_rx_reg[0]_0 ),
        .O(i_in_out_reg_0));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_bit_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9
   (E,
    i_in_out_reg_0,
    \FSM_sequential_sm_reset_tx_reg[0] ,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    i_in_meta_reg_0,
    drpclk_in,
    Q,
    gtwiz_reset_tx_done_int0__0,
    sm_reset_tx_timer_clr0__0,
    sm_reset_tx_timer_sat,
    \FSM_sequential_sm_reset_tx_reg[0]_0 ,
    \FSM_sequential_sm_reset_tx_reg[0]_1 ,
    gtwiz_reset_tx_done_int_reg,
    gtwiz_reset_tx_any_sync,
    \gen_gtwizard_gthe4.gttxreset_int );
  output [0:0]E;
  output i_in_out_reg_0;
  output \FSM_sequential_sm_reset_tx_reg[0] ;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  input i_in_meta_reg_0;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input gtwiz_reset_tx_done_int0__0;
  input sm_reset_tx_timer_clr0__0;
  input sm_reset_tx_timer_sat;
  input \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  input \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  input gtwiz_reset_tx_done_int_reg;
  input gtwiz_reset_tx_any_sync;
  input \gen_gtwizard_gthe4.gttxreset_int ;

  wire [0:0]E;
  wire \FSM_sequential_sm_reset_tx[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0] ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_0 ;
  wire \FSM_sequential_sm_reset_tx_reg[0]_1 ;
  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gttxreset_int ;
  wire gtwiz_reset_tx_any_sync;
  wire gtwiz_reset_tx_done_int0__0;
  wire gtwiz_reset_tx_done_int_reg;
  (* async_reg = "true" *) wire i_in_meta;
  wire i_in_meta_reg_0;
  wire i_in_out_reg_0;
  (* async_reg = "true" *) wire i_in_sync1;
  (* async_reg = "true" *) wire i_in_sync2;
  (* async_reg = "true" *) wire i_in_sync3;
  wire plllock_tx_sync;
  wire sm_reset_tx_timer_clr012_out__0;
  wire sm_reset_tx_timer_clr0__0;
  wire sm_reset_tx_timer_sat;

  LUT6 #(
    .INIT(64'h3A0A3A3A3A0A0A0A)) 
    \FSM_sequential_sm_reset_tx[2]_i_1 
       (.I0(\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(gtwiz_reset_tx_done_int0__0),
        .I4(Q[0]),
        .I5(sm_reset_tx_timer_clr0__0),
        .O(E));
  LUT6 #(
    .INIT(64'h00B0FFFF00B00000)) 
    \FSM_sequential_sm_reset_tx[2]_i_3 
       (.I0(plllock_tx_sync),
        .I1(Q[0]),
        .I2(sm_reset_tx_timer_sat),
        .I3(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I4(Q[1]),
        .I5(\FSM_sequential_sm_reset_tx_reg[0]_1 ),
        .O(\FSM_sequential_sm_reset_tx[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF7F0000003C)) 
    gttxreset_out_i_1
       (.I0(sm_reset_tx_timer_clr012_out__0),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(gtwiz_reset_tx_any_sync),
        .I5(\gen_gtwizard_gthe4.gttxreset_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h40)) 
    gttxreset_out_i_2
       (.I0(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .I1(sm_reset_tx_timer_sat),
        .I2(plllock_tx_sync),
        .O(sm_reset_tx_timer_clr012_out__0));
  LUT6 #(
    .INIT(64'hFFCFFFFF00008080)) 
    gtwiz_reset_tx_done_int_i_1
       (.I0(gtwiz_reset_tx_done_int0__0),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(plllock_tx_sync),
        .I4(Q[1]),
        .I5(gtwiz_reset_tx_done_int_reg),
        .O(\FSM_sequential_sm_reset_tx_reg[0] ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta_reg_0),
        .Q(i_in_meta),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    i_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync3),
        .Q(plllock_tx_sync),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_meta),
        .Q(i_in_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync1),
        .Q(i_in_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    i_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(i_in_sync2),
        .Q(i_in_sync3),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT4 #(
    .INIT(16'h00B0)) 
    sm_reset_tx_timer_clr_i_3
       (.I0(plllock_tx_sync),
        .I1(Q[0]),
        .I2(sm_reset_tx_timer_sat),
        .I3(\FSM_sequential_sm_reset_tx_reg[0]_0 ),
        .O(i_in_out_reg_0));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gte4_drp_arb
   (Q,
    cal_on_tx_drdy,
    DEN_O,
    DWE_O,
    DADDR_O,
    DI_O,
    drprst_in_sync,
    drpclk_in,
    DO_I,
    cal_on_tx_drpen_out,
    \addr_i_reg[27]_0 ,
    \data_i_reg[47]_0 ,
    cal_on_tx_drpwe_out,
    \gen_gtwizard_gthe4.drprdy_int );
  output [15:0]Q;
  output cal_on_tx_drdy;
  output DEN_O;
  output DWE_O;
  output [6:0]DADDR_O;
  output [15:0]DI_O;
  input drprst_in_sync;
  input [0:0]drpclk_in;
  input [15:0]DO_I;
  input cal_on_tx_drpen_out;
  input [6:0]\addr_i_reg[27]_0 ;
  input [15:0]\data_i_reg[47]_0 ;
  input cal_on_tx_drpwe_out;
  input \gen_gtwizard_gthe4.drprdy_int ;

  wire CEB2;
  wire [6:0]DADDR_O;
  wire \DADDR_O[7]_i_1_n_0 ;
  wire DEN_O;
  wire DEN_O_i_1_n_0;
  wire DEN_O_i_2_n_0;
  wire [15:0]DI_O;
  wire \DI_O[15]_i_1_n_0 ;
  wire [15:0]DO_I;
  wire [47:32]DO_USR_O0;
  wire \DO_USR_O[47]_i_1_n_0 ;
  wire \DRDY_USR_O[2]_i_1_n_0 ;
  wire \DRDY_USR_O[2]_i_2_n_0 ;
  wire DWE_O;
  wire [15:0]Q;
  wire [27:21]addr_i;
  wire [6:0]\addr_i_reg[27]_0 ;
  wire [3:0]arb_state;
  wire cal_on_tx_drdy;
  wire cal_on_tx_drpen_out;
  wire cal_on_tx_drpwe_out;
  wire [7:1]daddr;
  wire [7:1]daddr0;
  wire [47:32]data_i;
  wire [15:0]\data_i_reg[47]_0 ;
  wire di;
  wire \di[0]_i_1_n_0 ;
  wire \di[10]_i_1_n_0 ;
  wire \di[11]_i_1_n_0 ;
  wire \di[12]_i_1_n_0 ;
  wire \di[13]_i_1_n_0 ;
  wire \di[14]_i_1_n_0 ;
  wire \di[15]_i_1_n_0 ;
  wire \di[1]_i_1_n_0 ;
  wire \di[2]_i_1_n_0 ;
  wire \di[3]_i_1_n_0 ;
  wire \di[4]_i_1_n_0 ;
  wire \di[5]_i_1_n_0 ;
  wire \di[6]_i_1_n_0 ;
  wire \di[7]_i_1_n_0 ;
  wire \di[8]_i_1_n_0 ;
  wire \di[9]_i_1_n_0 ;
  wire \di_reg_n_0_[0] ;
  wire \di_reg_n_0_[10] ;
  wire \di_reg_n_0_[11] ;
  wire \di_reg_n_0_[12] ;
  wire \di_reg_n_0_[13] ;
  wire \di_reg_n_0_[14] ;
  wire \di_reg_n_0_[15] ;
  wire \di_reg_n_0_[1] ;
  wire \di_reg_n_0_[2] ;
  wire \di_reg_n_0_[3] ;
  wire \di_reg_n_0_[4] ;
  wire \di_reg_n_0_[5] ;
  wire \di_reg_n_0_[6] ;
  wire \di_reg_n_0_[7] ;
  wire \di_reg_n_0_[8] ;
  wire \di_reg_n_0_[9] ;
  wire done_i_1_n_0;
  wire done_i_2_n_0;
  wire done_i_3_n_0;
  wire done_reg_n_0;
  wire [6:0]drp_state;
  wire \drp_state[0]_i_2_n_0 ;
  wire \drp_state[1]_i_2_n_0 ;
  wire \drp_state[4]_i_2_n_0 ;
  wire \drp_state[5]_i_2_n_0 ;
  wire \drp_state[6]_i_2_n_0 ;
  wire \drp_state[6]_i_3_n_0 ;
  wire \drp_state[6]_i_4_n_0 ;
  wire \drp_state_reg_n_0_[0] ;
  wire \drp_state_reg_n_0_[1] ;
  wire \drp_state_reg_n_0_[2] ;
  wire \drp_state_reg_n_0_[4] ;
  wire \drp_state_reg_n_0_[5] ;
  wire \drp_state_reg_n_0_[6] ;
  wire [0:0]drpclk_in;
  wire drprst_in_sync;
  wire [2:2]en;
  wire \en[2]_i_2_n_0 ;
  wire \gen_gtwizard_gthe4.drprdy_int ;
  wire \idx[0]_i_1_n_0 ;
  wire \idx[1]_i_2_n_0 ;
  wire \idx_reg_n_0_[0] ;
  wire \idx_reg_n_0_[1] ;
  wire [3:0]p_0_in;
  wire rd;
  wire rd_i_1_n_0;
  wire rd_reg_n_0;
  wire [7:0]timeout_cntr;
  wire \timeout_cntr[5]_i_2_n_0 ;
  wire \timeout_cntr[7]_i_1_n_0 ;
  wire \timeout_cntr[7]_i_3_n_0 ;
  wire \timeout_cntr[7]_i_4_n_0 ;
  wire \timeout_cntr_reg_n_0_[0] ;
  wire \timeout_cntr_reg_n_0_[1] ;
  wire \timeout_cntr_reg_n_0_[2] ;
  wire \timeout_cntr_reg_n_0_[3] ;
  wire \timeout_cntr_reg_n_0_[4] ;
  wire \timeout_cntr_reg_n_0_[5] ;
  wire \timeout_cntr_reg_n_0_[6] ;
  wire \timeout_cntr_reg_n_0_[7] ;
  wire [2:2]we;
  wire \we[2]_i_1_n_0 ;
  wire \we_reg_n_0_[2] ;
  wire wr;
  wire wr_reg_n_0;

  LUT6 #(
    .INIT(64'h0000000100010000)) 
    \DADDR_O[7]_i_1 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(\drp_state_reg_n_0_[0] ),
        .I3(\drp_state_reg_n_0_[6] ),
        .I4(\drp_state_reg_n_0_[4] ),
        .I5(\drp_state_reg_n_0_[1] ),
        .O(\DADDR_O[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[1] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[1]),
        .Q(DADDR_O[0]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[2] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[2]),
        .Q(DADDR_O[1]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[3] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[3]),
        .Q(DADDR_O[2]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[4] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[4]),
        .Q(DADDR_O[3]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[5] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[5]),
        .Q(DADDR_O[4]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[6] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[6]),
        .Q(DADDR_O[5]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DADDR_O_reg[7] 
       (.C(drpclk_in),
        .CE(\DADDR_O[7]_i_1_n_0 ),
        .D(daddr[7]),
        .Q(DADDR_O[6]),
        .R(drprst_in_sync));
  LUT6 #(
    .INIT(64'h0000000000000116)) 
    DEN_O_i_1
       (.I0(\drp_state_reg_n_0_[4] ),
        .I1(\drp_state_reg_n_0_[1] ),
        .I2(\drp_state_reg_n_0_[5] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .I4(\drp_state_reg_n_0_[6] ),
        .I5(\drp_state_reg_n_0_[0] ),
        .O(DEN_O_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    DEN_O_i_2
       (.I0(\drp_state_reg_n_0_[5] ),
        .I1(\drp_state_reg_n_0_[2] ),
        .O(DEN_O_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    DEN_O_reg
       (.C(drpclk_in),
        .CE(DEN_O_i_1_n_0),
        .D(DEN_O_i_2_n_0),
        .Q(DEN_O),
        .R(drprst_in_sync));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \DI_O[15]_i_1 
       (.I0(\drp_state_reg_n_0_[1] ),
        .I1(\drp_state_reg_n_0_[4] ),
        .I2(\drp_state_reg_n_0_[2] ),
        .I3(\drp_state_reg_n_0_[5] ),
        .I4(\drp_state_reg_n_0_[0] ),
        .I5(\drp_state_reg_n_0_[6] ),
        .O(\DI_O[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[0] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[0] ),
        .Q(DI_O[0]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[10] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[10] ),
        .Q(DI_O[10]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[11] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[11] ),
        .Q(DI_O[11]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[12] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[12] ),
        .Q(DI_O[12]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[13] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[13] ),
        .Q(DI_O[13]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[14] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[14] ),
        .Q(DI_O[14]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[15] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[15] ),
        .Q(DI_O[15]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[1] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[1] ),
        .Q(DI_O[1]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[2] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[2] ),
        .Q(DI_O[2]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[3] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[3] ),
        .Q(DI_O[3]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[4] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[4] ),
        .Q(DI_O[4]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[5] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[5] ),
        .Q(DI_O[5]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[6] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[6] ),
        .Q(DI_O[6]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[7] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[7] ),
        .Q(DI_O[7]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[8] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[8] ),
        .Q(DI_O[8]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DI_O_reg[9] 
       (.C(drpclk_in),
        .CE(\DI_O[15]_i_1_n_0 ),
        .D(\di_reg_n_0_[9] ),
        .Q(DI_O[9]),
        .R(drprst_in_sync));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    \DO_USR_O[47]_i_1 
       (.I0(arb_state[0]),
        .I1(arb_state[3]),
        .I2(arb_state[2]),
        .I3(arb_state[1]),
        .I4(\idx_reg_n_0_[0] ),
        .I5(\idx_reg_n_0_[1] ),
        .O(\DO_USR_O[47]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[32] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[32]),
        .Q(Q[0]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[33] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[33]),
        .Q(Q[1]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[34] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[34]),
        .Q(Q[2]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[35] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[35]),
        .Q(Q[3]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[36] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[36]),
        .Q(Q[4]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[37] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[37]),
        .Q(Q[5]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[38] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[38]),
        .Q(Q[6]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[39] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[39]),
        .Q(Q[7]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[40] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[40]),
        .Q(Q[8]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[41] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[41]),
        .Q(Q[9]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[42] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[42]),
        .Q(Q[10]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[43] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[43]),
        .Q(Q[11]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[44] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[44]),
        .Q(Q[12]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[45] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[45]),
        .Q(Q[13]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[46] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[46]),
        .Q(Q[14]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \DO_USR_O_reg[47] 
       (.C(drpclk_in),
        .CE(\DO_USR_O[47]_i_1_n_0 ),
        .D(DO_USR_O0[47]),
        .Q(Q[15]),
        .R(drprst_in_sync));
  LUT6 #(
    .INIT(64'hFFFFFDFF00000020)) 
    \DRDY_USR_O[2]_i_1 
       (.I0(\DRDY_USR_O[2]_i_2_n_0 ),
        .I1(arb_state[1]),
        .I2(arb_state[2]),
        .I3(arb_state[3]),
        .I4(arb_state[0]),
        .I5(cal_on_tx_drdy),
        .O(\DRDY_USR_O[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \DRDY_USR_O[2]_i_2 
       (.I0(\idx_reg_n_0_[1] ),
        .I1(\idx_reg_n_0_[0] ),
        .O(\DRDY_USR_O[2]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DRDY_USR_O_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\DRDY_USR_O[2]_i_1_n_0 ),
        .Q(cal_on_tx_drdy),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    DWE_O_reg
       (.C(drpclk_in),
        .CE(DEN_O_i_1_n_0),
        .D(\drp_state_reg_n_0_[4] ),
        .Q(DWE_O),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[21] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [0]),
        .Q(addr_i[21]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[22] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [1]),
        .Q(addr_i[22]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[23] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [2]),
        .Q(addr_i[23]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[24] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [3]),
        .Q(addr_i[24]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[25] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [4]),
        .Q(addr_i[25]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[26] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [5]),
        .Q(addr_i[26]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \addr_i_reg[27] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\addr_i_reg[27]_0 [6]),
        .Q(addr_i[27]),
        .R(drprst_in_sync));
  LUT4 #(
    .INIT(16'hFEEB)) 
    \arb_state[0]_i_1 
       (.I0(arb_state[3]),
        .I1(arb_state[0]),
        .I2(arb_state[1]),
        .I3(arb_state[2]),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFF00010000)) 
    \arb_state[1]_i_1 
       (.I0(arb_state[0]),
        .I1(arb_state[3]),
        .I2(done_reg_n_0),
        .I3(arb_state[2]),
        .I4(arb_state[1]),
        .I5(di),
        .O(p_0_in[1]));
  LUT5 #(
    .INIT(32'h00000008)) 
    \arb_state[2]_i_1 
       (.I0(arb_state[1]),
        .I1(done_reg_n_0),
        .I2(arb_state[2]),
        .I3(arb_state[3]),
        .I4(arb_state[0]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h00000000000F0700)) 
    \arb_state[3]_i_1 
       (.I0(en),
        .I1(\idx_reg_n_0_[1] ),
        .I2(arb_state[3]),
        .I3(arb_state[0]),
        .I4(arb_state[2]),
        .I5(arb_state[1]),
        .O(p_0_in[3]));
  (* FSM_ENCODED_STATES = "ARB_START:0001,ARB_INC:1000,ARB_WAIT:0010,ARB_REPORT:0100" *) 
  FDSE #(
    .INIT(1'b1)) 
    \arb_state_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(arb_state[0]),
        .S(drprst_in_sync));
  (* FSM_ENCODED_STATES = "ARB_START:0001,ARB_INC:1000,ARB_WAIT:0010,ARB_REPORT:0100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \arb_state_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(arb_state[1]),
        .R(drprst_in_sync));
  (* FSM_ENCODED_STATES = "ARB_START:0001,ARB_INC:1000,ARB_WAIT:0010,ARB_REPORT:0100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \arb_state_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(arb_state[2]),
        .R(drprst_in_sync));
  (* FSM_ENCODED_STATES = "ARB_START:0001,ARB_INC:1000,ARB_WAIT:0010,ARB_REPORT:0100" *) 
  FDRE #(
    .INIT(1'b0)) 
    \arb_state_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(arb_state[3]),
        .R(drprst_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[1]_i_1 
       (.I0(addr_i[21]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[1]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[2]_i_1 
       (.I0(addr_i[22]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[2]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[3]_i_1 
       (.I0(addr_i[23]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[3]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[4]_i_1 
       (.I0(addr_i[24]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[4]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[5]_i_1 
       (.I0(addr_i[25]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[5]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[6]_i_1 
       (.I0(addr_i[26]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[6]));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    \daddr[7]_i_1 
       (.I0(arb_state[1]),
        .I1(arb_state[2]),
        .I2(arb_state[0]),
        .I3(arb_state[3]),
        .I4(\idx_reg_n_0_[1] ),
        .I5(en),
        .O(di));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \daddr[7]_i_2 
       (.I0(addr_i[27]),
        .I1(\idx_reg_n_0_[0] ),
        .O(daddr0[7]));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[1] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[1]),
        .Q(daddr[1]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[2] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[2]),
        .Q(daddr[2]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[3] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[3]),
        .Q(daddr[3]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[4] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[4]),
        .Q(daddr[4]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[5] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[5]),
        .Q(daddr[5]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[6] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[6]),
        .Q(daddr[6]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[7] 
       (.C(drpclk_in),
        .CE(di),
        .D(daddr0[7]),
        .Q(daddr[7]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[32] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [0]),
        .Q(data_i[32]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[33] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [1]),
        .Q(data_i[33]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[34] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [2]),
        .Q(data_i[34]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[35] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [3]),
        .Q(data_i[35]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[36] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [4]),
        .Q(data_i[36]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[37] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [5]),
        .Q(data_i[37]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[38] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [6]),
        .Q(data_i[38]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[39] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [7]),
        .Q(data_i[39]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[40] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [8]),
        .Q(data_i[40]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[41] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [9]),
        .Q(data_i[41]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[42] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [10]),
        .Q(data_i[42]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[43] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [11]),
        .Q(data_i[43]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[44] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [12]),
        .Q(data_i[44]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[45] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [13]),
        .Q(data_i[45]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[46] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [14]),
        .Q(data_i[46]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \data_i_reg[47] 
       (.C(drpclk_in),
        .CE(cal_on_tx_drpen_out),
        .D(\data_i_reg[47]_0 [15]),
        .Q(data_i[47]),
        .R(drprst_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[0]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[32]),
        .O(\di[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[10]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[42]),
        .O(\di[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[11]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[43]),
        .O(\di[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[12]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[44]),
        .O(\di[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[13]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[45]),
        .O(\di[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[14]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[46]),
        .O(\di[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[15]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[47]),
        .O(\di[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[1]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[33]),
        .O(\di[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[2]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[34]),
        .O(\di[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[3]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[35]),
        .O(\di[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[4]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[36]),
        .O(\di[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[5]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[37]),
        .O(\di[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[6]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[38]),
        .O(\di[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[7]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[39]),
        .O(\di[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[8]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[40]),
        .O(\di[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \di[9]_i_1 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(data_i[41]),
        .O(\di[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[0] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[0]_i_1_n_0 ),
        .Q(\di_reg_n_0_[0] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[10] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[10]_i_1_n_0 ),
        .Q(\di_reg_n_0_[10] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[11] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[11]_i_1_n_0 ),
        .Q(\di_reg_n_0_[11] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[12] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[12]_i_1_n_0 ),
        .Q(\di_reg_n_0_[12] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[13] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[13]_i_1_n_0 ),
        .Q(\di_reg_n_0_[13] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[14] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[14]_i_1_n_0 ),
        .Q(\di_reg_n_0_[14] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[15] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[15]_i_1_n_0 ),
        .Q(\di_reg_n_0_[15] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[1] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[1]_i_1_n_0 ),
        .Q(\di_reg_n_0_[1] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[2] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[2]_i_1_n_0 ),
        .Q(\di_reg_n_0_[2] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[3] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[3]_i_1_n_0 ),
        .Q(\di_reg_n_0_[3] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[4] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[4]_i_1_n_0 ),
        .Q(\di_reg_n_0_[4] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[5] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[5]_i_1_n_0 ),
        .Q(\di_reg_n_0_[5] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[6] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[6]_i_1_n_0 ),
        .Q(\di_reg_n_0_[6] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[7] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[7]_i_1_n_0 ),
        .Q(\di_reg_n_0_[7] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[8] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[8]_i_1_n_0 ),
        .Q(\di_reg_n_0_[8] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \di_reg[9] 
       (.C(drpclk_in),
        .CE(di),
        .D(\di[9]_i_1_n_0 ),
        .Q(\di_reg_n_0_[9] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[0] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[0]),
        .Q(DO_USR_O0[32]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[10] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[10]),
        .Q(DO_USR_O0[42]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[11] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[11]),
        .Q(DO_USR_O0[43]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[12] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[12]),
        .Q(DO_USR_O0[44]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[13] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[13]),
        .Q(DO_USR_O0[45]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[14] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[14]),
        .Q(DO_USR_O0[46]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[15] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[15]),
        .Q(DO_USR_O0[47]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[1] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[1]),
        .Q(DO_USR_O0[33]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[2] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[2]),
        .Q(DO_USR_O0[34]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[3] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[3]),
        .Q(DO_USR_O0[35]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[4] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[4]),
        .Q(DO_USR_O0[36]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[5] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[5]),
        .Q(DO_USR_O0[37]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[6] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[6]),
        .Q(DO_USR_O0[38]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[7] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[7]),
        .Q(DO_USR_O0[39]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[8] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[8]),
        .Q(DO_USR_O0[40]),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \do_r_reg[9] 
       (.C(drpclk_in),
        .CE(drp_state[6]),
        .D(DO_I[9]),
        .Q(DO_USR_O0[41]),
        .R(drprst_in_sync));
  LUT5 #(
    .INIT(32'hAAABAAA8)) 
    done_i_1
       (.I0(done_i_2_n_0),
        .I1(drp_state[6]),
        .I2(\DADDR_O[7]_i_1_n_0 ),
        .I3(done_i_3_n_0),
        .I4(done_reg_n_0),
        .O(done_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    done_i_2
       (.I0(\drp_state_reg_n_0_[6] ),
        .I1(\drp_state_reg_n_0_[1] ),
        .I2(\drp_state_reg_n_0_[4] ),
        .I3(\drp_state[6]_i_3_n_0 ),
        .O(done_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    done_i_3
       (.I0(\drp_state_reg_n_0_[0] ),
        .I1(\drp_state_reg_n_0_[6] ),
        .I2(\drp_state_reg_n_0_[2] ),
        .I3(\drp_state_reg_n_0_[5] ),
        .I4(\drp_state_reg_n_0_[1] ),
        .I5(\drp_state_reg_n_0_[4] ),
        .O(done_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    done_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(done_i_1_n_0),
        .Q(done_reg_n_0),
        .R(drprst_in_sync));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEF8)) 
    \drp_state[0]_i_1 
       (.I0(\drp_state_reg_n_0_[4] ),
        .I1(\drp_state_reg_n_0_[1] ),
        .I2(\drp_state_reg_n_0_[6] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .I4(\drp_state_reg_n_0_[5] ),
        .I5(\drp_state[0]_i_2_n_0 ),
        .O(drp_state[0]));
  LUT6 #(
    .INIT(64'hF0F0F0F0F0F0F01F)) 
    \drp_state[0]_i_2 
       (.I0(wr_reg_n_0),
        .I1(rd_reg_n_0),
        .I2(\drp_state_reg_n_0_[0] ),
        .I3(\drp_state_reg_n_0_[4] ),
        .I4(\drp_state_reg_n_0_[1] ),
        .I5(\drp_state[1]_i_2_n_0 ),
        .O(\drp_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \drp_state[1]_i_1 
       (.I0(rd_reg_n_0),
        .I1(\drp_state_reg_n_0_[0] ),
        .I2(\drp_state_reg_n_0_[6] ),
        .I3(\drp_state[1]_i_2_n_0 ),
        .I4(\drp_state_reg_n_0_[1] ),
        .I5(\drp_state_reg_n_0_[4] ),
        .O(drp_state[1]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \drp_state[1]_i_2 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .O(\drp_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000000C0400)) 
    \drp_state[2]_i_1 
       (.I0(\drp_state[6]_i_3_n_0 ),
        .I1(\drp_state[5]_i_2_n_0 ),
        .I2(\drp_state_reg_n_0_[5] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .I4(\drp_state_reg_n_0_[1] ),
        .I5(\drp_state_reg_n_0_[4] ),
        .O(drp_state[2]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    \drp_state[4]_i_1 
       (.I0(\drp_state_reg_n_0_[0] ),
        .I1(\drp_state_reg_n_0_[6] ),
        .I2(wr_reg_n_0),
        .I3(rd_reg_n_0),
        .I4(\drp_state[4]_i_2_n_0 ),
        .O(drp_state[4]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \drp_state[4]_i_2 
       (.I0(\drp_state_reg_n_0_[4] ),
        .I1(\drp_state_reg_n_0_[1] ),
        .I2(\drp_state_reg_n_0_[5] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .O(\drp_state[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000000C0040)) 
    \drp_state[5]_i_1 
       (.I0(\drp_state[6]_i_3_n_0 ),
        .I1(\drp_state[5]_i_2_n_0 ),
        .I2(\drp_state_reg_n_0_[5] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .I4(\drp_state_reg_n_0_[4] ),
        .I5(\drp_state_reg_n_0_[1] ),
        .O(drp_state[5]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \drp_state[5]_i_2 
       (.I0(\drp_state_reg_n_0_[0] ),
        .I1(\drp_state_reg_n_0_[6] ),
        .O(\drp_state[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000600000000)) 
    \drp_state[6]_i_1 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(\drp_state_reg_n_0_[6] ),
        .I3(\drp_state_reg_n_0_[0] ),
        .I4(\drp_state[6]_i_2_n_0 ),
        .I5(\drp_state[6]_i_3_n_0 ),
        .O(drp_state[6]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \drp_state[6]_i_2 
       (.I0(\drp_state_reg_n_0_[1] ),
        .I1(\drp_state_reg_n_0_[4] ),
        .O(\drp_state[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF80000000)) 
    \drp_state[6]_i_3 
       (.I0(\drp_state[6]_i_4_n_0 ),
        .I1(\timeout_cntr_reg_n_0_[7] ),
        .I2(\timeout_cntr_reg_n_0_[6] ),
        .I3(\timeout_cntr_reg_n_0_[0] ),
        .I4(\timeout_cntr_reg_n_0_[1] ),
        .I5(\gen_gtwizard_gthe4.drprdy_int ),
        .O(\drp_state[6]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \drp_state[6]_i_4 
       (.I0(\timeout_cntr_reg_n_0_[3] ),
        .I1(\timeout_cntr_reg_n_0_[2] ),
        .I2(\timeout_cntr_reg_n_0_[5] ),
        .I3(\timeout_cntr_reg_n_0_[4] ),
        .O(\drp_state[6]_i_4_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \drp_state_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[0]),
        .Q(\drp_state_reg_n_0_[0] ),
        .S(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \drp_state_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[1]),
        .Q(\drp_state_reg_n_0_[1] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \drp_state_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[2]),
        .Q(\drp_state_reg_n_0_[2] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \drp_state_reg[4] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[4]),
        .Q(\drp_state_reg_n_0_[4] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \drp_state_reg[5] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[5]),
        .Q(\drp_state_reg_n_0_[5] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \drp_state_reg[6] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(drp_state[6]),
        .Q(\drp_state_reg_n_0_[6] ),
        .R(drprst_in_sync));
  LUT4 #(
    .INIT(16'hAEAA)) 
    \en[2]_i_1 
       (.I0(cal_on_tx_drpen_out),
        .I1(\idx_reg_n_0_[1] ),
        .I2(\idx_reg_n_0_[0] ),
        .I3(done_reg_n_0),
        .O(we));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hDF00)) 
    \en[2]_i_2 
       (.I0(\idx_reg_n_0_[1] ),
        .I1(\idx_reg_n_0_[0] ),
        .I2(done_reg_n_0),
        .I3(cal_on_tx_drpen_out),
        .O(\en[2]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \en_reg[2] 
       (.C(drpclk_in),
        .CE(we),
        .D(\en[2]_i_2_n_0 ),
        .Q(en),
        .R(drprst_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \idx[0]_i_1 
       (.I0(\idx_reg_n_0_[1] ),
        .I1(\idx_reg_n_0_[0] ),
        .O(\idx[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \idx[1]_i_1 
       (.I0(arb_state[0]),
        .I1(arb_state[3]),
        .I2(arb_state[2]),
        .I3(arb_state[1]),
        .O(CEB2));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \idx[1]_i_2 
       (.I0(\idx_reg_n_0_[0] ),
        .I1(\idx_reg_n_0_[1] ),
        .O(\idx[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \idx_reg[0] 
       (.C(drpclk_in),
        .CE(CEB2),
        .D(\idx[0]_i_1_n_0 ),
        .Q(\idx_reg_n_0_[0] ),
        .R(drprst_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \idx_reg[1] 
       (.C(drpclk_in),
        .CE(CEB2),
        .D(\idx[1]_i_2_n_0 ),
        .Q(\idx_reg_n_0_[1] ),
        .R(drprst_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    rd_i_1
       (.I0(arb_state[1]),
        .I1(\we_reg_n_0_[2] ),
        .I2(\idx_reg_n_0_[1] ),
        .I3(en),
        .O(rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rd_reg
       (.C(drpclk_in),
        .CE(rd),
        .D(rd_i_1_n_0),
        .Q(rd_reg_n_0),
        .R(drprst_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \timeout_cntr[0]_i_1 
       (.I0(\drp_state_reg_n_0_[5] ),
        .I1(\drp_state_reg_n_0_[2] ),
        .I2(\timeout_cntr_reg_n_0_[0] ),
        .O(timeout_cntr[0]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h0EE0)) 
    \timeout_cntr[1]_i_1 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(\timeout_cntr_reg_n_0_[0] ),
        .I3(\timeout_cntr_reg_n_0_[1] ),
        .O(timeout_cntr[1]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h77708880)) 
    \timeout_cntr[2]_i_1 
       (.I0(\timeout_cntr_reg_n_0_[1] ),
        .I1(\timeout_cntr_reg_n_0_[0] ),
        .I2(\drp_state_reg_n_0_[5] ),
        .I3(\drp_state_reg_n_0_[2] ),
        .I4(\timeout_cntr_reg_n_0_[2] ),
        .O(timeout_cntr[2]));
  LUT6 #(
    .INIT(64'h7F7F7F0080808000)) 
    \timeout_cntr[3]_i_1 
       (.I0(\timeout_cntr_reg_n_0_[2] ),
        .I1(\timeout_cntr_reg_n_0_[0] ),
        .I2(\timeout_cntr_reg_n_0_[1] ),
        .I3(\drp_state_reg_n_0_[5] ),
        .I4(\drp_state_reg_n_0_[2] ),
        .I5(\timeout_cntr_reg_n_0_[3] ),
        .O(timeout_cntr[3]));
  LUT6 #(
    .INIT(64'h7FFF000080000000)) 
    \timeout_cntr[4]_i_1 
       (.I0(\timeout_cntr_reg_n_0_[1] ),
        .I1(\timeout_cntr_reg_n_0_[0] ),
        .I2(\timeout_cntr_reg_n_0_[2] ),
        .I3(\timeout_cntr_reg_n_0_[3] ),
        .I4(\drp_state[1]_i_2_n_0 ),
        .I5(\timeout_cntr_reg_n_0_[4] ),
        .O(timeout_cntr[4]));
  LUT6 #(
    .INIT(64'hFF7F000000800000)) 
    \timeout_cntr[5]_i_1 
       (.I0(\timeout_cntr_reg_n_0_[4] ),
        .I1(\timeout_cntr_reg_n_0_[3] ),
        .I2(\timeout_cntr_reg_n_0_[2] ),
        .I3(\timeout_cntr[5]_i_2_n_0 ),
        .I4(\drp_state[1]_i_2_n_0 ),
        .I5(\timeout_cntr_reg_n_0_[5] ),
        .O(timeout_cntr[5]));
  LUT2 #(
    .INIT(4'h7)) 
    \timeout_cntr[5]_i_2 
       (.I0(\timeout_cntr_reg_n_0_[1] ),
        .I1(\timeout_cntr_reg_n_0_[0] ),
        .O(\timeout_cntr[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'hA854)) 
    \timeout_cntr[6]_i_1 
       (.I0(\timeout_cntr[7]_i_4_n_0 ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(\drp_state_reg_n_0_[2] ),
        .I3(\timeout_cntr_reg_n_0_[6] ),
        .O(timeout_cntr[6]));
  LUT5 #(
    .INIT(32'h0000055C)) 
    \timeout_cntr[7]_i_1 
       (.I0(\drp_state[4]_i_2_n_0 ),
        .I1(\timeout_cntr[7]_i_3_n_0 ),
        .I2(\drp_state_reg_n_0_[6] ),
        .I3(\drp_state_reg_n_0_[0] ),
        .I4(drprst_in_sync),
        .O(\timeout_cntr[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'hE00EE0E0)) 
    \timeout_cntr[7]_i_2 
       (.I0(\drp_state_reg_n_0_[5] ),
        .I1(\drp_state_reg_n_0_[2] ),
        .I2(\timeout_cntr_reg_n_0_[7] ),
        .I3(\timeout_cntr[7]_i_4_n_0 ),
        .I4(\timeout_cntr_reg_n_0_[6] ),
        .O(timeout_cntr[7]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'h0116)) 
    \timeout_cntr[7]_i_3 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(\drp_state_reg_n_0_[1] ),
        .I3(\drp_state_reg_n_0_[4] ),
        .O(\timeout_cntr[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \timeout_cntr[7]_i_4 
       (.I0(\timeout_cntr_reg_n_0_[1] ),
        .I1(\timeout_cntr_reg_n_0_[0] ),
        .I2(\timeout_cntr_reg_n_0_[2] ),
        .I3(\timeout_cntr_reg_n_0_[3] ),
        .I4(\timeout_cntr_reg_n_0_[4] ),
        .I5(\timeout_cntr_reg_n_0_[5] ),
        .O(\timeout_cntr[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[0] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[0]),
        .Q(\timeout_cntr_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[1] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[1]),
        .Q(\timeout_cntr_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[2] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[2]),
        .Q(\timeout_cntr_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[3] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[3]),
        .Q(\timeout_cntr_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[4] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[4]),
        .Q(\timeout_cntr_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[5] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[5]),
        .Q(\timeout_cntr_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[6] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[6]),
        .Q(\timeout_cntr_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \timeout_cntr_reg[7] 
       (.C(drpclk_in),
        .CE(\timeout_cntr[7]_i_1_n_0 ),
        .D(timeout_cntr[7]),
        .Q(\timeout_cntr_reg_n_0_[7] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'hDF00)) 
    \we[2]_i_1 
       (.I0(\idx_reg_n_0_[1] ),
        .I1(\idx_reg_n_0_[0] ),
        .I2(done_reg_n_0),
        .I3(cal_on_tx_drpwe_out),
        .O(\we[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \we_reg[2] 
       (.C(drpclk_in),
        .CE(we),
        .D(\we[2]_i_1_n_0 ),
        .Q(\we_reg_n_0_[2] ),
        .R(drprst_in_sync));
  LUT4 #(
    .INIT(16'h0012)) 
    wr_i_1
       (.I0(arb_state[1]),
        .I1(arb_state[2]),
        .I2(arb_state[0]),
        .I3(arb_state[3]),
        .O(rd));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    wr_i_2
       (.I0(arb_state[1]),
        .I1(\we_reg_n_0_[2] ),
        .I2(\idx_reg_n_0_[1] ),
        .I3(en),
        .O(wr));
  FDRE #(
    .INIT(1'b0)) 
    wr_reg
       (.C(drpclk_in),
        .CE(rd),
        .D(wr),
        .Q(wr_reg_n_0),
        .R(drprst_in_sync));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_channel
   (in0,
    \gen_gtwizard_gthe4.drprdy_int ,
    gthtxn_out,
    gthtxp_out,
    \gen_gtwizard_gthe4.gtpowergood_int ,
    rxcdrlock_out,
    rxoutclk_out,
    rxoutclkpcs_out,
    gtwiz_userclk_rx_active_out,
    rxresetdone_out,
    txoutclk_out,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ,
    txresetdone_out,
    gtwiz_userdata_rx_out,
    D,
    rxctrl0_out,
    rxctrl1_out,
    rxclkcorcnt_out,
    txbufstatus_out,
    rxbufstatus_out,
    rxctrl2_out,
    rxctrl3_out,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ,
    drpclk_in,
    DEN_O,
    DWE_O,
    gthrxn_in,
    gthrxp_in,
    gtrefclk0_in,
    \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ,
    \gen_gtwizard_gthe4.gttxreset_int ,
    rxmcommaalignen_in,
    \gen_gtwizard_gthe4.rxprogdivreset_int ,
    RXRATE,
    \gen_gtwizard_gthe4.rxuserrdy_int ,
    rxusrclk_in,
    txelecidle_in,
    \gen_gtwizard_gthe4.txprogdivreset_ch_int ,
    \gen_gtwizard_gthe4.txuserrdy_int ,
    gtwiz_userdata_tx_in,
    Q,
    txctrl0_in,
    txctrl1_in,
    RXPD,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ,
    txctrl2_in,
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5);
  output in0;
  output \gen_gtwizard_gthe4.drprdy_int ;
  output [0:0]gthtxn_out;
  output [0:0]gthtxp_out;
  output \gen_gtwizard_gthe4.gtpowergood_int ;
  output [0:0]rxcdrlock_out;
  output [0:0]rxoutclk_out;
  output [0:0]rxoutclkpcs_out;
  output [0:0]gtwiz_userclk_rx_active_out;
  output [0:0]rxresetdone_out;
  output [0:0]txoutclk_out;
  output \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ;
  output [0:0]txresetdone_out;
  output [15:0]gtwiz_userdata_rx_out;
  output [15:0]D;
  output [1:0]rxctrl0_out;
  output [1:0]rxctrl1_out;
  output [1:0]rxclkcorcnt_out;
  output [0:0]txbufstatus_out;
  output [0:0]rxbufstatus_out;
  output [1:0]rxctrl2_out;
  output [1:0]rxctrl3_out;
  output \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ;
  input \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ;
  input \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ;
  input [0:0]drpclk_in;
  input DEN_O;
  input DWE_O;
  input [0:0]gthrxn_in;
  input [0:0]gthrxp_in;
  input [0:0]gtrefclk0_in;
  input \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  input \gen_gtwizard_gthe4.gttxreset_int ;
  input [0:0]rxmcommaalignen_in;
  input \gen_gtwizard_gthe4.rxprogdivreset_int ;
  input [0:0]RXRATE;
  input \gen_gtwizard_gthe4.rxuserrdy_int ;
  input [0:0]rxusrclk_in;
  input [0:0]txelecidle_in;
  input \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  input \gen_gtwizard_gthe4.txuserrdy_int ;
  input [15:0]gtwiz_userdata_tx_in;
  input [15:0]Q;
  input [1:0]txctrl0_in;
  input [1:0]txctrl1_in;
  input [0:0]RXPD;
  input [2:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ;
  input [1:0]txctrl2_in;
  input [6:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 ;
  input lopt;
  input lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;

  wire [15:0]D;
  wire DEN_O;
  wire DWE_O;
  wire [15:0]Q;
  wire [0:0]RXPD;
  wire [0:0]RXRATE;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  wire \gen_gtwizard_gthe4.drprdy_int ;
  wire \gen_gtwizard_gthe4.gtpowergood_int ;
  wire \gen_gtwizard_gthe4.gttxreset_int ;
  wire \gen_gtwizard_gthe4.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe4.rxuserrdy_int ;
  wire \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  wire \gen_gtwizard_gthe4.txuserrdy_int ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ;
  wire [2:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ;
  wire [6:0]\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_0 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_1 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_100 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_101 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_102 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_103 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_104 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_105 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_106 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_107 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_108 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_109 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_11 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_110 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_111 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_112 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_113 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_114 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_115 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_116 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_117 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_118 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_119 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_12 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_120 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_121 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_122 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_123 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_124 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_125 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_126 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_127 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_128 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_129 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_13 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_130 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_131 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_132 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_133 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_134 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_135 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_136 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_137 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_138 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_139 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_14 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_140 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_141 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_142 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_143 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_144 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_145 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_146 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_147 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_148 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_149 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_15 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_150 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_151 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_152 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_153 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_154 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_155 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_156 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_157 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_158 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_159 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_16 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_160 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_161 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_162 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_163 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_164 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_165 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_166 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_167 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_168 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_169 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_17 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_170 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_171 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_172 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_173 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_174 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_175 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_176 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_177 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_178 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_179 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_18 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_180 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_181 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_182 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_183 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_184 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_185 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_186 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_187 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_188 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_189 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_19 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_190 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_2 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_20 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_207 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_208 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_209 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_21 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_210 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_211 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_212 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_213 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_214 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_215 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_216 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_217 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_218 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_219 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_22 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_220 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_221 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_222 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_239 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_24 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_240 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_241 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_242 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_243 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_244 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_245 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_246 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_247 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_248 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_249 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_25 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_250 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_251 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_252 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_253 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_254 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_255 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_256 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_257 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_258 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_259 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_26 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_260 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_261 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_262 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_263 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_264 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_265 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_266 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_267 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_268 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_269 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_27 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_270 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_271 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_272 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_273 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_274 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_275 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_276 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_277 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_278 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_279 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_28 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_280 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_281 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_282 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_283 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_284 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_287 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_288 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_289 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_29 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_290 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_291 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_292 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_293 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_294 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_295 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_296 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_297 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_298 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_299 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_30 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_300 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_303 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_304 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_305 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_306 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_309 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_31 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_310 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_311 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_312 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_313 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_314 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_316 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_317 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_318 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_319 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_32 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_320 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_321 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_322 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_324 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_325 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_326 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_327 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_328 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_329 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_33 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_330 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_331 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_332 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_333 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_334 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_335 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_336 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_337 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_338 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_339 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_34 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_340 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_341 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_342 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_343 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_344 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_345 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_348 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_349 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_35 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_350 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_351 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_352 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_353 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_356 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_357 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_358 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_359 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_36 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_360 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_361 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_362 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_363 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_364 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_365 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_366 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_367 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_368 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_369 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_37 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_370 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_371 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_372 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_373 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_374 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_375 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_376 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_377 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_378 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_379 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_38 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_380 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_39 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_4 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_40 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_41 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_43 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_45 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_46 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_48 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_49 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_5 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_50 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_51 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_52 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_53 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_54 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_56 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_57 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_58 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_59 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_60 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_61 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_62 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_63 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_64 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_65 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_67 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_68 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_69 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_7 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_70 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_71 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_73 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_74 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_75 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_77 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_78 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_79 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_80 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_81 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_82 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_83 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_84 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_85 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_86 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_87 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_88 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_89 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_90 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_91 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_92 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_93 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_94 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_95 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_96 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_97 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_98 ;
  wire \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_99 ;
  wire [0:0]gthrxn_in;
  wire [0:0]gthrxp_in;
  wire [0:0]gthtxn_out;
  wire [0:0]gthtxp_out;
  wire [0:0]gtrefclk0_in;
  wire [0:0]gtwiz_userclk_rx_active_out;
  wire [15:0]gtwiz_userdata_rx_out;
  wire [15:0]gtwiz_userdata_tx_in;
  wire in0;
  wire lopt;
  wire lopt_1;
  wire [0:0]rxbufstatus_out;
  wire [0:0]rxcdrlock_out;
  wire [1:0]rxclkcorcnt_out;
  wire [1:0]rxctrl0_out;
  wire [1:0]rxctrl1_out;
  wire [1:0]rxctrl2_out;
  wire [1:0]rxctrl3_out;
  wire [0:0]rxmcommaalignen_in;
  wire [0:0]rxoutclk_out;
  wire [0:0]rxoutclkpcs_out;
  wire [0:0]rxresetdone_out;
  wire [0:0]rxusrclk_in;
  wire [0:0]txbufstatus_out;
  wire [1:0]txctrl0_in;
  wire [1:0]txctrl1_in;
  wire [1:0]txctrl2_in;
  wire [0:0]txelecidle_in;
  wire [0:0]txoutclk_out;
  wire [0:0]txresetdone_out;
  wire xlnx_opt_;
  wire xlnx_opt__1;
  wire xlnx_opt__2;
  wire xlnx_opt__3;

  assign lopt_2 = xlnx_opt_;
  assign lopt_3 = xlnx_opt__1;
  assign lopt_4 = xlnx_opt__2;
  assign lopt_5 = xlnx_opt__3;
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC
       (.CE(lopt),
        .CESYNC(xlnx_opt_),
        .CLK(rxoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__1));
  (* OPT_MODIFIED = "MLO" *) 
  BUFG_GT_SYNC BUFG_GT_SYNC_1
       (.CE(lopt),
        .CESYNC(xlnx_opt__2),
        .CLK(txoutclk_out),
        .CLR(lopt_1),
        .CLRSYNC(xlnx_opt__3));
  LUT1 #(
    .INIT(2'h1)) 
    \gen_powergood_delay.intclk_rrst_n_r[4]_i_2 
       (.I0(\gen_gtwizard_gthe4.gtpowergood_int ),
        .O(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_1 ));
  (* box_type = "PRIMITIVE" *) 
  GTHE4_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(16'h1000),
    .ADAPT_CFG1(16'hC800),
    .ADAPT_CFG2(16'h0000),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .A_RXOSCALRESET(1'b0),
    .A_RXPROGDIVRESET(1'b0),
    .A_RXTERMINATION(1'b1),
    .A_TXDIFFCTRL(5'b01100),
    .A_TXPROGDIVRESET(1'b0),
    .CAPBYPASS_FORCE(1'b0),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CDR_SWAP_MODE_EN(1'b0),
    .CFOK_PWRSVE_EN(1'b1),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CH_HSPMUX(16'h3C3C),
    .CKCAL1_CFG_0(16'b1100000011000000),
    .CKCAL1_CFG_1(16'b0101000011000000),
    .CKCAL1_CFG_2(16'b0000000000001010),
    .CKCAL1_CFG_3(16'b0000000000000000),
    .CKCAL2_CFG_0(16'b1100000011000000),
    .CKCAL2_CFG_1(16'b1000000011000000),
    .CKCAL2_CFG_2(16'b0000000000000000),
    .CKCAL2_CFG_3(16'b0000000000000000),
    .CKCAL2_CFG_4(16'b0000000000000000),
    .CKCAL_RSVD0(16'h0080),
    .CKCAL_RSVD1(16'h0400),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(15),
    .CLK_COR_MIN_LAT(12),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG0(16'h01FA),
    .CPLL_CFG1(16'h0023),
    .CPLL_CFG2(16'h0002),
    .CPLL_CFG3(16'h0000),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(4),
    .CPLL_INIT_CFG0(16'h02B2),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .CTLE3_OCAP_EXT_CTRL(3'b000),
    .CTLE3_OCAP_EXT_EN(1'b0),
    .DDI_CTRL(2'b00),
    .DDI_REALIGN_WAIT(15),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DELAY_ELEC(1'b0),
    .DMONITOR_CFG0(10'h000),
    .DMONITOR_CFG1(8'h00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("FALSE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER0(16'h0000),
    .ES_QUALIFIER1(16'h0000),
    .ES_QUALIFIER2(16'h0000),
    .ES_QUALIFIER3(16'h0000),
    .ES_QUALIFIER4(16'h0000),
    .ES_QUALIFIER5(16'h0000),
    .ES_QUALIFIER6(16'h0000),
    .ES_QUALIFIER7(16'h0000),
    .ES_QUALIFIER8(16'h0000),
    .ES_QUALIFIER9(16'h0000),
    .ES_QUAL_MASK0(16'h0000),
    .ES_QUAL_MASK1(16'h0000),
    .ES_QUAL_MASK2(16'h0000),
    .ES_QUAL_MASK3(16'h0000),
    .ES_QUAL_MASK4(16'h0000),
    .ES_QUAL_MASK5(16'h0000),
    .ES_QUAL_MASK6(16'h0000),
    .ES_QUAL_MASK7(16'h0000),
    .ES_QUAL_MASK8(16'h0000),
    .ES_QUAL_MASK9(16'h0000),
    .ES_SDATA_MASK0(16'h0000),
    .ES_SDATA_MASK1(16'h0000),
    .ES_SDATA_MASK2(16'h0000),
    .ES_SDATA_MASK3(16'h0000),
    .ES_SDATA_MASK4(16'h0000),
    .ES_SDATA_MASK5(16'h0000),
    .ES_SDATA_MASK6(16'h0000),
    .ES_SDATA_MASK7(16'h0000),
    .ES_SDATA_MASK8(16'h0000),
    .ES_SDATA_MASK9(16'h0000),
    .EYE_SCAN_SWAP_EN(1'b0),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(5'b00000),
    .ISCAN_CK_PH_SEL2(1'b0),
    .LOCAL_MASTER(1'b1),
    .LPBK_BIAS_CTRL(3'b100),
    .LPBK_EN_RCAL_B(1'b0),
    .LPBK_EXT_RCAL(4'b1000),
    .LPBK_IND_CTRL0(3'b000),
    .LPBK_IND_CTRL1(3'b000),
    .LPBK_IND_CTRL2(3'b000),
    .LPBK_RG_CTRL(4'b1110),
    .OOBDIVCTL(2'b00),
    .OOB_PWRUP(1'b0),
    .PCI3_AUTO_REALIGN("OVR_1K_BLK"),
    .PCI3_PIPE_RX_ELECIDLE(1'b0),
    .PCI3_RX_ASYNC_EBUF_BYPASS(2'b00),
    .PCI3_RX_ELECIDLE_EI2_ENABLE(1'b0),
    .PCI3_RX_ELECIDLE_H2L_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_H2L_DISABLE(3'b000),
    .PCI3_RX_ELECIDLE_HI_COUNT(6'b000000),
    .PCI3_RX_ELECIDLE_LP4_DISABLE(1'b0),
    .PCI3_RX_FIFO_DISABLE(1'b0),
    .PCIE3_CLK_COR_EMPTY_THRSH(5'b00000),
    .PCIE3_CLK_COR_FULL_THRSH(6'b010000),
    .PCIE3_CLK_COR_MAX_LAT(5'b00100),
    .PCIE3_CLK_COR_MIN_LAT(5'b00000),
    .PCIE3_CLK_COR_THRSH_TIMER(6'b001000),
    .PCIE_BUFG_DIV_CTRL(16'h1000),
    .PCIE_PLL_SEL_MODE_GEN12(2'h0),
    .PCIE_PLL_SEL_MODE_GEN3(2'h3),
    .PCIE_PLL_SEL_MODE_GEN4(2'h2),
    .PCIE_RXPCS_CFG_GEN3(16'h0AA5),
    .PCIE_RXPMA_CFG(16'h280A),
    .PCIE_TXPCS_CFG_GEN3(16'h2CA4),
    .PCIE_TXPMA_CFG(16'h280A),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD0(16'b0000000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PREIQ_FREQ_BST(0),
    .PROCESS_PAR(3'b010),
    .RATE_SW_USE_DRP(1'b1),
    .RCLK_SIPO_DLY_ENB(1'b0),
    .RCLK_SIPO_INV_EN(1'b0),
    .RESET_POWERSAVE_DISABLE(1'b0),
    .RTX_BUF_CML_CTRL(3'b010),
    .RTX_BUF_TERM_CTRL(2'b00),
    .RXBUFRESET_TIME(5'b00011),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(0),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(4),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG0(16'h0003),
    .RXCDR_CFG0_GEN3(16'h0003),
    .RXCDR_CFG1(16'h0000),
    .RXCDR_CFG1_GEN3(16'h0000),
    .RXCDR_CFG2(16'h0249),
    .RXCDR_CFG2_GEN2(10'h249),
    .RXCDR_CFG2_GEN3(16'h0249),
    .RXCDR_CFG2_GEN4(16'h0164),
    .RXCDR_CFG3(16'h0012),
    .RXCDR_CFG3_GEN2(6'h12),
    .RXCDR_CFG3_GEN3(16'h0012),
    .RXCDR_CFG3_GEN4(16'h0012),
    .RXCDR_CFG4(16'h5CF6),
    .RXCDR_CFG4_GEN3(16'h5CF6),
    .RXCDR_CFG5(16'hB46B),
    .RXCDR_CFG5_GEN3(16'h146B),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG0(16'h2201),
    .RXCDR_LOCK_CFG1(16'h9FFF),
    .RXCDR_LOCK_CFG2(16'h77C3),
    .RXCDR_LOCK_CFG3(16'h0001),
    .RXCDR_LOCK_CFG4(16'h0000),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXCFOK_CFG0(16'h0000),
    .RXCFOK_CFG1(16'h8015),
    .RXCFOK_CFG2(16'h02AE),
    .RXCKCAL1_IQ_LOOP_RST_CFG(16'h0004),
    .RXCKCAL1_I_LOOP_RST_CFG(16'h0004),
    .RXCKCAL1_Q_LOOP_RST_CFG(16'h0004),
    .RXCKCAL2_DX_LOOP_RST_CFG(16'h0004),
    .RXCKCAL2_D_LOOP_RST_CFG(16'h0004),
    .RXCKCAL2_S_LOOP_RST_CFG(16'h0004),
    .RXCKCAL2_X_LOOP_RST_CFG(16'h0004),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDFELPM_KL_CFG0(16'h0000),
    .RXDFELPM_KL_CFG1(16'hA0E2),
    .RXDFELPM_KL_CFG2(16'h0100),
    .RXDFE_CFG0(16'h0A00),
    .RXDFE_CFG1(16'h0000),
    .RXDFE_GC_CFG0(16'h0000),
    .RXDFE_GC_CFG1(16'h8000),
    .RXDFE_GC_CFG2(16'hFFE0),
    .RXDFE_H2_CFG0(16'h0000),
    .RXDFE_H2_CFG1(16'h0002),
    .RXDFE_H3_CFG0(16'h0000),
    .RXDFE_H3_CFG1(16'h8002),
    .RXDFE_H4_CFG0(16'h0000),
    .RXDFE_H4_CFG1(16'h8002),
    .RXDFE_H5_CFG0(16'h0000),
    .RXDFE_H5_CFG1(16'h8002),
    .RXDFE_H6_CFG0(16'h0000),
    .RXDFE_H6_CFG1(16'h8002),
    .RXDFE_H7_CFG0(16'h0000),
    .RXDFE_H7_CFG1(16'h8002),
    .RXDFE_H8_CFG0(16'h0000),
    .RXDFE_H8_CFG1(16'h8002),
    .RXDFE_H9_CFG0(16'h0000),
    .RXDFE_H9_CFG1(16'h8002),
    .RXDFE_HA_CFG0(16'h0000),
    .RXDFE_HA_CFG1(16'h8002),
    .RXDFE_HB_CFG0(16'h0000),
    .RXDFE_HB_CFG1(16'h8002),
    .RXDFE_HC_CFG0(16'h0000),
    .RXDFE_HC_CFG1(16'h8002),
    .RXDFE_HD_CFG0(16'h0000),
    .RXDFE_HD_CFG1(16'h8002),
    .RXDFE_HE_CFG0(16'h0000),
    .RXDFE_HE_CFG1(16'h8002),
    .RXDFE_HF_CFG0(16'h0000),
    .RXDFE_HF_CFG1(16'h8002),
    .RXDFE_KH_CFG0(16'h0000),
    .RXDFE_KH_CFG1(16'h8000),
    .RXDFE_KH_CFG2(16'h2613),
    .RXDFE_KH_CFG3(16'h411C),
    .RXDFE_OS_CFG0(16'h0000),
    .RXDFE_OS_CFG1(16'h8002),
    .RXDFE_PWR_SAVING(1'b1),
    .RXDFE_UT_CFG0(16'h0000),
    .RXDFE_UT_CFG1(16'h0003),
    .RXDFE_UT_CFG2(16'h0000),
    .RXDFE_VP_CFG0(16'h0000),
    .RXDFE_VP_CFG1(16'h8033),
    .RXDLY_CFG(16'h0010),
    .RXDLY_LCFG(16'h0030),
    .RXELECIDLE_CFG("SIGCFG_4"),
    .RXGBOX_FIFO_INIT_RD_ADDR(4),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_CFG(16'h0000),
    .RXLPM_GC_CFG(16'h8000),
    .RXLPM_KH_CFG0(16'h0000),
    .RXLPM_KH_CFG1(16'h0002),
    .RXLPM_OS_CFG0(16'h0000),
    .RXLPM_OS_CFG1(16'h8002),
    .RXOOB_CFG(9'b000000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00011),
    .RXPHBEACON_CFG(16'h0000),
    .RXPHDLY_CFG(16'h2070),
    .RXPHSAMP_CFG(16'h2100),
    .RXPHSLIP_CFG(16'h9933),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_AUTO_BW_SEL_BYPASS(1'b0),
    .RXPI_CFG0(16'h1300),
    .RXPI_CFG1(16'b0000000011111101),
    .RXPI_LPM(1'b0),
    .RXPI_SEL_LC(2'b00),
    .RXPI_STARTCODE(2'b00),
    .RXPI_VREFSEL(1'b0),
    .RXPMACLK_SEL("DATA"),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXPRBS_LINKACQ_CNT(15),
    .RXREFCLKDIV2_SEL(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b1),
    .RX_AFE_CM_EN(1'b0),
    .RX_BIAS_CFG0(16'h1554),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CAPFF_SARC_ENB(1'b0),
    .RX_CLK25_DIV(7),
    .RX_CLKMUX_EN(1'b1),
    .RX_CLK_SLIP_OVRD(5'b00000),
    .RX_CM_BUF_CFG(4'b1010),
    .RX_CM_BUF_PD(1'b0),
    .RX_CM_SEL(3),
    .RX_CM_TRIM(10),
    .RX_CTLE3_LPF(8'b11111111),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DEGEN_CTRL(3'b011),
    .RX_DFELPM_CFG0(6),
    .RX_DFELPM_CFG1(1'b1),
    .RX_DFELPM_KLKH_AGC_STUP_EN(1'b1),
    .RX_DFE_AGC_CFG0(2'b10),
    .RX_DFE_AGC_CFG1(4),
    .RX_DFE_KL_LPM_KH_CFG0(1),
    .RX_DFE_KL_LPM_KH_CFG1(4),
    .RX_DFE_KL_LPM_KL_CFG0(2'b01),
    .RX_DFE_KL_LPM_KL_CFG1(4),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_DIV2_MODE_B(1'b0),
    .RX_DIVRESET_TIME(5'b00001),
    .RX_EN_CTLE_RCAL_B(1'b0),
    .RX_EN_HI_LR(1'b1),
    .RX_EXT_RL_CTRL(9'b000000000),
    .RX_EYESCAN_VS_CODE(7'b0000000),
    .RX_EYESCAN_VS_NEG_DIR(1'b0),
    .RX_EYESCAN_VS_RANGE(2'b00),
    .RX_EYESCAN_VS_UT_SIGN(1'b0),
    .RX_FABINT_USRCLK_FLOP(1'b0),
    .RX_INT_DATAWIDTH(0),
    .RX_PMA_POWER_SAVE(1'b0),
    .RX_PMA_RSV0(16'h0000),
    .RX_PROGDIV_CFG(0.000000),
    .RX_PROGDIV_RATE(16'h0001),
    .RX_RESLOAD_CTRL(4'b0000),
    .RX_RESLOAD_OVRD(1'b0),
    .RX_SAMPLE_PERIOD(3'b111),
    .RX_SIG_VALID_DLY(11),
    .RX_SUM_DFETAPREP_EN(1'b0),
    .RX_SUM_IREF_TUNE(4'b0100),
    .RX_SUM_RESLOAD_CTRL(4'b0011),
    .RX_SUM_VCMTUNE(4'b0110),
    .RX_SUM_VCM_OVWR(1'b0),
    .RX_SUM_VREF_TUNE(3'b100),
    .RX_TUNE_AFE_OS(2'b00),
    .RX_VREG_CTRL(3'b101),
    .RX_VREG_PDB(1'b1),
    .RX_WIDEMODE_CDR(2'b00),
    .RX_WIDEMODE_CDR_GEN3(2'b00),
    .RX_WIDEMODE_CDR_GEN4(2'b01),
    .RX_XCLK_SEL("RXDES"),
    .RX_XMODE_SEL(1'b0),
    .SAMPLE_CLK_PHASE(1'b0),
    .SAS_12G_MODE(1'b0),
    .SATA_BURST_SEQ_LEN(4'b1111),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_DEVICE("ULTRASCALE_PLUS"),
    .SIM_MODE("FAST"),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("Z"),
    .SRSTMODE(1'b0),
    .TAPDLY_SET_TX(2'h0),
    .TEMPERATURE_PAR(4'b0010),
    .TERM_RCAL_CFG(15'b100001000010001),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV0(8'h00),
    .TST_RSV1(8'h00),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h8010),
    .TXDLY_LCFG(16'h0030),
    .TXDRVBIAS_N(4'b1010),
    .TXFIFO_ADDR_CFG("LOW"),
    .TXGBOX_FIFO_INIT_RD_ADDR(4),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00011),
    .TXPHDLY_CFG0(16'h6070),
    .TXPHDLY_CFG1(16'h000F),
    .TXPH_CFG(16'h0723),
    .TXPH_CFG2(16'h0000),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG(16'h03DF),
    .TXPI_CFG0(2'b00),
    .TXPI_CFG1(2'b00),
    .TXPI_CFG2(2'b00),
    .TXPI_CFG3(1'b1),
    .TXPI_CFG4(1'b1),
    .TXPI_CFG5(3'b000),
    .TXPI_GRAY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b0),
    .TXPI_LPM(1'b0),
    .TXPI_PPM(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b001),
    .TXPI_VREFSEL(1'b0),
    .TXPMARESET_TIME(5'b00011),
    .TXREFCLKDIV2_SEL(1'b0),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(7),
    .TX_CLKMUX_EN(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DCC_LOOP_RST_CFG(16'h0004),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DEEMPH2(6'b000000),
    .TX_DEEMPH3(6'b000000),
    .TX_DIVRESET_TIME(5'b00001),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_DRVMUX_CTRL(2),
    .TX_EIDLE_ASSERT_DELAY(3'b100),
    .TX_EIDLE_DEASSERT_DELAY(3'b011),
    .TX_FABINT_USRCLK_FLOP(1'b0),
    .TX_FIFO_BYP_EN(1'b0),
    .TX_IDLE_DATA_ZERO(1'b0),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1011111),
    .TX_MARGIN_FULL_1(7'b1011110),
    .TX_MARGIN_FULL_2(7'b1011100),
    .TX_MARGIN_FULL_3(7'b1011010),
    .TX_MARGIN_FULL_4(7'b1011000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000101),
    .TX_MARGIN_LOW_2(7'b1000011),
    .TX_MARGIN_LOW_3(7'b1000010),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PHICAL_CFG0(16'h0000),
    .TX_PHICAL_CFG1(16'h7E00),
    .TX_PHICAL_CFG2(16'h0201),
    .TX_PI_BIASSET(0),
    .TX_PI_IBIAS_MID(2'b00),
    .TX_PMADATA_OPT(1'b0),
    .TX_PMA_POWER_SAVE(1'b0),
    .TX_PMA_RSV0(16'h0008),
    .TX_PREDRV_CTRL(2),
    .TX_PROGCLK_SEL("CPLL"),
    .TX_PROGDIV_CFG(20.000000),
    .TX_PROGDIV_RATE(16'h0001),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h0032),
    .TX_RXDETECT_REF(4),
    .TX_SAMPLE_PERIOD(3'b111),
    .TX_SARC_LPBK_ENB(1'b0),
    .TX_SW_MEAS(2'b00),
    .TX_VREG_CTRL(3'b000),
    .TX_VREG_PDB(1'b0),
    .TX_VREG_VREFSEL(2'b00),
    .TX_XCLK_SEL("TXOUT"),
    .USB_BOTH_BURST_IDLE(1'b0),
    .USB_BURSTMAX_U3WAKE(7'b1111111),
    .USB_BURSTMIN_U3WAKE(7'b1100011),
    .USB_CLK_COR_EQ_EN(1'b0),
    .USB_EXT_CNTL(1'b1),
    .USB_IDLEMAX_POLLING(10'b1010111011),
    .USB_IDLEMIN_POLLING(10'b0100101011),
    .USB_LFPSPING_BURST(9'b000000101),
    .USB_LFPSPOLLING_BURST(9'b000110001),
    .USB_LFPSPOLLING_IDLE_MS(9'b000000100),
    .USB_LFPSU1EXIT_BURST(9'b000011101),
    .USB_LFPSU2LPEXIT_BURST_MS(9'b001100011),
    .USB_LFPSU3WAKE_BURST_MS(9'b111110011),
    .USB_LFPS_TPERIOD(4'b0011),
    .USB_LFPS_TPERIOD_ACCURATE(1'b1),
    .USB_MODE(1'b0),
    .USB_PCIE_ERR_REP_DIS(1'b0),
    .USB_PING_SATA_MAX_INIT(21),
    .USB_PING_SATA_MIN_INIT(12),
    .USB_POLL_SATA_MAX_BURST(8),
    .USB_POLL_SATA_MIN_BURST(4),
    .USB_RAW_ELEC(1'b0),
    .USB_RXIDLE_P0_CTRL(1'b1),
    .USB_TXIDLE_TUNE_ENABLE(1'b1),
    .USB_U1_SATA_MAX_WAKE(7),
    .USB_U1_SATA_MIN_WAKE(4),
    .USB_U2_SAS_MAX_COM(64),
    .USB_U2_SAS_MIN_COM(36),
    .USE_PCS_CLK_PHASE_SEL(1'b0),
    .Y_ALL_MODE(1'b0)) 
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST 
       (.BUFGTCE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_0 ),
        .BUFGTCEMASK({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_317 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_318 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_319 }),
        .BUFGTDIV({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_372 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_373 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_374 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_375 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_376 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_377 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_378 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_379 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_380 }),
        .BUFGTRESET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_1 ),
        .BUFGTRSTMASK({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_320 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_321 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_322 }),
        .CDRSTEPDIR(1'b0),
        .CDRSTEPSQ(1'b0),
        .CDRSTEPSX(1'b0),
        .CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .CPLLFBCLKLOST(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_2 ),
        .CPLLFREQLOCK(1'b0),
        .CPLLLOCK(in0),
        .CPLLLOCKDETCLK(1'b0),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_2 ),
        .CPLLREFCLKLOST(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_4 ),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_3 ),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_207 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_208 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_209 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_210 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_211 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_212 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_213 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_214 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_215 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_216 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_217 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_218 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_219 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_220 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_221 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_222 }),
        .DMONITOROUTCLK(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_5 ),
        .DRPADDR({1'b0,1'b0,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_5 [5]}),
        .DRPCLK(drpclk_in),
        .DRPDI(Q),
        .DRPDO(D),
        .DRPEN(DEN_O),
        .DRPRDY(\gen_gtwizard_gthe4.drprdy_int ),
        .DRPRST(1'b0),
        .DRPWE(DWE_O),
        .EYESCANDATAERROR(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_7 ),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .FREQOS(1'b0),
        .GTGREFCLK(1'b0),
        .GTHRXN(gthrxn_in),
        .GTHRXP(gthrxp_in),
        .GTHTXN(gthtxn_out),
        .GTHTXP(gthtxp_out),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTPOWERGOOD(\gen_gtwizard_gthe4.gtpowergood_int ),
        .GTREFCLK0(gtrefclk0_in),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_11 ),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ),
        .GTRXRESETSEL(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(\gen_gtwizard_gthe4.gttxreset_int ),
        .GTTXRESETSEL(1'b0),
        .INCPCTRL(1'b0),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCIEEQRXEQADAPTDONE(1'b0),
        .PCIERATEGEN3(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_12 ),
        .PCIERATEIDLE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_13 ),
        .PCIERATEQPLLPD({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_303 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_304 }),
        .PCIERATEQPLLRESET({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_305 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_306 }),
        .PCIERSTIDLE(1'b0),
        .PCIERSTTXSYNCSTART(1'b0),
        .PCIESYNCTXSYNCDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_14 ),
        .PCIEUSERGEN3RDY(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_15 ),
        .PCIEUSERPHYSTATUSRST(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_16 ),
        .PCIEUSERRATEDONE(1'b0),
        .PCIEUSERRATESTART(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_17 ),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_239 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_240 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_241 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_242 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_243 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_244 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_245 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_246 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_247 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_248 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_249 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_250 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_251 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_252 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_253 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_254 }),
        .PHYSTATUS(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_18 ),
        .PINRSRVDAS({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_255 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_256 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_257 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_258 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_259 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_260 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_261 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_262 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_263 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_264 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_265 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_266 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_267 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_268 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_269 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_270 }),
        .POWERPRESENT(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_19 ),
        .QPLL0CLK(1'b0),
        .QPLL0FREQLOCK(1'b0),
        .QPLL0REFCLK(1'b0),
        .QPLL1CLK(1'b0),
        .QPLL1FREQLOCK(1'b0),
        .QPLL1REFCLK(1'b0),
        .RESETEXCEPTION(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_20 ),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXAFECFOKEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({rxbufstatus_out,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_324 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_325 }),
        .RXBYTEISALIGNED(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_21 ),
        .RXBYTEREALIGN(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_22 ),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(rxcdrlock_out),
        .RXCDROVRDEN(1'b0),
        .RXCDRPHDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_24 ),
        .RXCDRRESET(1'b0),
        .RXCHANBONDSEQ(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_25 ),
        .RXCHANISALIGNED(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_26 ),
        .RXCHANREALIGN(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_27 ),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_329 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_330 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_331 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_332 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_333 }),
        .RXCHBONDSLAVE(1'b0),
        .RXCKCALDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_28 ),
        .RXCKCALRESET(1'b0),
        .RXCKCALSTART({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCLKCORCNT(rxclkcorcnt_out),
        .RXCOMINITDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_29 ),
        .RXCOMMADET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_30 ),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_31 ),
        .RXCOMWAKEDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_32 ),
        .RXCTRL0({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_271 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_272 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_273 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_274 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_275 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_276 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_277 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_278 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_279 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_280 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_281 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_282 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_283 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_284 ,rxctrl0_out}),
        .RXCTRL1({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_287 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_288 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_289 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_290 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_291 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_292 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_293 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_294 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_295 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_296 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_297 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_298 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_299 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_300 ,rxctrl1_out}),
        .RXCTRL2({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_340 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_341 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_342 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_343 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_344 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_345 ,rxctrl2_out}),
        .RXCTRL3({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_348 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_349 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_350 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_351 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_352 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_353 ,rxctrl3_out}),
        .RXDATA({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_79 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_80 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_81 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_82 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_83 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_84 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_85 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_86 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_87 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_88 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_89 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_90 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_91 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_92 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_93 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_94 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_95 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_96 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_97 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_98 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_99 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_100 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_101 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_102 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_103 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_104 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_105 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_106 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_107 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_108 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_109 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_110 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_111 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_112 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_113 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_114 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_115 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_116 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_117 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_118 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_119 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_120 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_121 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_122 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_123 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_124 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_125 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_126 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_127 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_128 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_129 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_130 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_131 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_132 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_133 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_134 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_135 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_136 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_137 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_138 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_139 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_140 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_141 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_142 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_143 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_144 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_145 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_146 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_147 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_148 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_149 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_150 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_151 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_152 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_153 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_154 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_155 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_156 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_157 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_158 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_159 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_160 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_161 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_162 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_163 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_164 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_165 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_166 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_167 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_168 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_169 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_170 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_171 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_172 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_173 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_174 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_175 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_176 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_177 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_178 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_179 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_180 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_181 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_182 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_183 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_184 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_185 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_186 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_187 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_188 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_189 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_190 ,gtwiz_userdata_rx_out}),
        .RXDATAEXTENDRSVD({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_356 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_357 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_358 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_359 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_360 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_361 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_362 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_363 }),
        .RXDATAVALID({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_309 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_310 }),
        .RXDFEAGCCTRL({1'b0,1'b1}),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECFOKFCNUM({1'b1,1'b1,1'b0,1'b1}),
        .RXDFECFOKFEN(1'b0),
        .RXDFECFOKFPULSE(1'b0),
        .RXDFECFOKHOLD(1'b0),
        .RXDFECFOKOVREN(1'b0),
        .RXDFEKHHOLD(1'b0),
        .RXDFEKHOVRDEN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP10HOLD(1'b0),
        .RXDFETAP10OVRDEN(1'b0),
        .RXDFETAP11HOLD(1'b0),
        .RXDFETAP11OVRDEN(1'b0),
        .RXDFETAP12HOLD(1'b0),
        .RXDFETAP12OVRDEN(1'b0),
        .RXDFETAP13HOLD(1'b0),
        .RXDFETAP13OVRDEN(1'b0),
        .RXDFETAP14HOLD(1'b0),
        .RXDFETAP14OVRDEN(1'b0),
        .RXDFETAP15HOLD(1'b0),
        .RXDFETAP15OVRDEN(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFETAP6HOLD(1'b0),
        .RXDFETAP6OVRDEN(1'b0),
        .RXDFETAP7HOLD(1'b0),
        .RXDFETAP7OVRDEN(1'b0),
        .RXDFETAP8HOLD(1'b0),
        .RXDFETAP8OVRDEN(1'b0),
        .RXDFETAP9HOLD(1'b0),
        .RXDFETAP9OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_33 ),
        .RXELECIDLE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_34 ),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXEQTRAINING(1'b0),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_334 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_335 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_336 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_337 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_338 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_339 }),
        .RXHEADERVALID({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_311 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_312 }),
        .RXLATCLK(1'b0),
        .RXLFPSTRESETDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_35 ),
        .RXLFPSU2LPEXITDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_36 ),
        .RXLFPSU3WAKEDET(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_37 ),
        .RXLPMEN(1'b1),
        .RXLPMGCHOLD(1'b0),
        .RXLPMGCOVRDEN(1'b0),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXLPMOSHOLD(1'b0),
        .RXLPMOSOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(rxmcommaalignen_in),
        .RXMONITOROUT({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_364 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_365 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_366 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_367 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_368 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_369 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_370 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_371 }),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_38 ),
        .RXOSINTSTARTED(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_39 ),
        .RXOSINTSTROBEDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_40 ),
        .RXOSINTSTROBESTARTED(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_41 ),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk_out),
        .RXOUTCLKFABRIC(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_43 ),
        .RXOUTCLKPCS(rxoutclkpcs_out),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(rxmcommaalignen_in),
        .RXPCSRESET(1'b0),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_45 ),
        .RXPHALIGNEN(1'b0),
        .RXPHALIGNERR(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_46 ),
        .RXPHDLYPD(1'b1),
        .RXPHDLYRESET(1'b0),
        .RXPHOVRDEN(1'b0),
        .RXPLLCLKSEL({1'b0,1'b0}),
        .RXPMARESET(1'b0),
        .RXPMARESETDONE(gtwiz_userclk_rx_active_out),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_48 ),
        .RXPRBSLOCKED(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_49 ),
        .RXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .RXPRGDIVRESETDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_50 ),
        .RXPROGDIVRESET(\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .RXQPIEN(1'b0),
        .RXQPISENN(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_51 ),
        .RXQPISENP(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_52 ),
        .RXRATE({1'b0,1'b0,RXRATE}),
        .RXRATEDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_53 ),
        .RXRATEMODE(RXRATE),
        .RXRECCLKOUT(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_54 ),
        .RXRESETDONE(rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSLIDERDY(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_56 ),
        .RXSLIPDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_57 ),
        .RXSLIPOUTCLK(1'b0),
        .RXSLIPOUTCLKRDY(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_58 ),
        .RXSLIPPMA(1'b0),
        .RXSLIPPMARDY(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_59 ),
        .RXSTARTOFSEQ({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_313 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_314 }),
        .RXSTATUS({\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_326 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_327 ,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_328 }),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_60 ),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_61 ),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXTERMINATION(1'b0),
        .RXUSERRDY(\gen_gtwizard_gthe4.rxuserrdy_int ),
        .RXUSRCLK(rxusrclk_in),
        .RXUSRCLK2(rxusrclk_in),
        .RXVALID(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_62 ),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFSTATUS({txbufstatus_out,\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_316 }),
        .TXCOMFINISH(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_63 ),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXCTRL0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl0_in}),
        .TXCTRL1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl1_in}),
        .TXCTRL2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,txctrl2_in}),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtwiz_userdata_tx_in}),
        .TXDATAEXTENDRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXDCCDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_64 ),
        .TXDCCFORCESTART(1'b0),
        .TXDCCRESET(1'b0),
        .TXDEEMPH({1'b0,1'b0}),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b1,1'b0,1'b0,1'b0}),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_65 ),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(txelecidle_in),
        .TXHEADER({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXLATCLK(1'b0),
        .TXLFPSTRESET(1'b0),
        .TXLFPSU2LPEXIT(1'b0),
        .TXLFPSU3WAKE(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXMUXDCDEXHOLD(1'b0),
        .TXMUXDCDORWREN(1'b0),
        .TXONESZEROS(1'b0),
        .TXOUTCLK(txoutclk_out),
        .TXOUTCLKFABRIC(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_67 ),
        .TXOUTCLKPCS(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_68 ),
        .TXOUTCLKSEL(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_4 ),
        .TXPCSRESET(1'b0),
        .TXPD({txelecidle_in,txelecidle_in}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_69 ),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b1),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_70 ),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b0),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPLLCLKSEL({1'b0,1'b0}),
        .TXPMARESET(1'b0),
        .TXPMARESETDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_71 ),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRGDIVRESETDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_0 ),
        .TXPROGDIVRESET(\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_73 ),
        .TXQPISENP(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_74 ),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_75 ),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_77 ),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(\gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_n_78 ),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(\gen_gtwizard_gthe4.txuserrdy_int ),
        .TXUSRCLK(rxusrclk_in),
        .TXUSRCLK2(rxusrclk_in));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal
   (\gen_gtwizard_gthe4.txprogdivreset_ch_int ,
    cpllpd_int_reg,
    cpllreset_int_reg,
    USER_CPLLLOCK_OUT_reg,
    rst_in0,
    Q,
    DEN_O,
    DWE_O,
    DADDR_O,
    DI_O,
    in0,
    i_in_meta_reg,
    i_in_meta_reg_0,
    txoutclk_out,
    drpclk_in,
    RESET_IN,
    DO_I,
    \gen_gtwizard_gthe4.drprdy_int ,
    lopt,
    lopt_1);
  output \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  output cpllpd_int_reg;
  output cpllreset_int_reg;
  output USER_CPLLLOCK_OUT_reg;
  output rst_in0;
  output [2:0]Q;
  output DEN_O;
  output DWE_O;
  output [6:0]DADDR_O;
  output [15:0]DI_O;
  input in0;
  input i_in_meta_reg;
  input i_in_meta_reg_0;
  input [0:0]txoutclk_out;
  input [0:0]drpclk_in;
  input RESET_IN;
  input [15:0]DO_I;
  input \gen_gtwizard_gthe4.drprdy_int ;
  input lopt;
  input lopt_1;

  wire [6:0]DADDR_O;
  wire DEN_O;
  wire [15:0]DI_O;
  wire [15:0]DO_I;
  wire DWE_O;
  wire [2:0]Q;
  wire RESET_IN;
  wire USER_CPLLLOCK_OUT_reg;
  wire [17:1]\U_TXOUTCLK_FREQ_COUNTER/testclk_cnt_reg ;
  wire [15:0]cal_on_tx_dout;
  wire cal_on_tx_drdy;
  wire [7:1]cal_on_tx_drpaddr_out;
  wire [15:0]cal_on_tx_drpdi_out;
  wire cal_on_tx_drpen_out;
  wire cal_on_tx_drpwe_out;
  wire cal_on_tx_reset_in_sync;
  wire cpllpd_int_reg;
  wire cpllreset_int_reg;
  wire [0:0]drpclk_in;
  wire drprst_in_sync;
  wire \gen_gtwizard_gthe4.drprdy_int ;
  wire \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  wire gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx_i_n_24;
  wire \i_/i_/i__carry__0_n_0 ;
  wire \i_/i_/i__carry__0_n_1 ;
  wire \i_/i_/i__carry__0_n_10 ;
  wire \i_/i_/i__carry__0_n_11 ;
  wire \i_/i_/i__carry__0_n_12 ;
  wire \i_/i_/i__carry__0_n_13 ;
  wire \i_/i_/i__carry__0_n_14 ;
  wire \i_/i_/i__carry__0_n_15 ;
  wire \i_/i_/i__carry__0_n_2 ;
  wire \i_/i_/i__carry__0_n_3 ;
  wire \i_/i_/i__carry__0_n_4 ;
  wire \i_/i_/i__carry__0_n_5 ;
  wire \i_/i_/i__carry__0_n_6 ;
  wire \i_/i_/i__carry__0_n_7 ;
  wire \i_/i_/i__carry__0_n_8 ;
  wire \i_/i_/i__carry__0_n_9 ;
  wire \i_/i_/i__carry__1_n_14 ;
  wire \i_/i_/i__carry__1_n_15 ;
  wire \i_/i_/i__carry__1_n_7 ;
  wire \i_/i_/i__carry_n_0 ;
  wire \i_/i_/i__carry_n_1 ;
  wire \i_/i_/i__carry_n_10 ;
  wire \i_/i_/i__carry_n_11 ;
  wire \i_/i_/i__carry_n_12 ;
  wire \i_/i_/i__carry_n_13 ;
  wire \i_/i_/i__carry_n_14 ;
  wire \i_/i_/i__carry_n_15 ;
  wire \i_/i_/i__carry_n_2 ;
  wire \i_/i_/i__carry_n_3 ;
  wire \i_/i_/i__carry_n_4 ;
  wire \i_/i_/i__carry_n_5 ;
  wire \i_/i_/i__carry_n_6 ;
  wire \i_/i_/i__carry_n_7 ;
  wire \i_/i_/i__carry_n_8 ;
  wire \i_/i_/i__carry_n_9 ;
  wire i_in_meta_reg;
  wire i_in_meta_reg_0;
  wire in0;
  wire lopt;
  wire lopt_1;
  wire rst_in0;
  wire [0:0]txoutclk_out;
  wire [7:1]\NLW_i_/i_/i__carry__1_CO_UNCONNECTED ;
  wire [7:2]\NLW_i_/i_/i__carry__1_O_UNCONNECTED ;

  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_19 bit_synchronizer_drprst_inst
       (.drpclk_in(drpclk_in),
        .drprst_in_sync(drprst_in_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gte4_drp_arb gtwizard_ultrascale_v1_7_13_gte4_drp_arb_i
       (.DADDR_O(DADDR_O),
        .DEN_O(DEN_O),
        .DI_O(DI_O),
        .DO_I(DO_I),
        .DWE_O(DWE_O),
        .Q(cal_on_tx_dout),
        .\addr_i_reg[27]_0 (cal_on_tx_drpaddr_out),
        .cal_on_tx_drdy(cal_on_tx_drdy),
        .cal_on_tx_drpen_out(cal_on_tx_drpen_out),
        .cal_on_tx_drpwe_out(cal_on_tx_drpwe_out),
        .\data_i_reg[47]_0 (cal_on_tx_drpdi_out),
        .drpclk_in(drpclk_in),
        .drprst_in_sync(drprst_in_sync),
        .\gen_gtwizard_gthe4.drprdy_int (\gen_gtwizard_gthe4.drprdy_int ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx_i
       (.D(\U_TXOUTCLK_FREQ_COUNTER/testclk_cnt_reg ),
        .O({\i_/i_/i__carry_n_8 ,\i_/i_/i__carry_n_9 ,\i_/i_/i__carry_n_10 ,\i_/i_/i__carry_n_11 ,\i_/i_/i__carry_n_12 ,\i_/i_/i__carry_n_13 ,\i_/i_/i__carry_n_14 ,\i_/i_/i__carry_n_15 }),
        .Q(cal_on_tx_dout),
        .S(gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx_i_n_24),
        .USER_CPLLLOCK_OUT_reg_0(USER_CPLLLOCK_OUT_reg),
        .cal_on_tx_drdy(cal_on_tx_drdy),
        .cal_on_tx_drpen_out(cal_on_tx_drpen_out),
        .cal_on_tx_drpwe_out(cal_on_tx_drpwe_out),
        .cal_on_tx_reset_in_sync(cal_on_tx_reset_in_sync),
        .cpllpd_int_reg_0(cpllpd_int_reg),
        .cpllreset_int_reg_0(cpllreset_int_reg),
        .\daddr_reg[7]_0 (cal_on_tx_drpaddr_out),
        .\di_reg[15]_0 (cal_on_tx_drpdi_out),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.txprogdivreset_ch_int (\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .i_in_meta_reg(i_in_meta_reg),
        .i_in_meta_reg_0(i_in_meta_reg_0),
        .in0(in0),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 (Q),
        .rst_in0(rst_in0),
        .\testclk_cnt_reg[15] ({\i_/i_/i__carry__0_n_8 ,\i_/i_/i__carry__0_n_9 ,\i_/i_/i__carry__0_n_10 ,\i_/i_/i__carry__0_n_11 ,\i_/i_/i__carry__0_n_12 ,\i_/i_/i__carry__0_n_13 ,\i_/i_/i__carry__0_n_14 ,\i_/i_/i__carry__0_n_15 }),
        .\testclk_cnt_reg[17] ({\i_/i_/i__carry__1_n_14 ,\i_/i_/i__carry__1_n_15 }),
        .txoutclk_out(txoutclk_out));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \i_/i_/i__carry 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\i_/i_/i__carry_n_0 ,\i_/i_/i__carry_n_1 ,\i_/i_/i__carry_n_2 ,\i_/i_/i__carry_n_3 ,\i_/i_/i__carry_n_4 ,\i_/i_/i__carry_n_5 ,\i_/i_/i__carry_n_6 ,\i_/i_/i__carry_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\i_/i_/i__carry_n_8 ,\i_/i_/i__carry_n_9 ,\i_/i_/i__carry_n_10 ,\i_/i_/i__carry_n_11 ,\i_/i_/i__carry_n_12 ,\i_/i_/i__carry_n_13 ,\i_/i_/i__carry_n_14 ,\i_/i_/i__carry_n_15 }),
        .S({\U_TXOUTCLK_FREQ_COUNTER/testclk_cnt_reg [7:1],gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx_i_n_24}));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \i_/i_/i__carry__0 
       (.CI(\i_/i_/i__carry_n_0 ),
        .CI_TOP(1'b0),
        .CO({\i_/i_/i__carry__0_n_0 ,\i_/i_/i__carry__0_n_1 ,\i_/i_/i__carry__0_n_2 ,\i_/i_/i__carry__0_n_3 ,\i_/i_/i__carry__0_n_4 ,\i_/i_/i__carry__0_n_5 ,\i_/i_/i__carry__0_n_6 ,\i_/i_/i__carry__0_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__0_n_8 ,\i_/i_/i__carry__0_n_9 ,\i_/i_/i__carry__0_n_10 ,\i_/i_/i__carry__0_n_11 ,\i_/i_/i__carry__0_n_12 ,\i_/i_/i__carry__0_n_13 ,\i_/i_/i__carry__0_n_14 ,\i_/i_/i__carry__0_n_15 }),
        .S(\U_TXOUTCLK_FREQ_COUNTER/testclk_cnt_reg [15:8]));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \i_/i_/i__carry__1 
       (.CI(\i_/i_/i__carry__0_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_i_/i_/i__carry__1_CO_UNCONNECTED [7:1],\i_/i_/i__carry__1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_i_/i_/i__carry__1_O_UNCONNECTED [7:2],\i_/i_/i__carry__1_n_14 ,\i_/i_/i__carry__1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\U_TXOUTCLK_FREQ_COUNTER/testclk_cnt_reg [17:16]}));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_20 reset_synchronizer_resetin_rx_inst
       (.drpclk_in(drpclk_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_21 reset_synchronizer_resetin_tx_inst
       (.RESET_IN(RESET_IN),
        .cal_on_tx_reset_in_sync(cal_on_tx_reset_in_sync),
        .drpclk_in(drpclk_in));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_freq_counter
   (done_o_reg_0,
    D,
    rst_in_out_reg,
    rst_in_out_reg_0,
    S,
    done_o_reg_1,
    DI,
    \freq_cnt_o_reg[16]_0 ,
    \freq_cnt_o_reg[15]_0 ,
    \freq_cnt_o_reg[14]_0 ,
    \freq_cnt_o_reg[0]_0 ,
    \freq_cnt_o_reg[17]_0 ,
    \freq_cnt_o_reg[16]_1 ,
    drpclk_in,
    \state_reg[1]_0 ,
    txoutclkmon,
    O,
    \testclk_cnt_reg[15]_0 ,
    \testclk_cnt_reg[17]_0 ,
    cal_on_tx_reset_in_sync,
    \repeat_ctr_reg[3] ,
    CO,
    \repeat_ctr_reg[3]_0 ,
    Q,
    cal_fail_store_reg,
    cal_fail_store__0,
    \cpll_cal_state_reg[21] ,
    cal_fail_store_reg_0,
    cal_fail_store_reg_1,
    \cpll_cal_state_reg[13] ,
    \cpll_cal_state_reg[13]_0 ,
    \cpll_cal_state_reg[13]_1 ,
    \cpll_cal_state_reg[13]_2 );
  output done_o_reg_0;
  output [16:0]D;
  output rst_in_out_reg;
  output rst_in_out_reg_0;
  output [0:0]S;
  output [1:0]done_o_reg_1;
  output [4:0]DI;
  output [7:0]\freq_cnt_o_reg[16]_0 ;
  output [4:0]\freq_cnt_o_reg[15]_0 ;
  output [7:0]\freq_cnt_o_reg[14]_0 ;
  output \freq_cnt_o_reg[0]_0 ;
  output [0:0]\freq_cnt_o_reg[17]_0 ;
  output [0:0]\freq_cnt_o_reg[16]_1 ;
  input [0:0]drpclk_in;
  input \state_reg[1]_0 ;
  input txoutclkmon;
  input [7:0]O;
  input [7:0]\testclk_cnt_reg[15]_0 ;
  input [1:0]\testclk_cnt_reg[17]_0 ;
  input cal_on_tx_reset_in_sync;
  input \repeat_ctr_reg[3] ;
  input [0:0]CO;
  input [0:0]\repeat_ctr_reg[3]_0 ;
  input [5:0]Q;
  input cal_fail_store_reg;
  input cal_fail_store__0;
  input [0:0]\cpll_cal_state_reg[21] ;
  input cal_fail_store_reg_0;
  input cal_fail_store_reg_1;
  input \cpll_cal_state_reg[13] ;
  input \cpll_cal_state_reg[13]_0 ;
  input \cpll_cal_state_reg[13]_1 ;
  input \cpll_cal_state_reg[13]_2 ;

  wire [0:0]CO;
  wire [16:0]D;
  wire [4:0]DI;
  wire [7:0]O;
  wire [5:0]Q;
  wire [0:0]S;
  wire cal_fail_store__0;
  wire cal_fail_store_i_2_n_0;
  wire cal_fail_store_i_3_n_0;
  wire cal_fail_store_reg;
  wire cal_fail_store_reg_0;
  wire cal_fail_store_reg_1;
  wire cal_on_tx_reset_in_sync;
  wire clear;
  wire \cpll_cal_state[21]_i_2_n_0 ;
  wire \cpll_cal_state_reg[13] ;
  wire \cpll_cal_state_reg[13]_0 ;
  wire \cpll_cal_state_reg[13]_1 ;
  wire \cpll_cal_state_reg[13]_2 ;
  wire [0:0]\cpll_cal_state_reg[21] ;
  wire done_o_reg_0;
  wire [1:0]done_o_reg_1;
  wire [0:0]drpclk_in;
  wire \freq_cnt_o[17]_i_1_n_0 ;
  wire \freq_cnt_o_reg[0]_0 ;
  wire [7:0]\freq_cnt_o_reg[14]_0 ;
  wire [4:0]\freq_cnt_o_reg[15]_0 ;
  wire [7:0]\freq_cnt_o_reg[16]_0 ;
  wire [0:0]\freq_cnt_o_reg[16]_1 ;
  wire [0:0]\freq_cnt_o_reg[17]_0 ;
  wire \freq_cnt_o_reg_n_0_[0] ;
  wire \freq_cnt_o_reg_n_0_[10] ;
  wire \freq_cnt_o_reg_n_0_[11] ;
  wire \freq_cnt_o_reg_n_0_[12] ;
  wire \freq_cnt_o_reg_n_0_[14] ;
  wire \freq_cnt_o_reg_n_0_[15] ;
  wire \freq_cnt_o_reg_n_0_[16] ;
  wire \freq_cnt_o_reg_n_0_[17] ;
  wire \freq_cnt_o_reg_n_0_[1] ;
  wire \freq_cnt_o_reg_n_0_[2] ;
  wire \freq_cnt_o_reg_n_0_[3] ;
  wire \freq_cnt_o_reg_n_0_[4] ;
  wire \freq_cnt_o_reg_n_0_[5] ;
  wire \freq_cnt_o_reg_n_0_[6] ;
  wire \freq_cnt_o_reg_n_0_[7] ;
  wire \freq_cnt_o_reg_n_0_[8] ;
  wire \freq_cnt_o_reg_n_0_[9] ;
  wire \hold_clk[2]_i_1_n_0 ;
  wire \hold_clk[5]_i_1_n_0 ;
  wire [5:0]hold_clk_reg;
  wire [5:0]p_0_in__0;
  wire [15:1]p_0_in__1;
  wire p_1_in;
  wire refclk_cnt0_carry__0_n_2;
  wire refclk_cnt0_carry__0_n_3;
  wire refclk_cnt0_carry__0_n_4;
  wire refclk_cnt0_carry__0_n_5;
  wire refclk_cnt0_carry__0_n_6;
  wire refclk_cnt0_carry__0_n_7;
  wire refclk_cnt0_carry_n_0;
  wire refclk_cnt0_carry_n_1;
  wire refclk_cnt0_carry_n_2;
  wire refclk_cnt0_carry_n_3;
  wire refclk_cnt0_carry_n_4;
  wire refclk_cnt0_carry_n_5;
  wire refclk_cnt0_carry_n_6;
  wire refclk_cnt0_carry_n_7;
  wire \refclk_cnt[0]_i_1_n_0 ;
  wire [15:0]refclk_cnt_reg;
  wire \repeat_ctr[3]_i_4_n_0 ;
  wire \repeat_ctr_reg[3] ;
  wire [0:0]\repeat_ctr_reg[3]_0 ;
  wire rst_in_out;
  wire rst_in_out_reg;
  wire rst_in_out_reg_0;
  wire \state[0]_i_1_n_0 ;
  wire \state[1]_i_1_n_0 ;
  wire \state[1]_i_2_n_0 ;
  wire \state[2]_i_1_n_0 ;
  wire \state[2]_i_2_n_0 ;
  wire \state[2]_i_3_n_0 ;
  wire \state[2]_i_4_n_0 ;
  wire \state[2]_i_5_n_0 ;
  wire \state[3]_i_1_n_0 ;
  wire \state[3]_i_2_n_0 ;
  wire \state[4]_i_1_n_0 ;
  wire \state_reg[1]_0 ;
  wire \state_reg_n_0_[2] ;
  wire \state_reg_n_0_[4] ;
  wire testclk_cnt0_n_0;
  wire [0:0]testclk_cnt_reg;
  wire [7:0]\testclk_cnt_reg[15]_0 ;
  wire [1:0]\testclk_cnt_reg[17]_0 ;
  wire [3:0]testclk_div4;
  wire testclk_en;
  (* async_reg = "true" *) wire testclk_en_dly1;
  (* async_reg = "true" *) wire testclk_en_dly2;
  wire testclk_rst;
  (* async_reg = "true" *) wire tstclk_rst_dly1;
  (* async_reg = "true" *) wire tstclk_rst_dly2;
  wire txoutclkmon;
  wire [7:6]NLW_refclk_cnt0_carry__0_CO_UNCONNECTED;
  wire [7:7]NLW_refclk_cnt0_carry__0_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hFFFFDFDF30331010)) 
    cal_fail_store_i_1
       (.I0(cal_fail_store_i_2_n_0),
        .I1(cal_on_tx_reset_in_sync),
        .I2(cal_fail_store_i_3_n_0),
        .I3(cal_fail_store_reg),
        .I4(Q[5]),
        .I5(cal_fail_store__0),
        .O(rst_in_out_reg_0));
  LUT2 #(
    .INIT(4'h8)) 
    cal_fail_store_i_2
       (.I0(CO),
        .I1(\repeat_ctr_reg[3]_0 ),
        .O(cal_fail_store_i_2_n_0));
  LUT6 #(
    .INIT(64'hF222000000000000)) 
    cal_fail_store_i_3
       (.I0(cal_fail_store_reg_0),
        .I1(cal_fail_store_reg_1),
        .I2(CO),
        .I3(\repeat_ctr_reg[3]_0 ),
        .I4(Q[3]),
        .I5(done_o_reg_0),
        .O(cal_fail_store_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_1
       (.I0(\freq_cnt_o_reg_n_0_[0] ),
        .I1(\freq_cnt_o_reg_n_0_[1] ),
        .O(\freq_cnt_o_reg[0]_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    cpll_cal_state2_carry_i_10
       (.I0(\freq_cnt_o_reg_n_0_[10] ),
        .I1(\freq_cnt_o_reg_n_0_[11] ),
        .O(\freq_cnt_o_reg[16]_0 [4]));
  LUT2 #(
    .INIT(4'h8)) 
    cpll_cal_state2_carry_i_11
       (.I0(\freq_cnt_o_reg_n_0_[9] ),
        .I1(\freq_cnt_o_reg_n_0_[8] ),
        .O(\freq_cnt_o_reg[16]_0 [3]));
  LUT2 #(
    .INIT(4'h2)) 
    cpll_cal_state2_carry_i_12
       (.I0(\freq_cnt_o_reg_n_0_[7] ),
        .I1(\freq_cnt_o_reg_n_0_[6] ),
        .O(\freq_cnt_o_reg[16]_0 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    cpll_cal_state2_carry_i_13
       (.I0(\freq_cnt_o_reg_n_0_[4] ),
        .I1(\freq_cnt_o_reg_n_0_[5] ),
        .O(\freq_cnt_o_reg[16]_0 [1]));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_14
       (.I0(\freq_cnt_o_reg_n_0_[2] ),
        .I1(\freq_cnt_o_reg_n_0_[3] ),
        .O(\freq_cnt_o_reg[16]_0 [0]));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_2
       (.I0(\freq_cnt_o_reg_n_0_[12] ),
        .I1(\freq_cnt_o_reg[15]_0 [3]),
        .O(DI[4]));
  LUT2 #(
    .INIT(4'h7)) 
    cpll_cal_state2_carry_i_3
       (.I0(\freq_cnt_o_reg_n_0_[11] ),
        .I1(\freq_cnt_o_reg_n_0_[10] ),
        .O(DI[3]));
  LUT2 #(
    .INIT(4'h7)) 
    cpll_cal_state2_carry_i_4
       (.I0(\freq_cnt_o_reg_n_0_[8] ),
        .I1(\freq_cnt_o_reg_n_0_[9] ),
        .O(DI[2]));
  LUT1 #(
    .INIT(2'h1)) 
    cpll_cal_state2_carry_i_5
       (.I0(\freq_cnt_o_reg_n_0_[7] ),
        .O(DI[1]));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_6
       (.I0(\freq_cnt_o_reg_n_0_[4] ),
        .I1(\freq_cnt_o_reg_n_0_[5] ),
        .O(DI[0]));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_7
       (.I0(\freq_cnt_o_reg_n_0_[16] ),
        .I1(\freq_cnt_o_reg_n_0_[17] ),
        .O(\freq_cnt_o_reg[16]_0 [7]));
  LUT2 #(
    .INIT(4'h1)) 
    cpll_cal_state2_carry_i_8
       (.I0(\freq_cnt_o_reg_n_0_[14] ),
        .I1(\freq_cnt_o_reg_n_0_[15] ),
        .O(\freq_cnt_o_reg[16]_0 [6]));
  LUT2 #(
    .INIT(4'h2)) 
    cpll_cal_state2_carry_i_9
       (.I0(\freq_cnt_o_reg_n_0_[12] ),
        .I1(\freq_cnt_o_reg[15]_0 [3]),
        .O(\freq_cnt_o_reg[16]_0 [5]));
  LUT6 #(
    .INIT(64'hFFFFFFFF8F888888)) 
    \cpll_cal_state[13]_i_1 
       (.I0(\cpll_cal_state_reg[13] ),
        .I1(Q[2]),
        .I2(\cpll_cal_state[21]_i_2_n_0 ),
        .I3(Q[3]),
        .I4(done_o_reg_0),
        .I5(Q[1]),
        .O(done_o_reg_1[0]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \cpll_cal_state[21]_i_1 
       (.I0(done_o_reg_0),
        .I1(Q[3]),
        .I2(\cpll_cal_state[21]_i_2_n_0 ),
        .I3(\cpll_cal_state_reg[21] ),
        .I4(Q[4]),
        .O(done_o_reg_1[1]));
  LUT6 #(
    .INIT(64'hF888888888888888)) 
    \cpll_cal_state[21]_i_2 
       (.I0(\repeat_ctr_reg[3]_0 ),
        .I1(CO),
        .I2(\cpll_cal_state_reg[13]_0 ),
        .I3(\cpll_cal_state_reg[13]_1 ),
        .I4(\cpll_cal_state_reg[13]_2 ),
        .I5(cal_fail_store_reg_0),
        .O(\cpll_cal_state[21]_i_2_n_0 ));
  FDCE done_o_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(\state_reg[1]_0 ),
        .D(\state_reg_n_0_[4] ),
        .Q(done_o_reg_0));
  LUT2 #(
    .INIT(4'h2)) 
    \freq_cnt_o[17]_i_1 
       (.I0(p_1_in),
        .I1(\state_reg[1]_0 ),
        .O(\freq_cnt_o[17]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[0] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(testclk_cnt_reg),
        .Q(\freq_cnt_o_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[10] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[9]),
        .Q(\freq_cnt_o_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[11] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[10]),
        .Q(\freq_cnt_o_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[12] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[11]),
        .Q(\freq_cnt_o_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[13] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[12]),
        .Q(\freq_cnt_o_reg[15]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[14] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[13]),
        .Q(\freq_cnt_o_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[15] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[14]),
        .Q(\freq_cnt_o_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[16] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[15]),
        .Q(\freq_cnt_o_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[17] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[16]),
        .Q(\freq_cnt_o_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[1] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[0]),
        .Q(\freq_cnt_o_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[2] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[1]),
        .Q(\freq_cnt_o_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[3] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[2]),
        .Q(\freq_cnt_o_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[4] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[3]),
        .Q(\freq_cnt_o_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[5] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[4]),
        .Q(\freq_cnt_o_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[6] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[5]),
        .Q(\freq_cnt_o_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[7] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[6]),
        .Q(\freq_cnt_o_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[8] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[7]),
        .Q(\freq_cnt_o_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \freq_cnt_o_reg[9] 
       (.C(drpclk_in),
        .CE(\freq_cnt_o[17]_i_1_n_0 ),
        .D(D[8]),
        .Q(\freq_cnt_o_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \hold_clk[0]_i_1 
       (.I0(hold_clk_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \hold_clk[1]_i_1 
       (.I0(hold_clk_reg[0]),
        .I1(hold_clk_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \hold_clk[2]_i_1 
       (.I0(hold_clk_reg[0]),
        .I1(hold_clk_reg[1]),
        .I2(hold_clk_reg[2]),
        .O(\hold_clk[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \hold_clk[3]_i_1 
       (.I0(hold_clk_reg[1]),
        .I1(hold_clk_reg[0]),
        .I2(hold_clk_reg[2]),
        .I3(hold_clk_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \hold_clk[4]_i_1 
       (.I0(hold_clk_reg[2]),
        .I1(hold_clk_reg[0]),
        .I2(hold_clk_reg[1]),
        .I3(hold_clk_reg[3]),
        .I4(hold_clk_reg[4]),
        .O(p_0_in__0[4]));
  LUT2 #(
    .INIT(4'h1)) 
    \hold_clk[5]_i_1 
       (.I0(testclk_rst),
        .I1(\state_reg_n_0_[2] ),
        .O(\hold_clk[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \hold_clk[5]_i_2 
       (.I0(hold_clk_reg[3]),
        .I1(hold_clk_reg[1]),
        .I2(hold_clk_reg[0]),
        .I3(hold_clk_reg[2]),
        .I4(hold_clk_reg[4]),
        .I5(hold_clk_reg[5]),
        .O(p_0_in__0[5]));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(hold_clk_reg[0]),
        .R(\hold_clk[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(hold_clk_reg[1]),
        .R(\hold_clk[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\hold_clk[2]_i_1_n_0 ),
        .Q(hold_clk_reg[2]),
        .R(\hold_clk[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(hold_clk_reg[3]),
        .R(\hold_clk[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[4] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__0[4]),
        .Q(hold_clk_reg[4]),
        .R(\hold_clk[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \hold_clk_reg[5] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__0[5]),
        .Q(hold_clk_reg[5]),
        .R(\hold_clk[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry__0_i_1
       (.I0(\freq_cnt_o_reg_n_0_[17] ),
        .I1(\freq_cnt_o_reg_n_0_[16] ),
        .O(\freq_cnt_o_reg[17]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry__0_i_2
       (.I0(\freq_cnt_o_reg_n_0_[16] ),
        .I1(\freq_cnt_o_reg_n_0_[17] ),
        .O(\freq_cnt_o_reg[16]_1 ));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1
       (.I0(testclk_cnt_reg),
        .O(S));
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_10
       (.I0(\freq_cnt_o_reg_n_0_[4] ),
        .I1(\freq_cnt_o_reg_n_0_[5] ),
        .O(\freq_cnt_o_reg[14]_0 [2]));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_11
       (.I0(\freq_cnt_o_reg_n_0_[2] ),
        .I1(\freq_cnt_o_reg_n_0_[3] ),
        .O(\freq_cnt_o_reg[14]_0 [1]));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_12
       (.I0(\freq_cnt_o_reg_n_0_[0] ),
        .I1(\freq_cnt_o_reg_n_0_[1] ),
        .O(\freq_cnt_o_reg[14]_0 [0]));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_1__0
       (.I0(\freq_cnt_o_reg_n_0_[15] ),
        .I1(\freq_cnt_o_reg_n_0_[14] ),
        .O(\freq_cnt_o_reg[15]_0 [4]));
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_2
       (.I0(\freq_cnt_o_reg_n_0_[9] ),
        .I1(\freq_cnt_o_reg_n_0_[8] ),
        .O(\freq_cnt_o_reg[15]_0 [2]));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_3
       (.I0(\freq_cnt_o_reg_n_0_[3] ),
        .I1(\freq_cnt_o_reg_n_0_[2] ),
        .O(\freq_cnt_o_reg[15]_0 [1]));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_4
       (.I0(\freq_cnt_o_reg_n_0_[1] ),
        .I1(\freq_cnt_o_reg_n_0_[0] ),
        .O(\freq_cnt_o_reg[15]_0 [0]));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_5
       (.I0(\freq_cnt_o_reg_n_0_[14] ),
        .I1(\freq_cnt_o_reg_n_0_[15] ),
        .O(\freq_cnt_o_reg[14]_0 [7]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry_i_6
       (.I0(\freq_cnt_o_reg_n_0_[12] ),
        .I1(\freq_cnt_o_reg[15]_0 [3]),
        .O(\freq_cnt_o_reg[14]_0 [6]));
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_7
       (.I0(\freq_cnt_o_reg_n_0_[10] ),
        .I1(\freq_cnt_o_reg_n_0_[11] ),
        .O(\freq_cnt_o_reg[14]_0 [5]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry_i_8
       (.I0(\freq_cnt_o_reg_n_0_[9] ),
        .I1(\freq_cnt_o_reg_n_0_[8] ),
        .O(\freq_cnt_o_reg[14]_0 [4]));
  LUT2 #(
    .INIT(4'h8)) 
    i__carry_i_9
       (.I0(\freq_cnt_o_reg_n_0_[7] ),
        .I1(\freq_cnt_o_reg_n_0_[6] ),
        .O(\freq_cnt_o_reg[14]_0 [3]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 refclk_cnt0_carry
       (.CI(refclk_cnt_reg[0]),
        .CI_TOP(1'b0),
        .CO({refclk_cnt0_carry_n_0,refclk_cnt0_carry_n_1,refclk_cnt0_carry_n_2,refclk_cnt0_carry_n_3,refclk_cnt0_carry_n_4,refclk_cnt0_carry_n_5,refclk_cnt0_carry_n_6,refclk_cnt0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in__1[8:1]),
        .S(refclk_cnt_reg[8:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 refclk_cnt0_carry__0
       (.CI(refclk_cnt0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_refclk_cnt0_carry__0_CO_UNCONNECTED[7:6],refclk_cnt0_carry__0_n_2,refclk_cnt0_carry__0_n_3,refclk_cnt0_carry__0_n_4,refclk_cnt0_carry__0_n_5,refclk_cnt0_carry__0_n_6,refclk_cnt0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_refclk_cnt0_carry__0_O_UNCONNECTED[7],p_0_in__1[15:9]}),
        .S({1'b0,refclk_cnt_reg[15:9]}));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_cnt[0]_i_1 
       (.I0(refclk_cnt_reg[0]),
        .O(\refclk_cnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_cnt[15]_i_1 
       (.I0(testclk_en),
        .O(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\refclk_cnt[0]_i_1_n_0 ),
        .Q(refclk_cnt_reg[0]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[10] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[10]),
        .Q(refclk_cnt_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[11] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[11]),
        .Q(refclk_cnt_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[12] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[12]),
        .Q(refclk_cnt_reg[12]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[13] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[13]),
        .Q(refclk_cnt_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[14] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[14]),
        .Q(refclk_cnt_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[15] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[15]),
        .Q(refclk_cnt_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[1]),
        .Q(refclk_cnt_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[2]),
        .Q(refclk_cnt_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[3]),
        .Q(refclk_cnt_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[4] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[4]),
        .Q(refclk_cnt_reg[4]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[5] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[5]),
        .Q(refclk_cnt_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[6] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[6]),
        .Q(refclk_cnt_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[7] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[7]),
        .Q(refclk_cnt_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[8] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[8]),
        .Q(refclk_cnt_reg[8]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_cnt_reg[9] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(p_0_in__1[9]),
        .Q(refclk_cnt_reg[9]),
        .R(clear));
  LUT6 #(
    .INIT(64'h5555555500000111)) 
    \repeat_ctr[3]_i_1 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(\repeat_ctr_reg[3] ),
        .I2(CO),
        .I3(\repeat_ctr_reg[3]_0 ),
        .I4(\repeat_ctr[3]_i_4_n_0 ),
        .I5(Q[0]),
        .O(rst_in_out_reg));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \repeat_ctr[3]_i_4 
       (.I0(done_o_reg_0),
        .I1(Q[3]),
        .O(\repeat_ctr[3]_i_4_n_0 ));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_28 reset_synchronizer_testclk_rst_inst
       (.out(testclk_rst),
        .rst_in_out(rst_in_out),
        .txoutclkmon(txoutclkmon));
  LUT6 #(
    .INIT(64'hAA2AAAAAAAAAAAAA)) 
    \state[0]_i_1 
       (.I0(testclk_rst),
        .I1(hold_clk_reg[2]),
        .I2(hold_clk_reg[3]),
        .I3(\state[3]_i_2_n_0 ),
        .I4(hold_clk_reg[4]),
        .I5(hold_clk_reg[5]),
        .O(\state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \state[1]_i_1 
       (.I0(\state[1]_i_2_n_0 ),
        .I1(testclk_rst),
        .I2(\state[2]_i_2_n_0 ),
        .I3(testclk_en),
        .O(\state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \state[1]_i_2 
       (.I0(hold_clk_reg[5]),
        .I1(hold_clk_reg[4]),
        .I2(hold_clk_reg[1]),
        .I3(hold_clk_reg[0]),
        .I4(hold_clk_reg[3]),
        .I5(hold_clk_reg[2]),
        .O(\state[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \state[2]_i_1 
       (.I0(\state[2]_i_2_n_0 ),
        .I1(testclk_en),
        .I2(\state[2]_i_3_n_0 ),
        .I3(\state_reg_n_0_[2] ),
        .O(\state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \state[2]_i_2 
       (.I0(refclk_cnt_reg[13]),
        .I1(refclk_cnt_reg[14]),
        .I2(refclk_cnt_reg[12]),
        .I3(refclk_cnt_reg[15]),
        .I4(\state[2]_i_4_n_0 ),
        .I5(\state[2]_i_5_n_0 ),
        .O(\state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFFFFFFFFFFF)) 
    \state[2]_i_3 
       (.I0(hold_clk_reg[0]),
        .I1(hold_clk_reg[1]),
        .I2(hold_clk_reg[4]),
        .I3(hold_clk_reg[5]),
        .I4(hold_clk_reg[3]),
        .I5(hold_clk_reg[2]),
        .O(\state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \state[2]_i_4 
       (.I0(refclk_cnt_reg[0]),
        .I1(refclk_cnt_reg[1]),
        .I2(refclk_cnt_reg[2]),
        .I3(refclk_cnt_reg[3]),
        .I4(refclk_cnt_reg[4]),
        .I5(refclk_cnt_reg[5]),
        .O(\state[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFFFFFFFFFF)) 
    \state[2]_i_5 
       (.I0(refclk_cnt_reg[6]),
        .I1(refclk_cnt_reg[7]),
        .I2(refclk_cnt_reg[8]),
        .I3(refclk_cnt_reg[10]),
        .I4(refclk_cnt_reg[11]),
        .I5(refclk_cnt_reg[9]),
        .O(\state[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \state[3]_i_1 
       (.I0(\state_reg_n_0_[2] ),
        .I1(hold_clk_reg[2]),
        .I2(hold_clk_reg[3]),
        .I3(\state[3]_i_2_n_0 ),
        .I4(hold_clk_reg[4]),
        .I5(hold_clk_reg[5]),
        .O(\state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \state[3]_i_2 
       (.I0(hold_clk_reg[1]),
        .I1(hold_clk_reg[0]),
        .O(\state[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \state[4]_i_1 
       (.I0(p_1_in),
        .I1(\state_reg_n_0_[4] ),
        .O(\state[4]_i_1_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \state_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\state[0]_i_1_n_0 ),
        .PRE(\state_reg[1]_0 ),
        .Q(testclk_rst));
  FDCE #(
    .INIT(1'b0)) 
    \state_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(\state_reg[1]_0 ),
        .D(\state[1]_i_1_n_0 ),
        .Q(testclk_en));
  FDCE #(
    .INIT(1'b0)) 
    \state_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(\state_reg[1]_0 ),
        .D(\state[2]_i_1_n_0 ),
        .Q(\state_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \state_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(\state_reg[1]_0 ),
        .D(\state[3]_i_1_n_0 ),
        .Q(p_1_in));
  FDCE #(
    .INIT(1'b0)) 
    \state_reg[4] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(\state_reg[1]_0 ),
        .D(\state[4]_i_1_n_0 ),
        .Q(\state_reg_n_0_[4] ));
  LUT5 #(
    .INIT(32'h00000020)) 
    testclk_cnt0
       (.I0(testclk_en_dly2),
        .I1(testclk_div4[1]),
        .I2(testclk_div4[3]),
        .I3(testclk_div4[2]),
        .I4(testclk_div4[0]),
        .O(testclk_cnt0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[0] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[0]),
        .Q(testclk_cnt_reg));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[10] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [2]),
        .Q(D[9]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[11] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [3]),
        .Q(D[10]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[12] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [4]),
        .Q(D[11]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[13] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [5]),
        .Q(D[12]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[14] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [6]),
        .Q(D[13]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[15] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [7]),
        .Q(D[14]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[16] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[17]_0 [0]),
        .Q(D[15]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[17] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[17]_0 [1]),
        .Q(D[16]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[1] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[1]),
        .Q(D[0]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[2] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[2]),
        .Q(D[1]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[3] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[3]),
        .Q(D[2]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[4] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[4]),
        .Q(D[3]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[5] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[5]),
        .Q(D[4]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[6] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[6]),
        .Q(D[5]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[7] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(O[7]),
        .Q(D[6]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[8] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [0]),
        .Q(D[7]));
  FDCE #(
    .INIT(1'b0)) 
    \testclk_cnt_reg[9] 
       (.C(txoutclkmon),
        .CE(testclk_cnt0_n_0),
        .CLR(rst_in_out),
        .D(\testclk_cnt_reg[15]_0 [1]),
        .Q(D[8]));
  FDSE #(
    .INIT(1'b1)) 
    \testclk_div4_reg[0] 
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_div4[3]),
        .Q(testclk_div4[0]),
        .S(tstclk_rst_dly2));
  FDRE #(
    .INIT(1'b0)) 
    \testclk_div4_reg[1] 
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_div4[0]),
        .Q(testclk_div4[1]),
        .R(tstclk_rst_dly2));
  FDRE #(
    .INIT(1'b0)) 
    \testclk_div4_reg[2] 
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_div4[1]),
        .Q(testclk_div4[2]),
        .R(tstclk_rst_dly2));
  FDRE #(
    .INIT(1'b0)) 
    \testclk_div4_reg[3] 
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_div4[2]),
        .Q(testclk_div4[3]),
        .R(tstclk_rst_dly2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE testclk_en_dly1_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_en),
        .Q(testclk_en_dly1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE testclk_en_dly2_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_en_dly1),
        .Q(testclk_en_dly2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE tstclk_rst_dly1_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(testclk_rst),
        .Q(tstclk_rst_dly1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDRE tstclk_rst_dly2_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(tstclk_rst_dly1),
        .Q(tstclk_rst_dly2),
        .R(1'b0));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_tx
   (\gen_gtwizard_gthe4.txprogdivreset_ch_int ,
    cpllpd_int_reg_0,
    cpllreset_int_reg_0,
    USER_CPLLLOCK_OUT_reg_0,
    cal_on_tx_drpen_out,
    cal_on_tx_drpwe_out,
    D,
    rst_in0,
    S,
    \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 ,
    \daddr_reg[7]_0 ,
    \di_reg[15]_0 ,
    in0,
    i_in_meta_reg,
    i_in_meta_reg_0,
    txoutclk_out,
    drpclk_in,
    cal_on_tx_reset_in_sync,
    O,
    \testclk_cnt_reg[15] ,
    \testclk_cnt_reg[17] ,
    Q,
    cal_on_tx_drdy,
    lopt,
    lopt_1);
  output \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  output cpllpd_int_reg_0;
  output cpllreset_int_reg_0;
  output USER_CPLLLOCK_OUT_reg_0;
  output cal_on_tx_drpen_out;
  output cal_on_tx_drpwe_out;
  output [16:0]D;
  output rst_in0;
  output [0:0]S;
  output [2:0]\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 ;
  output [6:0]\daddr_reg[7]_0 ;
  output [15:0]\di_reg[15]_0 ;
  input in0;
  input i_in_meta_reg;
  input i_in_meta_reg_0;
  input [0:0]txoutclk_out;
  input [0:0]drpclk_in;
  input cal_on_tx_reset_in_sync;
  input [7:0]O;
  input [7:0]\testclk_cnt_reg[15] ;
  input [1:0]\testclk_cnt_reg[17] ;
  input [15:0]Q;
  input cal_on_tx_drdy;
  input lopt;
  input lopt_1;

  wire [16:0]D;
  wire [7:0]O;
  wire [15:0]Q;
  wire [0:0]S;
  wire USER_CPLLLOCK_OUT_reg_0;
  wire U_TXOUTCLK_FREQ_COUNTER_n_0;
  wire U_TXOUTCLK_FREQ_COUNTER_n_18;
  wire U_TXOUTCLK_FREQ_COUNTER_n_19;
  wire U_TXOUTCLK_FREQ_COUNTER_n_23;
  wire U_TXOUTCLK_FREQ_COUNTER_n_24;
  wire U_TXOUTCLK_FREQ_COUNTER_n_25;
  wire U_TXOUTCLK_FREQ_COUNTER_n_26;
  wire U_TXOUTCLK_FREQ_COUNTER_n_27;
  wire U_TXOUTCLK_FREQ_COUNTER_n_28;
  wire U_TXOUTCLK_FREQ_COUNTER_n_29;
  wire U_TXOUTCLK_FREQ_COUNTER_n_30;
  wire U_TXOUTCLK_FREQ_COUNTER_n_31;
  wire U_TXOUTCLK_FREQ_COUNTER_n_32;
  wire U_TXOUTCLK_FREQ_COUNTER_n_33;
  wire U_TXOUTCLK_FREQ_COUNTER_n_34;
  wire U_TXOUTCLK_FREQ_COUNTER_n_35;
  wire U_TXOUTCLK_FREQ_COUNTER_n_36;
  wire U_TXOUTCLK_FREQ_COUNTER_n_37;
  wire U_TXOUTCLK_FREQ_COUNTER_n_38;
  wire U_TXOUTCLK_FREQ_COUNTER_n_39;
  wire U_TXOUTCLK_FREQ_COUNTER_n_40;
  wire U_TXOUTCLK_FREQ_COUNTER_n_41;
  wire U_TXOUTCLK_FREQ_COUNTER_n_42;
  wire U_TXOUTCLK_FREQ_COUNTER_n_43;
  wire U_TXOUTCLK_FREQ_COUNTER_n_44;
  wire U_TXOUTCLK_FREQ_COUNTER_n_45;
  wire U_TXOUTCLK_FREQ_COUNTER_n_46;
  wire U_TXOUTCLK_FREQ_COUNTER_n_47;
  wire U_TXOUTCLK_FREQ_COUNTER_n_48;
  wire U_TXOUTCLK_FREQ_COUNTER_n_49;
  wire U_TXOUTCLK_FREQ_COUNTER_n_50;
  wire U_TXOUTCLK_FREQ_COUNTER_n_51;
  wire bit_synchronizer_cplllock_inst_n_0;
  wire bit_synchronizer_cplllock_inst_n_1;
  wire bit_synchronizer_txoutclksel_inst0_n_0;
  wire bit_synchronizer_txoutclksel_inst1_n_0;
  wire bit_synchronizer_txoutclksel_inst2_n_0;
  wire bit_synchronizer_txprgdivresetdone_inst_n_0;
  wire bit_synchronizer_txprogdivreset_inst_n_0;
  wire cal_fail_store__0;
  wire cal_fail_store_i_4_n_0;
  wire cal_on_tx_drdy;
  wire cal_on_tx_drpen_out;
  wire cal_on_tx_drpwe_out;
  wire cal_on_tx_reset_in_sync;
  wire cpll_cal_state2;
  wire cpll_cal_state26_in;
  wire cpll_cal_state2_carry_n_1;
  wire cpll_cal_state2_carry_n_2;
  wire cpll_cal_state2_carry_n_3;
  wire cpll_cal_state2_carry_n_4;
  wire cpll_cal_state2_carry_n_5;
  wire cpll_cal_state2_carry_n_6;
  wire cpll_cal_state2_carry_n_7;
  wire \cpll_cal_state2_inferred__0/i__carry_n_0 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_1 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_2 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_3 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_4 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_5 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_6 ;
  wire \cpll_cal_state2_inferred__0/i__carry_n_7 ;
  wire [31:1]cpll_cal_state7_out;
  wire \cpll_cal_state[17]_i_2_n_0 ;
  wire \cpll_cal_state[17]_i_3_n_0 ;
  wire \cpll_cal_state[17]_i_4_n_0 ;
  wire \cpll_cal_state[17]_i_5_n_0 ;
  wire \cpll_cal_state[17]_i_6_n_0 ;
  wire \cpll_cal_state[17]_i_7_n_0 ;
  wire \cpll_cal_state[17]_i_8_n_0 ;
  wire \cpll_cal_state_reg_n_0_[0] ;
  wire \cpll_cal_state_reg_n_0_[12] ;
  wire \cpll_cal_state_reg_n_0_[27] ;
  wire \cpll_cal_state_reg_n_0_[28] ;
  wire \cpll_cal_state_reg_n_0_[29] ;
  wire \cpll_cal_state_reg_n_0_[30] ;
  wire \cpll_cal_state_reg_n_0_[31] ;
  wire cpllpd_int_i_1_n_0;
  wire cpllpd_int_reg_0;
  wire cpllreset_int_i_1_n_0;
  wire cpllreset_int_reg_0;
  wire [4:1]daddr0_in;
  wire \daddr[2]_i_1__0_n_0 ;
  wire \daddr[5]_i_1__0_n_0 ;
  wire \daddr[5]_i_2_n_0 ;
  wire \daddr[6]_i_1__0_n_0 ;
  wire \daddr[6]_i_2_n_0 ;
  wire \daddr[7]_i_1__0_n_0 ;
  wire \daddr[7]_i_2__0_n_0 ;
  wire [6:0]\daddr_reg[7]_0 ;
  wire den_i_1_n_0;
  wire \di_msk[0]_i_1_n_0 ;
  wire \di_msk[10]_i_1_n_0 ;
  wire \di_msk[11]_i_1_n_0 ;
  wire \di_msk[12]_i_1_n_0 ;
  wire \di_msk[12]_i_2_n_0 ;
  wire \di_msk[12]_i_3_n_0 ;
  wire \di_msk[13]_i_1_n_0 ;
  wire \di_msk[13]_i_2_n_0 ;
  wire \di_msk[14]_i_1_n_0 ;
  wire \di_msk[14]_i_2_n_0 ;
  wire \di_msk[15]_i_1_n_0 ;
  wire \di_msk[15]_i_2_n_0 ;
  wire \di_msk[15]_i_3_n_0 ;
  wire \di_msk[15]_i_4_n_0 ;
  wire \di_msk[1]_i_1_n_0 ;
  wire \di_msk[1]_i_2_n_0 ;
  wire \di_msk[2]_i_1_n_0 ;
  wire \di_msk[3]_i_1_n_0 ;
  wire \di_msk[4]_i_1_n_0 ;
  wire \di_msk[5]_i_1_n_0 ;
  wire \di_msk[5]_i_2_n_0 ;
  wire \di_msk[6]_i_1_n_0 ;
  wire \di_msk[6]_i_2_n_0 ;
  wire \di_msk[7]_i_1_n_0 ;
  wire \di_msk[8]_i_1_n_0 ;
  wire \di_msk[9]_i_1_n_0 ;
  wire \di_msk_reg_n_0_[0] ;
  wire \di_msk_reg_n_0_[10] ;
  wire \di_msk_reg_n_0_[11] ;
  wire \di_msk_reg_n_0_[12] ;
  wire \di_msk_reg_n_0_[13] ;
  wire \di_msk_reg_n_0_[14] ;
  wire \di_msk_reg_n_0_[15] ;
  wire \di_msk_reg_n_0_[1] ;
  wire \di_msk_reg_n_0_[2] ;
  wire \di_msk_reg_n_0_[3] ;
  wire \di_msk_reg_n_0_[4] ;
  wire \di_msk_reg_n_0_[5] ;
  wire \di_msk_reg_n_0_[6] ;
  wire \di_msk_reg_n_0_[7] ;
  wire \di_msk_reg_n_0_[8] ;
  wire \di_msk_reg_n_0_[9] ;
  wire [15:0]\di_reg[15]_0 ;
  wire drp_done;
  wire \drp_state[0]_i_1__0_n_0 ;
  wire \drp_state[1]_i_1__0_n_0 ;
  wire \drp_state[2]_i_1__0_n_0 ;
  wire \drp_state[3]_i_1_n_0 ;
  wire \drp_state[4]_i_1__0_n_0 ;
  wire \drp_state[5]_i_1__0_n_0 ;
  wire \drp_state[6]_i_1__0_n_0 ;
  wire \drp_state_reg_n_0_[0] ;
  wire \drp_state_reg_n_0_[1] ;
  wire \drp_state_reg_n_0_[2] ;
  wire \drp_state_reg_n_0_[3] ;
  wire \drp_state_reg_n_0_[4] ;
  wire \drp_state_reg_n_0_[5] ;
  wire [0:0]drpclk_in;
  wire dwe_i_1_n_0;
  wire freq_counter_rst_reg_n_0;
  wire \gen_gtwizard_gthe4.txprogdivreset_ch_int ;
  wire i_in_meta_reg;
  wire i_in_meta_reg_0;
  wire in0;
  wire lopt;
  wire lopt_1;
  wire mask_user_in_i_1_n_0;
  wire mask_user_in_reg_n_0;
  wire [2:0]\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 ;
  wire p_0_in;
  wire p_0_in0_in;
  wire p_0_in3_in;
  wire p_0_in7_in;
  wire p_11_in;
  wire p_12_in;
  wire p_13_in;
  wire p_14_in;
  wire p_15_in;
  wire p_16_in;
  wire p_17_in;
  wire p_18_in;
  wire p_1_in;
  wire p_1_in10_in;
  wire p_1_in2_in;
  wire p_1_in5_in;
  wire p_25_in;
  wire p_29_in;
  wire p_2_in;
  wire p_2_in1_in;
  wire p_2_in4_in;
  wire p_2_in8_in;
  wire p_3_in;
  wire p_3_in9_in;
  wire p_4_in;
  wire progclk_sel_store;
  wire \progclk_sel_store_reg_n_0_[0] ;
  wire \progclk_sel_store_reg_n_0_[10] ;
  wire \progclk_sel_store_reg_n_0_[11] ;
  wire \progclk_sel_store_reg_n_0_[12] ;
  wire \progclk_sel_store_reg_n_0_[13] ;
  wire \progclk_sel_store_reg_n_0_[14] ;
  wire \progclk_sel_store_reg_n_0_[1] ;
  wire \progclk_sel_store_reg_n_0_[2] ;
  wire \progclk_sel_store_reg_n_0_[3] ;
  wire \progclk_sel_store_reg_n_0_[4] ;
  wire \progclk_sel_store_reg_n_0_[5] ;
  wire \progclk_sel_store_reg_n_0_[6] ;
  wire \progclk_sel_store_reg_n_0_[7] ;
  wire \progclk_sel_store_reg_n_0_[8] ;
  wire \progclk_sel_store_reg_n_0_[9] ;
  wire progdiv_cfg_store;
  wire \progdiv_cfg_store[15]_i_1_n_0 ;
  wire \progdiv_cfg_store_reg_n_0_[0] ;
  wire \progdiv_cfg_store_reg_n_0_[10] ;
  wire \progdiv_cfg_store_reg_n_0_[11] ;
  wire \progdiv_cfg_store_reg_n_0_[12] ;
  wire \progdiv_cfg_store_reg_n_0_[13] ;
  wire \progdiv_cfg_store_reg_n_0_[14] ;
  wire \progdiv_cfg_store_reg_n_0_[15] ;
  wire \progdiv_cfg_store_reg_n_0_[1] ;
  wire \progdiv_cfg_store_reg_n_0_[2] ;
  wire \progdiv_cfg_store_reg_n_0_[3] ;
  wire \progdiv_cfg_store_reg_n_0_[4] ;
  wire \progdiv_cfg_store_reg_n_0_[5] ;
  wire \progdiv_cfg_store_reg_n_0_[6] ;
  wire \progdiv_cfg_store_reg_n_0_[7] ;
  wire \progdiv_cfg_store_reg_n_0_[8] ;
  wire \progdiv_cfg_store_reg_n_0_[9] ;
  wire rd;
  wire rd_i_1__0_n_0;
  wire rd_i_2_n_0;
  wire \repeat_ctr[0]_i_1_n_0 ;
  wire \repeat_ctr[1]_i_1_n_0 ;
  wire \repeat_ctr[2]_i_1_n_0 ;
  wire \repeat_ctr[3]_i_2_n_0 ;
  wire \repeat_ctr[3]_i_3_n_0 ;
  wire \repeat_ctr_reg_n_0_[0] ;
  wire \repeat_ctr_reg_n_0_[1] ;
  wire \repeat_ctr_reg_n_0_[2] ;
  wire \repeat_ctr_reg_n_0_[3] ;
  wire rst_in0;
  wire status_store__0;
  wire status_store_i_1_n_0;
  wire [7:0]\testclk_cnt_reg[15] ;
  wire [1:0]\testclk_cnt_reg[17] ;
  wire [0:0]txoutclk_out;
  wire txoutclkmon;
  wire [2:2]txoutclksel_int;
  wire \txoutclksel_int[2]_i_1_n_0 ;
  wire txprogdivreset_int;
  wire txprogdivreset_int_i_1_n_0;
  wire wait_ctr0_carry__0_n_0;
  wire wait_ctr0_carry__0_n_1;
  wire wait_ctr0_carry__0_n_10;
  wire wait_ctr0_carry__0_n_11;
  wire wait_ctr0_carry__0_n_12;
  wire wait_ctr0_carry__0_n_13;
  wire wait_ctr0_carry__0_n_14;
  wire wait_ctr0_carry__0_n_15;
  wire wait_ctr0_carry__0_n_2;
  wire wait_ctr0_carry__0_n_3;
  wire wait_ctr0_carry__0_n_4;
  wire wait_ctr0_carry__0_n_5;
  wire wait_ctr0_carry__0_n_6;
  wire wait_ctr0_carry__0_n_7;
  wire wait_ctr0_carry__0_n_8;
  wire wait_ctr0_carry__0_n_9;
  wire wait_ctr0_carry__1_n_1;
  wire wait_ctr0_carry__1_n_10;
  wire wait_ctr0_carry__1_n_11;
  wire wait_ctr0_carry__1_n_12;
  wire wait_ctr0_carry__1_n_13;
  wire wait_ctr0_carry__1_n_14;
  wire wait_ctr0_carry__1_n_15;
  wire wait_ctr0_carry__1_n_2;
  wire wait_ctr0_carry__1_n_3;
  wire wait_ctr0_carry__1_n_4;
  wire wait_ctr0_carry__1_n_5;
  wire wait_ctr0_carry__1_n_6;
  wire wait_ctr0_carry__1_n_7;
  wire wait_ctr0_carry__1_n_8;
  wire wait_ctr0_carry__1_n_9;
  wire wait_ctr0_carry_n_0;
  wire wait_ctr0_carry_n_1;
  wire wait_ctr0_carry_n_10;
  wire wait_ctr0_carry_n_11;
  wire wait_ctr0_carry_n_12;
  wire wait_ctr0_carry_n_13;
  wire wait_ctr0_carry_n_14;
  wire wait_ctr0_carry_n_15;
  wire wait_ctr0_carry_n_2;
  wire wait_ctr0_carry_n_3;
  wire wait_ctr0_carry_n_4;
  wire wait_ctr0_carry_n_5;
  wire wait_ctr0_carry_n_6;
  wire wait_ctr0_carry_n_7;
  wire wait_ctr0_carry_n_8;
  wire wait_ctr0_carry_n_9;
  wire \wait_ctr[0]_i_1_n_0 ;
  wire \wait_ctr[10]_i_1_n_0 ;
  wire \wait_ctr[11]_i_1_n_0 ;
  wire \wait_ctr[12]_i_1_n_0 ;
  wire \wait_ctr[13]_i_1_n_0 ;
  wire \wait_ctr[14]_i_1_n_0 ;
  wire \wait_ctr[15]_i_1_n_0 ;
  wire \wait_ctr[16]_i_1_n_0 ;
  wire \wait_ctr[17]_i_1_n_0 ;
  wire \wait_ctr[18]_i_1_n_0 ;
  wire \wait_ctr[19]_i_1_n_0 ;
  wire \wait_ctr[1]_i_1_n_0 ;
  wire \wait_ctr[20]_i_1_n_0 ;
  wire \wait_ctr[21]_i_1_n_0 ;
  wire \wait_ctr[22]_i_1_n_0 ;
  wire \wait_ctr[23]_i_1_n_0 ;
  wire \wait_ctr[24]_i_1_n_0 ;
  wire \wait_ctr[24]_i_2_n_0 ;
  wire \wait_ctr[2]_i_1_n_0 ;
  wire \wait_ctr[3]_i_1_n_0 ;
  wire \wait_ctr[5]_i_1_n_0 ;
  wire \wait_ctr[6]_i_1_n_0 ;
  wire \wait_ctr[9]_i_10_n_0 ;
  wire \wait_ctr[9]_i_11_n_0 ;
  wire \wait_ctr[9]_i_12_n_0 ;
  wire \wait_ctr[9]_i_13_n_0 ;
  wire \wait_ctr[9]_i_14_n_0 ;
  wire \wait_ctr[9]_i_1_n_0 ;
  wire \wait_ctr[9]_i_2_n_0 ;
  wire \wait_ctr[9]_i_3_n_0 ;
  wire \wait_ctr[9]_i_4_n_0 ;
  wire \wait_ctr[9]_i_5_n_0 ;
  wire \wait_ctr[9]_i_6_n_0 ;
  wire \wait_ctr[9]_i_7_n_0 ;
  wire \wait_ctr[9]_i_8_n_0 ;
  wire \wait_ctr[9]_i_9_n_0 ;
  wire \wait_ctr_reg_n_0_[0] ;
  wire \wait_ctr_reg_n_0_[10] ;
  wire \wait_ctr_reg_n_0_[11] ;
  wire \wait_ctr_reg_n_0_[12] ;
  wire \wait_ctr_reg_n_0_[13] ;
  wire \wait_ctr_reg_n_0_[14] ;
  wire \wait_ctr_reg_n_0_[15] ;
  wire \wait_ctr_reg_n_0_[16] ;
  wire \wait_ctr_reg_n_0_[17] ;
  wire \wait_ctr_reg_n_0_[18] ;
  wire \wait_ctr_reg_n_0_[19] ;
  wire \wait_ctr_reg_n_0_[1] ;
  wire \wait_ctr_reg_n_0_[20] ;
  wire \wait_ctr_reg_n_0_[21] ;
  wire \wait_ctr_reg_n_0_[22] ;
  wire \wait_ctr_reg_n_0_[23] ;
  wire \wait_ctr_reg_n_0_[24] ;
  wire \wait_ctr_reg_n_0_[2] ;
  wire \wait_ctr_reg_n_0_[3] ;
  wire \wait_ctr_reg_n_0_[4] ;
  wire \wait_ctr_reg_n_0_[5] ;
  wire \wait_ctr_reg_n_0_[6] ;
  wire \wait_ctr_reg_n_0_[7] ;
  wire \wait_ctr_reg_n_0_[8] ;
  wire \wait_ctr_reg_n_0_[9] ;
  wire wr;
  wire wr_i_1__0_n_0;
  wire \x0e1_store[14]_i_1_n_0 ;
  wire \x0e1_store_reg_n_0_[0] ;
  wire \x0e1_store_reg_n_0_[12] ;
  wire \x0e1_store_reg_n_0_[13] ;
  wire \x0e1_store_reg_n_0_[14] ;
  wire \x0e1_store_reg_n_0_[1] ;
  wire \x0e1_store_reg_n_0_[2] ;
  wire \x0e1_store_reg_n_0_[3] ;
  wire \x0e1_store_reg_n_0_[4] ;
  wire \x0e1_store_reg_n_0_[5] ;
  wire \x0e1_store_reg_n_0_[6] ;
  wire \x0e1_store_reg_n_0_[7] ;
  wire \x0e1_store_reg_n_0_[8] ;
  wire \x0e1_store_reg_n_0_[9] ;
  wire [7:0]NLW_cpll_cal_state2_carry_O_UNCONNECTED;
  wire [7:0]\NLW_cpll_cal_state2_inferred__0/i__carry_O_UNCONNECTED ;
  wire [7:1]\NLW_cpll_cal_state2_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [7:0]\NLW_cpll_cal_state2_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [7:7]NLW_wait_ctr0_carry__1_CO_UNCONNECTED;

  FDRE USER_CPLLLOCK_OUT_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_cplllock_inst_n_1),
        .Q(USER_CPLLLOCK_OUT_reg_0),
        .R(1'b0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_cpll_cal_freq_counter U_TXOUTCLK_FREQ_COUNTER
       (.CO(cpll_cal_state2),
        .D(D),
        .DI({U_TXOUTCLK_FREQ_COUNTER_n_23,U_TXOUTCLK_FREQ_COUNTER_n_24,U_TXOUTCLK_FREQ_COUNTER_n_25,U_TXOUTCLK_FREQ_COUNTER_n_26,U_TXOUTCLK_FREQ_COUNTER_n_27}),
        .O(O),
        .Q({\cpll_cal_state_reg_n_0_[27] ,p_2_in8_in,p_11_in,p_18_in,\cpll_cal_state_reg_n_0_[12] ,\cpll_cal_state_reg_n_0_[0] }),
        .S(S),
        .cal_fail_store__0(cal_fail_store__0),
        .cal_fail_store_reg(bit_synchronizer_cplllock_inst_n_0),
        .cal_fail_store_reg_0(\repeat_ctr_reg_n_0_[3] ),
        .cal_fail_store_reg_1(cal_fail_store_i_4_n_0),
        .cal_on_tx_reset_in_sync(cal_on_tx_reset_in_sync),
        .\cpll_cal_state_reg[13] (\wait_ctr[9]_i_4_n_0 ),
        .\cpll_cal_state_reg[13]_0 (\repeat_ctr_reg_n_0_[1] ),
        .\cpll_cal_state_reg[13]_1 (\repeat_ctr_reg_n_0_[0] ),
        .\cpll_cal_state_reg[13]_2 (\repeat_ctr_reg_n_0_[2] ),
        .\cpll_cal_state_reg[21] (drp_done),
        .done_o_reg_0(U_TXOUTCLK_FREQ_COUNTER_n_0),
        .done_o_reg_1({cpll_cal_state7_out[21],cpll_cal_state7_out[13]}),
        .drpclk_in(drpclk_in),
        .\freq_cnt_o_reg[0]_0 (U_TXOUTCLK_FREQ_COUNTER_n_49),
        .\freq_cnt_o_reg[14]_0 ({U_TXOUTCLK_FREQ_COUNTER_n_41,U_TXOUTCLK_FREQ_COUNTER_n_42,U_TXOUTCLK_FREQ_COUNTER_n_43,U_TXOUTCLK_FREQ_COUNTER_n_44,U_TXOUTCLK_FREQ_COUNTER_n_45,U_TXOUTCLK_FREQ_COUNTER_n_46,U_TXOUTCLK_FREQ_COUNTER_n_47,U_TXOUTCLK_FREQ_COUNTER_n_48}),
        .\freq_cnt_o_reg[15]_0 ({U_TXOUTCLK_FREQ_COUNTER_n_36,U_TXOUTCLK_FREQ_COUNTER_n_37,U_TXOUTCLK_FREQ_COUNTER_n_38,U_TXOUTCLK_FREQ_COUNTER_n_39,U_TXOUTCLK_FREQ_COUNTER_n_40}),
        .\freq_cnt_o_reg[16]_0 ({U_TXOUTCLK_FREQ_COUNTER_n_28,U_TXOUTCLK_FREQ_COUNTER_n_29,U_TXOUTCLK_FREQ_COUNTER_n_30,U_TXOUTCLK_FREQ_COUNTER_n_31,U_TXOUTCLK_FREQ_COUNTER_n_32,U_TXOUTCLK_FREQ_COUNTER_n_33,U_TXOUTCLK_FREQ_COUNTER_n_34,U_TXOUTCLK_FREQ_COUNTER_n_35}),
        .\freq_cnt_o_reg[16]_1 (U_TXOUTCLK_FREQ_COUNTER_n_51),
        .\freq_cnt_o_reg[17]_0 (U_TXOUTCLK_FREQ_COUNTER_n_50),
        .\repeat_ctr_reg[3] (\repeat_ctr[3]_i_3_n_0 ),
        .\repeat_ctr_reg[3]_0 (cpll_cal_state26_in),
        .rst_in_out_reg(U_TXOUTCLK_FREQ_COUNTER_n_18),
        .rst_in_out_reg_0(U_TXOUTCLK_FREQ_COUNTER_n_19),
        .\state_reg[1]_0 (freq_counter_rst_reg_n_0),
        .\testclk_cnt_reg[15]_0 (\testclk_cnt_reg[15] ),
        .\testclk_cnt_reg[17]_0 (\testclk_cnt_reg[17] ),
        .txoutclkmon(txoutclkmon));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_22 bit_synchronizer_cplllock_inst
       (.Q({\cpll_cal_state_reg_n_0_[30] ,\cpll_cal_state_reg_n_0_[0] }),
        .USER_CPLLLOCK_OUT_reg(mask_user_in_reg_n_0),
        .cal_on_tx_reset_in_sync(cal_on_tx_reset_in_sync),
        .\cpll_cal_state_reg[0] (bit_synchronizer_cplllock_inst_n_1),
        .drpclk_in(drpclk_in),
        .i_in_out_reg_0(bit_synchronizer_cplllock_inst_n_0),
        .in0(in0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_23 bit_synchronizer_txoutclksel_inst0
       (.D(bit_synchronizer_txoutclksel_inst0_n_0),
        .drpclk_in(drpclk_in),
        .\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] (mask_user_in_reg_n_0),
        .txoutclksel_int(txoutclksel_int));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_24 bit_synchronizer_txoutclksel_inst1
       (.D(bit_synchronizer_txoutclksel_inst1_n_0),
        .drpclk_in(drpclk_in),
        .\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] (mask_user_in_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_25 bit_synchronizer_txoutclksel_inst2
       (.D(bit_synchronizer_txoutclksel_inst2_n_0),
        .drpclk_in(drpclk_in),
        .\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] (mask_user_in_reg_n_0),
        .txoutclksel_int(txoutclksel_int));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_26 bit_synchronizer_txprgdivresetdone_inst
       (.D({cpll_cal_state7_out[31:29],cpll_cal_state7_out[20:19]}),
        .Q({\cpll_cal_state_reg_n_0_[31] ,\cpll_cal_state_reg_n_0_[30] ,\cpll_cal_state_reg_n_0_[29] ,\cpll_cal_state_reg_n_0_[28] ,p_11_in,p_12_in,p_13_in,p_16_in,p_17_in}),
        .cal_fail_store__0(cal_fail_store__0),
        .cal_on_tx_reset_in_sync(cal_on_tx_reset_in_sync),
        .\cpll_cal_state_reg[14] (bit_synchronizer_txprgdivresetdone_inst_n_0),
        .\cpll_cal_state_reg[19] (\wait_ctr[9]_i_4_n_0 ),
        .\cpll_cal_state_reg[20] (U_TXOUTCLK_FREQ_COUNTER_n_0),
        .drpclk_in(drpclk_in),
        .freq_counter_rst_reg(\wait_ctr[9]_i_7_n_0 ),
        .freq_counter_rst_reg_0(freq_counter_rst_reg_n_0),
        .freq_counter_rst_reg_1(\cpll_cal_state[17]_i_2_n_0 ),
        .i_in_meta_reg_0(i_in_meta_reg_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_27 bit_synchronizer_txprogdivreset_inst
       (.drpclk_in(drpclk_in),
        .i_in_meta_reg_0(i_in_meta_reg),
        .\non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg (mask_user_in_reg_n_0),
        .txprogdivreset_int(txprogdivreset_int),
        .txprogdivreset_int_reg(bit_synchronizer_txprogdivreset_inst_n_0));
  (* OPT_MODIFIED = "MLO" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFG_GT #(
    .SIM_DEVICE("ULTRASCALE"),
    .STARTUP_SYNC("FALSE")) 
    bufg_gt_txoutclkmon_inst
       (.CE(lopt),
        .CEMASK(1'b1),
        .CLR(lopt_1),
        .CLRMASK(1'b1),
        .DIV({1'b0,1'b0,1'b0}),
        .I(txoutclk_out),
        .O(txoutclkmon));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    cal_fail_store_i_4
       (.I0(\repeat_ctr_reg_n_0_[1] ),
        .I1(\repeat_ctr_reg_n_0_[0] ),
        .I2(\repeat_ctr_reg_n_0_[2] ),
        .O(cal_fail_store_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cal_fail_store_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(U_TXOUTCLK_FREQ_COUNTER_n_19),
        .Q(cal_fail_store__0),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 cpll_cal_state2_carry
       (.CI(U_TXOUTCLK_FREQ_COUNTER_n_49),
        .CI_TOP(1'b0),
        .CO({cpll_cal_state2,cpll_cal_state2_carry_n_1,cpll_cal_state2_carry_n_2,cpll_cal_state2_carry_n_3,cpll_cal_state2_carry_n_4,cpll_cal_state2_carry_n_5,cpll_cal_state2_carry_n_6,cpll_cal_state2_carry_n_7}),
        .DI({1'b0,1'b0,U_TXOUTCLK_FREQ_COUNTER_n_23,U_TXOUTCLK_FREQ_COUNTER_n_24,U_TXOUTCLK_FREQ_COUNTER_n_25,U_TXOUTCLK_FREQ_COUNTER_n_26,U_TXOUTCLK_FREQ_COUNTER_n_27,1'b0}),
        .O(NLW_cpll_cal_state2_carry_O_UNCONNECTED[7:0]),
        .S({U_TXOUTCLK_FREQ_COUNTER_n_28,U_TXOUTCLK_FREQ_COUNTER_n_29,U_TXOUTCLK_FREQ_COUNTER_n_30,U_TXOUTCLK_FREQ_COUNTER_n_31,U_TXOUTCLK_FREQ_COUNTER_n_32,U_TXOUTCLK_FREQ_COUNTER_n_33,U_TXOUTCLK_FREQ_COUNTER_n_34,U_TXOUTCLK_FREQ_COUNTER_n_35}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 \cpll_cal_state2_inferred__0/i__carry 
       (.CI(1'b1),
        .CI_TOP(1'b0),
        .CO({\cpll_cal_state2_inferred__0/i__carry_n_0 ,\cpll_cal_state2_inferred__0/i__carry_n_1 ,\cpll_cal_state2_inferred__0/i__carry_n_2 ,\cpll_cal_state2_inferred__0/i__carry_n_3 ,\cpll_cal_state2_inferred__0/i__carry_n_4 ,\cpll_cal_state2_inferred__0/i__carry_n_5 ,\cpll_cal_state2_inferred__0/i__carry_n_6 ,\cpll_cal_state2_inferred__0/i__carry_n_7 }),
        .DI({U_TXOUTCLK_FREQ_COUNTER_n_36,U_TXOUTCLK_FREQ_COUNTER_n_37,1'b0,U_TXOUTCLK_FREQ_COUNTER_n_38,1'b0,1'b0,U_TXOUTCLK_FREQ_COUNTER_n_39,U_TXOUTCLK_FREQ_COUNTER_n_40}),
        .O(\NLW_cpll_cal_state2_inferred__0/i__carry_O_UNCONNECTED [7:0]),
        .S({U_TXOUTCLK_FREQ_COUNTER_n_41,U_TXOUTCLK_FREQ_COUNTER_n_42,U_TXOUTCLK_FREQ_COUNTER_n_43,U_TXOUTCLK_FREQ_COUNTER_n_44,U_TXOUTCLK_FREQ_COUNTER_n_45,U_TXOUTCLK_FREQ_COUNTER_n_46,U_TXOUTCLK_FREQ_COUNTER_n_47,U_TXOUTCLK_FREQ_COUNTER_n_48}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY8 \cpll_cal_state2_inferred__0/i__carry__0 
       (.CI(\cpll_cal_state2_inferred__0/i__carry_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_cpll_cal_state2_inferred__0/i__carry__0_CO_UNCONNECTED [7:1],cpll_cal_state26_in}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,U_TXOUTCLK_FREQ_COUNTER_n_50}),
        .O(\NLW_cpll_cal_state2_inferred__0/i__carry__0_O_UNCONNECTED [7:0]),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,U_TXOUTCLK_FREQ_COUNTER_n_51}));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cpll_cal_state[12]_i_1 
       (.I0(drp_done),
        .I1(p_0_in),
        .O(cpll_cal_state7_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h8F88)) 
    \cpll_cal_state[14]_i_1 
       (.I0(p_17_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_4_n_0 ),
        .I3(p_18_in),
        .O(cpll_cal_state7_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT4 #(
    .INIT(16'hF222)) 
    \cpll_cal_state[15]_i_1 
       (.I0(p_17_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_7_n_0 ),
        .I3(p_16_in),
        .O(cpll_cal_state7_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'h8F88)) 
    \cpll_cal_state[16]_i_1 
       (.I0(p_15_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_7_n_0 ),
        .I3(p_16_in),
        .O(cpll_cal_state7_out[16]));
  LUT4 #(
    .INIT(16'hF222)) 
    \cpll_cal_state[17]_i_1 
       (.I0(p_15_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_7_n_0 ),
        .I3(p_14_in),
        .O(cpll_cal_state7_out[17]));
  LUT6 #(
    .INIT(64'h0100010001000101)) 
    \cpll_cal_state[17]_i_2 
       (.I0(\cpll_cal_state[17]_i_3_n_0 ),
        .I1(\cpll_cal_state[17]_i_4_n_0 ),
        .I2(\cpll_cal_state[17]_i_5_n_0 ),
        .I3(\cpll_cal_state[17]_i_6_n_0 ),
        .I4(\cpll_cal_state[17]_i_7_n_0 ),
        .I5(\cpll_cal_state[17]_i_8_n_0 ),
        .O(\cpll_cal_state[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cpll_cal_state[17]_i_3 
       (.I0(\wait_ctr_reg_n_0_[13] ),
        .I1(\wait_ctr_reg_n_0_[14] ),
        .I2(\wait_ctr_reg_n_0_[15] ),
        .I3(\wait_ctr_reg_n_0_[16] ),
        .O(\cpll_cal_state[17]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cpll_cal_state[17]_i_4 
       (.I0(\wait_ctr_reg_n_0_[23] ),
        .I1(\wait_ctr_reg_n_0_[24] ),
        .I2(\wait_ctr_reg_n_0_[21] ),
        .I3(\wait_ctr_reg_n_0_[22] ),
        .O(\cpll_cal_state[17]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cpll_cal_state[17]_i_5 
       (.I0(\wait_ctr_reg_n_0_[17] ),
        .I1(\wait_ctr_reg_n_0_[18] ),
        .I2(\wait_ctr_reg_n_0_[19] ),
        .I3(\wait_ctr_reg_n_0_[20] ),
        .O(\cpll_cal_state[17]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cpll_cal_state[17]_i_6 
       (.I0(\wait_ctr_reg_n_0_[12] ),
        .I1(\wait_ctr_reg_n_0_[11] ),
        .O(\cpll_cal_state[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hF000E000F0000000)) 
    \cpll_cal_state[17]_i_7 
       (.I0(\wait_ctr_reg_n_0_[2] ),
        .I1(\wait_ctr_reg_n_0_[1] ),
        .I2(\wait_ctr_reg_n_0_[6] ),
        .I3(\wait_ctr_reg_n_0_[5] ),
        .I4(\wait_ctr_reg_n_0_[4] ),
        .I5(\wait_ctr_reg_n_0_[3] ),
        .O(\cpll_cal_state[17]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cpll_cal_state[17]_i_8 
       (.I0(\wait_ctr_reg_n_0_[10] ),
        .I1(\wait_ctr_reg_n_0_[8] ),
        .I2(\wait_ctr_reg_n_0_[7] ),
        .I3(\wait_ctr_reg_n_0_[9] ),
        .O(\cpll_cal_state[17]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT4 #(
    .INIT(16'h8F88)) 
    \cpll_cal_state[18]_i_1 
       (.I0(\wait_ctr[9]_i_4_n_0 ),
        .I1(p_13_in),
        .I2(\wait_ctr[9]_i_7_n_0 ),
        .I3(p_14_in),
        .O(cpll_cal_state7_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \cpll_cal_state[1]_i_1 
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(drp_done),
        .I2(p_1_in10_in),
        .O(cpll_cal_state7_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cpll_cal_state[27]_i_1 
       (.I0(drp_done),
        .I1(p_3_in),
        .O(cpll_cal_state7_out[27]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \cpll_cal_state[28]_i_1 
       (.I0(\cpll_cal_state_reg_n_0_[27] ),
        .I1(\wait_ctr[9]_i_4_n_0 ),
        .I2(\cpll_cal_state_reg_n_0_[28] ),
        .O(cpll_cal_state7_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cpll_cal_state[2]_i_1 
       (.I0(p_1_in10_in),
        .I1(drp_done),
        .O(cpll_cal_state7_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \cpll_cal_state[3]_i_1 
       (.I0(drp_done),
        .I1(p_1_in2_in),
        .I2(status_store__0),
        .I3(p_29_in),
        .O(cpll_cal_state7_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'hFFF888F8)) 
    \cpll_cal_state[5]_i_1 
       (.I0(p_29_in),
        .I1(status_store__0),
        .I2(p_1_in5_in),
        .I3(drp_done),
        .I4(p_0_in7_in),
        .O(cpll_cal_state7_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cpll_cal_state[6]_i_1 
       (.I0(drp_done),
        .I1(p_1_in5_in),
        .O(cpll_cal_state7_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \cpll_cal_state[7]_i_1 
       (.I0(drp_done),
        .I1(p_1_in),
        .I2(status_store__0),
        .I3(p_25_in),
        .O(cpll_cal_state7_out[7]));
  LUT5 #(
    .INIT(32'hFFE2E2E2)) 
    \cpll_cal_state[9]_i_1 
       (.I0(p_4_in),
        .I1(drp_done),
        .I2(p_0_in3_in),
        .I3(status_store__0),
        .I4(p_25_in),
        .O(cpll_cal_state7_out[9]));
  FDSE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .Q(\cpll_cal_state_reg_n_0_[0] ),
        .S(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[10] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_4_in),
        .Q(p_0_in0_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[11] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_0_in0_in),
        .Q(p_0_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[12] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[12]),
        .Q(\cpll_cal_state_reg_n_0_[12] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[13] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[13]),
        .Q(p_18_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[14] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[14]),
        .Q(p_17_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[15] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[15]),
        .Q(p_16_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[16] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[16]),
        .Q(p_15_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[17] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[17]),
        .Q(p_14_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[18] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[18]),
        .Q(p_13_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[19] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[19]),
        .Q(p_12_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[1]),
        .Q(p_1_in10_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[20] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[20]),
        .Q(p_11_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[21] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[21]),
        .Q(p_2_in8_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[22] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_2_in8_in),
        .Q(p_2_in4_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[23] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_2_in4_in),
        .Q(p_2_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[24] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_2_in),
        .Q(p_2_in1_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[25] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_2_in1_in),
        .Q(p_3_in9_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[26] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_3_in9_in),
        .Q(p_3_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[27] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[27]),
        .Q(\cpll_cal_state_reg_n_0_[27] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[28] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[28]),
        .Q(\cpll_cal_state_reg_n_0_[28] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[29] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[29]),
        .Q(\cpll_cal_state_reg_n_0_[29] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[2]),
        .Q(p_29_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[30] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[30]),
        .Q(\cpll_cal_state_reg_n_0_[30] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[31] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[31]),
        .Q(\cpll_cal_state_reg_n_0_[31] ),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[3]),
        .Q(p_1_in2_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[4] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_1_in2_in),
        .Q(p_0_in7_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[5] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[5]),
        .Q(p_1_in5_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[6] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[6]),
        .Q(p_25_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[7] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[7]),
        .Q(p_1_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[8] 
       (.C(drpclk_in),
        .CE(drp_done),
        .D(p_1_in),
        .Q(p_0_in3_in),
        .R(cal_on_tx_reset_in_sync));
  FDRE #(
    .INIT(1'b0)) 
    \cpll_cal_state_reg[9] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpll_cal_state7_out[9]),
        .Q(p_4_in),
        .R(cal_on_tx_reset_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'hFFDD2F00)) 
    cpllpd_int_i_1
       (.I0(p_17_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_4_n_0 ),
        .I3(p_18_in),
        .I4(cpllpd_int_reg_0),
        .O(cpllpd_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cpllpd_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpllpd_int_i_1_n_0),
        .Q(cpllpd_int_reg_0),
        .R(cal_on_tx_reset_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'hFFDD2F00)) 
    cpllreset_int_i_1
       (.I0(p_15_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(\wait_ctr[9]_i_7_n_0 ),
        .I3(p_16_in),
        .I4(cpllreset_int_reg_0),
        .O(cpllreset_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    cpllreset_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(cpllreset_int_i_1_n_0),
        .Q(cpllreset_int_reg_0),
        .R(cal_on_tx_reset_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \daddr[1]_i_1__0 
       (.I0(daddr0_in[3]),
        .I1(\daddr[5]_i_2_n_0 ),
        .I2(p_1_in2_in),
        .I3(p_2_in1_in),
        .I4(p_0_in0_in),
        .O(daddr0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \daddr[2]_i_1__0 
       (.I0(daddr0_in[3]),
        .I1(p_1_in5_in),
        .I2(p_2_in4_in),
        .I3(p_0_in3_in),
        .I4(p_3_in),
        .O(\daddr[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \daddr[3]_i_1__0 
       (.I0(p_4_in),
        .I1(p_2_in8_in),
        .I2(p_3_in9_in),
        .I3(p_1_in10_in),
        .I4(p_0_in7_in),
        .I5(\cpll_cal_state_reg_n_0_[0] ),
        .O(daddr0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h0002AAAA)) 
    \daddr[4]_i_1__0 
       (.I0(daddr0_in[3]),
        .I1(p_1_in2_in),
        .I2(p_2_in1_in),
        .I3(p_0_in0_in),
        .I4(\daddr[5]_i_2_n_0 ),
        .O(daddr0_in[4]));
  LUT6 #(
    .INIT(64'h1111111555555555)) 
    \daddr[5]_i_1__0 
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(\daddr[5]_i_2_n_0 ),
        .I2(p_0_in0_in),
        .I3(p_2_in1_in),
        .I4(p_1_in2_in),
        .I5(\daddr[6]_i_2_n_0 ),
        .O(\daddr[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \daddr[5]_i_2 
       (.I0(p_3_in),
        .I1(p_0_in3_in),
        .I2(p_2_in4_in),
        .I3(p_1_in5_in),
        .O(\daddr[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5555555455555555)) 
    \daddr[6]_i_1__0 
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(p_3_in),
        .I2(p_0_in3_in),
        .I3(p_2_in4_in),
        .I4(p_1_in5_in),
        .I5(\daddr[6]_i_2_n_0 ),
        .O(\daddr[6]_i_1__0_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \daddr[6]_i_2 
       (.I0(p_0_in7_in),
        .I1(p_1_in10_in),
        .I2(p_3_in9_in),
        .I3(p_2_in8_in),
        .I4(p_4_in),
        .O(\daddr[6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \daddr[7]_i_1__0 
       (.I0(p_0_in),
        .I1(p_2_in),
        .I2(p_1_in),
        .I3(daddr0_in[1]),
        .O(\daddr[7]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h5555555555555554)) 
    \daddr[7]_i_2__0 
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(p_4_in),
        .I2(p_2_in8_in),
        .I3(p_3_in9_in),
        .I4(p_1_in10_in),
        .I5(p_0_in7_in),
        .O(\daddr[7]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[1] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(daddr0_in[1]),
        .Q(\daddr_reg[7]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[2] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(\daddr[2]_i_1__0_n_0 ),
        .Q(\daddr_reg[7]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[3] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(daddr0_in[3]),
        .Q(\daddr_reg[7]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[4] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(daddr0_in[4]),
        .Q(\daddr_reg[7]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[5] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(\daddr[5]_i_1__0_n_0 ),
        .Q(\daddr_reg[7]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[6] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(\daddr[6]_i_1__0_n_0 ),
        .Q(\daddr_reg[7]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \daddr_reg[7] 
       (.C(drpclk_in),
        .CE(\daddr[7]_i_1__0_n_0 ),
        .D(\daddr[7]_i_2__0_n_0 ),
        .Q(\daddr_reg[7]_0 [6]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFCDFFCC)) 
    den_i_1
       (.I0(\drp_state_reg_n_0_[5] ),
        .I1(\drp_state_reg_n_0_[4] ),
        .I2(\drp_state_reg_n_0_[2] ),
        .I3(\drp_state_reg_n_0_[1] ),
        .I4(cal_on_tx_drpen_out),
        .O(den_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    den_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(den_i_1_n_0),
        .Q(cal_on_tx_drpen_out));
  LUT6 #(
    .INIT(64'hFFFF44F444F444F4)) 
    \di_msk[0]_i_1 
       (.I0(\di_msk[12]_i_2_n_0 ),
        .I1(\progclk_sel_store_reg_n_0_[0] ),
        .I2(\progdiv_cfg_store_reg_n_0_[0] ),
        .I3(\di_msk[12]_i_3_n_0 ),
        .I4(p_0_in0_in),
        .I5(\x0e1_store_reg_n_0_[0] ),
        .O(\di_msk[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFE0E0E0E0E0)) 
    \di_msk[10]_i_1 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[10] ),
        .I3(p_0_in7_in),
        .I4(p_2_in1_in),
        .I5(\progclk_sel_store_reg_n_0_[10] ),
        .O(\di_msk[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFE0E0FFE0)) 
    \di_msk[11]_i_1 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[11] ),
        .I3(\progclk_sel_store_reg_n_0_[11] ),
        .I4(\di_msk[12]_i_2_n_0 ),
        .I5(p_0_in0_in),
        .O(\di_msk[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF44F444F444F4)) 
    \di_msk[12]_i_1 
       (.I0(\di_msk[12]_i_2_n_0 ),
        .I1(\progclk_sel_store_reg_n_0_[12] ),
        .I2(\progdiv_cfg_store_reg_n_0_[12] ),
        .I3(\di_msk[12]_i_3_n_0 ),
        .I4(p_0_in0_in),
        .I5(\x0e1_store_reg_n_0_[12] ),
        .O(\di_msk[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \di_msk[12]_i_2 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .O(\di_msk[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \di_msk[12]_i_3 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .O(\di_msk[12]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFE0)) 
    \di_msk[13]_i_1 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[13] ),
        .I3(\di_msk[13]_i_2_n_0 ),
        .O(\di_msk[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0E0E0)) 
    \di_msk[13]_i_2 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .I2(\progclk_sel_store_reg_n_0_[13] ),
        .I3(p_0_in0_in),
        .I4(\x0e1_store_reg_n_0_[13] ),
        .I5(p_0_in),
        .O(\di_msk[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFE0)) 
    \di_msk[14]_i_1 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .I2(\progclk_sel_store_reg_n_0_[14] ),
        .I3(\di_msk[14]_i_2_n_0 ),
        .O(\di_msk[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0E0E0)) 
    \di_msk[14]_i_2 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[14] ),
        .I3(p_0_in0_in),
        .I4(\x0e1_store_reg_n_0_[14] ),
        .I5(p_0_in),
        .O(\di_msk[14]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h54)) 
    \di_msk[15]_i_1 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(\di_msk[15]_i_3_n_0 ),
        .I2(\cpll_cal_state_reg_n_0_[0] ),
        .O(\di_msk[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'hFFFFFFE0)) 
    \di_msk[15]_i_2 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[15] ),
        .I3(p_0_in7_in),
        .I4(p_0_in),
        .O(\di_msk[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \di_msk[15]_i_3 
       (.I0(p_0_in0_in),
        .I1(p_3_in9_in),
        .I2(p_0_in3_in),
        .I3(p_3_in),
        .I4(\di_msk[12]_i_2_n_0 ),
        .I5(\di_msk[15]_i_4_n_0 ),
        .O(\di_msk[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \di_msk[15]_i_4 
       (.I0(p_0_in),
        .I1(p_2_in),
        .O(\di_msk[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT4 #(
    .INIT(16'hFFE0)) 
    \di_msk[1]_i_1 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[1] ),
        .I3(\di_msk[1]_i_2_n_0 ),
        .O(\di_msk[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0E0E0)) 
    \di_msk[1]_i_2 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .I2(\progclk_sel_store_reg_n_0_[1] ),
        .I3(p_0_in0_in),
        .I4(\x0e1_store_reg_n_0_[1] ),
        .I5(p_0_in),
        .O(\di_msk[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF444F444FFFFF444)) 
    \di_msk[2]_i_1 
       (.I0(\di_msk[12]_i_3_n_0 ),
        .I1(\progdiv_cfg_store_reg_n_0_[2] ),
        .I2(p_0_in0_in),
        .I3(\x0e1_store_reg_n_0_[2] ),
        .I4(\progclk_sel_store_reg_n_0_[2] ),
        .I5(\di_msk[12]_i_2_n_0 ),
        .O(\di_msk[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF44F444F444F4)) 
    \di_msk[3]_i_1 
       (.I0(\di_msk[12]_i_2_n_0 ),
        .I1(\progclk_sel_store_reg_n_0_[3] ),
        .I2(\progdiv_cfg_store_reg_n_0_[3] ),
        .I3(\di_msk[12]_i_3_n_0 ),
        .I4(p_0_in0_in),
        .I5(\x0e1_store_reg_n_0_[3] ),
        .O(\di_msk[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF444F444FFFFF444)) 
    \di_msk[4]_i_1 
       (.I0(\di_msk[12]_i_3_n_0 ),
        .I1(\progdiv_cfg_store_reg_n_0_[4] ),
        .I2(p_0_in0_in),
        .I3(\x0e1_store_reg_n_0_[4] ),
        .I4(\progclk_sel_store_reg_n_0_[4] ),
        .I5(\di_msk[12]_i_2_n_0 ),
        .O(\di_msk[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFE0)) 
    \di_msk[5]_i_1 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[5] ),
        .I3(\di_msk[5]_i_2_n_0 ),
        .O(\di_msk[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0E0E0)) 
    \di_msk[5]_i_2 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .I2(\progclk_sel_store_reg_n_0_[5] ),
        .I3(p_0_in0_in),
        .I4(\x0e1_store_reg_n_0_[5] ),
        .I5(p_0_in),
        .O(\di_msk[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT4 #(
    .INIT(16'hFFE0)) 
    \di_msk[6]_i_1 
       (.I0(p_0_in7_in),
        .I1(p_2_in1_in),
        .I2(\progclk_sel_store_reg_n_0_[6] ),
        .I3(\di_msk[6]_i_2_n_0 ),
        .O(\di_msk[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE0E0E0)) 
    \di_msk[6]_i_2 
       (.I0(p_2_in),
        .I1(p_0_in3_in),
        .I2(\progdiv_cfg_store_reg_n_0_[6] ),
        .I3(p_0_in0_in),
        .I4(\x0e1_store_reg_n_0_[6] ),
        .I5(p_0_in),
        .O(\di_msk[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF444F444FFFFF444)) 
    \di_msk[7]_i_1 
       (.I0(\di_msk[12]_i_3_n_0 ),
        .I1(\progdiv_cfg_store_reg_n_0_[7] ),
        .I2(p_0_in0_in),
        .I3(\x0e1_store_reg_n_0_[7] ),
        .I4(\progclk_sel_store_reg_n_0_[7] ),
        .I5(\di_msk[12]_i_2_n_0 ),
        .O(\di_msk[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF44F444F444F4)) 
    \di_msk[8]_i_1 
       (.I0(\di_msk[12]_i_2_n_0 ),
        .I1(\progclk_sel_store_reg_n_0_[8] ),
        .I2(\progdiv_cfg_store_reg_n_0_[8] ),
        .I3(\di_msk[12]_i_3_n_0 ),
        .I4(p_0_in0_in),
        .I5(\x0e1_store_reg_n_0_[8] ),
        .O(\di_msk[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF44F444F444F4)) 
    \di_msk[9]_i_1 
       (.I0(\di_msk[12]_i_2_n_0 ),
        .I1(\progclk_sel_store_reg_n_0_[9] ),
        .I2(\progdiv_cfg_store_reg_n_0_[9] ),
        .I3(\di_msk[12]_i_3_n_0 ),
        .I4(p_0_in0_in),
        .I5(\x0e1_store_reg_n_0_[9] ),
        .O(\di_msk[9]_i_1_n_0 ));
  FDRE \di_msk_reg[0] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[0]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \di_msk_reg[10] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[10]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \di_msk_reg[11] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[11]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \di_msk_reg[12] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[12]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \di_msk_reg[13] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[13]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \di_msk_reg[14] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[14]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \di_msk_reg[15] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[15]_i_2_n_0 ),
        .Q(\di_msk_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \di_msk_reg[1] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[1]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \di_msk_reg[2] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[2]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \di_msk_reg[3] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[3]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \di_msk_reg[4] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[4]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \di_msk_reg[5] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[5]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \di_msk_reg[6] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[6]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \di_msk_reg[7] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[7]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \di_msk_reg[8] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[8]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \di_msk_reg[9] 
       (.C(drpclk_in),
        .CE(\di_msk[15]_i_1_n_0 ),
        .D(\di_msk[9]_i_1_n_0 ),
        .Q(\di_msk_reg_n_0_[9] ),
        .R(1'b0));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[0] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[0] ),
        .Q(\di_reg[15]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[10] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[10] ),
        .Q(\di_reg[15]_0 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[11] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[11] ),
        .Q(\di_reg[15]_0 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[12] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[12] ),
        .Q(\di_reg[15]_0 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[13] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[13] ),
        .Q(\di_reg[15]_0 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[14] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[14] ),
        .Q(\di_reg[15]_0 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[15] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[15] ),
        .Q(\di_reg[15]_0 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[1] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[1] ),
        .Q(\di_reg[15]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[2] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[2] ),
        .Q(\di_reg[15]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[3] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[3] ),
        .Q(\di_reg[15]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[4] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[4] ),
        .Q(\di_reg[15]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[5] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[5] ),
        .Q(\di_reg[15]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[6] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[6] ),
        .Q(\di_reg[15]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[7] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[7] ),
        .Q(\di_reg[15]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[8] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[8] ),
        .Q(\di_reg[15]_0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \di_reg[9] 
       (.C(drpclk_in),
        .CE(\drp_state_reg_n_0_[4] ),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\di_msk_reg_n_0_[9] ),
        .Q(\di_reg[15]_0 [9]));
  LUT4 #(
    .INIT(16'hAABA)) 
    \drp_state[0]_i_1__0 
       (.I0(drp_done),
        .I1(rd),
        .I2(\drp_state_reg_n_0_[0] ),
        .I3(wr),
        .O(\drp_state[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \drp_state[1]_i_1__0 
       (.I0(rd),
        .I1(\drp_state_reg_n_0_[0] ),
        .O(\drp_state[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \drp_state[2]_i_1__0 
       (.I0(\drp_state_reg_n_0_[1] ),
        .I1(cal_on_tx_drdy),
        .I2(\drp_state_reg_n_0_[2] ),
        .O(\drp_state[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \drp_state[3]_i_1 
       (.I0(\drp_state_reg_n_0_[2] ),
        .I1(cal_on_tx_drdy),
        .I2(rd),
        .O(\drp_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'hBAAA)) 
    \drp_state[4]_i_1__0 
       (.I0(\drp_state_reg_n_0_[3] ),
        .I1(rd),
        .I2(\drp_state_reg_n_0_[0] ),
        .I3(wr),
        .O(\drp_state[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \drp_state[5]_i_1__0 
       (.I0(\drp_state_reg_n_0_[4] ),
        .I1(cal_on_tx_drdy),
        .I2(\drp_state_reg_n_0_[5] ),
        .O(\drp_state[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'hA888)) 
    \drp_state[6]_i_1__0 
       (.I0(cal_on_tx_drdy),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(rd),
        .I3(\drp_state_reg_n_0_[2] ),
        .O(\drp_state[6]_i_1__0_n_0 ));
  FDPE #(
    .INIT(1'b1)) 
    \drp_state_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\drp_state[0]_i_1__0_n_0 ),
        .PRE(cal_on_tx_reset_in_sync),
        .Q(\drp_state_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[1]_i_1__0_n_0 ),
        .Q(\drp_state_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[2]_i_1__0_n_0 ),
        .Q(\drp_state_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[3] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[3]_i_1_n_0 ),
        .Q(\drp_state_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[4] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[4]_i_1__0_n_0 ),
        .Q(\drp_state_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[5] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[5]_i_1__0_n_0 ),
        .Q(\drp_state_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \drp_state_reg[6] 
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(\drp_state[6]_i_1__0_n_0 ),
        .Q(drp_done));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    dwe_i_1
       (.I0(\drp_state_reg_n_0_[4] ),
        .I1(\drp_state_reg_n_0_[5] ),
        .I2(cal_on_tx_drpwe_out),
        .O(dwe_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    dwe_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .CLR(cal_on_tx_reset_in_sync),
        .D(dwe_i_1_n_0),
        .Q(cal_on_tx_drpwe_out));
  FDRE #(
    .INIT(1'b1)) 
    freq_counter_rst_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_txprgdivresetdone_inst_n_0),
        .Q(freq_counter_rst_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEEEFEEEE)) 
    mask_user_in_i_1
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(p_1_in10_in),
        .I2(\cpll_cal_state_reg_n_0_[30] ),
        .I3(\cpll_cal_state_reg_n_0_[31] ),
        .I4(mask_user_in_reg_n_0),
        .O(mask_user_in_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mask_user_in_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(mask_user_in_i_1_n_0),
        .Q(mask_user_in_reg_n_0),
        .R(cal_on_tx_reset_in_sync));
  FDRE \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[0] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_txoutclksel_inst0_n_0),
        .Q(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 [0]),
        .R(1'b0));
  FDRE \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[1] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_txoutclksel_inst1_n_0),
        .Q(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 [1]),
        .R(1'b0));
  FDRE \non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_txoutclksel_inst2_n_0),
        .Q(\non_pcie_txoutclksel.GTHE4_TXOUTCLKSEL_OUT_reg[2]_0 [2]),
        .R(1'b0));
  FDRE \non_pcie_txoutclksel.GTHE4_TXPROGDIVRESET_OUT_reg 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_txprogdivreset_inst_n_0),
        .Q(\gen_gtwizard_gthe4.txprogdivreset_ch_int ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h4440)) 
    \progclk_sel_store[14]_i_1 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(drp_done),
        .I2(p_2_in8_in),
        .I3(p_1_in2_in),
        .O(progclk_sel_store));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[0] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[0]),
        .Q(\progclk_sel_store_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[10] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[10]),
        .Q(\progclk_sel_store_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[11] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[11]),
        .Q(\progclk_sel_store_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[12] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[12]),
        .Q(\progclk_sel_store_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[13] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[13]),
        .Q(\progclk_sel_store_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[14] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[14]),
        .Q(\progclk_sel_store_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[1] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[1]),
        .Q(\progclk_sel_store_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[2] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[2]),
        .Q(\progclk_sel_store_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[3] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[3]),
        .Q(\progclk_sel_store_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[4] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[4]),
        .Q(\progclk_sel_store_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[5] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[5]),
        .Q(\progclk_sel_store_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[6] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[6]),
        .Q(\progclk_sel_store_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[7] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[7]),
        .Q(\progclk_sel_store_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[8] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[8]),
        .Q(\progclk_sel_store_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progclk_sel_store_reg[9] 
       (.C(drpclk_in),
        .CE(progclk_sel_store),
        .D(Q[9]),
        .Q(\progclk_sel_store_reg_n_0_[9] ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h4440)) 
    \progdiv_cfg_store[14]_i_1 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(drp_done),
        .I2(p_2_in4_in),
        .I3(p_1_in),
        .O(progdiv_cfg_store));
  LUT6 #(
    .INIT(64'hFFFFEFFF30302000)) 
    \progdiv_cfg_store[15]_i_1 
       (.I0(Q[15]),
        .I1(cal_on_tx_reset_in_sync),
        .I2(drp_done),
        .I3(p_2_in4_in),
        .I4(p_1_in),
        .I5(\progdiv_cfg_store_reg_n_0_[15] ),
        .O(\progdiv_cfg_store[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[0] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[0]),
        .Q(\progdiv_cfg_store_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[10] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[10]),
        .Q(\progdiv_cfg_store_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[11] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[11]),
        .Q(\progdiv_cfg_store_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[12] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[12]),
        .Q(\progdiv_cfg_store_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[13] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[13]),
        .Q(\progdiv_cfg_store_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[14] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[14]),
        .Q(\progdiv_cfg_store_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[15] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\progdiv_cfg_store[15]_i_1_n_0 ),
        .Q(\progdiv_cfg_store_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[1] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[1]),
        .Q(\progdiv_cfg_store_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[2] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[2]),
        .Q(\progdiv_cfg_store_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[3] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[3]),
        .Q(\progdiv_cfg_store_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[4] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[4]),
        .Q(\progdiv_cfg_store_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[5] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[5]),
        .Q(\progdiv_cfg_store_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[6] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[6]),
        .Q(\progdiv_cfg_store_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[7] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[7]),
        .Q(\progdiv_cfg_store_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[8] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[8]),
        .Q(\progdiv_cfg_store_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \progdiv_cfg_store_reg[9] 
       (.C(drpclk_in),
        .CE(progdiv_cfg_store),
        .D(Q[9]),
        .Q(\progdiv_cfg_store_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h5555555755555554)) 
    rd_i_1__0
       (.I0(drp_done),
        .I1(p_1_in),
        .I2(p_2_in4_in),
        .I3(p_4_in),
        .I4(rd_i_2_n_0),
        .I5(rd),
        .O(rd_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    rd_i_2
       (.I0(p_1_in5_in),
        .I1(p_1_in10_in),
        .I2(p_2_in8_in),
        .I3(p_1_in2_in),
        .O(rd_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rd_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rd_i_1__0_n_0),
        .Q(rd),
        .R(cal_on_tx_reset_in_sync));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \repeat_ctr[0]_i_1 
       (.I0(p_11_in),
        .I1(\repeat_ctr_reg_n_0_[0] ),
        .O(\repeat_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \repeat_ctr[1]_i_1 
       (.I0(p_11_in),
        .I1(\repeat_ctr_reg_n_0_[0] ),
        .I2(\repeat_ctr_reg_n_0_[1] ),
        .O(\repeat_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'h2888)) 
    \repeat_ctr[2]_i_1 
       (.I0(p_11_in),
        .I1(\repeat_ctr_reg_n_0_[2] ),
        .I2(\repeat_ctr_reg_n_0_[1] ),
        .I3(\repeat_ctr_reg_n_0_[0] ),
        .O(\repeat_ctr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'h28888888)) 
    \repeat_ctr[3]_i_2 
       (.I0(p_11_in),
        .I1(\repeat_ctr_reg_n_0_[3] ),
        .I2(\repeat_ctr_reg_n_0_[2] ),
        .I3(\repeat_ctr_reg_n_0_[0] ),
        .I4(\repeat_ctr_reg_n_0_[1] ),
        .O(\repeat_ctr[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \repeat_ctr[3]_i_3 
       (.I0(\repeat_ctr_reg_n_0_[3] ),
        .I1(\repeat_ctr_reg_n_0_[2] ),
        .I2(\repeat_ctr_reg_n_0_[0] ),
        .I3(\repeat_ctr_reg_n_0_[1] ),
        .O(\repeat_ctr[3]_i_3_n_0 ));
  FDRE \repeat_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(U_TXOUTCLK_FREQ_COUNTER_n_18),
        .D(\repeat_ctr[0]_i_1_n_0 ),
        .Q(\repeat_ctr_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \repeat_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(U_TXOUTCLK_FREQ_COUNTER_n_18),
        .D(\repeat_ctr[1]_i_1_n_0 ),
        .Q(\repeat_ctr_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \repeat_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(U_TXOUTCLK_FREQ_COUNTER_n_18),
        .D(\repeat_ctr[2]_i_1_n_0 ),
        .Q(\repeat_ctr_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \repeat_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(U_TXOUTCLK_FREQ_COUNTER_n_18),
        .D(\repeat_ctr[3]_i_2_n_0 ),
        .Q(\repeat_ctr_reg_n_0_[3] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_meta_i_1__3
       (.I0(USER_CPLLLOCK_OUT_reg_0),
        .O(rst_in0));
  LUT6 #(
    .INIT(64'hFFABFFFF00A80000)) 
    status_store_i_1
       (.I0(Q[15]),
        .I1(p_1_in5_in),
        .I2(p_1_in10_in),
        .I3(cal_on_tx_reset_in_sync),
        .I4(drp_done),
        .I5(status_store__0),
        .O(status_store_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    status_store_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(status_store_i_1_n_0),
        .Q(status_store__0),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h0E)) 
    \txoutclksel_int[2]_i_1 
       (.I0(txoutclksel_int),
        .I1(\cpll_cal_state_reg_n_0_[12] ),
        .I2(\cpll_cal_state_reg_n_0_[0] ),
        .O(\txoutclksel_int[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \txoutclksel_int_reg[2] 
       (.C(drpclk_in),
        .CE(1'b1),
        .D(\txoutclksel_int[2]_i_1_n_0 ),
        .Q(txoutclksel_int),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hABA8)) 
    txprogdivreset_int_i_1
       (.I0(\wait_ctr[9]_i_4_n_0 ),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(txprogdivreset_int),
        .O(txprogdivreset_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    txprogdivreset_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(txprogdivreset_int_i_1_n_0),
        .Q(txprogdivreset_int),
        .R(cal_on_tx_reset_in_sync));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 wait_ctr0_carry
       (.CI(\wait_ctr_reg_n_0_[0] ),
        .CI_TOP(1'b0),
        .CO({wait_ctr0_carry_n_0,wait_ctr0_carry_n_1,wait_ctr0_carry_n_2,wait_ctr0_carry_n_3,wait_ctr0_carry_n_4,wait_ctr0_carry_n_5,wait_ctr0_carry_n_6,wait_ctr0_carry_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({wait_ctr0_carry_n_8,wait_ctr0_carry_n_9,wait_ctr0_carry_n_10,wait_ctr0_carry_n_11,wait_ctr0_carry_n_12,wait_ctr0_carry_n_13,wait_ctr0_carry_n_14,wait_ctr0_carry_n_15}),
        .S({\wait_ctr_reg_n_0_[8] ,\wait_ctr_reg_n_0_[7] ,\wait_ctr_reg_n_0_[6] ,\wait_ctr_reg_n_0_[5] ,\wait_ctr_reg_n_0_[4] ,\wait_ctr_reg_n_0_[3] ,\wait_ctr_reg_n_0_[2] ,\wait_ctr_reg_n_0_[1] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 wait_ctr0_carry__0
       (.CI(wait_ctr0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({wait_ctr0_carry__0_n_0,wait_ctr0_carry__0_n_1,wait_ctr0_carry__0_n_2,wait_ctr0_carry__0_n_3,wait_ctr0_carry__0_n_4,wait_ctr0_carry__0_n_5,wait_ctr0_carry__0_n_6,wait_ctr0_carry__0_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({wait_ctr0_carry__0_n_8,wait_ctr0_carry__0_n_9,wait_ctr0_carry__0_n_10,wait_ctr0_carry__0_n_11,wait_ctr0_carry__0_n_12,wait_ctr0_carry__0_n_13,wait_ctr0_carry__0_n_14,wait_ctr0_carry__0_n_15}),
        .S({\wait_ctr_reg_n_0_[16] ,\wait_ctr_reg_n_0_[15] ,\wait_ctr_reg_n_0_[14] ,\wait_ctr_reg_n_0_[13] ,\wait_ctr_reg_n_0_[12] ,\wait_ctr_reg_n_0_[11] ,\wait_ctr_reg_n_0_[10] ,\wait_ctr_reg_n_0_[9] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY8 wait_ctr0_carry__1
       (.CI(wait_ctr0_carry__0_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_wait_ctr0_carry__1_CO_UNCONNECTED[7],wait_ctr0_carry__1_n_1,wait_ctr0_carry__1_n_2,wait_ctr0_carry__1_n_3,wait_ctr0_carry__1_n_4,wait_ctr0_carry__1_n_5,wait_ctr0_carry__1_n_6,wait_ctr0_carry__1_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({wait_ctr0_carry__1_n_8,wait_ctr0_carry__1_n_9,wait_ctr0_carry__1_n_10,wait_ctr0_carry__1_n_11,wait_ctr0_carry__1_n_12,wait_ctr0_carry__1_n_13,wait_ctr0_carry__1_n_14,wait_ctr0_carry__1_n_15}),
        .S({\wait_ctr_reg_n_0_[24] ,\wait_ctr_reg_n_0_[23] ,\wait_ctr_reg_n_0_[22] ,\wait_ctr_reg_n_0_[21] ,\wait_ctr_reg_n_0_[20] ,\wait_ctr_reg_n_0_[19] ,\wait_ctr_reg_n_0_[18] ,\wait_ctr_reg_n_0_[17] }));
  LUT6 #(
    .INIT(64'h5554555555555555)) 
    \wait_ctr[0]_i_1 
       (.I0(\wait_ctr_reg_n_0_[0] ),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[10]_i_1 
       (.I0(wait_ctr0_carry__0_n_14),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[11]_i_1 
       (.I0(wait_ctr0_carry__0_n_13),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[12]_i_1 
       (.I0(wait_ctr0_carry__0_n_12),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[13]_i_1 
       (.I0(wait_ctr0_carry__0_n_11),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[14]_i_1 
       (.I0(wait_ctr0_carry__0_n_10),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[15]_i_1 
       (.I0(wait_ctr0_carry__0_n_9),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[16]_i_1 
       (.I0(wait_ctr0_carry__0_n_8),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[17]_i_1 
       (.I0(wait_ctr0_carry__1_n_15),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[18]_i_1 
       (.I0(wait_ctr0_carry__1_n_14),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[19]_i_1 
       (.I0(wait_ctr0_carry__1_n_13),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[1]_i_1 
       (.I0(wait_ctr0_carry_n_15),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[20]_i_1 
       (.I0(wait_ctr0_carry__1_n_12),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[21]_i_1 
       (.I0(wait_ctr0_carry__1_n_11),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[22]_i_1 
       (.I0(wait_ctr0_carry__1_n_10),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[23]_i_1 
       (.I0(wait_ctr0_carry__1_n_9),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000008AAAAAAAA)) 
    \wait_ctr[24]_i_1 
       (.I0(\wait_ctr[9]_i_2_n_0 ),
        .I1(\wait_ctr[9]_i_3_n_0 ),
        .I2(\wait_ctr[9]_i_4_n_0 ),
        .I3(p_16_in),
        .I4(p_14_in),
        .I5(\wait_ctr[9]_i_7_n_0 ),
        .O(\wait_ctr[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[24]_i_2 
       (.I0(wait_ctr0_carry__1_n_8),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[2]_i_1 
       (.I0(wait_ctr0_carry_n_14),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[3]_i_1 
       (.I0(wait_ctr0_carry_n_13),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[5]_i_1 
       (.I0(wait_ctr0_carry_n_11),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \wait_ctr[6]_i_1 
       (.I0(wait_ctr0_carry_n_10),
        .I1(\cpll_cal_state_reg_n_0_[28] ),
        .I2(p_13_in),
        .I3(p_18_in),
        .I4(\wait_ctr[9]_i_6_n_0 ),
        .I5(\wait_ctr[9]_i_3_n_0 ),
        .O(\wait_ctr[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA200A2A200000000)) 
    \wait_ctr[9]_i_1 
       (.I0(\wait_ctr[9]_i_3_n_0 ),
        .I1(\wait_ctr[9]_i_4_n_0 ),
        .I2(\wait_ctr[9]_i_5_n_0 ),
        .I3(\wait_ctr[9]_i_6_n_0 ),
        .I4(\wait_ctr[9]_i_7_n_0 ),
        .I5(\wait_ctr[9]_i_2_n_0 ),
        .O(\wait_ctr[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_ctr[9]_i_10 
       (.I0(\cpll_cal_state[17]_i_8_n_0 ),
        .I1(\wait_ctr_reg_n_0_[11] ),
        .I2(\wait_ctr_reg_n_0_[12] ),
        .I3(\wait_ctr_reg_n_0_[5] ),
        .I4(\wait_ctr_reg_n_0_[6] ),
        .I5(\wait_ctr[9]_i_13_n_0 ),
        .O(\wait_ctr[9]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \wait_ctr[9]_i_11 
       (.I0(\wait_ctr_reg_n_0_[13] ),
        .I1(\wait_ctr_reg_n_0_[14] ),
        .I2(\wait_ctr_reg_n_0_[12] ),
        .I3(\wait_ctr_reg_n_0_[15] ),
        .O(\wait_ctr[9]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005557)) 
    \wait_ctr[9]_i_12 
       (.I0(\wait_ctr_reg_n_0_[5] ),
        .I1(\wait_ctr_reg_n_0_[3] ),
        .I2(\wait_ctr_reg_n_0_[4] ),
        .I3(\wait_ctr_reg_n_0_[2] ),
        .I4(\wait_ctr[9]_i_14_n_0 ),
        .I5(\wait_ctr_reg_n_0_[6] ),
        .O(\wait_ctr[9]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hFE000000)) 
    \wait_ctr[9]_i_13 
       (.I0(\wait_ctr_reg_n_0_[2] ),
        .I1(\wait_ctr_reg_n_0_[1] ),
        .I2(\wait_ctr_reg_n_0_[0] ),
        .I3(\wait_ctr_reg_n_0_[3] ),
        .I4(\wait_ctr_reg_n_0_[4] ),
        .O(\wait_ctr[9]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \wait_ctr[9]_i_14 
       (.I0(\wait_ctr_reg_n_0_[9] ),
        .I1(\wait_ctr_reg_n_0_[7] ),
        .I2(\wait_ctr_reg_n_0_[8] ),
        .O(\wait_ctr[9]_i_14_n_0 ));
  LUT5 #(
    .INIT(32'h55555554)) 
    \wait_ctr[9]_i_2 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(\wait_ctr[9]_i_8_n_0 ),
        .I2(p_18_in),
        .I3(p_13_in),
        .I4(\cpll_cal_state_reg_n_0_[28] ),
        .O(\wait_ctr[9]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'h37)) 
    \wait_ctr[9]_i_3 
       (.I0(p_15_in),
        .I1(\cpll_cal_state[17]_i_2_n_0 ),
        .I2(p_17_in),
        .O(\wait_ctr[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \wait_ctr[9]_i_4 
       (.I0(\wait_ctr_reg_n_0_[13] ),
        .I1(\wait_ctr_reg_n_0_[14] ),
        .I2(\wait_ctr_reg_n_0_[15] ),
        .I3(\wait_ctr_reg_n_0_[16] ),
        .I4(\wait_ctr[9]_i_9_n_0 ),
        .I5(\wait_ctr[9]_i_10_n_0 ),
        .O(\wait_ctr[9]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \wait_ctr[9]_i_5 
       (.I0(p_18_in),
        .I1(p_13_in),
        .I2(\cpll_cal_state_reg_n_0_[28] ),
        .O(\wait_ctr[9]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \wait_ctr[9]_i_6 
       (.I0(p_16_in),
        .I1(p_14_in),
        .O(\wait_ctr[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h1011101010111011)) 
    \wait_ctr[9]_i_7 
       (.I0(\wait_ctr[9]_i_9_n_0 ),
        .I1(\wait_ctr_reg_n_0_[16] ),
        .I2(\wait_ctr[9]_i_11_n_0 ),
        .I3(\wait_ctr_reg_n_0_[11] ),
        .I4(\wait_ctr[9]_i_12_n_0 ),
        .I5(\wait_ctr_reg_n_0_[10] ),
        .O(\wait_ctr[9]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFEFFFEFEFFFFFFFF)) 
    \wait_ctr[9]_i_8 
       (.I0(\cpll_cal_state_reg_n_0_[0] ),
        .I1(p_15_in),
        .I2(p_17_in),
        .I3(drp_done),
        .I4(p_0_in),
        .I5(\wait_ctr[9]_i_6_n_0 ),
        .O(\wait_ctr[9]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \wait_ctr[9]_i_9 
       (.I0(\wait_ctr_reg_n_0_[22] ),
        .I1(\wait_ctr_reg_n_0_[21] ),
        .I2(\wait_ctr_reg_n_0_[24] ),
        .I3(\wait_ctr_reg_n_0_[23] ),
        .I4(\cpll_cal_state[17]_i_5_n_0 ),
        .O(\wait_ctr[9]_i_9_n_0 ));
  FDRE \wait_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[0]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[0] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[10] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[10]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[10] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[11] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[11]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[11] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[12] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[12]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[12] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[13] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[13]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[13] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[14] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[14]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[14] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[15] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[15]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[15] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[16] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[16]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[16] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[17] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[17]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[17] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[18] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[18]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[18] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[19] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[19]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[19] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[1]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[1] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[20] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[20]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[20] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[21] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[21]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[21] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[22] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[22]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[22] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[23] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[23]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[23] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[24] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[24]_i_2_n_0 ),
        .Q(\wait_ctr_reg_n_0_[24] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[2]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[2] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[3]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[3] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(wait_ctr0_carry_n_12),
        .Q(\wait_ctr_reg_n_0_[4] ),
        .R(\wait_ctr[9]_i_1_n_0 ));
  FDRE \wait_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[5]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[5] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(\wait_ctr[6]_i_1_n_0 ),
        .Q(\wait_ctr_reg_n_0_[6] ),
        .R(\wait_ctr[24]_i_1_n_0 ));
  FDRE \wait_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(wait_ctr0_carry_n_9),
        .Q(\wait_ctr_reg_n_0_[7] ),
        .R(\wait_ctr[9]_i_1_n_0 ));
  FDRE \wait_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(wait_ctr0_carry_n_8),
        .Q(\wait_ctr_reg_n_0_[8] ),
        .R(\wait_ctr[9]_i_1_n_0 ));
  FDRE \wait_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\wait_ctr[9]_i_2_n_0 ),
        .D(wait_ctr0_carry__0_n_15),
        .Q(\wait_ctr_reg_n_0_[9] ),
        .R(\wait_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h74)) 
    wr_i_1__0
       (.I0(drp_done),
        .I1(\di_msk[15]_i_3_n_0 ),
        .I2(wr),
        .O(wr_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    wr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(wr_i_1__0_n_0),
        .Q(wr),
        .R(cal_on_tx_reset_in_sync));
  LUT3 #(
    .INIT(8'h40)) 
    \x0e1_store[14]_i_1 
       (.I0(cal_on_tx_reset_in_sync),
        .I1(p_4_in),
        .I2(drp_done),
        .O(\x0e1_store[14]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[0] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[0]),
        .Q(\x0e1_store_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[12] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[12]),
        .Q(\x0e1_store_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[13] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[13]),
        .Q(\x0e1_store_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[14] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[14]),
        .Q(\x0e1_store_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[1] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[1]),
        .Q(\x0e1_store_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[2] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[2]),
        .Q(\x0e1_store_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[3] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[3]),
        .Q(\x0e1_store_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[4] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[4]),
        .Q(\x0e1_store_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[5] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[5]),
        .Q(\x0e1_store_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[6] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[6]),
        .Q(\x0e1_store_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[7] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[7]),
        .Q(\x0e1_store_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[8] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[8]),
        .Q(\x0e1_store_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \x0e1_store_reg[9] 
       (.C(drpclk_in),
        .CE(\x0e1_store[14]_i_1_n_0 ),
        .D(Q[9]),
        .Q(\x0e1_store_reg_n_0_[9] ),
        .R(1'b0));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gthe4_delay_powergood
   (out,
    RXPD,
    RXRATE,
    rxoutclkpcs_out,
    \gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ,
    rxpd_in);
  output out;
  output [0:0]RXPD;
  output [0:0]RXRATE;
  input [0:0]rxoutclkpcs_out;
  input \gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ;
  input [0:0]rxpd_in;

  wire [0:0]RXPD;
  wire [0:0]RXRATE;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* shreg_extract = "no" *) wire \gen_powergood_delay.int_pwr_on_fsm ;
  wire \gen_powergood_delay.int_pwr_on_fsm_i_1_n_0 ;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [4:0]\gen_powergood_delay.intclk_rrst_n_r ;
  wire \gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ;
  wire \gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) (* shreg_extract = "no" *) wire \gen_powergood_delay.pwr_on_fsm ;
  (* async_reg = "true" *) (* shreg_extract = "no" *) wire [8:0]\gen_powergood_delay.wait_cnt ;
  wire \gen_powergood_delay.wait_cnt[0]_i_1_n_0 ;
  wire \gen_powergood_delay.wait_cnt[8]_i_1_n_0 ;
  wire [0:0]rxoutclkpcs_out;
  wire [0:0]rxpd_in;

  assign out = \gen_powergood_delay.pwr_on_fsm ;
  LUT2 #(
    .INIT(4'hE)) 
    \gen_powergood_delay.int_pwr_on_fsm_i_1 
       (.I0(\gen_powergood_delay.int_pwr_on_fsm ),
        .I1(\gen_powergood_delay.wait_cnt [7]),
        .O(\gen_powergood_delay.int_pwr_on_fsm_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.int_pwr_on_fsm_reg 
       (.C(rxoutclkpcs_out),
        .CE(1'b1),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(\gen_powergood_delay.int_pwr_on_fsm_i_1_n_0 ),
        .Q(\gen_powergood_delay.int_pwr_on_fsm ));
  LUT1 #(
    .INIT(2'h1)) 
    \gen_powergood_delay.intclk_rrst_n_r[4]_i_1 
       (.I0(\gen_powergood_delay.int_pwr_on_fsm ),
        .O(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.intclk_rrst_n_r_reg[0] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(1'b1),
        .Q(\gen_powergood_delay.intclk_rrst_n_r [0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.intclk_rrst_n_r_reg[1] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(\gen_powergood_delay.intclk_rrst_n_r [0]),
        .Q(\gen_powergood_delay.intclk_rrst_n_r [1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.intclk_rrst_n_r_reg[2] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(\gen_powergood_delay.intclk_rrst_n_r [1]),
        .Q(\gen_powergood_delay.intclk_rrst_n_r [2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.intclk_rrst_n_r_reg[3] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(\gen_powergood_delay.intclk_rrst_n_r [2]),
        .Q(\gen_powergood_delay.intclk_rrst_n_r [3]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDCE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.intclk_rrst_n_r_reg[4] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .CLR(\gen_powergood_delay.intclk_rrst_n_r_reg[4]_0 ),
        .D(\gen_powergood_delay.intclk_rrst_n_r [3]),
        .Q(\gen_powergood_delay.intclk_rrst_n_r [4]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \gen_powergood_delay.pwr_on_fsm_reg 
       (.C(rxoutclkpcs_out),
        .CE(1'b1),
        .D(\gen_powergood_delay.int_pwr_on_fsm ),
        .Q(\gen_powergood_delay.pwr_on_fsm ),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h7)) 
    \gen_powergood_delay.wait_cnt[0]_i_1 
       (.I0(\gen_powergood_delay.intclk_rrst_n_r [4]),
        .I1(\gen_powergood_delay.int_pwr_on_fsm ),
        .O(\gen_powergood_delay.wait_cnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \gen_powergood_delay.wait_cnt[8]_i_1 
       (.I0(\gen_powergood_delay.intclk_rrst_n_r [4]),
        .O(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[0] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.wait_cnt[0]_i_1_n_0 ),
        .D(\gen_powergood_delay.intclk_rrst_n_r [4]),
        .Q(\gen_powergood_delay.wait_cnt [0]),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[1] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [0]),
        .Q(\gen_powergood_delay.wait_cnt [1]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[2] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [1]),
        .Q(\gen_powergood_delay.wait_cnt [2]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[3] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [2]),
        .Q(\gen_powergood_delay.wait_cnt [3]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[4] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [3]),
        .Q(\gen_powergood_delay.wait_cnt [4]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[5] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [4]),
        .Q(\gen_powergood_delay.wait_cnt [5]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[6] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [5]),
        .Q(\gen_powergood_delay.wait_cnt [6]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[7] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [6]),
        .Q(\gen_powergood_delay.wait_cnt [7]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  (* SHREG_EXTRACT = "no" *) 
  FDRE \gen_powergood_delay.wait_cnt_reg[8] 
       (.C(rxoutclkpcs_out),
        .CE(\gen_powergood_delay.intclk_rrst_n_r[4]_i_1_n_0 ),
        .D(\gen_powergood_delay.wait_cnt [7]),
        .Q(\gen_powergood_delay.wait_cnt [8]),
        .R(\gen_powergood_delay.wait_cnt[8]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_i_2 
       (.I0(\gen_powergood_delay.pwr_on_fsm ),
        .O(RXRATE));
  LUT2 #(
    .INIT(4'hB)) 
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_i_3 
       (.I0(rxpd_in),
        .I1(\gen_powergood_delay.pwr_on_fsm ),
        .O(RXPD));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_gtwiz_reset
   (rst_in_out_reg,
    gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_out,
    \gen_gtwizard_gthe4.gttxreset_int ,
    \gen_gtwizard_gthe4.txuserrdy_int ,
    \gen_gtwizard_gthe4.rxprogdivreset_int ,
    \gen_gtwizard_gthe4.rxuserrdy_int ,
    \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ,
    RESET_IN,
    gtpowergood_out,
    i_in_meta_reg,
    gtwiz_userclk_rx_active_out,
    rxcdrlock_out,
    drpclk_in,
    gtwiz_reset_all_in,
    gtwiz_reset_tx_datapath_in,
    rst_in0,
    rxusrclk_in,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ,
    \gen_gtwizard_gthe4.gtpowergood_int ,
    gtwiz_reset_rx_datapath_in);
  output rst_in_out_reg;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  output \gen_gtwizard_gthe4.gttxreset_int ;
  output \gen_gtwizard_gthe4.txuserrdy_int ;
  output \gen_gtwizard_gthe4.rxprogdivreset_int ;
  output \gen_gtwizard_gthe4.rxuserrdy_int ;
  output \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  output RESET_IN;
  input [0:0]gtpowergood_out;
  input i_in_meta_reg;
  input [0:0]gtwiz_userclk_rx_active_out;
  input [0:0]rxcdrlock_out;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in0;
  input [0:0]rxusrclk_in;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  input \gen_gtwizard_gthe4.gtpowergood_int ;
  input [0:0]gtwiz_reset_rx_datapath_in;

  wire \FSM_sequential_sm_reset_all[2]_i_3_n_0 ;
  wire \FSM_sequential_sm_reset_all[2]_i_4_n_0 ;
  wire \FSM_sequential_sm_reset_rx[2]_i_2_n_0 ;
  wire RESET_IN;
  wire bit_synchronizer_gtpowergood_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1;
  wire bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2;
  wire bit_synchronizer_plllock_rx_inst_n_0;
  wire bit_synchronizer_plllock_rx_inst_n_1;
  wire bit_synchronizer_plllock_rx_inst_n_2;
  wire bit_synchronizer_plllock_rx_inst_n_3;
  wire bit_synchronizer_plllock_rx_inst_n_4;
  wire bit_synchronizer_plllock_tx_inst_n_0;
  wire bit_synchronizer_plllock_tx_inst_n_1;
  wire bit_synchronizer_plllock_tx_inst_n_2;
  wire bit_synchronizer_plllock_tx_inst_n_3;
  wire bit_synchronizer_rxcdrlock_inst_n_0;
  wire bit_synchronizer_rxcdrlock_inst_n_1;
  wire bit_synchronizer_rxcdrlock_inst_n_2;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ;
  wire \gen_gtwizard_gthe4.gtpowergood_int ;
  wire \gen_gtwizard_gthe4.gtrxreset_int ;
  wire \gen_gtwizard_gthe4.gttxreset_int ;
  wire \gen_gtwizard_gthe4.rxprogdivreset_int ;
  wire \gen_gtwizard_gthe4.rxuserrdy_int ;
  wire \gen_gtwizard_gthe4.txuserrdy_int ;
  wire [0:0]gtpowergood_out;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  wire gtwiz_reset_rx_any_sync;
  wire gtwiz_reset_rx_datapath_dly;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire gtwiz_reset_rx_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_datapath_sync;
  wire gtwiz_reset_rx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_rx_done_out;
  wire gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_rx_pll_and_datapath_sync;
  wire gtwiz_reset_tx_any_sync;
  wire gtwiz_reset_tx_datapath_dly;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire gtwiz_reset_tx_datapath_sync;
  wire gtwiz_reset_tx_done_int0__0;
  wire gtwiz_reset_tx_done_int_reg_n_0;
  wire [0:0]gtwiz_reset_tx_done_out;
  wire gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_int_reg_n_0;
  wire gtwiz_reset_tx_pll_and_datapath_sync;
  wire [0:0]gtwiz_userclk_rx_active_out;
  wire i_in_meta_reg;
  wire p_0_in;
  wire p_0_in11_out__0;
  wire [9:0]p_0_in__2;
  wire [9:0]p_0_in__3;
  wire reset_synchronizer_gtwiz_reset_rx_any_inst_n_1;
  wire reset_synchronizer_gtwiz_reset_tx_any_inst_n_1;
  wire rst_in0;
  wire rst_in_out_reg;
  wire [0:0]rxcdrlock_out;
  wire [0:0]rxusrclk_in;
  wire sel;
  wire [2:0]sm_reset_all;
  wire [2:0]sm_reset_all__0;
  wire sm_reset_all_timer_clr_i_1_n_0;
  wire sm_reset_all_timer_clr_i_2_n_0;
  wire sm_reset_all_timer_clr_reg_n_0;
  wire [2:0]sm_reset_all_timer_ctr;
  wire \sm_reset_all_timer_ctr0_inferred__0/i__n_0 ;
  wire \sm_reset_all_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_all_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_all_timer_sat;
  wire sm_reset_all_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_rx;
  wire [1:0]sm_reset_rx__0;
  wire sm_reset_rx_cdr_to_clr;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr[0]_i_8_n_0 ;
  wire [25:0]sm_reset_rx_cdr_to_ctr_reg;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ;
  wire \sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ;
  wire sm_reset_rx_cdr_to_sat;
  wire sm_reset_rx_cdr_to_sat_i_1_n_0;
  wire sm_reset_rx_pll_timer_clr_i_1_n_0;
  wire sm_reset_rx_pll_timer_clr_reg_n_0;
  wire \sm_reset_rx_pll_timer_ctr[2]_i_1_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[7]_i_2_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ;
  wire \sm_reset_rx_pll_timer_ctr[9]_i_4_n_0 ;
  wire [9:0]sm_reset_rx_pll_timer_ctr_reg;
  wire sm_reset_rx_pll_timer_sat;
  wire sm_reset_rx_pll_timer_sat_i_1_n_0;
  wire sm_reset_rx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_rx_timer_ctr;
  wire \sm_reset_rx_timer_ctr0_inferred__0/i__n_0 ;
  wire \sm_reset_rx_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_rx_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_rx_timer_sat;
  wire sm_reset_rx_timer_sat_i_1_n_0;
  wire [2:0]sm_reset_tx;
  wire [2:0]sm_reset_tx__0;
  wire sm_reset_tx_pll_timer_clr_i_1_n_0;
  wire sm_reset_tx_pll_timer_clr_reg_n_0;
  wire \sm_reset_tx_pll_timer_ctr[2]_i_1_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[7]_i_2_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ;
  wire \sm_reset_tx_pll_timer_ctr[9]_i_4_n_0 ;
  wire [9:0]sm_reset_tx_pll_timer_ctr_reg;
  wire sm_reset_tx_pll_timer_sat;
  wire sm_reset_tx_pll_timer_sat_i_1_n_0;
  wire sm_reset_tx_timer_clr013_out__0;
  wire sm_reset_tx_timer_clr0__0;
  wire sm_reset_tx_timer_clr_reg_n_0;
  wire [2:0]sm_reset_tx_timer_ctr;
  wire \sm_reset_tx_timer_ctr[0]_i_1_n_0 ;
  wire \sm_reset_tx_timer_ctr[1]_i_1_n_0 ;
  wire \sm_reset_tx_timer_ctr[2]_i_1_n_0 ;
  wire sm_reset_tx_timer_sat;
  wire sm_reset_tx_timer_sat_i_1_n_0;
  wire [7:1]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED ;
  wire [7:2]\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00FFF70000FFFFFF)) 
    \FSM_sequential_sm_reset_all[0]_i_1 
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .I3(sm_reset_all[2]),
        .I4(sm_reset_all[1]),
        .I5(sm_reset_all[0]),
        .O(sm_reset_all__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'h34)) 
    \FSM_sequential_sm_reset_all[1]_i_1 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[0]),
        .O(sm_reset_all__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'h4A)) 
    \FSM_sequential_sm_reset_all[2]_i_2 
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .O(sm_reset_all__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_sm_reset_all[2]_i_3 
       (.I0(sm_reset_all_timer_sat),
        .I1(gtwiz_reset_rx_done_int_reg_n_0),
        .I2(sm_reset_all_timer_clr_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_all[2]_i_4 
       (.I0(sm_reset_all_timer_clr_reg_n_0),
        .I1(sm_reset_all_timer_sat),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .O(\FSM_sequential_sm_reset_all[2]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[0]),
        .Q(sm_reset_all[0]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[1]),
        .Q(sm_reset_all[1]),
        .R(gtwiz_reset_all_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_ALL_BRANCH:000,ST_RESET_ALL_TX_PLL_WAIT:010,ST_RESET_ALL_RX_WAIT:101,ST_RESET_ALL_TX_PLL:001,ST_RESET_ALL_RX_PLL:100,ST_RESET_ALL_RX_DP:011,ST_RESET_ALL_INIT:111,iSTATE:110" *) 
  FDRE #(
    .INIT(1'b1)) 
    \FSM_sequential_sm_reset_all_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_gtpowergood_inst_n_0),
        .D(sm_reset_all__0[2]),
        .Q(sm_reset_all[2]),
        .R(gtwiz_reset_all_sync));
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_rx[1]_i_2 
       (.I0(sm_reset_rx_timer_clr_reg_n_0),
        .I1(sm_reset_rx_timer_sat),
        .I2(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .O(p_0_in11_out__0));
  LUT6 #(
    .INIT(64'hFFFFFF000800FF00)) 
    \FSM_sequential_sm_reset_rx[2]_i_2 
       (.I0(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .I1(sm_reset_rx_timer_sat),
        .I2(sm_reset_rx_timer_clr_reg_n_0),
        .I3(sm_reset_rx[2]),
        .I4(sm_reset_rx[1]),
        .I5(sm_reset_rx[0]),
        .O(\FSM_sequential_sm_reset_rx[2]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_rx_inst_n_0),
        .D(sm_reset_rx__0[0]),
        .Q(sm_reset_rx[0]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_rx_inst_n_0),
        .D(sm_reset_rx__0[1]),
        .Q(sm_reset_rx[1]),
        .R(gtwiz_reset_rx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_RX_WAIT_LOCK:011,ST_RESET_RX_WAIT_CDR:100,ST_RESET_RX_WAIT_USERRDY:101,ST_RESET_RX_WAIT_RESETDONE:110,ST_RESET_RX_DATAPATH:010,ST_RESET_RX_PLL:001,ST_RESET_RX_BRANCH:000,ST_RESET_RX_IDLE:111" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_rx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_rx_inst_n_0),
        .D(\FSM_sequential_sm_reset_rx[2]_i_2_n_0 ),
        .Q(sm_reset_rx[2]),
        .R(gtwiz_reset_rx_any_sync));
  LUT3 #(
    .INIT(8'h38)) 
    \FSM_sequential_sm_reset_tx[2]_i_2 
       (.I0(sm_reset_tx[0]),
        .I1(sm_reset_tx[1]),
        .I2(sm_reset_tx[2]),
        .O(sm_reset_tx__0[2]));
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_sm_reset_tx[2]_i_4 
       (.I0(sm_reset_tx_timer_clr_reg_n_0),
        .I1(sm_reset_tx_timer_sat),
        .I2(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .O(gtwiz_reset_tx_done_int0__0));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[0] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_tx_inst_n_0),
        .D(sm_reset_tx__0[0]),
        .Q(sm_reset_tx[0]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[1] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_tx_inst_n_0),
        .D(sm_reset_tx__0[1]),
        .Q(sm_reset_tx[1]),
        .R(gtwiz_reset_tx_any_sync));
  (* FSM_ENCODED_STATES = "ST_RESET_TX_BRANCH:000,ST_RESET_TX_WAIT_LOCK:011,ST_RESET_TX_WAIT_USERRDY:100,ST_RESET_TX_WAIT_RESETDONE:101,ST_RESET_TX_IDLE:110,ST_RESET_TX_DATAPATH:010,ST_RESET_TX_PLL:001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_sm_reset_tx_reg[2] 
       (.C(drpclk_in),
        .CE(bit_synchronizer_plllock_tx_inst_n_0),
        .D(sm_reset_tx__0[2]),
        .Q(sm_reset_tx[2]),
        .R(gtwiz_reset_tx_any_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_1 bit_synchronizer_gtpowergood_inst
       (.E(bit_synchronizer_gtpowergood_inst_n_0),
        .\FSM_sequential_sm_reset_all_reg[0] (\FSM_sequential_sm_reset_all[2]_i_3_n_0 ),
        .\FSM_sequential_sm_reset_all_reg[0]_0 (\FSM_sequential_sm_reset_all[2]_i_4_n_0 ),
        .Q(sm_reset_all),
        .drpclk_in(drpclk_in),
        .gtpowergood_out(gtpowergood_out));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_2 bit_synchronizer_gtwiz_reset_rx_datapath_dly_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .in0(gtwiz_reset_rx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_3 bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst
       (.D(sm_reset_rx__0),
        .\FSM_sequential_sm_reset_rx[2]_i_3 (sm_reset_rx_pll_timer_clr_reg_n_0),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_dly(gtwiz_reset_rx_datapath_dly),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync),
        .p_0_in11_out__0(p_0_in11_out__0),
        .sm_reset_rx_pll_timer_sat(sm_reset_rx_pll_timer_sat),
        .sm_reset_rx_pll_timer_sat_reg(bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_4 bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_dly(gtwiz_reset_tx_datapath_dly),
        .in0(gtwiz_reset_tx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_5 bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst
       (.D(sm_reset_tx__0[1:0]),
        .\FSM_sequential_sm_reset_tx[2]_i_3 (sm_reset_tx_pll_timer_clr_reg_n_0),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_dly(gtwiz_reset_tx_datapath_dly),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync),
        .sm_reset_tx_pll_timer_sat(sm_reset_tx_pll_timer_sat),
        .sm_reset_tx_pll_timer_sat_reg(bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_6 bit_synchronizer_gtwiz_reset_userclk_rx_active_inst
       (.\FSM_sequential_sm_reset_rx_reg[0] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[0]_1 (bit_synchronizer_rxcdrlock_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.rxresetdone_sync ),
        .\gen_gtwizard_gthe4.rxuserrdy_int (\gen_gtwizard_gthe4.rxuserrdy_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_userclk_rx_active_out(gtwiz_userclk_rx_active_out),
        .p_0_in11_out__0(p_0_in11_out__0),
        .sm_reset_rx_cdr_to_sat(sm_reset_rx_cdr_to_sat),
        .sm_reset_rx_timer_clr_reg(sm_reset_rx_timer_clr_reg_n_0),
        .sm_reset_rx_timer_clr_reg_0(bit_synchronizer_plllock_rx_inst_n_1),
        .sm_reset_rx_timer_sat(sm_reset_rx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_7 bit_synchronizer_gtwiz_reset_userclk_tx_active_inst
       (.\FSM_sequential_sm_reset_tx_reg[1] (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .\FSM_sequential_sm_reset_tx_reg[2] (bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.txresetdone_sync ),
        .\gen_gtwizard_gthe4.txuserrdy_int (\gen_gtwizard_gthe4.txuserrdy_int ),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .sm_reset_tx_timer_clr013_out__0(sm_reset_tx_timer_clr013_out__0),
        .sm_reset_tx_timer_clr0__0(sm_reset_tx_timer_clr0__0),
        .sm_reset_tx_timer_clr_reg(sm_reset_tx_timer_clr_reg_n_0),
        .sm_reset_tx_timer_clr_reg_0(bit_synchronizer_plllock_tx_inst_n_1),
        .sm_reset_tx_timer_sat(sm_reset_tx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_8 bit_synchronizer_plllock_rx_inst
       (.E(bit_synchronizer_plllock_rx_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0] (bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0]_0 (sm_reset_rx_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_rx_reg[0]_1 (bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_plllock_rx_inst_n_2),
        .\FSM_sequential_sm_reset_rx_reg[1]_0 (bit_synchronizer_plllock_rx_inst_n_4),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_plllock_rx_inst_n_3),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gtrxreset_int (\gen_gtwizard_gthe4.gtrxreset_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_reset_rx_done_int_reg(gtwiz_reset_rx_done_int_reg_n_0),
        .i_in_meta_reg_0(i_in_meta_reg),
        .i_in_out_reg_0(bit_synchronizer_plllock_rx_inst_n_1),
        .p_0_in11_out__0(p_0_in11_out__0),
        .sm_reset_rx_timer_sat(sm_reset_rx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_9 bit_synchronizer_plllock_tx_inst
       (.E(bit_synchronizer_plllock_tx_inst_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0] (bit_synchronizer_plllock_tx_inst_n_2),
        .\FSM_sequential_sm_reset_tx_reg[0]_0 (sm_reset_tx_timer_clr_reg_n_0),
        .\FSM_sequential_sm_reset_tx_reg[0]_1 (bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst_n_0),
        .\FSM_sequential_sm_reset_tx_reg[1] (bit_synchronizer_plllock_tx_inst_n_3),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gttxreset_int (\gen_gtwizard_gthe4.gttxreset_int ),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .gtwiz_reset_tx_done_int0__0(gtwiz_reset_tx_done_int0__0),
        .gtwiz_reset_tx_done_int_reg(gtwiz_reset_tx_done_int_reg_n_0),
        .i_in_meta_reg_0(i_in_meta_reg),
        .i_in_out_reg_0(bit_synchronizer_plllock_tx_inst_n_1),
        .sm_reset_tx_timer_clr0__0(sm_reset_tx_timer_clr0__0),
        .sm_reset_tx_timer_sat(sm_reset_tx_timer_sat));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_bit_synchronizer_10 bit_synchronizer_rxcdrlock_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (bit_synchronizer_rxcdrlock_inst_n_1),
        .\FSM_sequential_sm_reset_rx_reg[2] (bit_synchronizer_rxcdrlock_inst_n_2),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.rxprogdivreset_int (\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .i_in_out_reg_0(bit_synchronizer_rxcdrlock_inst_n_0),
        .rxcdrlock_out(rxcdrlock_out),
        .sm_reset_rx_cdr_to_clr(sm_reset_rx_cdr_to_clr),
        .sm_reset_rx_cdr_to_clr_reg(bit_synchronizer_plllock_rx_inst_n_2),
        .sm_reset_rx_cdr_to_sat(sm_reset_rx_cdr_to_sat));
  LUT3 #(
    .INIT(8'h8B)) 
    \gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST_i_1 
       (.I0(\gen_gtwizard_gthe4.gtrxreset_int ),
        .I1(gtpowergood_out),
        .I2(\gen_gtwizard_gthe4.gtpowergood_int ),
        .O(\gen_gtwizard_gthe4.delay_pwrgood_gtrxreset_int ));
  FDRE #(
    .INIT(1'b1)) 
    gtrxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_rx_inst_n_4),
        .Q(\gen_gtwizard_gthe4.gtrxreset_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    gttxreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_tx_inst_n_3),
        .Q(\gen_gtwizard_gthe4.gttxreset_int ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'hF740)) 
    gtwiz_reset_rx_datapath_int_i_1
       (.I0(sm_reset_all[2]),
        .I1(sm_reset_all[0]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_rx_inst_n_3),
        .Q(gtwiz_reset_rx_done_int_reg_n_0),
        .R(gtwiz_reset_rx_any_sync));
  LUT4 #(
    .INIT(16'hF704)) 
    gtwiz_reset_rx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[2]),
        .I2(sm_reset_all[1]),
        .I3(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_rx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_rx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_done_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_plllock_tx_inst_n_2),
        .Q(gtwiz_reset_tx_done_int_reg_n_0),
        .R(gtwiz_reset_tx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT4 #(
    .INIT(16'hFB02)) 
    gtwiz_reset_tx_pll_and_datapath_int_i_1
       (.I0(sm_reset_all[0]),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .O(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtwiz_reset_tx_pll_and_datapath_int_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(gtwiz_reset_tx_pll_and_datapath_int_i_1_n_0),
        .Q(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0),
        .R(gtwiz_reset_all_sync));
  FDRE #(
    .INIT(1'b0)) 
    pllreset_rx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    pllreset_tx_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .Q(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .R(1'b0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer reset_synchronizer_gtwiz_reset_all_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_all_in(gtwiz_reset_all_in),
        .gtwiz_reset_all_sync(gtwiz_reset_all_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11 reset_synchronizer_gtwiz_reset_rx_any_inst
       (.\FSM_sequential_sm_reset_rx_reg[1] (reset_synchronizer_gtwiz_reset_rx_any_inst_n_1),
        .Q(sm_reset_rx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .gtwiz_reset_rx_any_sync(gtwiz_reset_rx_any_sync),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0),
        .rst_in_out_reg_1(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12 reset_synchronizer_gtwiz_reset_rx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_rx_datapath_in(gtwiz_reset_rx_datapath_in),
        .in0(gtwiz_reset_rx_datapath_sync),
        .rst_in_out_reg_0(gtwiz_reset_rx_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13 reset_synchronizer_gtwiz_reset_rx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_rx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_rx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14 reset_synchronizer_gtwiz_reset_tx_any_inst
       (.\FSM_sequential_sm_reset_tx_reg[1] (reset_synchronizer_gtwiz_reset_tx_any_inst_n_1),
        .Q(sm_reset_tx),
        .drpclk_in(drpclk_in),
        .\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int (\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .gtwiz_reset_tx_any_sync(gtwiz_reset_tx_any_sync),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .rst_in_out_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15 reset_synchronizer_gtwiz_reset_tx_datapath_inst
       (.drpclk_in(drpclk_in),
        .gtwiz_reset_tx_datapath_in(gtwiz_reset_tx_datapath_in),
        .in0(gtwiz_reset_tx_datapath_sync));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16 reset_synchronizer_gtwiz_reset_tx_pll_and_datapath_inst
       (.drpclk_in(drpclk_in),
        .in0(gtwiz_reset_tx_pll_and_datapath_sync),
        .rst_in_meta_reg_0(gtwiz_reset_tx_pll_and_datapath_int_reg_n_0));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer reset_synchronizer_rx_done_inst
       (.gtwiz_reset_rx_done_out(gtwiz_reset_rx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_rx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17 reset_synchronizer_tx_done_inst
       (.gtwiz_reset_tx_done_out(gtwiz_reset_tx_done_out),
        .rst_in_sync2_reg_0(gtwiz_reset_tx_done_int_reg_n_0),
        .rxusrclk_in(rxusrclk_in));
  PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18 reset_synchronizer_txprogdivreset_inst
       (.drpclk_in(drpclk_in),
        .rst_in0(rst_in0),
        .rst_in_out_reg_0(rst_in_out_reg));
  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__2
       (.I0(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .I1(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(RESET_IN));
  FDRE #(
    .INIT(1'b1)) 
    rxprogdivreset_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_rxcdrlock_inst_n_2),
        .Q(\gen_gtwizard_gthe4.rxprogdivreset_int ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_2),
        .Q(\gen_gtwizard_gthe4.rxuserrdy_int ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hEFFA200A)) 
    sm_reset_all_timer_clr_i_1
       (.I0(sm_reset_all_timer_clr_i_2_n_0),
        .I1(sm_reset_all[1]),
        .I2(sm_reset_all[2]),
        .I3(sm_reset_all[0]),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_clr_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000B0003333BB33)) 
    sm_reset_all_timer_clr_i_2
       (.I0(gtwiz_reset_rx_done_int_reg_n_0),
        .I1(sm_reset_all[2]),
        .I2(gtwiz_reset_tx_done_int_reg_n_0),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .I5(sm_reset_all[1]),
        .O(sm_reset_all_timer_clr_i_2_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_all_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_clr_i_1_n_0),
        .Q(sm_reset_all_timer_clr_reg_n_0),
        .S(gtwiz_reset_all_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    \sm_reset_all_timer_ctr0_inferred__0/i_ 
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .O(\sm_reset_all_timer_ctr0_inferred__0/i__n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_all_timer_ctr[0]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .O(\sm_reset_all_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_all_timer_ctr[1]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .O(\sm_reset_all_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_all_timer_ctr[2]_i_1 
       (.I0(sm_reset_all_timer_ctr[0]),
        .I1(sm_reset_all_timer_ctr[1]),
        .I2(sm_reset_all_timer_ctr[2]),
        .O(\sm_reset_all_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_all_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_all_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[0]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_all_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_all_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[1]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_all_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_all_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_all_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_all_timer_ctr[2]),
        .R(sm_reset_all_timer_clr_reg_n_0));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_all_timer_sat_i_1
       (.I0(sm_reset_all_timer_ctr[2]),
        .I1(sm_reset_all_timer_ctr[0]),
        .I2(sm_reset_all_timer_ctr[1]),
        .I3(sm_reset_all_timer_sat),
        .I4(sm_reset_all_timer_clr_reg_n_0),
        .O(sm_reset_all_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_all_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_all_timer_sat_i_1_n_0),
        .Q(sm_reset_all_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_cdr_to_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_rxcdrlock_inst_n_1),
        .Q(sm_reset_rx_cdr_to_clr),
        .S(gtwiz_reset_rx_any_sync));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_1 
       (.I0(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ),
        .I1(sm_reset_rx_cdr_to_ctr_reg[12]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[13]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[18]),
        .I4(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ),
        .I5(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_3 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[7]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[4]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[11]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[9]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_4 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[3]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[1]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[2]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[0]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[19]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[20]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00020000)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_5 
       (.I0(\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ),
        .I1(sm_reset_rx_cdr_to_ctr_reg[15]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[14]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[16]),
        .I4(\sm_reset_rx_cdr_to_ctr[0]_i_8_n_0 ),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_6 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[0]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_7 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[24]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[23]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[25]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[17]),
        .I4(sm_reset_rx_cdr_to_ctr_reg[21]),
        .I5(sm_reset_rx_cdr_to_ctr_reg[22]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \sm_reset_rx_cdr_to_ctr[0]_i_8 
       (.I0(sm_reset_rx_cdr_to_ctr_reg[10]),
        .I1(sm_reset_rx_cdr_to_ctr_reg[8]),
        .I2(sm_reset_rx_cdr_to_ctr_reg[6]),
        .I3(sm_reset_rx_cdr_to_ctr_reg[5]),
        .O(\sm_reset_rx_cdr_to_ctr[0]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[0]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[0]_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_15 }),
        .S({sm_reset_rx_cdr_to_ctr_reg[7:1],\sm_reset_rx_cdr_to_ctr[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[10] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[10]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[11] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[11]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[12] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[12]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[13] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[13]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[14] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[14]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[15] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[15]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[16] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[16]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[16]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[23:16]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[17] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[17]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[18] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[18]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[19] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[19]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[1]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[20] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[20]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[21] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[21]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[22] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[22]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[23] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[23]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[24] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[24]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[24]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[16]_i_1_n_0 ),
        .CI_TOP(1'b0),
        .CO({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_CO_UNCONNECTED [7:1],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_sm_reset_rx_cdr_to_ctr_reg[24]_i_1_O_UNCONNECTED [7:2],\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_15 }),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,sm_reset_rx_cdr_to_ctr_reg[25:24]}));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[25] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[24]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[25]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_13 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[2]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_12 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[3]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_11 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[4]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_10 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[5]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_9 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[6]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_8 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[7]),
        .R(sm_reset_rx_cdr_to_clr));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[8]),
        .R(sm_reset_rx_cdr_to_clr));
  (* ADDER_THRESHOLD = "16" *) 
  CARRY8 \sm_reset_rx_cdr_to_ctr_reg[8]_i_1 
       (.CI(\sm_reset_rx_cdr_to_ctr_reg[0]_i_2_n_0 ),
        .CI_TOP(1'b0),
        .CO({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_0 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_1 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_2 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_3 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_4 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_5 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_6 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .O({\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_8 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_9 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_10 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_11 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_12 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_13 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ,\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_15 }),
        .S(sm_reset_rx_cdr_to_ctr_reg[15:8]));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_cdr_to_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .D(\sm_reset_rx_cdr_to_ctr_reg[8]_i_1_n_14 ),
        .Q(sm_reset_rx_cdr_to_ctr_reg[9]),
        .R(sm_reset_rx_cdr_to_clr));
  LUT3 #(
    .INIT(8'h0D)) 
    sm_reset_rx_cdr_to_sat_i_1
       (.I0(\sm_reset_rx_cdr_to_ctr[0]_i_1_n_0 ),
        .I1(sm_reset_rx_cdr_to_sat),
        .I2(sm_reset_rx_cdr_to_clr),
        .O(sm_reset_rx_cdr_to_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_cdr_to_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_cdr_to_sat_i_1_n_0),
        .Q(sm_reset_rx_cdr_to_sat),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF3000B)) 
    sm_reset_rx_pll_timer_clr_i_1
       (.I0(sm_reset_rx_pll_timer_sat),
        .I1(sm_reset_rx[0]),
        .I2(sm_reset_rx[1]),
        .I3(sm_reset_rx[2]),
        .I4(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .O(\sm_reset_rx_pll_timer_ctr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_rx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[3]),
        .O(p_0_in__3[3]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[4]),
        .O(p_0_in__3[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_rx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[5]),
        .O(p_0_in__3[5]));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[6]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I1(\sm_reset_rx_pll_timer_ctr[7]_i_2_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[5]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(p_0_in__3[6]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_rx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[5]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I2(\sm_reset_rx_pll_timer_ctr[7]_i_2_n_0 ),
        .I3(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[7]),
        .O(p_0_in__3[7]));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \sm_reset_rx_pll_timer_ctr[7]_i_2 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .O(\sm_reset_rx_pll_timer_ctr[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_rx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_4_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .O(p_0_in__3[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[4]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I5(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_rx_pll_timer_ctr[9]_i_4_n_0 ),
        .I2(sm_reset_rx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[9]),
        .O(p_0_in__3[9]));
  LUT5 #(
    .INIT(32'h01000000)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[5]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[6]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \sm_reset_rx_pll_timer_ctr[9]_i_4 
       (.I0(sm_reset_rx_pll_timer_ctr_reg[5]),
        .I1(sm_reset_rx_pll_timer_ctr_reg[3]),
        .I2(sm_reset_rx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_rx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_rx_pll_timer_ctr_reg[2]),
        .I5(sm_reset_rx_pll_timer_ctr_reg[4]),
        .O(\sm_reset_rx_pll_timer_ctr[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[0]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[0]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[1]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[1]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(\sm_reset_rx_pll_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_rx_pll_timer_ctr_reg[2]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[3]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[3]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[4]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[4]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[5]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[5]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[6]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[6]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[7]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[7]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[8]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[8]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .D(p_0_in__3[9]),
        .Q(sm_reset_rx_pll_timer_ctr_reg[9]),
        .R(sm_reset_rx_pll_timer_clr_reg_n_0));
  LUT3 #(
    .INIT(8'h0D)) 
    sm_reset_rx_pll_timer_sat_i_1
       (.I0(\sm_reset_rx_pll_timer_ctr[9]_i_1_n_0 ),
        .I1(sm_reset_rx_pll_timer_sat),
        .I2(sm_reset_rx_pll_timer_clr_reg_n_0),
        .O(sm_reset_rx_pll_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_pll_timer_sat),
        .R(1'b0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_rx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_rx_active_inst_n_1),
        .Q(sm_reset_rx_timer_clr_reg_n_0),
        .S(gtwiz_reset_rx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    \sm_reset_rx_timer_ctr0_inferred__0/i_ 
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .O(\sm_reset_rx_timer_ctr0_inferred__0/i__n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_rx_timer_ctr[0]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .O(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_rx_timer_ctr[1]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .O(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_rx_timer_ctr[2]_i_1 
       (.I0(sm_reset_rx_timer_ctr[0]),
        .I1(sm_reset_rx_timer_ctr[1]),
        .I2(sm_reset_rx_timer_ctr[2]),
        .O(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_rx_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[0]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_rx_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[1]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_rx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(\sm_reset_rx_timer_ctr0_inferred__0/i__n_0 ),
        .D(\sm_reset_rx_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_rx_timer_ctr[2]),
        .R(sm_reset_rx_timer_clr_reg_n_0));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_rx_timer_sat_i_1
       (.I0(sm_reset_rx_timer_ctr[2]),
        .I1(sm_reset_rx_timer_ctr[0]),
        .I2(sm_reset_rx_timer_ctr[1]),
        .I3(sm_reset_rx_timer_sat),
        .I4(sm_reset_rx_timer_clr_reg_n_0),
        .O(sm_reset_rx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_rx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_rx_timer_sat_i_1_n_0),
        .Q(sm_reset_rx_timer_sat),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFF3000B)) 
    sm_reset_tx_pll_timer_clr_i_1
       (.I0(sm_reset_tx_pll_timer_sat),
        .I1(sm_reset_tx[0]),
        .I2(sm_reset_tx[1]),
        .I3(sm_reset_tx[2]),
        .I4(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_clr_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_pll_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_clr_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_pll_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_pll_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_pll_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .O(\sm_reset_tx_pll_timer_ctr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_tx_pll_timer_ctr[3]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[4]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_tx_pll_timer_ctr[5]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[5]),
        .O(p_0_in__2[5]));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[6]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I1(\sm_reset_tx_pll_timer_ctr[7]_i_2_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[5]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(p_0_in__2[6]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \sm_reset_tx_pll_timer_ctr[7]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[5]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I2(\sm_reset_tx_pll_timer_ctr[7]_i_2_n_0 ),
        .I3(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[7]),
        .O(p_0_in__2[7]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \sm_reset_tx_pll_timer_ctr[7]_i_2 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .O(\sm_reset_tx_pll_timer_ctr[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \sm_reset_tx_pll_timer_ctr[8]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_4_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .O(p_0_in__2[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_1 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[4]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I5(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ),
        .O(sel));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_2 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I1(\sm_reset_tx_pll_timer_ctr[9]_i_4_n_0 ),
        .I2(sm_reset_tx_pll_timer_ctr_reg[6]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[9]),
        .O(p_0_in__2[9]));
  LUT5 #(
    .INIT(32'h01000000)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_3 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[8]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[9]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[7]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[5]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[6]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \sm_reset_tx_pll_timer_ctr[9]_i_4 
       (.I0(sm_reset_tx_pll_timer_ctr_reg[5]),
        .I1(sm_reset_tx_pll_timer_ctr_reg[3]),
        .I2(sm_reset_tx_pll_timer_ctr_reg[0]),
        .I3(sm_reset_tx_pll_timer_ctr_reg[1]),
        .I4(sm_reset_tx_pll_timer_ctr_reg[2]),
        .I5(sm_reset_tx_pll_timer_ctr_reg[4]),
        .O(\sm_reset_tx_pll_timer_ctr[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[0]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[0]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[1]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[1]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(sel),
        .D(\sm_reset_tx_pll_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_tx_pll_timer_ctr_reg[2]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[3] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[3]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[3]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[4] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[4]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[4]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[5] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[5]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[5]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[6] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[6]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[6]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[7] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[7]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[7]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[8] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[8]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[8]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_pll_timer_ctr_reg[9] 
       (.C(drpclk_in),
        .CE(sel),
        .D(p_0_in__2[9]),
        .Q(sm_reset_tx_pll_timer_ctr_reg[9]),
        .R(sm_reset_tx_pll_timer_clr_reg_n_0));
  LUT3 #(
    .INIT(8'h0D)) 
    sm_reset_tx_pll_timer_sat_i_1
       (.I0(sel),
        .I1(sm_reset_tx_pll_timer_sat),
        .I2(sm_reset_tx_pll_timer_clr_reg_n_0),
        .O(sm_reset_tx_pll_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_pll_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_pll_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_pll_timer_sat),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sm_reset_tx_timer_clr_i_4
       (.I0(sm_reset_tx_timer_sat),
        .I1(sm_reset_tx_timer_clr_reg_n_0),
        .O(sm_reset_tx_timer_clr013_out__0));
  FDSE #(
    .INIT(1'b1)) 
    sm_reset_tx_timer_clr_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_1),
        .Q(sm_reset_tx_timer_clr_reg_n_0),
        .S(gtwiz_reset_tx_any_sync));
  LUT3 #(
    .INIT(8'h7F)) 
    \sm_reset_tx_timer_ctr0_inferred__0/i_ 
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .O(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    \sm_reset_tx_timer_ctr[0]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .O(\sm_reset_tx_timer_ctr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \sm_reset_tx_timer_ctr[1]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .O(\sm_reset_tx_timer_ctr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \sm_reset_tx_timer_ctr[2]_i_1 
       (.I0(sm_reset_tx_timer_ctr[0]),
        .I1(sm_reset_tx_timer_ctr[1]),
        .I2(sm_reset_tx_timer_ctr[2]),
        .O(\sm_reset_tx_timer_ctr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[0] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(\sm_reset_tx_timer_ctr[0]_i_1_n_0 ),
        .Q(sm_reset_tx_timer_ctr[0]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[1] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(\sm_reset_tx_timer_ctr[1]_i_1_n_0 ),
        .Q(sm_reset_tx_timer_ctr[1]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \sm_reset_tx_timer_ctr_reg[2] 
       (.C(drpclk_in),
        .CE(p_0_in),
        .D(\sm_reset_tx_timer_ctr[2]_i_1_n_0 ),
        .Q(sm_reset_tx_timer_ctr[2]),
        .R(sm_reset_tx_timer_clr_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sm_reset_tx_timer_sat_i_1
       (.I0(sm_reset_tx_timer_ctr[2]),
        .I1(sm_reset_tx_timer_ctr[0]),
        .I2(sm_reset_tx_timer_ctr[1]),
        .I3(sm_reset_tx_timer_sat),
        .I4(sm_reset_tx_timer_clr_reg_n_0),
        .O(sm_reset_tx_timer_sat_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sm_reset_tx_timer_sat_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(sm_reset_tx_timer_sat_i_1_n_0),
        .Q(sm_reset_tx_timer_sat),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txuserrdy_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(bit_synchronizer_gtwiz_reset_userclk_tx_active_inst_n_2),
        .Q(\gen_gtwizard_gthe4.txuserrdy_int ),
        .R(1'b0));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer
   (gtwiz_reset_rx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_rx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_rx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_rx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_inv_synchronizer_17
   (gtwiz_reset_tx_done_out,
    rxusrclk_in,
    rst_in_sync2_reg_0);
  output [0:0]gtwiz_reset_tx_done_out;
  input [0:0]rxusrclk_in;
  input rst_in_sync2_reg_0;

  wire [0:0]gtwiz_reset_tx_done_out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_i_1__0_n_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  wire rst_in_sync2_reg_0;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire [0:0]rxusrclk_in;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(1'b1),
        .Q(rst_in_meta));
  LUT1 #(
    .INIT(2'h1)) 
    rst_in_out_i_1__0
       (.I0(rst_in_sync2_reg_0),
        .O(rst_in_out_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync3),
        .Q(gtwiz_reset_tx_done_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_meta),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(rxusrclk_in),
        .CE(1'b1),
        .CLR(rst_in_out_i_1__0_n_0),
        .D(rst_in_sync2),
        .Q(rst_in_sync3));
endmodule

module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer
   (gtwiz_reset_all_sync,
    drpclk_in,
    gtwiz_reset_all_in);
  output gtwiz_reset_all_sync;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_all_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_all_in;
  wire gtwiz_reset_all_sync;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_all_in),
        .Q(gtwiz_reset_all_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_all_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_11
   (gtwiz_reset_rx_any_sync,
    \FSM_sequential_sm_reset_rx_reg[1] ,
    drpclk_in,
    Q,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ,
    rst_in_out_reg_0,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_1);
  output gtwiz_reset_rx_any_sync;
  output \FSM_sequential_sm_reset_rx_reg[1] ;
  input [0:0]drpclk_in;
  input [2:0]Q;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  input rst_in_out_reg_0;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_1;

  wire \FSM_sequential_sm_reset_rx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ;
  wire gtwiz_reset_rx_any;
  wire gtwiz_reset_rx_any_sync;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  wire rst_in_out_reg_1;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT5 #(
    .INIT(32'hFFDF0010)) 
    pllreset_rx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_rx_any_sync),
        .I4(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_rx_int ),
        .O(\FSM_sequential_sm_reset_rx_reg[1] ));
  LUT3 #(
    .INIT(8'hFE)) 
    rst_in_meta_i_1
       (.I0(rst_in_out_reg_0),
        .I1(gtwiz_reset_rx_datapath_in),
        .I2(rst_in_out_reg_1),
        .O(gtwiz_reset_rx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_rx_any),
        .Q(gtwiz_reset_rx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_rx_any),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_12
   (in0,
    drpclk_in,
    gtwiz_reset_rx_datapath_in,
    rst_in_out_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  input rst_in_out_reg_0;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_rx_datapath_in;
  wire in0;
  wire rst_in0_0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__0
       (.I0(gtwiz_reset_rx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(rst_in0_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_13
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_14
   (gtwiz_reset_tx_any_sync,
    \FSM_sequential_sm_reset_tx_reg[1] ,
    drpclk_in,
    gtwiz_reset_tx_datapath_in,
    rst_in_out_reg_0,
    Q,
    \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int );
  output gtwiz_reset_tx_any_sync;
  output \FSM_sequential_sm_reset_tx_reg[1] ;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input rst_in_out_reg_0;
  input [2:0]Q;
  input \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;

  wire \FSM_sequential_sm_reset_tx_reg[1] ;
  wire [2:0]Q;
  wire [0:0]drpclk_in;
  wire \gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ;
  wire gtwiz_reset_tx_any;
  wire gtwiz_reset_tx_any_sync;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  LUT5 #(
    .INIT(32'hFFDF0010)) 
    pllreset_tx_out_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(gtwiz_reset_tx_any_sync),
        .I4(\gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_pllreset_tx_int ),
        .O(\FSM_sequential_sm_reset_tx_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    rst_in_meta_i_1__1
       (.I0(gtwiz_reset_tx_datapath_in),
        .I1(rst_in_out_reg_0),
        .O(gtwiz_reset_tx_any));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_any),
        .Q(gtwiz_reset_tx_any_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_any),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_15
   (in0,
    drpclk_in,
    gtwiz_reset_tx_datapath_in);
  output in0;
  input [0:0]drpclk_in;
  input [0:0]gtwiz_reset_tx_datapath_in;

  wire [0:0]drpclk_in;
  wire [0:0]gtwiz_reset_tx_datapath_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(gtwiz_reset_tx_datapath_in),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_16
   (in0,
    drpclk_in,
    rst_in_meta_reg_0);
  output in0;
  input [0:0]drpclk_in;
  input rst_in_meta_reg_0;

  wire [0:0]drpclk_in;
  wire in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_meta_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in_meta_reg_0),
        .Q(in0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in_meta_reg_0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_18
   (rst_in_out_reg_0,
    drpclk_in,
    rst_in0);
  output rst_in_out_reg_0;
  input [0:0]drpclk_in;
  input rst_in0;

  wire [0:0]drpclk_in;
  wire rst_in0;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out_reg_0;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(rst_in0),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(rst_in0),
        .Q(rst_in_out_reg_0));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(rst_in0),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(rst_in0),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(rst_in0),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_20
   (drpclk_in);
  input [0:0]drpclk_in;

  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(1'b1),
        .Q(rst_in_meta));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(1'b1),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(1'b1),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(1'b1),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_21
   (cal_on_tx_reset_in_sync,
    drpclk_in,
    RESET_IN);
  output cal_on_tx_reset_in_sync;
  input [0:0]drpclk_in;
  input RESET_IN;

  wire RESET_IN;
  wire cal_on_tx_reset_in_sync;
  wire [0:0]drpclk_in;
  (* async_reg = "true" *) wire rst_in_meta;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(1'b0),
        .PRE(RESET_IN),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(RESET_IN),
        .Q(cal_on_tx_reset_in_sync));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(RESET_IN),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(RESET_IN),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(drpclk_in),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(RESET_IN),
        .Q(rst_in_sync3));
endmodule

(* ORIG_REF_NAME = "gtwizard_ultrascale_v1_7_13_reset_synchronizer" *) 
module PCS_PMA_gtwizard_ultrascale_v1_7_13_reset_synchronizer_28
   (rst_in_out,
    txoutclkmon,
    out);
  output rst_in_out;
  input txoutclkmon;
  input out;

  wire out;
  (* async_reg = "true" *) wire rst_in_meta;
  wire rst_in_out;
  (* async_reg = "true" *) wire rst_in_sync1;
  (* async_reg = "true" *) wire rst_in_sync2;
  (* async_reg = "true" *) wire rst_in_sync3;
  wire txoutclkmon;

  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_meta_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(1'b0),
        .PRE(out),
        .Q(rst_in_meta));
  FDPE #(
    .INIT(1'b0)) 
    rst_in_out_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(rst_in_sync3),
        .PRE(out),
        .Q(rst_in_out));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync1_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(rst_in_meta),
        .PRE(out),
        .Q(rst_in_sync1));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync2_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(rst_in_sync1),
        .PRE(out),
        .Q(rst_in_sync2));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE #(
    .INIT(1'b0)) 
    rst_in_sync3_reg
       (.C(txoutclkmon),
        .CE(1'b1),
        .D(rst_in_sync2),
        .PRE(out),
        .Q(rst_in_sync3));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XL0vCpwJkpY29C2iE4LPlf/odeUNPw9BVX/J5pEuKj2Daef6TwO4W44ER/rohRxort+oJ1FEnjTl
dO9suKxGx6l5qoEu601AYmdQx5qtrjpt5ZGKiDiqJHQu0sNZj2OpRSMBF2+xpK6q1k0YwWEsL2yM
Dk14qp/TPBMp5RE5dog=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk367+A7d85WWbWihXnmNhli57Ii8GCSlPvH8qHqwzR/ezoXFHJelkpzH2yVZqsPrfmk2NFaOsEs
M1axqfiNh0tU1KMP7/T8Z8SUUXEL8RHmFLGRFGDFU09+/htgWkyd52BTRgIK4xxqdNeHRvHuh9eO
Xoc91nJGkr5lyxxTROPFBa+JdoqRs9bDqyz3atfFQej6vJovFHG2okDG/vCx1XB1qvN+e1+epX31
2giRBGffUGfZdshykZtf0S0Kj1hobLe34cMhJaDdZ+jhjN6QiA9PF+Uhp/S/A8APv5yY2pLwZJi/
lx733RyXkWqUcnNtuuQXd+cbVvDu8Nkgy8Wrqg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PSDriSbxCGy1IkAQGX1Dpf4e+G70LYZYfQvHhkTdWu3f8dIzce38bnZUYwJ3PFkbLPD9xdrPHXpc
YHffwh/sskJmoWdc3xCXegJzAt03leKM0XeW0QDeuMElufJyRoPGciV0ISzDtCccOegxRPMnXkzI
kE04JwwijsIe2HS3mWA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mY+SycwdugcaAAgVirnNdFm8EBfn62CPaeo94BjJZ+vU9m28AxCSwDD3tD06N21maLpla50ThHcZ
2+106fXzJsWtL9Pz+RPRWduaY/aqQj9DI1lsK962ves+UJ55hZpmrK6XQ0LbTkTACnJ+rbn1XOr6
Sy6zYwJAJc8qnHmIgrQxv5S9PmPs3PD3w/KTPcknzXMtlxwEyfFFJv3qUPbJf4hQiKWId/2N0keC
yuxY3jIMroLsnWmLHYAHDH+KBlPKhm0T47WRfD7mAEUsdvMGdJJMQSAz7kZj14OUMXw4DFxp31LM
Mdw8lsakafIjy2kkFUJbghSGrmLhS9eejA4drA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XD7l6Li/98UDd4ASpKYFRLL/Bm3DF1ctodfSWQQYkOkHw+iPJrP4dUeL4uxbw5cmd13HI9d/+bl7
flwuZn1ZsI8+fTLM3T0oYPyVEcleZHq0WhbH4/fAZVtG1KCzFHAkmPbLs7uv7CMumqjJdmtmn5+j
xPyobFsdk7JkDBGTpiw6sLLYNRajRDRO+TtCCooQg1oZ9mbnKEQn+ccjBbpltTTovGTXxvIys5QE
AyX9dO8uSwtGll4an6rSWFnl0uDG8mKULJjCoJCx5igXn5MfbZyoun9fmtC0oBi6/z70Bc7Ngf/X
BxC2PFv9du+wdtufsrRExX5CtLY6SrrVbYmgsg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NnkpyUpgSR1m9dLBiJuJuOGCGzGq+qYsW2dFPuHEdelcqcyBjCfhAHOxsPTg47uYbXrmZKPQT9oB
mF2IFSybwtNxfbYFoozuT0BNJ/5tM80X+LXJbFfCwvgBsytlBfwh0uSzLrHE/8Rj8J7mLWry0qh3
iJAr2rFe8K6RVUpdeiifjliMaSreWEgvFSdo2esnYOcHcjY+Hu8svZHAEUWDKh73U70IF7FdFvqF
XO1yYXuXJRiceHuJPwpgh+dKsPDerxr30wA8JeIZXlrJf9HlT+0dlKVBCNqzJaYEpnPDQJz729Ff
Z07YHgx5oCRnxKUnnjT955+n0UO5Bm0CbNM98g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
C8Tp/eDRRCMOwHxdxcUmbuASA2jQT5JtPZgfJpftbLH97GxlWZMNcwUflF51EUdAwd7Ir0jGS4SN
cr6Uva26gsckiDjhmtq68IVcUBq8iifyFtfwFTkAYsSR9t4iFExJQmqmJhRj/kjacbUMGJYAC6zR
h3ljNiQdmkYQpOt5jaSWP95maYRqXft/7eCGmAeaT/hsFmBP3RQOCK0k9gUhLLR1PO5xnTyZjGQJ
VCk/JVMUOSmN3A3j8uruhVvih7YMqPc9iQBC+HtbR5h4rhfWuy61XFdNoAJHjYVA1tYMqW+AEV+Q
1VtSSnB2mmxlGlAt5Neajfvuyy7rlpFsJ45pjQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
xpgEYrMDyzTrppjK9pdbdERRVcGsOM1wehgNM05p7/GPYcE/Ldlf0NddSTOkeI7hjbtKJh5O+mOM
1DBGpPYqiLVAGGEkWOjemutvTwnFlOgFP/jBtscvT0xoJBauy19XM/qMu2zEdGpo+cTuJWzONd/i
3ghZO49KQIulbxfD2jQCC9rH6BOq1q57AbVoYFrWhtZyeWmQYWqoBBCoKhU0mW4HcQbiWcYymJHT
F7Wl3c/rvmZ19HaO7JHZa6PyhFnE8YeyhkUhNO5fcvZ7gFHlRumoJS365hjRroAoOu/CLJR/eLzy
ipT4tHFj/T7mhSJUeLz7A/6hK8fdFLzSZwEuZVstx+LDWxZ6pst0+57+uQ0enpOHMLlWG7IDZ9AV
vnJhH0UrMMbR196CYsdG3cIByN27DizesnW+jNkMQBaswtDLtVZnbdkXy8Zk9SXNXJvTwQegCw/a
5CAl8y//34XRWeFt4Wtkeso5A1iTLvpgBuH+GJMSKXA7KSxJoCnBU8Fi

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PtXIj+hfSzAR7L3qE+PnK05Exl2JklQ0WEvqE/2UzQ6NMKlYocvT6ipW6HQPMOEIcQZ0yLsnPM3H
AJTKwnCXBrDf9LrsG68+NcVRqGYlmQxBA+B/Wz13Is/n6cNLZF0gc3NyuJtBtL2Uxe3MwscxIw7q
kdbu2/O6Cyl0g687jBXJycalF9NXdTP1rxdkEcnqKylZS7CE4cy54owMRjqGSecZkwM9W6KM/LnC
gXlHpN84ld6K+TZYDQX69vk5C2jSfvikiyv+hOQBT9MYZBs7WpN6ZB7rzEIftz7mRrfVTftis8ny
vl11eoBQKss+QRJIL8eXborkKe8di5p1yilcPQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 148912)
`pragma protect data_block
oz9h02KBPhnaD6JMhhwJ3BziSoHcMUXxVF2yVEgAKubuRiEZIG0HpJngl4ZW/n24bOpZ4xJaQTQ3
EtBjfhNNiqiSP7Si+PVm3B6STY+HsPEznbhc3aUBIKGbdpmOeGkxQjEsHjqkBE9Tl/JJGLfAFZrC
/myp5Ol9pMj8HceqZU1i7rZDHPj4xy5sLV3N37Dk0xg8MZFoIRSEBjb9RGWO/hCJCrMk4hnBF8eG
+ddVffR3ZUJo45TZWEZchYY5poeqWHyfh/pn891SexMlCtPkHKu1OYzHt09aaj9aYZiuAC2si5UF
o20ZiuyJjBVjwfy2lE7YAak10orCwYwAr/GtwFGNnYjsmKGiCNe9miGAGQX+mqBSVtgq+FsX3gFE
RJxBA6B4P7dMeQymqYWXeELBz1vN2/qOyzGoL05e/HuwlGI/If2V0TRxJ6VyR9MLLl7tMc92etNS
YjT5xzf/mmchXP66ROEjD4/n8bgBUcCpIH3rnI44pp/5O1XGrZuY/8tPOelsUIxh+NFkWxioK1qw
NBD5ozRTASDRhqaBSu51jS4SU0jUxGbySmci8ZTJaxEAAi4H+PadPqOdd5kFGX5bFnAtaxzDQNib
53DIEFAhrKeAp6UazEPaImUAuGqtmvKKMtk+tYiM4+a5URED19H29nSmLIbAEMef+A9+xTPH7gRc
/rxzpd6Es3GSvk5aUzzjf0V3wREoapL6sGUQxVjmI9LIrSoJCldQjb69xhQBCq3ElYYCZmP9n1+w
h1PipNPozQ0Xr0KlqdGQ+cDrmJFIqFSj1o8XvhyNLZsQ4UH7wDjJHGFWfPJIoTcPx2DRc6N7Fmh+
m+fT3i2FyGOdbr7xogpRkvQK3XXTzeif7pc6Ed4k+pWwt7VxhOt89BdGb0BAib5atdGKH3GQIhpn
At8xFvlDVy0JObGWe97n6FfEsedUgqilqt2iIaOBEJamVXj7flW71TQqcpS6qGbqGeF6BJGsnj8a
SD3sdYV52o6oG75ceo7zJcB3eGEQQZ0XrfdfCUL5sUSblxMfgURT91FwlN/wdAOWatTkw50nUSkZ
r2kn33pjgtil78da/sXQ8sRzirV2Xg81FsBNIMPcrsbyp4HvbPGTiviPpweTF0SN+on6yCkZOzNO
H7ETYQUNoF58zYd7D5UN+DbJIH6NIbFwg6duWQq2aEcRCD3DrIb0EzAyLWOHmJzFXuCKu514PzXy
QHCjOgmaXg1Q/cga2/hiXLXwnUEKcXpgjjg556jPuTSEzj66h5c40Q68U1ktPWwwct8riVtmdTFG
rhHbAKmQglIli15nLwrUxzvtBOR3ngRuz8CnDAHE5AsKFWvMF+SkZVyKDqI8VfY0ORUuc4aoWgHG
AdpipidWacO4kTPJupB48Q2k7PZTEHOY7WBVFvhVKQODfdSqW0zkthE5UBhve177LzwSzF9njI2v
Dt9VOjdC1T627yBg/8CPwrcWmhx+LDv4FfjVz+QAn3sJVMB6Gy2lQK61PWNgfdWypgDFqYTGs2bT
/YUr524FD+v5F/RGQjN0mh7O7ryiyjRJTeFhCWV/4j8hJRUgJfvdookvFPFIw/kvSrlHzp5FTrgh
fLUXoT/R92egUY3YwAgIZcYgTcXjA/G1+eog0/OjHvkHjXCMt0CThqz6vxJ6iA2h81i3eDzCRX5s
sJ7cfBX/v3cEEDteusgkdhejlPB0eRL77sCAaJa1JnNo1VEbyOJyoLMcnSKZNHaNbFalLTu5947K
KDZzai7nS/awKjQXu0QYEhyKsJhbpX8t/zwdaqMP/XH3j/0vSZAkZMtwhMxVV9c38VIE0xP8kHUh
9tjj9vGXiJdnyWVdZl9L2hC+C+3HHND8OuHMz9dUlETJGT/dOVWOA7xiPZ6dYQPzE67gzn+9VK+f
XJJvbH24rSCXafS+hqobg/cKlVEA2znR25yo4qOns69jpsLqfFxriAvTXoRKk+snniORJNrPOxPh
eZSZIqodSfuVWZ8F1ZV8zRvHy/79dL1SsyK4WzRoUhIQRxFswnEPA+IDC3hW3BNU9VRjVkfoQ43Q
MUj2VVe89qa5hYUyuZwzFv3iSzLmrv1xpIivsH6VKogy83+Pe0oV7w7uOeEkp3210h7ftgGvpTdt
VdPRV1TCPU6k8StwdQgE/o3XRyOByW6i3VvzIq20t1M3iM7JRdKLJbbLPqaNYGLT1hfreneTAP8s
G02VX2F9q01EYh/+CKgqgjYwg8HKm64jt9ejBvxbS3ZY5atxFnMlSf0TzDamBR7uG6KGYIn/oyID
rHiUxrPICodPEqNruKujW5JLB6HyN3af2pfujVgmYSURt1fm5LocXBOSVkvFZJuirf7/XGN8SVUu
7IKiG23R464KDsPRv2f8yzzBFNmSurtexCV0eNalvAb3M8QP3WpcEoelxvU2Cx1y/EIciwdd/89E
YKPfJtysTVM7g24VV9iYlc6FEYZtAQQK1p83t0exk/6RjcyYXL1B0zI0vv7zgdnONZbqob3kXwFN
hlyY/+ZqagfTibYVaBcSZiKNkCcYg02F5faX/3K/dLcHXVFmxMdZuFZO/kl4uvpGf/Jh09NY+6MF
uQbjWmv9uIHdB5r7SjsFN0HCymlKxzDvkxESzs/i9g7pqdSu0jNU3HFpiGlnAy7c3rxV0DnmU9DG
meYjx0+/17VAJnViVhWmwjae4U4L/tZIG//kighHRr8A1WDa9EVvsX+0mYrofFtTCRqZqqmjM/Rb
PdKXhgFEb/DEeyVUabhsVg+55KVqVh8B0gaZvwy/j874cbY2ihA1CQpFxIVEFsMkZnu2qWNLyjfZ
4lkUE+Xznjd0RNrbh2W+KqQXKw8GY7ad6wcPcLgC3gkny+V4P6zheEAnNKPqug7nyefo4GLvLP0J
Hm72S/7xYRAqH1bY7AHEalPqy4P8VdKscAHiMHs9skZQ/+iq430U2fnmEEjF4N6glMlrWCbA6Dd/
ncB9A3oczra3DW7DV6E5IVt5cTrutsnB+pSw80xSDm0VPs/fZTnzzP21kfupolvy7/41sh2cWH1W
EQod8WPDaEx45vnx6xs0xd5T+RcUQwB0E8wzc5UBQ/s5JutY7hWcZbdWpnCSXey1RdAtn4el73Yv
phOR+/F3T+wvlQUJJf/Izhgm0ma0IcjS/08sx28zNIdYbFUurigTY44TidmRD3Sg68uxkQaopqIW
bkOfVXiSitD4zXhw2c1Q8nBYi1zFWQWGNKGwrChJDJ4uYVn0Izci1yMjd6uvKU303fdcsHfhn+Rj
3HLLaEzah0omJlVkZOEoNeF8XQ+v9MLGE3DBqA0Gp+RRA5yuAFPpgvyOXb/c8+IC/mdQ/nrG3D2g
8h4bEsVQecNea14z6lvsacCYX9ZTS8A4Q2db1GIySoDRq6ty7bwCOj6zsjhs9bhNt7M2wFZ4rWFg
tUOZJzy2JZAyQT7c5TsTeDthXCHXjep852/9LTjyDASIoXM82q+LfJKhzgw2xJKqOTYtDKpgYcN1
wVO++F1k7wxelKOPlW75BrigYFfborq7I3xoiDUV/+aw32FecAQumbwFwjITy/K7wrDkqHl6HoF9
+2kiuQ8O8at7R1tN9J5TrMHXoVtPO+wO8wiSiRK53iNJ5vBTXBjlDDsxct/AUMqU69A9zUBqILW5
wUDcMAEWx/Ej46zS0Gni/NpL0lQ5Q4aBr4G5o/LHASYMiIM2573U/l+NbdxvLnOuFAM9isWmc5zy
J739i41rpm/+TljBSbZTtZyS9GIzC/8ErK9tuwFOHbZQGNzM+iqx/5m7SAYjI/D2yWAesDJ6zXmo
QA8wVD1RoBw/oDiph8B0hPvJdZBVr1Ga+cEyE1kYkDd5CEhzoCfaYQsWMTV9zlVWKSy9mKR0Q//V
wjO8QSnZM1bsn7T4HdAAWC0+VMMqtQMjOX+3896CwesurYK/+qGDCfX0gd+dmx56tYpUCmyuTPeR
LgmdVyvS6ahWRBU3T9S7lzo7AvWjrDUwRFj9lERD1VNqsCzwYwzVZnsMs7YwSTbvqZT8WcDgvIhg
ldGkmYKH4pa6MGq0GaTlfh9PeV6nCHGEO+TTkyL6oCDu2EXDAchLcUCz7tvWi+bW8m/uVjZ5Ohwr
o9AT7O+vl/lo+ebNwYGeCjeorJDFCvirH5l1vBz9I4XEjQQSv1T/5Hdha4ApXiYYK8kLXqlckkbI
hlN2BBv10PnbVIeyp1hj8FIW137DREmFsG9iQj2+w5mi/hOmPIum+gMIIceaVPh+MhZbUtjFrX29
DHbM8j1gMgoRvRzfgeZPKqWvfWl+cVpEvohHDVSv7lz2/eo4qB6ZDxnVzLz8Mjprzpx7WsGPqPsf
fBDjWcdxqMX1YmwqAqG0zoQut+KUhrqM52+wSWjS4xZM1SqKA2Opwfn/LSyBbzdBNUh9Fr8im6bP
dQepx3f1vAAb1sxK9rBOhjQvj2z+NjSMTqBpCZiqgoepJpAD42gle1gcTQlJhfRXENmuSFcg+WJu
73KE0f+KZPTcoaq7enb0lt/IeMAOZlYg1q19tVnU+8OGeTBascBT6X8PIbE1T5RMgYuVGU67N+Zk
t/DhB8GKI7rjkEQ5EwGMzIqMeLSqRQFfRmElv1RUSxUf5pWkf3Vziqa7K32Kmp/M2oJh1MfkpRnB
/uqBf5IYJWuXk6Ft5squPEHxfOE0w6Tb7fa+Z5Hb5miqdmSzwM/PAtGHwuRXpiELXGHvBm2fk7Ae
j8BSIRSKqx46xtnngNpKg5IDm3UygKQuKMrRjWIM8TgqaXKVPN0DC9cTxLjNj/qv1p0+8MlLI4sN
bJEfx3FDPVz6wJ9fK8LcMCjcLlX2+qaxAPDSi4eeVU+V8kCxlCpF/4XV9UlZxlGZg/tH3pLPBV2U
s6yCV2w7Xb0YTe/NnoGKfBOpD9RoC1Yd5NMm6G280jolGg3xhR2zeEhsB7RsZ4rOfpLf+DKVrTTY
19a1HTVR0qF2ZkWKGAYSfgYYrBt5gJm2wlQT9RZs+fg2FxPfv3FIJ5TKZr50aGK6/hGgX0t4uo4D
4Si09Ia1zkNUTWN3ZXRW52eN/5oUdX3HQMZC9XWrU1euZh3JWE4mfk5HGlZV+kqJjQFfAJ7XUXum
htiyMizY6c7oav+Ys9HbWK744ohfKX5c0/HNAEjbaPbAmrIZ9kY0L/pmcllW3nWkYkCO0hFvS74s
sB20s9t6HgWZPr83bRitf+c9FfHMDlwfql1LCImPnk2P8nEsOEtYpBInH1+U4LUWybgs4qw7yKG9
dxfFxwxth+lQOUv1blKbeXxN1b+yNrJezCyEEuphaqmV1da2KCbBzR84uIyQEonOlm1GSsZdxHa5
ze3mbDWwMZWF7iSEBGdx7R51H5BQWHa0rluh9p5srDN2MXajMtVVydSgBK6xigCidYBOmnazhhdi
1/+o3VQt4xOWE7LlQlooXbEIUnU+czRpfnMDOt6JB5LGIF/AxTP/ff4qhRfSuKOcaFg32tv1Wzr7
J7ChdT3va12FeS2w9VbmeEePNHwoBRl2Eo3rlx4l+V7qF0s/DFj/k4HBQ7nikcTxSPvr5/5Mj0l6
o5D4ES8vOS+7ZEfO2yjaLAh2usKtWdC/l9mRDsTctiINDFFHF1apz06eGHsxx3YPHW9PEhFumFxW
fkmzD/1atE1+lt79mlt8j9qsFYk/2SrFXEgFO3QNML2Wrbu7XGlWyYeFcBGt5WZeH3chIhrLCWPg
1bquToi+ezFeLDqG+CJ/Caf2g74xKORp8apmp8q628FBH4cFj2cR/x1syzRf8ROb+VDw6KoiXLpO
tXIpW7rlSioLWDWVPbd78GTC49uwfuo76RFg2gaF11fRdYIGD5SL0nYyytEhII15i8oNvb5BisiN
jOREPEkLiJECPP6KQ5Jv+vxWw0+3RejchGuf6nIHiQKo1wP0yNky37sPCV9Ca9EcSxtBP0tqOP7q
7I0JLnqddwqeeNYtECbM5DfeN8P2o7ZEBYL/PIsnA4t24B0xGGUnMnJNP1J3AgjwkOxwpk99DXwz
2q3l+nvATTLl7DGW9Tr7ifQNmBO8TW/WvZlLqkVlmqdvGrE2HQ5vRS87qDtutvlb3OhS9WoQyn7y
j2MTYGXdU6DK9J+1dijjKidoejwjkcQIzvG6jUB7AL6OyWPNL2+QQ2afZc6S5D4WwwWvtSG8cyZ3
0ooeADvEWSfMvXQgnsO6uR38I5dVvCVNgabbnAFdOnuwdbBZA30fVTZ792Im0rpP1kg9p9BcOHb/
GVTgFm/axnnDrp+Ko3GPyJ2O5vgTxMJIH+spab04dL38sZGTLj5E+HqSs6836U+YKM7RLtO/RKay
Zw9YlY8+OZDUUQYR+gBwZUUvr/4xKQXbnouMa1q2rrorslUfo2oRhNWNqmk994VfJtjkHk+w2ZTa
uex2R+45AhFenShVXvtkO+1EZUPwy9bjMXS+9LmgIsUtRI0kdadGEzz6/cfJj3zRCo5sLupfncz/
7kJoTxMWA64g03SI2LMZnjv3cyecX12GvEb+kAYQrxLudCJgRk8+CDBnQuqfCm0ZIt4P7fOJ9W0t
SE9TisGG/2wWj7VXoauQjC/syVRNAHOCTUobHy5qNqpnNyT1AONiWqFc9vs6fjpV0cn32wYfG2Aj
qLRQV7tw2sB/Kzev/0p434de1N7mbJnoVYsoFOaXEIKhr2V5Vy0GLz/i6+Rsi65tOQjgFf4tJU1w
QDm80MwzajsfkXn3aU8SRh/IgL1vrcUM0QChgx2Aka6CC+y9mJa36xCoArUxd48I/nKMiom/lyri
tALVQOToKULX9diZv3k2BLN3NVt/DsiFXinxpMH6F9r+0tu4qNn+tUiBYDgDjnPCN7GLl0rltSIs
CSy9NisBzV9QfUGyiRKHYOQGz4MFBKs8S3mdCfq00aptrpLUg28IIFJxjlAqST69J/HqJW7xa8hH
8Y8W1Y7aXDsjYmgxe2dHBhK7VnV5WbmQfdcgBpqCP3BAMQEIC+EN9D6Q2ZlmDD+gwGDFt+cnMTlb
La2af7LknjTq6JdO2scvCHJDx3c3YNfZ42iAojvHHpe5E6R7KXr2QDD61W9PJCaOsUNqk8wfU1bb
ht6JZYkHONmTHSl35jntxfkXZUe6Ox3iuqDCAeXl5VaXgOnF36QKmVNCnL0RzHApLra2iIfogTrJ
9Klk6AsZE+Jl6Nqhd0XCuGPOwTBt/S8hXq7R3XcDFynlGkQTbx5Ylrf4uiRlyJYk2O6vT6yNG4rg
IJFXSX0zBf8sSU9tLHPQC6MFqIdr3M9dp4NVtUmx+N8I2GM9fLoWrDPhGxKg1EE/39PAZXDYb/B+
lK4BGH/qnADmKwvh8tovDw5Ql5OtdhMWE3nsMRbZ8brtA9ICiM3/XbvpU4/aPlur5hG+X1km7z5d
zNeRBwnUqZNbWv6dCVxrxcbkesDEvk19mJjl869tDgZSOIUVA3OucY4VcC8fOUK+XoMGWGEKOFPg
7sGxhSLIXyC7Zvy7Q+gMpzmLPVV3lJO7LPc+tswU5uBdAHrDnuA/+LdIOqWetoh5+5Rwz5pGz52v
kgE0SfUvfoCIiaVev56Td+n0eSGn+w1O54rjThUZunjsp8eG5IJ1f1NRmEOyujB16F1EVs1qUGAf
jTnQTKQ+y9M+IlQNE3mXGH//Y70+kyEE9ezpi9wh94ps5jEk6+NKUqdMgaAP9dzgnIbRV2C3me7u
vQ6/0dF2nVOVIq8apQDHmoq8wskoXZEKoV/uwkRhcEloS8//KIFaSMdlVPBkBJOjOOriHng1jDjE
vTjsmhoSyhmapIgy434Rhf6vmqI7VuEV/Y4g2/vLJBUvyPgySCx2X21HWr3sOp2BzljgBdSu9nX+
G0sFPJjphSDsxeIftJkGfo8dvDn2XedCVwsCV5LkH2im68xIvDRucowV/j47v8QwCMROdpCzGxiw
Qs9pzaceHJ0USge880MtwP4IDQ9gl30fSC6bWthmVcIeFF94uyWg9iCfWqYBwmD5ADS2weqqnxw6
5/Xv4ZAm0whgl3BBVGUzXxo+/z9V+BJIbyqfnv35LHk37OAtOiTVgULw4pOXfVxBr13YRrbIS2Pq
J+Cw12vXdByXsZZRWF8Y8X7DK0A5+pYIRPO8hJ2f7f+HQdKmjgXDObmUt+uwvONhopK67uTOpMVE
nQQj1tKktEkoAvQlBqonhyPAXJ3l1/hmDDAI4fASzhngU6AXK2B8UxqzlPH2IgiaqP0E+Oc+ZQeG
j4nFzd9E/1ZQz30056Q3Eo5vHxIr63xpLF8gG87lLPXoWdw2Rej5cnXGmMzQrVVXjr0F0Oz8dKwR
9nzOiXyq/CXREfPNn5m78ImzcLbRgU3lGhk1c54tbuY025kwJLZ58QmexICif4V8ApyC5l23ozsy
OaS5NHk6wl7u1QR+APwZ3B2TYqiPV6Y9oaPVE6O3xANDXhnPTXxx/gQ/MgknaW/xNS/D+2w8kenk
xrBZo3L4QLkhYLb53y4IOFCT70upXm8k7Yo5RtpB+BzieZADa4TaE2hd/wAtnGnMeynpZB1QeenF
Gf/7+telB8SOJqaPQ7RQS4y6ZmTAabnxlAIS24nt2LB33OLKkao0WdoDx3nzLrPfRA/0uL217iVJ
rtAUFEe7TI7OB/BSm182M7FI2Wi8t57ZNGFIy1oGoL6Tm7WA+Ux8xTGCnsn3wvkr4GUsRwV9Nx0d
Rr97wwiOkM4V8XuUwnWWMLG3Za/mUOC9PARX17OS/zmbtK/K7PcupGlqU5GBtCs6A9zeZmgkoYLe
6RSI1OITGDPA5bMLQKzpe/CHmFuWkeLxLmRZc6v1iO4dsHwsSeZdcVNBW3GzAjh9XHaisx8Z9SVP
NRpxRGYLTC0aKOOX/+U/c/UhsUYNGmSwE+lJKEqoQ7p8v+J+tW6MKYDM5uHxal4UNo3PvJsk891O
wtsoVp7Vs4+29/J8n+4Y6e9kZLHHN95ws/ZGt+PQhd7jVae2RdHbXKRpLSzV+uQ8orCDdYwOBG1g
JFbtQUEMOYh7ByteaBZjoie5k0pVmDIab7b0Xbk6VM1jGdI8OtBCZJFGC+Jb7OqZNfrldXpxqE1t
eKoWjnP2ArZUlSFdIGXsj5PQHYpIwVQiuBgXYGG8FRxteCyY+8qXJaNyDbcojC2z01xy5I709v6v
uYrlU/cWQKNW7xqMl7tita4fxKpoKu6qUMqEMm6e/uXvKyjNWeGiEbS0kAiYZk5GA+cQIS0BNsFi
yv23BuYnCVJc6uC2tAXPdx0kZrb2VnTqCnhFap1Hl2L/MdCIN0XH/0MPb8Pneu22O2Hyjsff2OuM
e9TiDYKo54LUS5RbuS9wrPFZoCijQu5vSIg5Z4uY8t83UoXMhWKo0zZiVI+vQdMlk1ydjGmJe6lm
kfMmkIRvM9rcAohvTZEpI/mHeMPp2Th6rqb410f72UMyWM0Xplsc8J2YFiFySI+jS2WlebmjWTmw
8EnRX9OgbBz9D3BqB7NqFlBT4ZVa7kRGfBD6zxJCQHGl4M9B9Nu9qEjlmW3+MXc2e/+BZCBvYLl7
XgmjdlmMZwsayBiGpVwLIpdJLRLIqYl1kaH+Bb2FVfAb68QgKNxOcNktCnGDjbuhKm4JOPiWpWvr
D0qcrhk7AhFOeWOpfTYkVW84WmzD1mqO/yCeuMuCOKcpXiW7+AOOiMU6rQKbZaBuHWynx1qOf07a
hOTYuw/kzQw5LbQUCEgMiiNPTJZAjBJi21/+d+CqLugvw85T8PI9Rr1UFWU/U7MLz3P1fIaMgqWZ
vk3SX6MI7dlUwovHuw1GbYINwoYgSBm/bM7XdPGkm3UdVrARcJEqDAxEP7eN4Vbwjoe2EklN+vQo
jq8FL8IKqqr46L2P1qp/3BBEyfQIBSdmRBw5JoyhD2NkLkHjNfy3fDxkbXiCfYI7HFEw5UKgmsbd
T00syGijAvaWXuQOkAqCCaGo2vLM9z+I7JcyVYWuGWLko+4fa0+qh08inndExI2hjkPsxF8cDb6X
iPMZZAgwL2siwFYOF+PigcDRM6Eh3Ri5lY9md3AYaOnPZDSQb1x9r1h5RD41OMw6sreF+8nsXyG1
0SCs3yuOsNY64iN2nFIzjFJe5tTzhW4Ch2rlKXmVNae8PXI8W2rERzQxjD0ILnz6QoZiPQ4sScZ2
Il2uXaJmj8t/vd16sXihCgq+yxISdJMgIxrRq/P33xMMdUbC/G3yBF5ut/7ZnL3/ef/9lUM/HnDB
1WAjnxoE4W/UBlahQfSrf/81Lj0mbUSpEVdbx5Xh413hnVa9dfS4akNdI70yONaGTg2WqRTXs62w
7sqpnVhMS90CGGV6knbfH6pFQQSV63WjzLkHk7W9InR5t1UFXY2BfZTobSN85wLhKZn7J9zBGXEd
u2sDpoRrzvMmFTKrKhrJm5DCIJcklVc/6UN7Sah2UHiIcjM/SqcwSa3DdisLv80WpxFGGOGEqn3G
RdrxyyCA8DHs7Kg1SpW009UtAXrOrzRZPeu4BkTbo6JX+GNjeweNJlmG9y4SVITnUf1fn04rgZbu
YI2JjuS5XlerWjutl382ge8QdSxVsbNpl6s9gcftfGQVn81Jhz2bH4vofAvA0kNzSj+Wzlw8j71n
fRD2KserRqCKOlgOezZQ06Z/elGwxQwz3uL2Z4Za/Dw4ApKCesrRZdLwwi1WeclO2jWN5gu3xrLO
4RBleXtjJcf/WdIEJzJn0SQDH0sg8LzOVdzyEn7YHa8WhjDrH7CdCxeXlj5oODKMKztQ1RJRo8Yx
83OKQpTtjHvhJocrCF6pPOMd/qO6kA39JlO/YyEu3kBOVs7c/A5VVzHHPTXmBVmPOZGtnep8J0pN
63hPDj/6jFwop1P7Zr3hxMESIKeF7kzYrW9oTMsBhQcS+vTeEyhPXcAC2esrwE2DHaAzImLqIuX9
8560aBYCphSALlH9xOPoq9RCF4Fdk2Ap4E6RF9yHqGogKzpZ0DQqjB/DaaQriuSIIa52Ml8iUqsG
4brE7saDoP9Cdo+wbEFxlHNrPh/szNzuXukOe/q0kyJAGRUCmFlYIpPHvFP+O8thkXeLwv64rGAT
Orf1YIwJLgZs5chFIZwmQ40byZ+MajQpPkp26kWUjP/WFnokuGKQA+0LAezTFlbHgk6e4ZFBkJAN
CFeBuz5d5uM3Hu4mqOcIjWMCzXJ2On56Kix6K3JneR3uMf7f2m25EWEObDfwScn9ZDqW/JjQwfiT
vlgW/4Itk5BU7rQYz+ZhgFWhWcz68ircY5RpyDXNhRGawNSCNbkMn7ZEgy1A/QJcNWOiEIKXGcyG
0QyECg+wOuobjP8bCjQNxpURGSsGqSwsqrDmsiE9wdrRNJDPzWc0fR1c7k5mfhrMbmcVPgJkVTri
s95qWawaGYO3EyYotPzs3Z7liid2VLCjRfodDmHe2I8b+zTPeh7gyx2vh/mNTd47QDcTVA/Egen8
LCfWBF8ppa1MG05DPSxnj87XSc3+w9xRYqNIGzBmxsEBD9LCQXCNmMLQonG/hx4Tqwlgf3L/FIOS
kgB25t2KV3jttmm2c15lmOSUbyB+wc7OYNln8rG2i+GGY2CzYjWinpm1HPEH+KiWMT9k9w095nWC
KVbtPnU+naEeyHzHt0GbIzmudOp8AkjF70Ya+ywRWBUIXoao1olz+mdBfypJi/8BKLIsRpPUMEg2
v0/RcJ1lyLSelV180dkurT3OsJ1zEeLcwZgCgiFxamNqQ7k7OWYcBQnAdfJYt9P35/NEmASbKhG2
H1ewPJWzakg8D44XLJd7awOYoAPVeBpNudpDey/Ds1U/YP18GOblVqGv1ESUQn/QTH+YpaBCrQkS
gCy9TgDmJMnyx5cZRTIspyZXn3E9/6EFT2GwvmSLvurM4R157BiH3pAcbjo1yCVmA1k20oqy7Zwo
UowO/oaRoZcrTNPZMBNc+NA433u51adZWcsAyIbhQrIWqPUvZrxylfZvoFYW4Ia01NYaj3l0u13D
B+jYLxSidSnN1pMDBI+0CI8/p3Q1keXrYg17frpaqGSvfY9nDmBM7IlRdjTLZLkL4VIe4zFp5sR7
SFDnBz02OI9N6qxRBYtXvrtJi8N1V/tMRE596Y9CURZ/UCpjXcSNs3aRLd7k2Jroz26L9tpH+h4n
gC5wRtWhWud87AwU4ShVUMoLJoAiUhWVq0D3sR0ZiT2GIGaVcesluEseCtjRqIVlx8NgSF6LBrjg
qOKP5wO/0SnX8vLCtBq7PX1Oj1JV1N1sD+7E7oVro645gdfYGi3gaJ8kNp84G1fyCBo+oS6i1rlg
kyztykZlgE4JE5tYSSVo8T2llc1FRWYOMgDxaLZXpO+UDpheNlnlSnf6yLFd1fWkSRnqnVXMldMO
hRVt2fV/rD7l7QfdgFkOmt++Vp4WyV0lMgL/AlOP3Tjf+okOmaDp4Duz2y/SiBRyG6ZMulJluygd
6k4ZYC41aDF9Qlf5nqKCmC7EGJEEKthwpWOrvJ0UdIRGCu9XdZx7eC3k6FkqK8IFLBNRu70+lm0c
1DIz01u+DPzWdmKvVvd632JlPuiARUHVDIHpRur5NybgMm6J0zNsienSh+8glgyPL8UgmtzjkL1j
QT90qbLogd8bANXzmkd8sJDB7anY/9xBl+vCVyOQOBusywpagwfhxAXQ9hMR2T4BI0eHeiXwB7/a
2wr94P4NNe8F6homDVQcnBjLqCgqcmTldQAP1+z120IiHwp3hFopfQIPGgs1t0VgTJzw/6NuUlT/
CXJ1Z+DE0wwd7LBZ3k1d5WfG1Jsyj6m1X74ySdlcxC0W0vNb0hIE9aygNMPJZuRPGzYewEwCLJdO
i4RDIIerWsTmMU68ZZhxKta5oVVhWHM+IN28TFVdHSduJ3cbpca0W9Vpx1LKUyEl6Mo41RqtvK+E
QxPcwuyeI/dK2tlmfep00mTH5uPkyAxTMVF0seBZEgXc2q4YtXYg6vGx7Y5ohZPTEMP3DMBFpR7d
HZOsGhLW90kvGJMVnBUk4ozQobomxZQt8Rl1T9M9tluGaQD2/wSjk3AhevBxnbfnoGjZSWi9W8Mv
wOKI1uM2PbXJDcudkIIX33xYaMNUWEFu+yhllLu4Kwwwz1ezdjm+r6132NPTipXqE68VyGLudkFt
nmWgfaBguCedhJSSP1Eo9GFSRvMqL8rJNSbJKjC/oYWaudf3YNAJpTTcDmPw4ugv0wgyxgi+vW5d
7PxBQzSCfY1p0vdAQ4GlHOCpCk0p6Ao5GiCgqeqgOwupSYmUBqjBAT4c/+8DbcLmCDGsJg06VlUp
q7RZP8WvSdQqd+mwkQs3ObCv0hU35afFaXyNMBZGghbinqtsGMGMvYZQsvH9sjmA2pgjmuwM4E3A
XIiPAsjJ8puESCYEo23nYOGQ8qVe21O8Eiab0cEoG95ARBCQjew34j5uYKePWXhz/9HdzEnz+9iR
D1Z7PmtwL7jVfY9C1KggrbPFKcESC/5+A6Cm9XZ+WVzVTH3wkQgwnpKnPMoz+cxZVY6Fjz1O/KlG
t0YCBeSodTVXDcmTe6c8TJlbj62I0HnAoeh7vnLzo2X8yFiPj0oGAeb8lprwV4T80ildHBgC+Qt2
LX0xy2bGcujwqkxK6+5iSiUjlODQLLkwl4QFi2d+Vyc5uv+xJikany3iDXjIxuxC25ZOfpTmlekG
Zhtd4zF0PEi3hoblAZS2NW/eVveP7ALlnDa5qfRc62CV4pLC6Y/O0Kfl4MMgIXL1eQ7/zvb7vGHS
UwBc/xyyT8PcSXJaMKd3AK1KR0+CUVFW75y8lbsQTlbEF3WJputUa1s87jI+ofNLfIrWlWxjWy2P
+Su/ssFaH+TGoil3XM8W5huap3Xul8yi1YEmxkxPV8FYtANi6TvjY5cX4w8NCSzN4O25acfnAuy6
Mv9ZHXRj9DC9tJj3gTlQKcALhrfbRrpfalBLTyn9kO6hmoChNRVT4lGJlTQmVMhew+gwfDYSQ3G5
d9V++K2PpwO9TsAnO3pgXkR+wyohaS+WENBejOQajCfnsO6i7Lub6/eVnpZilnaCF5YsorzK6N/i
+ZN3Ziw7vh+Naa8FYa7c11ZbKSz2moSX0UhWC+G/+6+MSwaiaCe7mDitz4ZIh1e1h5EdYy5bJ3Yo
kQiRUQzkiQRex1hf4EvNJ31f7P1BLfqMzaW3aNWyO3LagDsxGCCMFQf9Ven94Siu6vAl2sWou/Eb
xTSXJQMvN5JL+CPruEmWkxGh/1lkYzPwXkM3j06xpRdw6RbFGNBKM57PJdKYDxn2bGfdsjO6++xa
zRiWGQr635OH3qI7KyttQIWyhR7PRNUJ4DfyBRaaR8MseBuSCKb+n4Sqf9BSYjjeGwS6NlXMHmbq
X9a0WfBkde/zA1/sa/of7tHxy/usDMZCeliscePUElLlqQXfyK59ZVw1UhEttS1AqsrMYgJGUuZI
S+oKBOEbwCFnvGJ5OLmKEGlJtoAsKCT5uetIGM03zY601fBjwmRMFRbjgbpoh9AEttmGYxJbJffZ
6D8YwtmgUqEkj+GTl7Il3oxHtIUPOJLq0YXNjgRVOraZ9YYNPLTES2eG5/KD4T4sJSXpnoqjMN86
5QZXqh/YtAOYW61oyeBJ1qWzTuzkawVO/Btnxcyr7M61JiqThnar4fsQlkn4sqL9IPTWbX13YDBh
AZEp79bJEjWTS1A04SRKyHJfYeWKyBgQjbtGHPYoBuUnYV5UtZeDSW+OUkRT4SN39+2CIMzMt1dx
8nfFFfQt9tBw4dmkacACpVvBVIXEX+JSUHr6eOpnQ1VC4cGB5iXjZyfKyCX7WMbU9xv5pTi+hhmw
Nn6Ch5bXs//iszfcpDM1cCFjxq3Uv4AYZaLnUryaCcpVHnucBHCOY9CJGxoXGCDT/BnEAI4rlND4
XMNu6j6P8JPdZCClsHJe42AUpmlurBVSoDusOFnTpTpktV/gN4+x8JroCj4p0engN86lSgnlXlW0
EqoatNVluki0sZsMkYRqmPVNwVvzlnqEC6T2IMGrQ+xVuZJbyZuQTCJ0nK6/XWYEoLTIbZaXdIfW
K7Eu4Eq59lXUEPiVd8Py4QKRYmxSNSC+MhKipUYkPXCeltOrmTwAjOZ8XmotBp5R4yFAzXElaJQ/
FRiNgJQyjoX+9zzAOI4mRNTcbDiuTndWVpS4uFVrofBf2awXhq0M/fZplQnipLNLXhb/j8ftHdTX
QqsuhugSBBTKibIwmzZdt+T47LfQ6LHMaBRQeyxzeqLBRMEJ/wOjisIYHJM2gKEweVwvVlNxQ11D
1RuLc88F/0QBehJ/tiU3gaNN0QJeN223+H70XvXFh31anp3PhqziZe79AXLYgKOH2a7mUH8zcVOz
OT4etecAdT6s4eSRjiKnrIcoWGJAW8Y+VOBiXTyDpB0VVJTMdDNNACwFH7J4/tfJOuWD8tcdwZiI
8i/BdcqOjFprJwj9nLu7QuATsd2SQdAcfStgPuV8NECSvidZAgWf07gV5MMvLZmQ53ttJToldLIy
zd4ZFI3KsTemwNiNKLGSbpdMIW3vGJweg2ssHyKPdDoGTmII5uyekhr5syi5Gf9p+SsUGuBeHbQK
F9Toj958kb0l3i09GWek672f9bXzS1UUR049mAd+aBSttMwDQL9Tmdnhw+9pghsEkBE+3rJ2Gpb3
QepGS6foyvoiLE9DNyYeTMtXZSmVxuof/YqK4b7XSDykgtSItiKBKGtzEL3dlIaG31JuLlzcNlVO
IxWYK/53MzTEfl7A2PzshO5BQ072NKWlRkWB6HIOnTgbp+pTnNcKfkaz9NxbwqAAfUOl9QDNNw1h
O+iWHKgCZHVdEZ+5eAFAFshn3cyHCW2ohzEQ1uSlpp2xN/WfVcs/YJj4VX1Ly56znwGdm7S9V/XF
Xn+PQV6GFl5MCZPYLn6JpocllMprA9iMlmyIz2MPCg/XzN454OhxuxzWreN/9qFMZ3IRALqNjoA/
pEGfjXPomGdqMjyLr78/fAB4y4zC+4cEMM9VZ08BY9VlqPyI9G9MAlKJFM265WB0r51sQkG+MvNF
5pmNJY1txI9WJFmTz6CBzcFtv1F80ICVP58o2jGbiLoKA7SJ9IgVFB/hiu4g/mdaJJFO5ZMr01mw
5W3jPUmDV/y159Dpg7Zl1QTCRlZXzotfPkZW/c52wuXP0JS51JlUcTr/BjtmVITxZjA+8FubFGzZ
ceewgIlOpNXcIVmqDgvTXzrmnMZJq1wPsRshvp8rXcxqK0ZUxcisWGuZDIegj+QFvo43w7ywTGwG
Ybu+OBsSnXaWSCDPiiemfPS5/wlORhB2+bQoaWILlYIQU2J/d2T8JfFjiaMgjj4p4WkEwcUw0bQK
rCqe0dA+i6ke6OsCEHpRGENUNzNb9fNN+weeSFRXvoVaNNqPbztGqYcmXbO2d6DvStJueM9fL7Y2
hCh9wphDQ3Qs0aHUNmraTf3n1QgJrCJcY8Dff1haHKeY+u9j6ct8HnkVXN/HkUIQyErtwaKV/iOX
HmTVRXA65e1j0SXZvi+BXNa+gkjs4TfsGxaIt1KjGgL34XH1hLe6YSRQ8nTCfT/Nbqjcix6vIkc8
vNlhp9v1W4ccgPxMcNAEqOj6nuTFpvwgtHr96MEMfGBLkFmIXNSxWp9zFx39ap9Ysqk3FWAO/Qdx
ZwiO96noHZMBpfMwQu4GKnISj+VwSQlbuNMXGXDvMeUXnmG68/qEpVnvKifFwfnS+xmG+q/wulDb
cGDf4Rb1MceUCB1AExxTejFw2OCvo+FkWXzPnwL602QkYE0/i2yB6gn3IN5pla4m4ZfGio4qbdDl
jdQBRHF/p2WJ2XFsJX5A4gM7TgdeB/Ls2K0B0ozT6Ywhw7oBJ2Uw13nYKP/3Kn6NFOIx4O6+jxn1
dv8JXkIPOAxjoFkh288i0fdIRuF9VicMKGbGiyfonoxAvhGT+QQJJoo4wVeeFJx7OKqOpTaaEwi2
HTy6JsJ4I/Hoy7PwqARnxVf4MYLwqcCKDu3+K8vaqiDM73bYdM6mwJH7tmLvEhrpOvKPAK8M2WNa
FSrK4FLKfJnutXZ3KEU7Q/oo9QsM/3ur3x5N0oZs0RyUvkru1XWX2DgQfJ+u1jGaMvhOmder+Il3
855WKfNYI2ETIB2522Ya2J/puvbm49SqjItvTghwYzwvvNYqd54eacPaoLsYfQa726CsKKE8qXCP
1h95VqsCBNVTZZQJscaTkW+5qUOxPuA0oEbwSTDPGU5NZCwhM6EZzP7PikH5lpVkqw39L4oPZEqa
oPUTzYoUKuH/gEN1lJitFJkoIxswFgoAUv6isk1aaCJuZaJGrPAUu2ldTtvMRUwKsiH6MiQRY/wr
PRxgXoGnnL3RTEKoDGyzGX4nXl4Cn3Z9iO9svMwK7Yr+5Du8mvpYj2UvroF+uyd4VCJdYVc179r3
nhtKr815BogZghbP0Efeu68SMQehxnrhvoYNQ/AefjTsgHfE3cXw/r7i3XSG484FikjUmcm6jvzp
ixGbC+77rvj0I63+DIUdF2anUKGcphkoFcBPzUN0/HVV1GkYvWtyjOIWLe9EsHPlqAdR/Zhz8ejM
ex8WcHUJyb0sxS6sHg0XwvErBajuqdUysIlNH//oH9vcLMJHLAtAFEpR8pt5TgsMpSY1hJkBRvUu
Rd5/15fB/eCBwaRDw5DfopA6xbRxwLAJ5fOAAwSt/krDjFWBeI7DQOacFgLM8FbouKpsO8knYgCQ
+40A5MPLOqNxV/OKxLldgFpy6ydeqSNehL3w9Svd+w82BKCt+LCF9wqfqnrXsYMG1MBHKFjQSPCS
fdxvmvpfJYvdah1Jp9zCCorALsb/SXQypvqVF9hrgIaq1k0r321ROfryN9yEyDX346E3/CKnYeLS
7Zh2z3bdBPc7wavFuSUtPw5a+NXEXN/iCG+s9bovng8+ee1U3pjShOI9ZZunEzuRZNKeZGjnyS5l
nQwKr4KK7JtVKhM6PNMc2S24N2m6QY+1tACPvt6Bi2RsyuNSMdc5IvIa9MJSnttr5QHUyBKh9n5F
fS0RlRMVa9vgOJu0rP5WVpfHTL8WBuGjbrQGXss21gXIEZkji2391k5PHLl3k+NTSoHizAp+c2EX
kzV2gKoGtMezAF/ql3OTzmguyi7GooFlIlufWT7TLblt//OHKGoUFw2NGKB1xd9yx4sKRL+FQyNe
IPWLGPLHlrQ19CMlwWO9KulJaYdLFQGMCOGilM47JNcz4vyykadn6zyzDn7IFsM5wqcL8mpdxoV+
v+AoqUmCQ69oT1xHPAJQW1GbYkfYf6Fvc+Fp01Aia1BbFMbDkfpqCb3KEBNqopGk1eep6hY6W9a6
YLwaGQ14WYhsfXohDCE2ViWcUDT3IgpTF4ECSRfppTwH7hyqMUV9CYSzziM5tqXctJt//BWvloRP
uAfHIDi1Zwzp8gPohriahm9EWmfRJsw2H9BCCWe6H4di30Z/dkqsc1ocB9VOGH5IldnqF0YqHpnj
qNXp88m8yJHIeO1IqJczdq3OQ8pnSFUf/CygdGmTHo5DH0fiodrrP4YvQ+6uDl1o+xL4CRXjV1Qg
mxqXOBM1lOb0+EjDce5r8futHsqVvAzTA5JXwXNkdbmsN8mIXxHzcTPcnpegYy/amavT2t97hoXv
A0vZ2RU/O6Qn68EH99YYYoKmfQp7gK6hnb9hlhKMxvEPVYf0SgZvPL+WWm1pzucdVwNR3Jps9nRn
WprQ3U+KC8uvEt2mVRFVkidtEPBDkbQ6k0Gpn9ZjWjfAveEEONNmHMqG7YRHRENNqCOdGR112kLC
+daAeZS2GArPmg7UOOSlze61d5kn2CgVhxVjE2gSYRJ5R+6aix1uO2QbW3zj4LsLH2MC7ZFAL4y8
hx8mAdBgKY4UKXYtqZNmbzoip6sZxpqH0k7qRbBKCFBIpL6Nw9t95fmkL43ivig7ixB/qywc9w8A
21lChWY14Su7INvgonWK8y/+02O/OdvERhUZpLDJDPxr/Rm90FESuLqtYOyajKvjvpgZVJVRLYfn
F0Csjwmu98pYKrCgI2BbRICYiibeVvqkmoXDCS4WrfoJIQb7e6fbefz6RGvxY4n/kQRveiOLuBQK
uiY7+U/HsAqJJxx1sDGVDJ6TBdpEKv47TzlHxUhzlOKz9F5jSiet/s+LFWVYuOqQlQ54D6W9luFn
cMySMYJc/iIP7kTftl7Pcp2ZPiPJXyli5Q61YNM/Z4bDSunb7jAEYitEoZJbRVwCeTdvba9SJF+t
3ceHivmDRAB1mIkEvuoLIGc61o/99y5FWy333my622NYExWR5RJpsvkh80Bu1xHQ/VYBQ5SnNHVq
a2VwKrBCTOXznDkfDIImAUkaPBmAUv62HQNtxQMlhiGALYUBjnGTv/okLYkMDmrlMeUhgrgsQywB
9fAfRaciYgaBBXRl4WXxq0cJ7LtNDSi0ZdUyY6H1KnlaL/KVRn3d8QxlgnAMUhoTtIn78Qq5lfkV
siwExTZXMsZdtoFDopqm22gB34TsLMIBk1p+VcFn6AivmxQ4ude7lHjNFIDrkDr7P8Nb1oqnH+BW
13Vf6epzAAF7XzoUm8/K9wy1wA6WBjT+nFRGoe7Hrqrf7fb5OBjDgQq4x/0RLigpkljaMrbi+3zw
qABGmyuYn6DfMF9OLieo7eoE1QUi/AJZBB8FF6pfhZ4CZbyEkrg2Smgi6LHqb9tL9myJr8ejkF3J
ImCs4WU8I2En9mktXibINunwzrDwGuQPhfCoauMKcAX5rIYbnY1TmyOluP1PWeMp3CEQnUx78DwI
OXqvBl/I/hh5R9UZzPlK9cGkfYJWVl8FqE7/B8L8oCNHXgSjp65Quvd92CytCyMglCa6Pf/qKWJ+
lm1FlYbr6jYBd1Jfe0po95JEi13s76+XyrEjXZKLNsWlnuTNoezdeqBkKRaaxgNK03e8GuUM888d
BrzN/pF6Vb/9BiQjWOOjUdY7p/xnsSi0ASWWwVUVJiB67xS6KAEbUAQtJLOrPMCHS2BQEAKjCSWg
PXSRpljpumaWP1U37sR1lvJd3sh2W+Wk+jYiQlpfxIOKOGxh16qPStLLCDqKDz1JK+De4BpNWBLf
wH347QLBZgluWnQ/PoN3Wn8SyZ8Y1jarESg5aB1uOd7VoA4SNBykvoqfWVo9vklYcf3oarP6zyIJ
qAWP8VksUnOC6RDKk5hY/kT7SFWdb5epDf5kKq3J+kLpXHflvIxGbGJ7mJfsHjWojqZBz9n8TWJM
K02owx0dnj/ZLMU6RVc5jKzbweUBOBNCEJqs3kVUW4xUHShNC+i718tErFZ7ucCYZzlAnKBq9aXI
8rZh53Oq9ppni25lwQLNek8Wlb4vpx2dPYCOslDmr8giM7Pu2Xx0V7OM3FAZXETbfiYSJS2UmlXl
XLyDVM9YSNo9Y5slL4JW/yidihyfhtPJSAdnrwhipPmxw7lRga1WasNyj3lqNN6U9bKH705FPxFK
cMm8eE4nuKKVdvlqquNnWXvRjcauLKAN+NFjLYLQZWgBVNqO7QxQO8cmfO/4mfn+oK2+uQumIY6B
WiZYDU09T+cMRFpac3cKbMYvYbuI25o7LJlpDsuEqad6xDcwLUPVBNYXu+5cr0UdIBYKfkQXBpLn
2j31C6TaJfhSruFy8TR5c2YSPEqCnfkn+4U6QhfniAa+/0V2qAuC+HR6LK+8SuVze6wFJVGMRptw
rE7VXyCFBHNbpWvVkp9uHuUKsgU7z9z+OwQ44vXBq2sv7xrE+nyXceTL0PWNXZtBMC+QZyzZ0xKP
Es/4KxpPhIOPxwVV27qC+LBcitlkIJKPlC3Nay3YYrfMkZsOLSkoSbZmvWluMUs4LvrpQCalpQsR
Q/GOtPmH3SmCxW1Yjbddu63K8gnf6gsSRdqDNp490XPTcn0FbaOa16OYX23WjH5wW53xncIm5di2
knj7BjMf+Pt/55O8om37pjvcTqvEM63VVPKtm/62JyfyYuSmDP85zJWMfAUPEYRiZ4Am92ciRZa2
J+lDLt4h8WtrBWvvIcP8JiR5rvnQfOuDrAeecgaxNfJoXpExJrPfsmq1ikxY/Lj02QXa+OpX5z+D
0D9jhwHHiJ6gZjcqjdYDJ1k847rI6Qu2vYgEmgZBYaT43W+8fsLpxholy6NeHu0nLN91YnS3/QO/
YE/Zar/ktI+8QVJ3iskd+fJZENZS4wuQ16brNVfpanvxGb2s7fPbzDxSw7a1I0pyV/PsuKcYGm+S
mBf4B9SNVONfSE44PRQMmJMiVfabczDTx0WP6+s9gw4A0U+0S2XHGCNyS5/pqRJePgXxk9CUbbhk
nIIC0eZodlGnDLFEnrJ9lnnZtejU6Nok5S9Yyhrre3XXx5IHBd3pxx2x1kQBg70yPhb7dpV/5WDF
Ho+AYTohNyJuOqYRrd0JiCs9Lcwt6se90Wd0L54enbqZqXwGLTltoMXClVAd6JtCFBP2348WOhW2
B689mP4iK5VBqq6oKEUpAJp47LVcaUKUZBVHjSKtyc6QF61kXfJ7qY8fUGZi5xfPbBvs16FRFxbx
je3WTJPwVcYJp69gROJ2EFXOCDku8E3yA/vCvmZ54S1OTOA6WYUS31ZrOMkGziBP4lpV7MuOsYfD
DgDo26KVQ6myLuia7sr/2wP0PBGAWzNP0q3eQNrraH0EQdvdoN5kNqMCciMSntmmA/r9N+lZKtmC
k+6XqR0Eh85TujhHu6L3c9J3/AWqUFBSZF6qSfk3vU6Aom9Fdmt1NA4/As+1n3mFviJqOZabwqBQ
izcguZ7ttDgzBSc2OqmPWH5PCN74lUPS3gTk11isLvzH3pMhgrgrR0szxbg0mP493JfRH78qjT5P
0eg83kzOZe5/yg3J8Unan+G/tZ0zc9TbbEpCxxQy5eT90YKbcrUl2ozIiGvU8hh3m3bWOEtRTRYj
D4bjqLVGt33aTrk8+nZ9ecxNgzzzclkyk26Kwk2MUy25xx0cwD/eP5PExikMEOJYLtjAdlivnyrX
V8bnyXDWn7AeA5Dort72V19QAmm82K/wgNVzVMggD13YGSurAoe5q1BgljhXFerXyst5raeP6UnE
FKV0CDBvZl1T0If1YhdNQhKppYnbNK9e800RYLlhqQvHZpNhVRpeytItQO/Lz26GR2gaXZzQk6T/
35GbqE1kH/gNofh8DywGvnjuQzi6LEOCPmwj3H2OS6VW8DGWCYboIKaNopXD3V81INwFtSpv/Zi9
SCYIFjYc5MsGCWYhzMTGwPh6qQPj9e0cYFto1/CI9dSMRSuBKsQjV6wDVHrYFtsYGlfgHH3TXztr
dlYk7G/biV2cXyNWtMZLVvQnbF+bvOCp9LvGVvtFhlfjg65An1cjpbR3Aet/G4t/yiFFs3GQoGFw
1iZw7TLmWXQ09FLrPdQBVeiBUKGLbXYybLp/aTTbIsUKpcN7DTfSDmDA2I0TU6cP8LdJVUSrJ8KN
EJnrAEHI4OasL19Dpl0BdV8YOhtY4Q1U2G+WEUGiz7R6KlcNwJJtGWdVmy3lqnuNnChpjE1m78f6
UdeUJcXHtwX/rRlh+rUqAOlKjdqJ8C0ktpD1FFzLQn9LGp6TeKrB1Mr3J4KZwm4Y6rrFKOrJBlKz
dyfv5wp3gDOPl+52qMTmt8mAmhhkGoMVpij7OjI0MRZIERBw5WuIisanRnOwUv3E3vj+S5rFKqAT
mF8LO/83wowTbgvRk/+MBrWeKN/+jO315KEdE5YK+KYfr9SjKc9MrX8GAnurZyHB1Q0DgGE7vfMJ
zpdba/MUzZ4QkoubiUNpWjRd++8rHqfEHkUcMuU17oVm7kolOG3IPTwanl3XU3rYDF06uxHwHgPK
SLUej83OgRJHAEeN1XSYQYhtxU+Zu1usmImsN+UhiYTTeHrb+A14Mt6N9mH7SIWzaLpR12K4tpKU
E7GoiT9oB4TPRllcuVhEzI2/Hi9jdk0D5qRuUt79yt0OlyC0Tpjtz4m1wHCpic7EBIhG+AvJIufv
q9KRAaDaPfACGmswBUBIdwM+kVop7n1i1cXptLqvq3zYvM1rLcnJr2dc4XHaEzPeTYoYQLwv2E/1
2hfhQCi7IuCVeBdCQ1OHtC72s0d0mDNDH6xoKhaZwpEi9OBMCQ356sEruOh+rbU8Q8HMpfXQkzBs
QyFITeB6UVdnWKk0hilkk92GxF68Cug5Cihak+JErFV4S0anyFbnNc0bLviC6QCTcV4U62OWLjDH
h8c+1gZ4iuYGaSWRfZtiJO5LKnz5MPeQYNdnrKfRrlCeSTIx5Met6eGJ0IaWdnfJxPopw3J0nh1b
YzDivjiX8dPXihsxq0A5nLVLb2ER26hYE51TW6wnJlyxL/D/3j8fMXdBrcY5O9tKrFbQw99BmNos
xug5n5RLf2t8FEnbNahrqWYDrypYCaMNL39GruVSF0SYhkUO64UlrE2G/KWEGXQozIlhgdmswrFb
QeP2a6d4DCohtPH4OhBXNEqXHmuiTlB4wdNY+k7zAVilD1JAn8iiLxMyozqRUe8wOp0hEcF7iLjD
8oN61nv0YrqfA6jHHA/F09nISRABEs0v9r+ycKpdQHvA74kmeyUN3BzXCeCmSEja1sdBLGUfS4rB
bN2p5QYiGIuj5DwWTZaXIfMGacuPGptrF0i+PqYy5FCDgPMuBoIJZHGLJigtDei7L7s3S8qoCpC+
GdB+qp+pvhYl6YNXgDo9NqGbZ9rs24DXmAF4Pl+PuzpY+HQGmpEZnxIoEfJwSstUhpt+vqXQnJ8T
NcVPxbKH3kzQAhUuCpFNF0yi6I+vhgMPm13B4gkOfRnrskTEMzdZwYlNZzVtq3dvFLM8bDBEXw8J
7Ukt0PoR+dgWwAn8hke/PEH/ErCKP3K4vyZfPHE5Dno/19FM+aj3iNgAHQ+prrwNNAlBHHfLu040
dX08mbul5Z+TyZMQa+qxbCrEqR8fdTzhP1mwv/AGKLTs/NI7ZXtyxiTGGo/q7LW1VHz/6k8QEvWx
j8wD3TtiDPE+9wvbohAvC0qZkTymLOX3N+2AMmZUXRsbNX9zsNnjf2atrN70msShtUDzx4mDvara
EiP0iVXSOgjM9gOh56YgofT63sIZp1gEe4Hy5dbhJ7jXfBICAoL5MOuyekoC/ZuMA5sRU5iNcgAW
YTOXwxHB42HGQ0P10rz9Znvb/yJJ0N46QlISue0H0J2czl97o34Dsj0d1mDVWqOvwLfwHOmssQxf
5ueGel6V08dKlEQ12SDLGrUXm9K81Vplyxc9VAW4TqSepRyjqTbquhnKgyek9Uqqcwp4+7HNmBXu
COAW3aYfH93fIKpB8kFWXSeJQ42eFYMtVkgdiO82QPANRZIMhzV6lVMLiEkXm8oEn0inz6wnnABJ
pOcGKUSZzHwTb3PcuYO4lwxmBpdaLj5ql5ymJSezBB+AVsqN1vBkrstJBELQEEeFzB0mAc6uLEZI
KTJL9tfHHxvrxOVG88wt6f8Wjv1kQw+S0OBohSxF7r0Gh2ekVxk6DsCOGqtsvGtNn5FMHiyYGtll
FMJLA/SrkVRW4C5MIkqRQfIajz+yqN01JALMgp0d/e+RibYbUszT67h/d1fpV1zzozXnkfX1xeVS
8CgOxatGToBQFxo355N/1c1p8HVNXzcRgozN/jAJY9wtqh6kIPpbHWd/oign9pMPVT62JcL7WWJJ
f1UCKTaYcqK1GA9r4x7iMSPUYPdMeROo2ZS1iEApbkN1h+ZuoX0p1dOQSm6u/3qtd4I5JjEAMWZM
WJEzJyoCSSosEljeojyTzCk8RkGoErV79lhb6thR0RrrIPvGROMbZCGwGzZXkMbiRHwScs503f6l
XUdFhbUH6voZLPe9TcK2DA7P7jyWXwxdMVWzzxez5iiEtssTbPHJvZnMj6iYsPRdM74cgr9pdjKv
lgC51BDjMheKfmqraPiPTrCdPCHMf4ndAi1m6c+Vm3BhSuGiwz3OuuyIGGROAZBJzAJ01q78OaD4
NPRoIGtVd+9yzgRnCgwY+w6U6KhIj1rxBsj3Eb63pMRFr2mHHLYv/d6pLIwJtqzYaD4Oro5XVTOP
e5zGqBMCEVsGrj6jQ11GS0y0rqbTYJ7FcUN/t7wkj7EQF6ov7VWb2n+371YPy41QaEC8QRSNEMdk
2onh+XJ+gzeTsquzXh1ug4ibgrn9/2YoRBbcUNB8KDg4lgqiXdqZ0ejD0xnmV3FVu0AK8ZZ6mOBt
9wqITL9oOxSqLs/bxa5GlQ5Pr8eF+/lRCWwiHF9P4GwWtG3Lrk9Y9RiqU5yT5uSR4Rf/rjOjI9/1
9G87DoHQHkCQ5+qJU3v7dkrW11daNr2vCOXwpV7A1x+3Q/xmptUuD8hd3NHaWGIRaNOJkNMo6aVO
Lk5vzqTF5DQtZHOoHEiWq1wCLbxpu6BODilDh+Z7BdeHnzzSju8MfiR4lusO+p6zD/A+7z6UD1+z
hoRXkYdE1fJtuNyTtnBRP/bItvFFX+EpaieOMcRv4JueCgOmN/vTSZ86dZmnfSQdITiRuK6sf7pa
SKFTzVvWzKybLQZQhXZkpwH9RvRwk++S8vAVsIMgu2J4oRhu6oKGHDVrh/E004okqW45zja1KEiV
HW+aaoMv7I3tZEsj2y0gOzIZ5NAuC3P1KSxzPFpZnpwufWS6RI6/2a3IDfOALhIBtXoHPThBti5d
P2OWPHt1avsxIH/ZUnXODOFzoYtE4So/GwdktHXp1mvqVqBfYIuBtrQeQ0qEx1hrgIUKFrZTMMZ4
P1JWaLajqktumVbaPez2OjVClJa1rOk4k/wMd/u87ZBPmQD8Ocdn/7kRtszC9QV7HJrAt31NSflz
kC874EGvFOI0rVfEVYhsl1snI8Fvzx4iaiYla6MZattULPTkHVG+LnpZWjPdA3q3v4luxg4fWcD4
sJLgM5lMntrKwHh8jAvNs3eQ1gp3AXNvOwRC/OxjShTBVcA2/Q4XPiPe+ndqZr/oa+OAcIgcaFZS
cg2GexMUnDGlIj3zJA7nv6KLYYo9wk+u0t4DyUQxmNPm4bz0AZj2OXl6ObxDQ7fgHskYrT3s9e5V
6X7ezz0pNknuDetpiZiGFMBbcNaFSvS2tflUyQM0wx5n6X1m/hxajvSccU0ohj0y6afZK4nqte2b
Xo41hGi77hMos5ViOCJocJadO/dhXHcyxZF1+mf5NTeKl13z9eo4bUce8sQOZ4k7vNnoecrKVc4X
83rmRz2M+4beMZwjPznAzw2vmrP9Eoa9VZVhCPjdx93JegJ/ssRUIh4pZOkthS8Vas6fxCjWZMoh
8QVJqpDs/zgWWSCD9hGtTMCx3kfTIqYko1fkZJ63UuPjscUQuAVm3UwO4VLWqL4GXMXKIq7koho5
b2B1ew4xLc6mCCQB+mLAKKHwGEmI3PkyLFfgHjgyiaqhVbuOV13czbRBonm+EwFr2yguxndlQPRY
7nZSqUc8cWrwwOaaCjSyc61K5TTZ0uNZSVydkxIg3D3ukU4T4MRLkZfo7oBhot4Sxi8U5iIrk22d
EqB+cIz0rmYUlTAo9E4ETW+KLGo+/Xvhoq0sP8QR/dqnM11thxkkQatfS2+rbYKIZhk02+kcb5L6
wSpiscdYQ4UYSezMVkndEF6pgOmwEsarUWUxTduyR4H4xt9xWz4jKgHEjyqymuTWTlO9vjFAL3ln
dNHloAEYF4qKGMUU6PigHt1h+Vee9acL3DSckQnsnnlVYzZXBtGUhERvNlvIbnNQKt535EG50WLH
BRbjiT9uav/gObkWbTgltfkmPBE9Nz4UjtFTC34tBTIZ2KXvmbjV46RnHY3H9WZc+4655BeLGGS6
K1tT4YeDmiEsv3OIB2Caf2efMMabK+VnKZcVMskSRqPLCKWCzfKueCBmI/EjEjgza+UILhddsLjT
wxgtlz47DeidfJsPYOyQgWMwH1s7vW9XpfTw/GEV6gvfiHLbHOh83vI5CfG6Ngy5wVoHUmVfETWt
SwqWI3kDmQZYyktoX7lKUafn3pfbUBP5cQ2ywGX5mgAjNEeOGuxXWV3GoWAjuijsg8gBDR37gsu1
2jBCC5Z1RpZ0DiJvppbnCwuE9JNGcwjVbPn16uc7YmYN4SOF4gN88v0dcLuNX+8SuZetpNlLtdxg
XMwkIMY7786ONvNSHxHT26Jr4Du83/KNMCLOSO/hP450FkJpPLCGXOxiR23cjrZUSV5VL2ufbc2O
6XTmO5jK4+KN+PXn5v48YV2GEmsYUUuZCaZQu/9wz17Ne0YLfboNeF6Nv3RKKJ/NLzkPd7PGn54h
VIGsls6mKFYhMRbumvgtYazqKU4rncMDbKI7FX1Lepvbkn+Kot7CTajaZa91mQvQ4u+PmEIrEAcp
4hsI8aQT49QbpD/QjK1haxQM7HwLwxxK+o3sDPjqFet9Yh6fCxmPy2evjV4cxQpTKhDFJXFIWcQW
XVDpNDpTNOamY5wO4avT43ufAO22KMRQxCsNn0dCc2ZCmCSIBUo66SiDac1TYlx3EcAWVNcsgWVJ
lT9XtM8Oh0z5iTtmHvrqqlDr8eDvNrcwNGZ60blzwnKddxBvad4KnFRmfOrI4v/qF6oYzi96KrqP
J5y2GvZC2O0xgRga0h26zo912YAbDwX4YbwYVWHzG11DmIXLAXbZz0IAVP5oajT6n8+06HdJ/TEl
kZSYqL9Af81rX12Gpvau7WREWzyAgPdmq4w23aZ2xfq6bi7oLtgDK9Y5k8WNbqSg8WF5N29xeAl2
DX/WDWadnE6Y4sHSSyUgpDQTq9eoYE4dNzEDwoMf1ctTeLsX6n2uUEL9hnEjhDGu6JdJ3Rv+xpAH
CozXLSfjfAkVF4hU+PYPBqVeWRaKsfq4ReATGpbHRpIA9LK9UXoESR8WTAv2d8YrBhhW5Sgo06Re
gEKchyamDbT9xFMWZmEcJ4dlQpiTMqpwJzgkBgVC1LW0/T91HpynPpu7GZJqp+0Rfyfwn1r+CNHk
Ka7uop2qIvDla3zVAHtyqsDnZipKxGn0/01YrUoLzfnQiMoUVz7z1s64zBt2t0Fd8QJ8e+Qs2Qsx
7bLnfVQ2mtcPDVwm5iI2ZGdIJSIBrG+te2jYACI13Ip+tpNhuRYl0z2AOdeUC9Tn65x/k4e0Ea4B
YIib8Knw+0WPVjqWE874Sy8x+WFyGvVR0pjS7uI28KoQDRnR97BVEVy9iMLSb1JItDiU+zy39IfS
okkOBktLEpfHUjgoRxb/i/ko+lZ2TTTpNOqLjh83b2HqKhvbUmoqxCfu5c2x8Qr7Q0pjexuyWLTz
nsmTpxGr0IwWN8Xu2blremaS3hF+ZiUtk80AZ1l7rBixLM9FsN5cZ1BMj/mBFPvMi7UFl1SreBX3
fzDtKemWIJhDhwoIABBmb5y960kmwwamB59okO8m9B1FWQmq9TRB+jLs6PlPs0nW1XLbZjchyTdu
2yVFH1UYvB8Yt9DsanCJ3s2AhwsfkaTueKzmkC6vSWFx+VazB5fB+BBoJlbbj/VsSXYVejmHNKbO
h9sCh/5GhYzMgGO0qrEtI+48uO/v6mUEv7/+GeHipSzjYV2YANqtDyZRNKIyPXqYAd0TjksTBjjc
+9rLiBuwpq0xjVznZSMN3xjUhx5CR7CULMNZt/DV5hNn+nK/kA0RP5SrNrfXs+FSvjoSUEWXVKH2
ay0247Ql7rae6LMdvFOOmuV0EbJnO/ElhFY6Tl/Mkf2E48eRXWeEnht4ViopSMip8esnQaDwXUXx
2XMC7F20M8dJM1Lk3PCLb1ifqwn8HbW8Cmg0SmPCYzhWa4b0Uy9LSfXzmVxRTPlNqqeqZMnAwd9A
beS7wDSlr7pK8Yu16lWbwjXmypRnjzI/+ujCLXd/lAsRLMRhQESlCzv6z9JuiVwMaxaDxt9XkUOV
KDfzP9Cd2kYkRHCCFv23D7tNpRS6BH5b4v2SFVRhXvEPzpTRvleIIHk8yKH5Kmah4c5oUZ26/mZg
6AAB0nPbjaxhy4lsSLIb1M6VW+WQcTD9pofpBAORbDT9iXxtKvyGHrd6QcD/FZpJmKhtLCSHDsv2
e3ZbShcyHaH4Nr+DEmpWiGZkiHl6iGR+xvW75JcE0PBhR98FwnSQ2TrREG9Xbhr0kaly4lqAX1Dd
1tIuxPPbwsZqYftJNVL20Plsw8M1MsF2wGadtIjMS5pqvwFgQzMQS2CU2lZs/EKIwAls7OdIqgfm
Ss3NA48kYl3OI21CjP9upuFlitkGzVt3iiEYue6xmZkzBPqEZpYlPTL+MYWTip/pBowOoKESn/On
1kp9vRgNkAxWkrELIVnPhXLx1ehX7+aCd6cVDfqAnMUpGLinYNYrSWQmJ87UxkoOXKI0vVu2hlg+
a3ujyxaUxVionE5XthbSppOYyMKX2YLUC3xWGzgP+rL3xEzFVR+2MG31b2IdsqtHJJN7n3aUYTMx
/8V9rtgJXzjSnoZiw+c0lbXfdE1vzgrtGI49gJvdPWw7T87zginift5LEUDsbLzAF4g4bsa2xdKf
4VRrx62I6GQB7jrEYiVduAieWeVzFhvlzjQdi5hoyDyKEes9EhcTqTXDqSpKZWhOMh5MF/H38TTd
loMZPTq+JBfxGA8TjIbMJRqrHASw9F7e4Is2GSMjEAMFlCkfU+VCk+xlLteg7wK4L5Mlmb/ip65c
CP1ozbr5TpBRkzDPwYow/Xnhl0Ml9W/+RkELEF1geqvBNoitd7yaPWUmSbChBtSgwN+cS0Zk9IlX
c29pEoy9LbDEjZIixnYwwShsMSnMc3CFCMBwwq7iWoEULW6FhLKamoa+CXIFAV6HG76mN3lbC5me
GRKKknMj5l8n7/4wOuja5XYKyyUHhy/3J5heFP7Ln/oZkIcJh5f2cd/1Mm/sydcLJkK474pkb4Kz
pQpxjRu/IwsvokUchU1r2nb0Lcqex+YqFVk8AqL11mCEtx9J2WxlJgyF9ig3VYzNkPestpxaV9iW
g9oLiB24k7Q3XBy1ExLQerQjRMEHH286R9wCuu9XexVqcqMSoZP7Jg9bYwKfZcloAm0x8kk4UmLU
Xkmj+pkAgwUus1P7OjFn8+wKQXQvuKrJShSeq5kg3Y3sNygHVkGK8URBzxthc6CPClu6FusDY0O0
NoIdR3XzHsW0LxZTGTzXcq71l//po1IWgdUg/j/wDrJS3jV9IttZ1BNdQfB/ZdHqtsatyygY75bU
eD0JMUXwjoT3m6v4kpUhTinlvQ0b7IpL3rnD4CFXuWK0SDUT61eFi/OSxvmpGvtz95TaUenBYs/C
LlTGzehopTSq+EMIVeLozmMtF+SAA43gD5wjFAVA71ZIHOG5XrOpuXN+TJ53HAZMEizCJZpd0wze
71i+iiE2h8goHap5K5TMbBIVIFQf5bD9I069hvaGs41f09yQMi6IlBqUBNMfeOJgSVVsrD3iKXRc
DuPm3ror3CvOqcxwS/99DAWpVC1kbqbBpr7STIVbRoUF6fvoytagzzT9jvQ7W1spqxYn1bgG+mKi
RdQ6u10tP3YYRdAS/AsGDmhBIEY4nxJiod25A7Bs0Un/8z9/iEKHnJxQXfjWdEjrlG+NZ/xwzS9S
mirORzb7iv0TfibS9my6B/F+e1TyNPXNenQqIUw5BtG/AYOL377D/RvNivTA/unLAMhhETb8LG+u
5N2CnZv0HaZWtHL/5nsJMIAIwyXKh6yLsCX0FDQt8ipZuRfspKQgLnn949atz4fFbs3PZC4/Efn/
oNeoh8JHek1J7w054pjVhwbz4jmftVTrUGWRyoAiO5LrvtRtIMoez9hXfYgDBJlzrqdet/MdYKtO
yESW8Yd5eLP2KOhHsYQtH876jt9CfnZBP5YVZoSroz4vp0i7GIy/U7n2p2mJxRUdrxvXIhk3W3zt
2LQK2anmQo9+wcBvWiks7hNzOD5MNqshMUOG4x5oP76rqgRdldSwkVA/b59u1ROd5SQRGQFvQGOx
xE/n+pKJB3h5hWD1JnugZ+b1mSv+jb1TrMOR9Jq5j3F58x7dDgm/kKR+TdAh0XxTvpyRy8mfray0
KJZOdIQRc251zbJeFF77faS2//u24S6N7ziFL12pii3IVfpCc1zDyemQ5eZynNNbKKHHIeilb9BO
J73TV+XuKzV1sI2O5qsR5/Jkl0q0UC7gFsP8v1jIu957BIkcevN7X9pkX6UpP+oTl5qO7qQNSVjm
MRInXRsvomZvPfVkP4UONIXU3Qc96gQoQrc/UmiZgq2SWRR26sdvEI0VZGtn8mgSDRsj/M6K3ZDW
aOMbegHr2yMAca38kw8Fsy5nJwGgg/MCVtFcH+1piSCml0GE5eIN2k6CkVBdR9yEdq8qbJ2vMQ/U
+Ml3cy9tDbeIj+OUr6JSQV54iLsipKFLahST3c9IBkvU7iVcwkFZXlms1M4x23Tnj1eyD0jDddyX
RkfM/CMUxC8KRL4Y/mN76g1Gp8e4P5UXjsB/13SONdE9hsT5DXjxMC50nmfoe9y2StUri+Ko3XmP
KMueSpGCudUeWzBpHhym2GaHV/8x8LyeuG9LZfypQxfjgC2zt0t2Rk4OPAuhqcB3QB09R3GYtgHD
zVZ2iDYC7PpPuHYfZ+FL21SfGr3Watzz2dR62xxixQjACLo+fNaRi9WIFzxSbWcWkFYuEjswNsTJ
n33HFGE6RJeLwCh6nBVsWSSF7bPhTVdlNGQtrMLgek1j7hkWGGYYxeQoqhVhKRwiGVPMccV0LTGt
D77Ez1rRwlgHwFTX33eKs5Wx5mK85CnoF9mnK83zdZ38GJ7n1B67zH9xZ+uy05P2HmrssJV8DBSu
HZ+zFlV2mLh9th3/Y1zlwMnRnAUBpYy5c0iG9Pzq2Pvq/4xzxepK6v01dOxUPnWu+ZZOkX1rlheY
yJr/O3lc+MV/qGtfV/cl8dqPhwhPiWVp+5tU4FkhzRBkrLh8au5GLeYO2OqkVF8uGLeBTY6Nys5K
9bc4/dxv+AMuXVEBnBojLCeorMFcsstof1xSv1S0h9uG4JGWcR5d3Nrg7SSkox4SANrQZWD0chE+
5znlk9hkbG6otML7pIPtWsbPUIlMF2/+Bi4p/20f8+LhaJbhM5YHK5AcW+YhyNgGM9WzSr5lM1jU
wYDoEeOIkXjPEY/AIj29nrE3hogDY30fu3GJLZA0YFN7ULXapf0jNni8KbWBgQ9tzflj0afZpozc
HJVk5CQja1H9rxemlvzH9XiSS1YqVqKMXiJzTs8+qGYVpBocoAqgTIzkVFXbO1a1yJVoOCkmUGsW
ACcAh/YcIktf204Xf2KWMPKWqFSG2zog2ASy5S1sRrRuasOSH93BmeiZXpiduyiPHsRBep9miOIj
8Hb0KjaEY9l89QDKsMN3uP22kaJlhcTMa/9KFIRekNoe7NtmdMf9nCJU1n5z0u1KWp02yXAR1fXd
nbB7AfOM2XmfgQI/gt5u33X/Axxm8IDg7YWejjqmA2DJ6Phpg1bcZqH3Ym7Kx6vt+1FTorFeKtcC
i/8EHTj2yXuHTA81tGvxx8Yu4Iwp9FbpmWCHgQLXGtg21S+JVMNxOQWZ4ul8Qr3EKMfgJ03BmD1e
9BRvTwKekQs9iCHVV7g20ne2o7jF6KZBjnMEebIRa0h2ZKErCx9wowdZg1NmqyG4qC3Slts4kbWa
5Pjp4Dls9BMXy4n4nGokL9dpg9I0Q3Smr0+iupxwwyKTkxb5HNm/fJW6akfFmeYPKrJ4Q/YUGZaZ
9d5oXR+v+GTBzc3A2fFby/QrlfPunyh4+tWRr2EORbajqZaaIkOBhVM212QaSyfG4EmxHvH2Wa4Z
NQ2bVSDqHEjs61SbGp0kOuTYUSewQpwfN3a0kuqm5LnJG8naE4CSC1izLY6hDUb6xQHAfWqvbJRg
Cda2AhGYYq6Gbsd8vYazep3HTW5M8YmeXFvc/u/9qDRgWheVuk/OofwILPSKdstMAhPFe/WsJnJv
QR6FCMvHC1OROLSk6dVzegy0+bkBPSJQq87Ay4pu9H4g98g2gzpC/v1UWk/FACG93VjmBlrVCu7g
aNqX3f+UBicNftLjKcaRT6VoV+SMH7QrA2acblnAF7UO5VI86M/4zD8Evhe/QMgsj98qe5M4rhuv
NEBrM6lkhPwwzP7NNIEGpeLKcAv/3kg/0hP56cFW7uQnuMH/JSEqsky7GXqqDsLGIr8dPQZAPqBq
SPQfli/h2PV9T0oB/Md3UcqMUoXD4XpRJX/OdGHT9twa16K4mKiY+IyVgbq8OlrIi9DqV4zOxEDI
BSbNVcjZEfxtcZ1Q4jq2lTY5otnrlYJ8p/oin7UcokVmhNH0ekHCFQeIT2RGuvBA1viuXDhq4TZy
qfq/UunGfNacw6oRusllDoqeRWwIBJ8qQcn6kCX2lTAbDoYrd/I0l3Awdh2dti1K7HUh/MqNuhLF
Ns2tu5gJmp6ioSNCB8cbD3lgKdtGGxSsNkQVTr1ntLgMOd8oSMsF6y7d3TrdXmaG6DJeNXSCNvvw
HrcsdfSVrgLaj/qY9/gZmgqVznJ0gU3mLeUUlcMjlKb4OySe/M3gkpHqXWAhJ8Yq8vAo8SBwtt35
OtQ2a9F0sWi6IAWa0gZuQoZto6RF/EKzt1dh33pqK8V+frhtbdalFbh6cVylAl9SCHcyKnthFBZg
KgOgLwOgUqzE1vq7FfcPvs20wn6/UjK/Kq5pfDjIe/uJFhtWlWdyrKBEi53zhlHueLYBijHIyonX
WA2NcfqzRDgGL5Vpq/du24IcvSvLLUosue4NPKphHg3PnHcR5ECJEFBUmjsQho2w6gFpbi94636r
U+cWwJS3YhswJffCR1bpcFvPAbAkTWUCR1h45U6Qo49eif2KuLzV5RzzshRXRcjto/iPQ2A433T9
t2yk3MtVcKSSEshdAQ3sunvvaxSg1QJ8K1O6QhPY7mWaG7Nhl3y4J6H9egHzuignZbIhX1CKiL2E
bGPGzK3ebFzKE5YAL/giT+ZccTG0lPio5VF+QdHre+rS4J54f+R4cY9xB07sMPE/7DMDzbXPw03a
mRDuWK9uVNeeT1pPssD36IipxFLylCfnyhE779ZUz+sfnbNB/luPwZsDrz02HP7xB5lvdDOV3XnN
Vt7I1SaIC4jxdHfutzjhTei8Mspmb78sad/YtPEWBcTwcvimPPncZIoygqP4SGtChSMvjvFS2STr
5R7lxtHoIiQ/KMZpJEW0UIw1dhMoDS2T+NWpZBPfw2tAsmRfm6gdPg5YKh2U2AQWDLl2cIhlHM51
fOD+++eyaEWFA4hhMBeJ1AAqPynH9FVeTydEU5d7obs+Zb9fmD3F9xtN5THxRwwqz2bLpTLur5mh
v3XZxT8JWLq8MybZYKbFsFC0h1Qk6GJCRSYq/5oB87/pA7N8ezun7FiH7opicZ9X7qKBOW0bcTUn
0Pyge9ELR5vW8og7jpW4JlfwSGAqBev9Hh9zUi2yiy79KPgrEArxfc1LiRmNR8eTBdIfaUJxcCxq
6qSIRrbSeNuIp7s0pr6/6Prvt4JjknKwyX5TrCQsxz1GcF0BhuPuH/27axnJmTvLshwH4YbgAFyE
ChhvlVGrZd/RpyAL18Nqs3fdCjeIIXO7OEuafp4RMW9DnQBZLgZPVIZ//NGTOU1zltab4GsZTsAv
J4ywJL8ZHH7pn76qorMwNNe5TeE0Y2Vr9AQVRLcc3oQm3EuuXsgL34NTAeDuW8WQ9KHMhWQ8Ro/t
hws9LaV4F01gXxSda7+mzbDO5W0bXT1BdS4yeedukXBrLe6P1n0hVeLtgyaoVmPduW2OEUFrf/Tb
Ag9WKa+kylAIEpAdZ7lqqo2vmzrZlIDKn1rSaKOcqAzx92JY6hl4Q3wCWczkGBeX0n1swEZD9tzS
do1vgJ/YwBRdKFOGy+O+rF5eoLPIwFPTAQ0TDgj4Nz/GM2o69ODNrbpqfX3QLkRFnJeJOaSwnqTy
ofcazTGWfchGq5uvAmUyuJkym6MpdG+J5IWCiNf4b1T1AvUqsocJMvnSnu81hcEQHQiKY/GTBQvX
nJ3veFSZhA5daLz3cGXCpnAJXqcNrgb8UEh+dWJdgu1ox22ofrBaQ5T9bNBlSivca2Yo2ZfPxgIy
HgTR6NgI9uA7+T4e2vrOQCUQvKRzfL/wwmWZv8zTcTTu4HKf7ZBbAtYp3vQOGIJBjTMJkq/TrNVf
lT2fk4gVQzVQaobiDPESy5uXHylYLM6kRQeYfDHw5CmRXAbbJmMq5lwphCxao7LIreJbK68obdaQ
QZD37Xsi3MIyBKsAy/dD88eENxKdggxewHIidHB69MPowpiuMLVDDBn0qfq5roQYDhMhmmw49sBZ
6Td99VbzizPZCOZQtODvULpBryzmhh6CFMt1fhVeyVFZ29E7ch6+hmpUVKVtXqbbnV2xlb6fr3hb
5THATqQue/BGpCEifN2xkyvwPHQzPgh3yPMgjVP8yG/9G3Jj0knbQYjcAeQPMO/253Mrhd6TVYBM
AmTgAnp2nDe6aFZk1YqwEUDsY1N0UkKLwbUnBHvPbRPExU83NkpL6TEMweE07ODxSgGHcto8QvoV
9dztjITyuw4/VwA8yaBo1+u/exZoC+jnYVYIjTxb6EhphFwudHCEh1u/LKqfRtf/OT79jSMiKCBK
9HWgGYtO2OACCOb+NMcuvgVkd5vCzjvfuypI06NEqCn3oyaYg6ufOz+EC1P14yQR7OFgk4g/YE80
OPxfgoUHofd+mXlZBCLexWQt2e1/zrGdQc/Fyds4xB4z1snyNeDV3bsEPtuzjdidIU5S8E5Hn4vT
1QSHqetfG9jf0DP+9828PTiZckBrmC+Sr2aMk4/CCQyw326IkHWiww8dN8ji77Vo7SNa6+LwXgjJ
/zoudfB96XgZoxxzJuA4/+wrD3dXjp/dudkXFMU7tjIAaxoO00SyncdvDgD7e2YrBlBqVIPSzHny
VRNYfbCp6TZ0PKF1BywbgEsBtchSCGvBbjG/4X8Mmrkzoz8KaURwkBOjuPJBU7d2oLr7gsFwr6K3
CdUYugJwRFp0hYljNQmLaMUsDlH5L0gL7QFsts6Qtjctw8ytUJzrcdy3uxj3ZdvuiNS4Y+T4TxrR
kOuWbUZ7HYhWyzUbDuH9PEWEE6kD8IE9ZKyFho0o48MtCWcvVJsIgqp2H55WVG9XC0R0qO6XWykP
167eEwmIOUjIjAmplXAMd7Xx0TQq+0BNAzC05eGWv0r6bnRbjTBzCwAcTjfgmWGcsWEXT3Xn3xPh
WzXE7JVYNfKcmj5w339xHBG1zZtPMHlsC9p1NMziXZJuXIddMpmISZdpjF2DCVWVRgaD8/yyttzm
+iuFDh7Ut4EzfkmFlobnfQqjoJmnwWX/fiQBjbhifO91yvhcqJpwqjpPWBzcfA2Gqj+iBR1779NG
xdFVAmOD29UHHZWyySEXYypjlLqAfC3CpP8wc1vDUP373vUae9jo5DJW7b9jwCg4ndWxE3eDoyxL
D+a3nwAxsgwS3wc5Scrz3HA0GKC7fbcKxWjXoCsd5UHvQNGsBVPnUAjrhC6RvO+HsS9vTyZg5DlB
ijPI91H9wPP8SYNR8OnhnM2y4OGWo/ugJVcTdBrmV5TP9Kj4DSDe+OokhGsCjq4Dok1M2bm92r4M
iRRnOcIUrT7mPJMug4lOu5rWdkAAQNB5byNVErx82EDvuVuKkEWZsGcnlY10043ecgJkHzEvPQ2j
jT1ue2X7Xpmeq6kQPsGWR3YHfmL8NxenxPYOFTcKk4mPw88dTSpnJxYU7Bb2lQde+lT7HenoJC+h
D2A0YdBp9CMRAWdcBWdPJTAjbtkEPMRt8GEqBmtKvelvWSmQ169LPv9QYp0Ev2vLGWtk4PWVjoRs
kNdNR/grUdmvNDN/D874QF5+Tbf8wbnV/ff/5d8lON4YcoF30ykigcMWJ8/hSBZS8cU6qR5c5A+G
qYKtlvt+Nr2J0Z6g5VlFjzY9NtYfeQXWZQ7ZfRXLOuypLu/S3mChZbMwx/35kPCGP/C0Ly5G8xvj
untvUOzDhRvlumHYaVwBXga6zyap6xJ38kqCIoQwnQEvWRQ1fCUXxwF/AV9cWO+EOC3AjTBfkEij
zhUy+Ob3AlgEd2r/7uaHc2lfek77KmEX5dEfyXQaY9Csvp1UkjyvlhkxCClNInEwUvbNFHS+XjNX
oIbkHNOKNJa5UsXww1JTLs3CVBMwobS+UHOZ9B/srTU8oBhFrQzZBmieuBCn9JxO5B2y90iPKCDn
sRECRPgNavIer5VKc5etLCI7H0GV9sDafteDh99SylCQfmhXChB4aXGXRbCkV3VCRnxUdBQOpimE
SfbOB9Jq1y9I4/GntwzvJfmomiAwlPxknXoEbf6fLdYa8D+cebCIelKdV2NSYWlytcU6i3R1wdOo
druvsYPQN4p+KOfB2A1PF0Uz1LLiD6pBcqf+CNLpyvh1ajrGiLdJQ8N9qExjqjkEE8z5gKbJhKZQ
1WBw9z7Cq+h0LHg2exYVvH8881Xst3StRretRJDLIJxsNXVUPE3HXRqcYjs6LFD7YogXDOCnJGs4
2K4y1bP7plYp0nFZnaGH/szLAVLnNNPAjSAJmD633+Ixd0kdy5O9tDUJ+Tuc4mga1xZWCM0XQp9L
uoOc60zf1+55GU/vSfTFeDLZyItpcjLCJvEILFEUZehOgmTQW7rltYcHhVLrhM592++XY87feBwL
UkGgK9UAhA6v5z7PJbC+0vQVBwgGO6ZvVoOvdihKUPCU0A3zxO684C+gZvvT43ispCK6nA2nVu8z
WBE3tHgMaNXtULKD30jIXYKgPWXb3kjmCaiSn7W4CXI7L+YKa55DLJTEz/yXzCdvVn+utn4txK/q
QEkvOMNj8TrrGfXpXOHQEcYxJ5UzeUnsjZJLbbD8Vcf5vaAb4p9f7fGfTfs6HyuX1C33R67utMqm
Jhv5DOmoRpotWCxno6KBi68Owp9HB27XS8OHVj3D9RTz8r7/q9jL7kenyMb5kYzcvc1bnYnHzsS5
g31ARwx37581X5TGLxgRdjNHXDZ9Goj7x1tw6s6C53qJKqHIhJ0i2Vv+aJXQ5WQsdUk7ll/TvjwI
JZoIAk2k4Yy7S3qjyr1uVzegqRuuIDgoWfZLb+aOdZqP3VEBEuWQ8Ura/Gfm2iGesAEMUfQIxnM8
d9cK7REdEeF6Dz5MXr5c4dsyQufJumxxKd3OT0wqP2lcyjEtj2b48eNXQeZH+ciWiv6KCLAsz0T1
nWOqWHkrjnAEOI7tehnTTXgmHZaYAm1YAY+PeCKKAvhPPf9TNlW5tN2mYuBpyc5qMHNtTZ5cNRMd
85GUczzfTEckevfo/Zbq6Wb1Eq9v0B7hmPlXGCIIh2xnb7CXmY64jeV+SG6RL7cREUBcL4BHoxBS
MqvllGEqa/+sbocp+Zf1D2mALhC2aNKlVyS0b2DZjTWYXGv+hH/dEuxvxEyp9bAwS0LG4q8kO64J
vH8dFWbLdUvYCo4k0t2PgPtHA8wDl7P9rlDLOsrWBayM82LsL9i3RyC4v49sAhSr8QiMNMHnWJ6S
eiQdWx6WPQuPmlYndnU+RSFoPZw3tRez3WkP7vJprkfUe//xU3403X27xN70Vp6ArlS8IcSIeuYP
bcm58qUxZ9+dY0UM5tdkL/qnwUNoWe1M6P71AIuGm8+63LGks38Xfm69KDs6aIszXYyE7TKwZs+A
aXAYD4WzcsE9guQla/TxF04B8tvm4sa+ij+PxdwdX4eTSEXUJEOGdcKFyz6m6sE0bHQN4QawbqyN
wX2di+14SV5xKivdtlNEkCXsnevvao2pOBpdc7W0RzvrQ9p4WjqZiVZBZ5tUzZnHQ0zZR4pfU0QZ
40rhMolOgnDC4uiWJBW4aRAWuP+abBpf1wuGIYztLpnK84/SCgpBvokf8s5nAxpXtME01hkQtQK7
VYYhAbV6uZhaKzEc93Fr0OY+Hi9DZ40kXAhL+sGXaauhkZF2Stk9kPfrS4tNuJYINcoUx+CVfHHG
h0EhXBxfAmArRM7AVpbYDofdWc7HhqUsmrddYP1jY1xwZ1afg0gXy671jcdNIfnLNgZQ3PSps9+M
VcqmQHm4kdQUVg5jdx5uSuFeDoo0guVH0s3TJ8Q6E32yMzJ276mDacQCvUVh1BVPjVXbuda19SzW
e0HhpzgfQIh/+Zn25OjdGkZgpNAMRcDiwyORNOohcq/0XpbRyfkger61AqbTcCGfQhXInWNAj29q
2JcbYaFgIwK6FvOzEN8OzmjwjSQlRUmYI4lLpD+9rFLhxVDOlPzHVG3ghyTZwcJQF6YUKwSOsZPb
Naufjpv0Z33MqhlS4QKEb7X2kTU0N5OUSxxveL53ZHsfphSwzL1lWSf1h8I1CAAswtAM5jhQYBgC
ETtAv4DaGPksz8zfyxoJE+xGkg/o370gAnIuhPTFh6BPkZW/wDFjF45hmgOCY8O2+2VGsATJozix
UzZmhGP8CEfoqRf2HOnj0dyTnhej/r8VjdIgejzu7LkfPpgpeq6g/MheaRsPSOq1+ESaILzJQbAI
X+752KHqoVIxtBp7UR4ZSdISjiECQIrkZ8gE5tqcmzv4NthgIKz880HQVySvJ4QU+GC293nl2kpT
Vj1hEwu9UxXkP8sHL9AGX75qqaBci9uu5nZ6oh7TgMEk8zODr90AwjwIVIF2x7C12hC7te25naDJ
zSn9rtmHrhdQvGBUrrgeNNl13k856BqMM/TzWrQFt6ET5T9M9yp2hKF4yeAErUS5umSTUyAgDclw
l1TpluxVvCrDHLNJzMTL1vAy5fvJkO+HNvLOw2ZNQ3O/h0AJafr4GQTNHC65uEsiSMxin5Vahu3V
In8IGG3VwdXOXFLMwIaCfDsUBBoirqpmf3OEhckH/eExXtSQY6Nrcp2ONyATBUd7UFle+7B1M6f9
Vkib1mn3EvO0MOGOzVjLaOV8bI3DMd+vdb4wFu3qMRNOrqxqz8LrAblyxn7dsbdLXEXq8YPwmJ9O
PFuzwPBtSGyiydiShOlKwYNbFSf4DKUFvNDp0+lg8AmwBEn/3O8W+19pjdRC6AajXqIQCJP6k/os
FIHIcLEmsef+QMF6g6dnwp6lelK1qFljYVxyAv6EJy1FyqZ7JJ+m6r4M+9NERE7z0ERCaF4HIeFD
fVVvMrMlVDl4BYJNPNGrlxae69LmKYRK845yq4fhnwsIRSiJOZ4zr8dTWIoJ6C67K946EXYPCqNY
wTd2dlOpZIImXiSPD9gOB1rd4ieGfe6H2qWocN9QeO1rMF3FWJGyB8krLJb+WMLoL5UgPWvJpsna
N4+odbQh4wbBV7TZ740Wf5d7aSYdgg99l1NTGyjuXKAyPaSugmAW8tjYQs1ni2DJ5imAn3ieGAoM
qUJhDueYo5G/kfw+APMY0tSJLE3ne2cUz6Aqj5WVz78vNUSLoJNltESHLRwKUfXwl2kdEuTSTpbS
P/yE+tOI6bCRkNnRyXI2A4rdcYYk4MWrChEM6FP6Ja1vvqndwiTNMpZ9bZctESwlJEBH2BpCMXov
Kpr/HthT5qb0pcQVnjAM0pq+yyxuPdxEgQeJJVe7vNTZB57M2g90RNOOZneKTP4CId3t+rHDUUv6
kiDJHZ7u5s9waVi2LT4hwhKmBujcl2uzsj807gzWJGnq+Sxtfbb2ubVkuvp636u6qMHFY1LZf3YQ
lCQVnrjK71pBKDtIWH5rg6T/Xs/SlJKG2IF6pFy5V1za6kJ6lNhJZeFl6nC5Oz4t00/CefYkYsql
r8wW/QbR/kCrsv05NKTxcwZWP9MUbPcDbWHSnO7oSnGwhs3XfSE6xGHze6ET3rMmW2ni/vYVu0YA
OWxLtCmsh02sEyKboO5R55ABMOf1VQBuj1DIQQugZZtIwNI4Mh1Cm60w4+3Pk1nOdCkfnwQg5eJs
T7/8WiZTGUkNK8ePlI4W635GRLUiW7HMhJ+6M+B38RrMj/P7EOwCvmJd/O47lgfjRXrE/0pxYsj6
3z2s+H3Ey98jYx52KqzJQ+WToU7Pk8W2zs/SFv3nI0MTTnLNMg65KobKqA4/AV8f3JHQqd4lPCSg
r5wNiOMgpbfYrMgPxjmKUL1mZktTqydSbLvQiRbiA/iKH/jKdKmx4sRrHE0pJpYvms9xppJhWTe1
SVbHTORLczHol7yyK6UT/27/GwHNo2lejwk33A+rwjNY9fvgZof1/x/lkj9Xk+/85gozN6DTkg7C
+c0O08JFkK7/Rc7Nf9HdBHgG8FEwVamEfro5ovrIGWlY1AtwfE12G2xe+1kIe3FZtaAvJdd3mc9x
FU4i7S5/OLup7cJVcSD4OBOSw9wOc7sSfs+21vn8o8g6P68z/8jt7TlRkbAnrgXx3TsTuFFvu+a7
c4a8o8bxvPAu0f/yoy4IDAaStb4L6CYqz0izOQXQunFXiJG9kNhAMUrC5GmrUoYFihbCAx11AZtr
o+JMyO62nGYbW8JKDpzag1MLVsX5AQQtGWkrnMHGVlsSFufffMrY6s1giAKkjeeNRjxWlSfNrRUI
k6Y4ZoXAGP8lxT0CxJhWmQlNLVhEUcFWHCf6fW/76RxbjnRY2xgVPZqHgjXfwOp+D88qbQUMsRyV
Mgm+W5IshZJBBNIdU6Ja87ir6It+aFqZpDqUU7VhEVlCsyX02wAYW5Hqed39XBNYRRmbgqH8usfK
NXSnT/LIqoM50mMuLmY/b67k6+ug8+1pEcpg8lIFL/Gl+C155Vbaz4sicgm3Wwj86bezcrnggOPf
a4sSuh+4Dm3GCX3huzOtlgeh2/JxCNxmar/RZ1iHBxED8OHsUUg7tXJLFmuVyxIGCAX0gT35P9UK
BtX5PoGtpn/B6KlUEMYXFrb2dc6sw2rVFlysS0NkVewoXNSisfoZxP3w3thcg0oEihvFcZD0jBJZ
dvZSSBG6aCudu1WVIfnQWSLolewPzmcLr5BuoZnOhl4IvxAt9URs1KB3jqhnxkjLe3owL8bmYysl
hw8pGcUDlMJkKTUw066y1ziHE+Ecz6aORKn5fEJg2MAKnDd0zbrZK3JK++V2PZ+lCxiGwipd3MfK
o651OaEMM3W2sEpgK6fmZwrHeJqy9PNSknBBM0WelxVWfeSJY37VH6ioQ4gMMAOyPxKzGGa77h8W
YIQ0GhWHu0fXCzcto5ZC7rLMIsnA4YPfpF8Gb+xW2VMdkW8+NAbiRDkRXdONOuUUm8fpn8bbAyDm
8N3D4YGEiJ/V1GrvqD/RYP2YNj5BaR03+OvZ0X5IXWeecTeXNVLu86IYR5LTCi8jd+KLFSCu1Jsy
3v8weIgAaNyLoVX78QuymJNnuqwqlBeJbrNhgrsVSFQlU1tMxuQFu9idmrkiV1yDfmW/WrUkfS7r
kgfvHnqIWVmK3BsmLpyDok/ySB+Q4nkeML1Jy+p4powxI01Z9R3RlsknjfzUcO8qYf+xbzoNv0Dg
5Q4iPHcG59d7ahjJNBBTsAUIRdrjMX6wwzsv5ZL5+EqG4MVHfex8HH/HEBZhVPTk76xHxAKdOBHu
+bGwDDAl5V/XbZ82VAkG9+vnjPqy+MHymXS1Pjv+oOD1EdMhV+vngZhIITsSwIjDZIutFAXYPfAP
nNmj/5VrvtCJfKv6zz/QFR+zLBuFTxHRAf0V0wCEqMS/JUy8W97fWGtcQ/TPNI0/WOqJAcpRcctm
d+xoP1R19owO/Oi4dffmvsZWoZOczXvRBh4Iw6sl07BHUDSo2tSoKBd9iVjLCvIIYUq+sNFHdzPr
hd98XV+2fTMmAfZeoz5xe71P+3l+Mkx5RFBqEHVZ+YuKsr2vpJBiWcO3cXsjUCtRSiQjOZmQ2zw9
/IbqQKHiosEmWUSv/8sGESgWz/ccpyUE+u1q1Wfb+kSsOX8C0b/ZNebvM2mi91ARs3dUK5BTyvse
iFgB/juY5byoyO7CN2PuEUcblkat+nMSWdKJ0Nb/pFOMaBOsLiCIuO/wjSKDi+oAURk7rN2xHgLT
JLOAflIXvU57ZtVqo8bTf6bhtkaS97BuI9stm1KIFBX+p/Y7VVcoy7oI5+Tp0gsSbpjDpstopLbK
fA08pcT6lYA6o82lrNCCn9fEwcVY3BQ41Lta+hcHbkzBZ+sT1/6MkpLci3eCpFtoMOwMZ0rULbP6
i4DIf4k3iUto4BG/bWbv8Bg6Zflw5OSc07B5LkywUzGeddOvWLfePj5+xDn3Nb3pvIcK07NkNYGT
SXjphl18h0kpkvXuL+Dsv2RevMBKmIdXkbFinuotLgqIXpnRyugwr4unn6eVRZvod2obDXqqVpfM
WYq5+XNjKIM0E/DxWvKQ2VfVAPx3UyqTyOuKvCE7b1KAzV1HnHkkr9aF3xEFkN7kOKAeiP4bSKdW
XijnXp60PX3orGTVHXsla75oNxQt81ThDZ4RPAhc66vtain9M+WmNUh9YZuxvkio/D8pyiOjRIjg
uRV/Hrsjo3OyAZPNu7/IkMhQ2yI6ZBdMNAyDmUamLtpk0mn8wiuBhktTEqxXw/mZFQDN1RmgA1i3
qP0+RoxImbbggfsJuXMgimJ0vsMjkKrK/rFapXEoE8I2AVdRvTqb6/pAl199xuwyz7UmRGdSqtiG
1iwAst1ajPaeT7wfCPm9yd//DyWaIsERRmBWTCRvvklZKIUH67gH6u9v9GNRXlRIr8+dnLNGYuEK
z+fGxjticR5T4usJjAQ8ADpMh5Id0QYrz8VQXEvp98pkODcE+3hKLGHSO/uAnWn1bnqJBCAlZFwo
rDiM7QIgoIgsm5qdldrPBiUAqnLPtiiVPLLFxhBfGw8WCpLoZqz86rHC1/0nRxzLJ27IiXt6IexE
eOYVu6KxkKv/Q5CIWPkZxgIvX9RkDMCp4ayDoN8oBCrpNKLh/bA0BjlHPpEYWzenxAgy0+atciE0
P5WLdThfhnDMfcnWsbQuuXktNoBftL1uecSYMRMputtAhWxdemMFHRhTey8p1N42AsFNOWEBZO2U
MWiVgwjOgggZC8/VOaAKoZI0NeZKXEvocBPjOveV450lnX3KrIsJ/e+qqTDsN89uIkSgmvJWjiK/
qTY7Ws1u2KqzDAiALeg6dyYdQ+3XSwxuNho5+98hIT10Ot6V9i4JSCnI98PDbv+oKsLiZBFdtw8X
HkA5BKum9OybX3dVeN20L1aNRo1Lhwiekh0z3rB+rh6J3LWnrLWFQSTuobWTTGahvSdBh/O5SzWV
ylkWUXtfUZMG/mSOhLfK2eWDQE+cWesGHV3Am+qyTtlGrgJowHHeUZvbKH7U+FugWPAlXBoIUwoj
HCTR8/ctcr+56/0NoyLmzTc9af5r4JEjOboPT5BsFHWJSsRW/wA7AcvXeUxpDiNbsOmq5TIGMctY
kNjerhujfT9K/F4Isea2lDmDB4ziT1oujLQaYMnCM0XEudC/7uB9Nn+n0J2jPaWNtiC1HfCeiqmf
C1JuzeBr3c92T8u4XeOTABrdLN/4D8c/+1D0TPySFfXShB5Ba0+YIVLYHaev78gb6ZDRGdllRp9/
m0yS4hDj/i9qMYyXVX+ihVbz2o4PxEuIx4ZJb5IsO94kFPPrG1nEfyTCqcdZvmYrC1hvdMh8FzXT
ni6L3+3MlFdcJBljlEPD2R8j8M7neWLPUY5RvfzeQJ79BuaaZ+MTxF0MTA6BrdiywVD2qZkb7y2y
TviRAoCcmeSTMX9eXo53KTG3g6sd9vdnyuC7P25VE/uENUCdlVHg8wiljs4ZV7fsoKf4cyTtuEUf
nl2wsHKlaqcNBdBB81qWqkfvj0AESrRtCLXwTX5s5UXJZMOvIdsNDXRj8XfwARMq99Mpx818BZiJ
D/hPRpoQvS+6h69xScaZsDoUlsKXVn4bhfz0/Cp1jNcJp1dJZPMR2kR0pboRqKFx753/Vfa4cxsh
sH9pYoyIknVeL7I3EpQ6YAKPPlYrPpLZL1Sp+43pPiRSUmrLYs49YhYWMLNaYvlPz4EkF8a+med+
tb1AocuYTBqbUAYrCdKDStUy8U/jZWmsWEGBZwH5Hpkk1dXFkpcwPEXuZkIWqaK+Fe+rgqr2q1Ke
ngi1UjAF/Suu2hV9+2K7+qpQzTEums3xK/5JWz8TFhiXWjPJyUr6hOiC+BsUWIDq/wFer0LNsARZ
T5/UIckc3SXemtzZx2eZ1Z6NKmNXQxsn3jLYqpsJBC4wB41sWdttQENniVttTDBHZHQZb8Ff2ohL
Y3qeF3raNJVwnu9SiJVTdBWBIc87D4tD3jW5eAvi+DIY8a8cf4/M6jjS50RGhE4EkNdJE19HnnC5
lowCLoyJR2hq9VTx1JJq7jXf9mRekE9lp4d6OGt3yve6UtofGtq6jyd79XRIGPWwjKy0KU9tZ9qA
v97kMakYGxjI8oE/IOAtyPVt0SUh2NccZMYW1etUXk8ESEaACqAovzNXU0HuCA29q5PwkU0ZAvu/
gKJBGphAV+0czSnCGqNAS8JHUXhy1ngua2ArYnk7q80roFEROAURbSyGFeab64/lbf0fKPVh3bK7
ReTDQe65MkcecMgp/+P3vygBF0Dq+eKx7OnWSKvO6aR2czX9Ba8y2x6vrX3jCdVyMAjESpwIh1Pj
hmv2EFA90thvLParD3E3HK95Dba5t1T1eY7bipMdZruHxDD1fl4j15J4FCO4buERnGGjLIhlLtZm
aPr39XMXFeaBYEsgQhYTvHos5pYZsVS8gG2uds9LyrAKfdgF+ExQ+fp4GROipDHH4UGHTQgi+cUh
UqWy6kP+M6n+p8wQ+FykUdH98moX+T+AJwMMcTbzOcLptUwpbk7G7Ko/xjbKPwg4OF+zqAcgnOc+
bYeVR5C5AkbhypWYlS6USm62WcSg47ryetDVpWgByMHso6MlEf/ua6BJso/92IzkbZjtMdG5ie6N
TRQGIpjC4Km7ProKadlNZ1ic5/fGidiDNs3smT4URQQIe2WdcJY4gE9frK7houSVQUpn75BRDvzJ
1n3hPttFTVTClTbEakbZ+2Df1kbfYMtJiwaLIbjxyp01WaLSdkdQ6ETrhMpKIRhCLiMkgntWFsfu
Yx4SbvXDOtFH4IoJYwFrBCMfy3BWaM2pin1/K565uwH0V7Kc1syk3sIKAw8SxZLXBdeKsdy67xvH
vuxyti+FrzSvHfklMaQI3O6DPuFbLec6IYLYjS+Z5+75P7qCSGtoWmmDGB4tQK70QCLmBcbUvxzm
jrt/3l+REUq6iViXt35zqcvxYVo/HjyCVx1yJaCnK1IlA3hZ2dHVIPoQyB5K3df+Lwnox31i3DwN
8o1v/H5u1aUhqo7fsBw87lytKNjkY0gGlGrTjEV3rImYWCZBhfyiFCFk5iGjYXLcDqDOEyKD+5tD
owgUsklAFMizP9JT1C5Udx7ii989aL6r+Y7WIVE1VPw/M06p2dwzoXUPxZVoPPVPS2XOq5RvL24n
JotGJqrO4QaI5AoUeq8nCPOWRVLlMmScIluP4p4pZdFRs2+2xFCfO3Yp/ByORRFZiGPWrJ0YfbNM
MYlQumAgGZCLUHLobhURtT4VeuBvVhsECxamqwriqIyMdrJkn+IuBj0iiJ2xcVBwCSPk/fAYXg3p
MkfDj/bOJd9/pxkvsq7y75r9jwhudmQ0NzQG6jtOL6DID576HQh+Pukkv/TXZAEmskIbHWUNgpIe
PPs6mv8lqme3eWF7a/QoKW3KbRv/MaOGDx5AdlLBcJAD8Dh1zygyg+JVznVbhGtLkn6tnd0As/d7
kAd50l7ccEBFzoPA6ACdPjOUS6sPVFzIukG+0OVxKnZcd1meSwcod3lJ/AIbbRuP3IYeh90KWDON
IXIWSZJFkz3sDKImOOLob0qTQy9luREF7a7u2QkThG9jz5eePL0MoPCR/uTqzlHo7qidzqvBxFh7
/FXvdMo8MOKzX6pTbMS3evbAzEVMFBzAYxMTo9vhI6uvZp1yxG6DVXVvnRqmf0x2QcV+v4fC+ZNM
WRe/Av0rSaYs1rlKhuUmZIP97hjxIGOkwoRoxE0ZVE99JsRcaFN6KK2jdvistl5AmtgbUlXQs3sC
AYIviF+aSeG/Dbfk7v6qGZjrm2NhpwvRFDEfe5QMBknJY5vJ77D5bWopYfc1ogoKN4l3s/t5tzaP
v5m4YBXZmjIvIeK8lJ538iI2D3ecdjwhc5fqqP4yPQyoC+Hp8Ktn3uI5hjlz+Y5Cy+4SeSmi9p2p
Vij3S+tndZqVyTev9tBn+ap4mWkecCfafpb1Dc09V7lqo6HAJQPENHg/ZKt4QWXyXdOsSRCo/2L6
V11OtdiUjZrcO1g4DydxgXhfczAq0gKPQr0XdWG0NHjxm6AWIWxKhemonvdEst8Su6n+27Ni/Jx/
MdNLm1ssNoQI/9YvafwoBjygsOabeQlMV1WAWuQtTXLVxRshR/15LU0qpU4LHmZX0dKIIKl403m1
9+g+3EZcpatvPwu4aZN/uAE+nVJ9kbiGS5mKk5Mrc2/HWBlvozSDRVh94vdT6Zi5Np+BpVQSIZ+X
GZ5OIDz2bV8KggFKrKOTlFjHTVnvKa0BTQrsZCYmjGdMA+OirV70cwsnQHriMM5NjyQMsjklfH0n
Fg4HZttXEyPW6ZGo0Qe7wM91rDyLdHln2N0HgChdRWIhh7NtuCZCSPYCc81QP93afl4f2+79t2Rf
6/JjQL1/oOkSsrmlKZ+USjNfgxRZdKVAl3DVTGqk8CkDNdkDP3FdFmREAYVrfYmkwU4C8uiuB6TV
OKT3+BXJpysilerXEA5Qx+GDoxwQkRQiWqlw9U+DkHsW0hyiF1EHf3GZUK2N+4Xt7wx4iqy//77M
7Yq42ulCmMjPWmwAL7jR9pO12qZv13d/Jc/ewbBop9N57M+kcLPSeN5t5VWXmCp66Cpfky5GsXVj
yPMBAHgvmnXPV3NNG8VV0v9PZuJOmMC5iPHlB0nhyDy+MxlzE00RNscqL3QFsvIS1xXAtQK30KI8
W0BgSRRsM21lSTyOf0dUXdwAPUgEaEqQncxakXh4+5L0FvQytJLDm25DNwQILuKxslnu5muHVSfu
T7FC03G9AukfC2hBDmXrPPyZgjqzMgZAxqE2m6d9DRAqqP6NaAMHAmeGHhhI5gNtXgx23jqbv9Zl
ViDJidtzRAkmrU/Pjj3RKSIO/LMpDZjdMVmh0gbRCIqlScmVDvIC0GW0+TZLWcKlj7soKyQEZwd/
z2CEwjIGcxku3QzE6qmEir/V4oNBcSdneg7RomWttfis8aSVt2ZqKMqAnyPrnvKwrbHEa0C/yuQ4
sKuPFfZllb63/KfKAhEkrUqEi+ePf0yYa4hRtt2bUiyIeWZrlG+yYWFfw8hvXRZq+YPVlUYV3N9y
UuDRg/CbIYx9OIBOmVu6yIclFCtPHmH+BMfT8CU6+b6BCubLqhwTaPldXYkAgsBZ6myS1hLtCMFV
apV4TeCYN2BXb6zA77bA/3Yxh1Jwt+B98TUWFVhiMFWHiUWuz6nllcF24JKryuQlO2RjFr5Dne0R
yumOSICqBcvNPnRSgFePNIzHsZ7Ds3PBEdSxVKgJTT3g8nX2VbuKJdhYsVcyRrobce2ooQ+hxJhH
TmHtSItuzwozPMBZM52amMeQYt/M+Sy9tw+9RBT9Inxm+ozb/3K9ykgUotXbi8eChUGeJm1uRykY
blu+cI/QJHjtwOX6qQBPk7WVnqfwleZ89Qup8kMGqFYMTDq/+0F+eILQAOC1tFgpSNWY0FbXugQQ
lkdnD0+e+ACZbV32dxMYLIUju2hqwY7HptEA1cF31AYCQdoAOh25ITPFkEqDmQ/s+85pvSYnfT6m
Y9ShgrRlLjlnHP4d1jyG8nYnrd97kv1HkZSmR10sMBc3qMdR3tilPN3joPxBOHjetxHOnUl/fDhR
756iUNwBhzbg/WfTVL1neUqji3HodeSkuJx7JtCmfGUp20CafFxNyGL5/C3ofPwtzVwaxnHiOJYn
TDB+qF7LFSHJsKv7wlJ6w2kHnfN/8pV0AMAlL2GNgK7jbU+BPNOrb+LKBDlnD/zAlLH/dHIW9774
1C+bi2uXyPpEU2llmkjRh1FZXUHmJcC1dVVNus3aza5okF9+VyDwom5ufxOV3029So11H9QJH3xF
gWNcKby3whipH59YhBsodxvqZTrG0Uvsm7lh4Mg+PCNxL9sp9sC45+ENioIrCDO3iMsG8a5hLfj4
vZK3ZZ30N05eVrXZBfG/9G+Sg5rADVQuqEt84UDhu4ey4ZT1TaOkGVutc8vq7QmXDeJhH6Yc0kEt
YWPfj3/Ufnwm6GiBGjrO2azw+u1kT0GyG6IK36x6TlxLa38ZSLJTlAVpZpQ1y4tD1KPPgNw1o06E
m2U7U5g5zFLtuCJroG1avbhVN73ARyuDqwfngGQk5c76hgKLIMW2b147/oXo7GID7vKgm1XHOFo8
f57zRR5KQ7nRacQttQhQaHFDzPuDLy657V9Y6rv4CFR+CFKBB4b9HayTfoV6h0dfpGMEosKNaqyz
1JorI0wqkD7lekuxNYrQyz9Inye3kiOny9MtoEfgQFzwl83Df8S7rPKrnUjFir5c7ouDLVKTrOqJ
W+egqaVSXgBKknEqVdq1rvT5ofEizxA1xE5oC1Z2UqzWnjnIGA3woqWvyN+1hIllkmgbDr8xixEU
vomTt4sQprcJWfj3GCoElzomzmqb6yCLVrN8468QRtMH1PeidUesUxGcWgEdC4cQ0wc6zU7kqdk2
xFjQQN3Jf7XiJAhlClRho+1Hnq5qvnLoLnghwvxfhVVyyQX/PSwP69yNVJkl+0/1OYDsdEP04MQc
TOTN6C87EnDIPNoGoFZWJAYJJvqHZWLdfPbxRINiQ3mfrVjdVMAwv3nXjusgxwhpORGSlaamng1X
9Xy7FsdDXa69VdWUsbA9hRp2QomIy4gr9ZV6P2yjFL9KIhTaYNVF+lNmkJCcdAuiFHKfNN7jn4M1
iGhZ4iD7i/Uc1TAByFec/s3qGOIJLobeKalWDSpcM7K+N/nfD0I3pfmMdUJPHS6ks+WF+3JdAl0A
dLA+Ivv2ZiSD91k2PG8Ai6r+I74tsfJWcSztDk/LIlV+KL7xQaTnM16jbo50KSqmF1Qznsuh0gwN
As3MbKVxmPgpw+jRwD86xvcLfVF9EZEXJinc6osQNk1xWwrIrOWvqzO/FJUsY3qpykQMR6kVVD0j
Y9NkfMulJRvjAgWUyASZq9kE8dK2DsEwFOJEvbr+R04xPL7fuj7FCNuizzXs5I91VAcDDHEiqthY
39K+xRkWX21pMn9F62FROmTuxI1QewTHSJ4N8qDdTmqCgoYZdpDls5vCIH0LKYjwuOWBVOSivGst
gC0CQ9HP+kWrcQY6dZFc0ycn3fPllFH08YYHsxmBHeFEB95qEBocl8JRDJO7XzfbLorzmijcF/gx
nB09Wos24hm6EL5mskWyJbuxA/vnA6ha0nHeLa+2aKU+L9RJl7WmLViy2/tvajhVzAygZ3fnCpvQ
4ewxqgKsuRNOVMu2RHUenkafdLYbsulYBuBBDfhaXJjDW8QhfalH3pchncsgcWK8elYqhNAdcEhB
I2VkxQimurjuspoRNBuGRmcnpP5D55TfxDEQnc8Mv7Fu8ErzX4eI1X2Wq/SJthLTvuAWK+IwDmD+
o1ie6TialDDgxGPcvq0zYxKyZsvtK0vGx1CM9LmOiMhLhWSZENXa03BANbrg/2Ni9pjG9QN87OBY
DSzIYFJ1ygjMzsGfE8hafCCFBtN95Wa2PN0bobH69E7XRnQtaXZF98GYqflpl0uipfWun0IHROuR
TDQZU3IOVsyFnkVhOYhlCfMBUnG8BZCQJtBdhvuO0yDAH+kz5Zf4hqbTJe5Sgs6hYu20lWw/AP91
1SvP1tSMcx8hZpoi42YRYqn94QsgHTH8TxuHHv1l8Jsr5vDTTEPif977nWqyuQOdIGnnb12Fpb4j
HW/eHHmuD1LSoEmELfcUrl8+y1DvrD0L4Q6xedF9EBJKjB+GwUlZ6CMxe+Bgjq8P6d0VfpwbeZ33
/K0+3RlTjscu7FqwvM43Dja1/3mPX8NJnkVYGaK/6wZgtooRrhfsyeFJKaxTEJ+FRtiSk2LdYUR4
JGRJj8GipDiOQWG3aQdHR3Znybrp84LRs5kTk3a744xbOB19UEYtSE/cPBn5H6q2zqLU5odb+W/m
a2MNXhmb99d+E6wJNz49+yBPVO/+/7yHHJTPFUbXZ9iGMnkwS3utqM07jSznqKmVRMlJms4T3XJ1
zPPGhdemCmvQekb9BZy5ER3SSwJK11hReAjyEWWswna30u0WnAu+MNFXz71bo2GUa0uflfBqx03j
xooEJGiFmhDC280U5CgG9ra2ZNI4UgicBrZFZRzGFyYmACbTB2W88dFcBquNWBZAnVZ82y/tBtuZ
x2tam+NuAyfPd0T0AeQZlAqD+fGaauQetQLb8IKgM74zw6gL6o/kFgD2sEUvg3e59xMnHys06qax
TgPN4jX7p0z+PXwzWVzqv62eEAppeD2R+ixV4yaG9IzRg7VKXbUFGKh/T4CpOKxwl9E82OSc1AqO
0OxbLTzuv+0ZjMBF9d5w9bxb1tsAZTXptmOqlVfDHVWsJJZjS5Ae0UHxA4pleKNrvYXobneSvcNf
5HsoqHvZwHDcumxoPEenEoZvNtlBCeCjt+U3FU2WZxKvUBvPdliS1eiSZqFPrD5FEOr5FRXY9U73
0YT8AsNw05Ndg8oSGyEqAVA9qk7642A/nOZUHXCD0r7LWgL5sXzxTcwTv0sps5Z5Y6oAsEKMo3jP
jC0PBGp+8co4iyMsNLmvp1GiaeUXEVufcWpOtJsCaplUqSmQSFHwoTmKgUkYuLC/1aooeV6EJ57F
jvttDt5rvfb0vPAYB4+TIIuWgFlNk5pXXiZ6Ku7bxK6003VOcWN3YKUKejxdAXutbrG2M5ATVZOe
0DVUbeN4RemJH0HfGG0SaWBwxuYr2DmlJq6JgBWL13mlWaMzpDMllYm8O6Sek7LeNMlkhRslVCGj
LxdUDcC5p2LJhDO+CxjdhcXjewdBQAQBd7lTIGwqMo3IOiowvsvQy9FYmTQyEQRF2+u5ECCD0PJf
L4p7t/IttibybbrcR/CL2h9BfFTM+eUGQQ5LsQ1hOvJ2bvyurYGxYRBlDbO1fE89Nf1aaVX/7FBu
J9M4aMVnnWiQ/IojSANUkZVvFkOwq1Wg4oui9p3b/XEmIzcJ5BYJhmSKi1/8usWFNLRMtrQcvlO5
r9FOg96heP0233ZEUrtjr6eNXGWQZQI9KUaZCalKC5HQ1YGDAvPvG+UH7Zd++KQqAlBzY6PyM4O6
THvQALQDjRm5u9Gip1iQrmj2yi5FJbGr7+MaWLMBwYP+NSNGYVzNSCmEL3+vcpO2czT54Zo8uWld
EHQTKvxrt7F5dNqmpWQfv0jt0TTpq4h2saiC7uVyVyzPt5pngTGW5R082tI7ByI978cNRk+Agf/Q
Fmr5JsbSHe5b5aR/Y86gpnd7771Cjhg9ooZ3GweZ4ROM7qOl0EMYp5TbGvwvwxv4nBTfBBl0hETM
b5xpHp8nurzPQ+5VeaexFUgxABvbvYjfxtf+I7qV34m8RogCoNHLOSdClEBHqTVQQ33COqnIkL0z
JZJL4bycLMRJcTc0XYsCKUZfG/ZoG1ncPLw5VCtwl6ULoKwW7AOtsqxggUIMYUu2V5vq76aQyWxg
mAXUFSklnri+ZUExsWW8ZWlfa3xjcSFIN6e/2Q4uVomMeMyG7RDH5AbbH6BfUjSDMDHuWOAZpxLp
D9lpb+fSszgY7nPAsEbKboWLElzSXogVd6KIsVjz31Oauwn2Utl83aLPU0sTTQZ7proQRZ1TCmAT
UhA+rCCbNM0toQNft9J+RLNptUkaki4vaQEm9IDaemJOELv/U+0tjNhFalJ0ag22Im1OIyPaR6Fx
pPGrFYjp3h/YTM7YCKNgRBXXBEeWQ93cwg24bDNU5JMcOsPKOBDMRpwI963I+XK6kr4qjsuA6/C8
8EbRPoUI78T4ZLa+C4YvVLrZ9Y+sc2oy15T32ajYVH5qWpPZ8r1UHacXvBK7AKBHJt7d74rhYlbs
NmXmNDnhbABReinWj92kZ3xbo8eDpaWXWI4jekOa9t7+cHHstdwQeFCIK3kYzJZIzxI/XfZCCqk5
8YA907oEF2pX4glRow+rU4EifhD5tKlKHZfoQAkqA8N2lbODHZO/z3jz6wSy4wbRkRW4DEPtGa5w
E9bZMMEgHcTLSfdzJI+RF4FDgPW6ircalUhgHekKtkwVdPJMfI4bxRu8Vm3JWBmM2EFiKgYrE+h0
iHYPBv3rP5fkf9KT8OvktZl/hv+mUq1426FWFIxmJzEZg0SEqflD/As3RsTMQ+OO6F6rMIc3Iv63
w63oAr4dgYefEUehvp7BZgN8bo/iYZAwuMMKJXVkxi3Mvh2sn7zzHC6jOigZsI2UWmB/4l9+V8wc
6ykY3Ftt1w/MWKiz+GM6MjJwKHKe1nTBmtAJurqaJFk3mYPHCuGK+vOibCNauyufbUP47JwuHlDs
S8X0fflF+inxfIEbvJCjr4Lb14qjlRH5T1Nd6IezWmyfksIb0IQMg+A3whysTzvIE/3C/mVgs0JF
cWUxnTg2o2r8lu2oiwT5xaFTgdLFmtO24OMAPtevd+Y4Cd+KGJeyjKxHWZLy5Wp1Bgel5LIfsDzO
L/MgoamOnSbjAn7/0pXm+oP4XcGY/bKHN+SerqFb9n7RzwpcD8mD49Yi3zHpm2dV+LDATeJUJtDr
+w8oQGrlN7brgkNrE+ocUMdYn/RDbXT5/zWNjKi46/+IJZf7UaJS8k52B351cl+NsTmaRhdd4y1R
x2dmsVeSGTcA8zSRdfXlJCkqjMM/aAQBp62hoZDnJ+ACwqda3hKdBi8ERQIVqH7vwZwV8xQEIW7j
xRi8CVRQgGu5aZr0x0N4wnPcEaRTd0BkFakAJdzVBiTKTPtbgzR7mWAjEExgGOsBdsFDy2+KZzwc
3mm8TH5uKkG52GEvtbeCgtd6mcAYaBXQI4WVHBaC5F0Q3n/UXPgPkGgyhM9t76Q7z48shO44tnzM
TyTDkHgiWOHH0W/+6hzFfugva9O8OnZBeGKW97E0R42bgAGy/EVFP7+KL//3ekjwFpTg0fQ8ua1k
JTrlrlqCOG6Ogen/h5VDM0Yn+tvAk0bEfObqh1A9R/k2eXBdjDvctCJC1PfoMoACWOvQBut/oGuT
5xPHRYt3Z0CX/+UgZ03Y7kDZDWTPk4rv/CJqAcQ62iyZ/SLX22s/F3fGKTQbXud4vT1O4UAm7rTt
yyiadgbGlIr9O151nYH7HNa4Wp24h2TOOE4a8bcqs521+apT2f5fYrgu8xaQF7r2QfPp2bnwNH2u
j18EVwkFJTvZwqF168w8+qB3UQz6mYpJ4gfSrhLHks3GoNSswdzZainbYMsnaAONw2uWQDes+Ubx
wJk2x8yYNdjSGhakmRVU0v2Ats84G0xG/cd44zMrwztc9pYMwHO2+c5dNlJIzNosMXVbqz73m4qW
575IXe/k9o5ZGT6LQ0KgELx8DVgtdpCJWAyF/xP5O4K9C7mqx07VnJKMZXz53gcf8ERAJLp3WZJc
UylrWfrELagJpRnaAPq/Ga1Sqv99v6e+3YThgNniRWI+b6WJEHvcw7I5b4X3gIwuD2bga+NdK2Ku
bZFz8x21aAe4CYnArM/EyUXOyLxKiQ5fq1Egj+D6vAH6iwAKsLRpTCzPWMLv48tLtm6+qZrsXlG7
xmYgjY8OeZkLgNSROGWwAtU+2J72lGjtBoAsRK0g8hZTivpSn6o8bOSFNvBJGtS9VR/jUYkzSzTt
fQ9Xrj/AAHB3pONCOWiUWIh6DjhJiYx4FpgyRFi2yeEnIKflhOYWaGRh8dvioyIYh8cTV/5N57Gu
O2exCvqjcxiwhXZ+GqX+M1Tr37LFWLCIIjmYFh9swC0EiRhU5I+S/krFH9mgYDN/dqF5lUwBwnRy
aEz6Tgn1UcGFtaqgW9k2GFBKQv7/XmGETDVNgddMOwq/qg/XSvktarUIZnXIsN98s6BrLMLsDnad
jioLvOv8bb/GbUOekltLwCh7RXo0J/J4p7fq1dGz4zJ3cUY7TDd+h3leY+oDnwpN7dsOgSHUcOHP
ASHmkNWDRp3Kav75Vc7BSk/kVfyUBuScyV1nmY97n2CmsOiTCZO/DMjseMOC09PyX5WI6b9pJuPO
Rdd0mP5ECkqfqqzCRVBe1nTTM3X+yX8zM/vWWJepVp6s7Z5wy9mlIf0zlBBW0CNuTBtptZQMvSwP
3nFjodahojkrWVW6/Q1C0vQF1PVIHDQ1uu0gN7gYAPzf5fEaIDxBGU5EhLKS00qIXsZ3pIgDm1Tu
+kpB2KOt9unYC+hREM0WFD6IsiRJC1WQ3eYATr++gjBkSqNUDr1j4prGHQL9T3+jAEYPj3WuEIov
tF7CPJQGuuUDolXUFIaaV3/wtkYx93XjsFhdXtZRBNu3ZzR7UpJJykRZ4HylqxCjJ9VrfMUrwlyK
2QuIi12eV0MXzSUzf23tn9fQ4nDsLkWLt1XWw030bWa0ODiA4C57CZtyZ55FRUaCyweIC5KpXIzg
MSF/40ThPpyqyhaabgkE79G3i0Y/TlwEqtKcAVehz10W15h35M6cDE8RbuMoAZaGEV8lSclEbZtV
eo/17elZ7pbw6sNVnEPztHBZ/MriZNhdb2+yVZJX1x76697P1bQrdcs6WVaq81THf92izv6GCBNO
Ic4KxfJjE79QeF6D5DDywXCWvhN4k/jNmMTgpTm9comwPTHS4yL7M2uFwZIKeCLrcd0GOL0p6Mo+
gCbjDlO75nUEZIA3feDjc8HVk8ojV1wULOV8QoizmFuBX6gQvBYBgTmdZ42bdonuRKaWnfJsjx1J
FBSqntfsgm7H628hbI5RYETuZkaLGnu+7ujWd5cL94NtE1xJpUIdvfOoxaa3Exl6m5Fhkma/h6Ku
FdlKaBLtyM1qH0tvHuKYnyPIonncAOJ8jukhc4AzTGOJsuh1fTgi0msuxUBeilimPmBeaPvzpbkX
tb0B59Q1K4BRD+ZhXkLZHV2OEK7R2xh/6c4stKJ5INHS0F/SnMzUZTVBO3ybH8IfpZ3Ak+WxmukA
0ELoQKpjUnG6Ym06/pFfKX7U6y3TT4nwRd18AFVKC6YPv3EKheISbDNCKuqvjv2PpRN0GKkWERNh
geC6Bq2psjzKn+B2aPoTqI4Ds0ApRicTHDveTDNbAY2y7zXiLmnEXWnJytcNQmB/sapVLsq98WSj
ft1xDwHeDrU97g9xYZAIXBHRWbXNDqhyeFrm05Yg4FAufISVhXXWGnFvGs5j9tjPNzNxPwp3eU2u
x9nYAuDamxR0TYBZYgVfkFiZ2RrFbqRUzHaQf0wYZrAPuRIYU+xmlUryDjHMS/DylwuCxE96TlLZ
tYBLypJKsxc+1E9MHzfZeTqqJVgDw2Fb6rlsLlznSgxayxZs63n76vCoAPxoW1KoSfWmxJu/sbad
W+Gn7Vgg9Dxa0w+wFMzpu+JUzKKF4CJelxo1O4epz+45UiHHghLsOY28QxD0RtT8ROjq/+duxbLF
8ZFWpQ1XmxjUEDJZqLB3/gRQQ1knzLV511bOrI9/H2yy4c8y8hrzDl76sJd+rfzbNr88NqJGvlxc
14EhHp4tLMtIjA7p29bKJ3YB7joYlyCWQ1g81fIEvV/hj0Uqy6VDhUrOIMT7bDUV5EBc59hpPZ/x
W+Qj0s8nKafnHMFvSp3tOuP1P2HJS039wTRwkvs9y1CMS3bTJbBYOzSGouJy8Vam9JndZxpepYWw
G99lHjaLuvSwbrV0HhBWmL1MX/uomnro1l0tbWrMpKSrKd8DuPnsuP7AzUQJgNoYwNpLP4evaPRK
UHa/JBHyJ7FGFbNaJifiOeN9LxWZuDRwcAsW/Uiy/K4Ut8I43BhSiMcOJ97jQx0YRULrYus8PS8q
cnaCChbAlLRizGgDd3gEDrqS1Zr5TcPxoJScqjpEI19Ie7h15hbk5pnfHnwALXSJBYN1qxTUVJ6m
qqjKK9nmipir0BKN9kr062Q8F56CsQckieqrkf/VLytXuzLr6mNipEK13yDJ233zFSnxEf6sAnle
9nich6ydEvzK9U6hRVh6RM9kp+UpnPvZx7vohH6BMnfGN020I7K6DxPVnTg5SUIsfNyGmMaNgFbd
bEKz7tOCJg+27ZAmJdD4ooX60GlZg1/SZNeQT4HPurkbhsA/DHkugvE1YztI8d0Ce3W9Sj60K57o
zDScQA6ZsjWxUO6krK48FSwI8gvWJmWXg13Ft+kGY3OHKYFOlo5B3GrUtwtEmWDklqGo530klQtw
8Q+UrTz3ln1MMngChrU0b3pMntgiHQbRKktfvNHOBG5haao8ubShGYCECV7MWLItY/hqv5QqBtVV
Oou+aaVFm/ynGsgIeb1BNiBK2JmbWWaVpFH7bwNjl8yIu1ewzIWyyVEJ2z5EWMAu0/oPoXIcDpzp
NEPJE53/oBKCZvPmhqzKINdymUrxtjF0IzBPg5TwlCFrINqUtpp8hx4tjG5YXKTW6TvkACkyfJY0
1niE1YELXiDf5k1ZLZ+DAWILJuIrpy3rtGXRCL2unG8nsDKj6nMDdhcFvdDYr9n6Bm3BKbV+mwHE
dNZUp0LAaFxmydwFCe505gploggyBV9Vtm2gnWtHTikCWQsWtnkjjHNGimu1kIOJAPSylbXYVUgm
lyQNYY2ZNU3NGPAryxOCk2ih+h5v3EIve0rSylucOZh+puzh6yNBvKfcu6Lal1V1lgRWOUdcsKzd
Lqo9+mHKw4m21T52dyACRpFHDU9EkrgagucIFEEVXY9XdKRduvxzONL926kFshZP6NczOcpDKf8Q
anJv7yatr1/0jowTv9BI/k9dSwRBqA5ea98RqBr/FIFkYvdvW9DTVSLE5bthUSOfehAnCArzF/ko
fNdaVQbAOguLACxaCVQP+SkiYTkvdl/6DRgKh882dgtNZgRRymDEtUO8jaE0zaRVKOKiSAwWk4ij
/M6y/2IJdGLpXqrdzsihRd4FdQFx3ce1bxtTkaa+Y9+SGsStnwTejr9gQ/BiCaFSSZAfhib8rjLt
N04JHB0dtGETTsJ8/05V5Wiv+77si8T/MsMckbunp9nbz0+O/0Jl69r+yQgZifJi6dkgkNcCfDGC
C49WnmxvVrq9VFqCnQ8+wr0J+TkodUWBXnbkkUfllLnW5s/henosJ5JaMWJ2yFcl7JO889QBpUh6
2mEDUWppHeV2hFZUupN8TkNxOcJbLcjY7hf+llQxVxcsHExxsFozdY0fqx8plB7MS8mguCA6Fpjg
+O64YKr37ZRQGTyFVTODTphNbFYtTjapBEWkDAHPPpqPVi3bXfC6VyxSGCr9r01xd08adiCoDtwf
l+sNphf1Ur7SvH8TL/XWFpnGmjZsHq9FThFXOJ4ugSmDPb1iuicJKKOG4E+PFC1yo8BczWSNfjDL
tP/sgmCVO8eumERy/VAGoSorFTxJeF58TmyX99Fk6Vp39nnloK2dJeIJ/8hqJ9VJew5bqT+BHxOB
iQ0hVsKsfQSy85VfK6YdIQd/1PZJnSU17xXlhcErWAl2ynHFbHloYTPJ45y2Lebjbm7wQBwMvnW1
gASlfKNS/djwRgWsJ1mM4VE6QoIwZYyBUnpqdjcD8Sk60GJglpYGl3AMtSFc+hk7SG7saYHKyPpm
G360qSCw84QdlNIG96/Q9A2dUAmetD3I1GU2yMqtIs0DIKEFQNQFtIFgoThjaAca/+5YR9FzfhxD
InwlTgkkASgpouYkYbPU8oB36Te+1n/OzVMDZE0sUJaj90HIQdqicv8kqjXUTRYYvaJt5lyklWRb
I2Qku+vJtBep6fV+I+r+5a/DkQBdHot5Khlx1O6kNnMZsNSChqFzQyZFRAoLP6O7upDOvMbDZdem
DtrnWfB9IGEpg/3SAwjk6n+cH2GOKZoleqAWrV9HP2WXmtUUcm/4Gzx62gemp9+QOdgrJIxIDmw4
D2nW4JH9cxr0IrqfQ8hQVe0fWI8nWWC6qMHAQJHhDFuzddqEhG1jMU00ppTKM4qGpXGGt/uiImvy
IAYITFFUl10o5JCbBqm9n7hzHw79GxT6XmZufTuPTSmUhZFgIpGyGB+sZkT88gJX1k1GfX9XA0WQ
mPGh3PLX8wpN04g1GFdydk5ZkepBol1V3rmyoLdL4eIf2VzzgC2tEZBZraIzLMI1MUVj2kEu+tcS
FTnaqxx3MAgjRCM9uQr9d2/RQkhs5+DYG5XwR7wfjrLhbhYH26x5r08mBdPK6uIvyA270pv1x5eg
UQa5jnSj0EbU3PUDvDUobPn0hFLH1MabLeQq3PzPIscBiITzJorhJ3T1GhjUl5kI6RtZCfGMdwRp
nUgviZ43v61Bes8QjPSN/Du0QBgs+gj3yPZogVgmYjZMaJPMUM8eCZMf+kJqQGXbt4U3WZdl15KK
vClmST6SVx8Yd0WfPTMlU93rpSBXc2E+LC5qxeifZN8kMQ+slCBRm/k6MlOp5bBWq/dvzIaqCQMT
FxyHiMZWY6YNmy8iyfT94QRyE5NTuH2FJ6iArfWEWWUvcJhJxeqOhh1c+9RUDl1XBCCfP5/7sRWO
ZSAcWarYitMVhu+ryZdp32tDh7X8s36G8FVEcdnZZi6jXtDKke5EyPQWRcYQQ1uZ3qsTyw8qA4rq
ns7FwxIav7kNevX9yCJH9Wi4qkWOunP62uztBfVF9skYPtSB4wnQhW4danRHrZP0gM9PpqVZ63LP
daNUqcMuCSNB9jggOzo2LAFPpHDN8kpXrzZR1gdkcNq8WqsD1yJ9OnxEa3trf8eq/q476BgpQ44r
cNzncNCn0mep1CVshpjzUP1wYvX2ptvXAQSAXuKPOPmErn6zNzqijrZtM7zSc4lURu6jdyi9CiTA
GqTr03uTDOEa2YazTsNnzQlgtLZ1b5BxNFUcMDKPmJLzS/Yq7EDWa8NVxZ2AoP+GZhKz7Owysene
k8OEc1HRxpTFaH/uLHg0EzTqtFPCQe7HT/Z9zBcigSVvEzf86tCW8vBu/+61QAMY6+Ik49EQBOgk
QRMnZiWomzT9ukehQ2RkVqGP89Uv1jfsXzNBkhlrOfsc+JWDEApseZiebLwg75qHRHozRngLLspA
/yyWREgJTF7WgIOOMSbDNnYSQl5AFStat9NwKl/lyL8frHkOXGUNnMxgZMFbgUc2PmkY0vcp1QqU
E2TYxRsUNXEc8MSSSA5Jmtnokd62zFTYbe7F9fIVUBx5pCBhF5ykIVtRHwYWC0mBhbFdqh4MmbPG
aCElhLaisitD6UdIE06jZXFcKdX5rAO/MfcCAx13m8d+/ZqbSQ9De5plkpW4tzcWEqx9Eoo+wBY5
KtVjQiupfW8fMoH4YE8OdoTX0WQvXe8ovYiO/iq9RuUpNhc2BZp29Yl+GoBxOSm5Qn0/pZH7LxYd
/SycwmKSvqm/19E/MkVKDxt9xndJH/QTJY6LYx8jVNu6f3uuWwi2wdYBS+ZjJ2AGFXP8+wEyd0D/
Zq8n0ayJSXr0FEJYckfFNritLjAtHeT9DYMPm+O1I4Tsvt6y6AwT0XK2nJI9AxhXp81WHjwDeF23
AfWT4YEO55FvtUZbUM4q/pfYkhA7TbbAC+5XYKUWw34jr3xFvW+fR7vDBH4NhT7fCQZvCCz3bcPo
utjOiZHcLSEWCKYu/JR/bXFlPW2Qs8nLvYizPYRBI/wNg7pxHSBgf+5ULKrUMWHl8VVnRpi0Eqhl
YVruukrs8zsIyWMPe518tCOPLyaiN4RgXk1YKgEa7wNGKdlE/C6HqDXSy7PnmX79fjKrir5DbGyK
g0affWoG8fXbNKXT+GxZtgqIDbULJSlPRUt/Jc7o6f4tjIBlSAfimjHjsWPspUmjUqVP7IWO4iRR
SYJEZ1yw1dxCsyyAJQy3ztTzZZxo8i+fZZR5QJNCcfXwA6V7kLAqi0yQjlaysJp7v29jfHqkSe3m
5uqv5j6vS082k7bV+60M3FQphF7jehlxFUsOFmLvFpVM7bMIpT50wylQBY19st3hM08MK9E4GqYC
59oSHx9wdKClLH6Uzec7u9+DjwBEx6T/aCnskPGEyXHH9A2ttxF+YGnI9kGLBEXLl3xwFvCKx3JJ
BanZANu8JW6Wn+QsicPYB2Mi+r3Gj8O+RuVZWZGJf972prbTv9RpTLmLhI7Ftu3Y0V+PB80x6IFD
E6p3+P85S7GkD1f/u3UC4WTG/Qe7yUOtifNPAby022OyJWQms2ZcWVSL/7FN3zxaT3Sang1lJNmU
FUdg4wszA6O45KBw7+rLLJqCHkbcQmCvEFvMP+kNlu287LAsDjLnyS5Pbz+t5kpk6XYgTDBQabhI
dEA8DsYY3vHZOPQIZ1U5YLb1mzgqsrImDtVm1znhAqs/CpFBRqLZPyh78Pf+9MkXvyAnekdL/Vc+
axPH+8huHFdje6V0+fa64Edax9hEqVILZf6YxtyPEAGc+er3p2wjvT6AUxT18TN3NU/t1neWK3Yr
haGbXaU7XWTd7traPfQH11tTnnWJ2d34jZWoWjJ0xY4/QRsak5VW1U1tlB/ZfaXT/SjuKIghFmMR
GIY3j39NR3zbcs3X6sxInRKocC3xXTcpF10fT6q3rJee+6TU6bJu19fYJodZp/EBhtiPtoHN0EUZ
Gl2dYCJytO+2bNSzKe0sRJF/IKNm/MhqAvH8UH3DN2NSmttx968iPz79E+t6v5SOPDwFj7XzA1Ec
z8bywpgE5i+fyjHlsWyMILqul05q6RtRZKH/p7g30qHTASZLOnq9apzdFYYiC0WAsYgFxsg3iKTP
VYNSClfSLkgx5y9DACX+wg6wdgsYylbeDX6NDuhVQHAQJHtyWEWlm+AmbI2KDaBWGhAAoes2HflR
X9rcXADblyZtZWO/Uq7ec0yx0ewUZo0QMhXX+S+jusIzbTmuNWFS+rEeJfkw3y2Z6ONLVh8R5seb
3g9EgtsX090iQAQZFUfC1Ty8d6s7ZxtVBprmDbU+pGeg48lBQycdlWn/33cPS3QUaobQPrAoVr4A
R/IH9KVxmlgjjkxGqmjivaE2lb70f7QWW9gFgec+LxFtFP0eGJRX3F5Ly01XY6rlZcsgxtYnU/He
WNCqrzN0SyIulbAltkhAukihMyRbA9MGXm20ji1vmIh9O29ltwer1BimolYwZaGo7KP7imSPL68L
ZRxkes3tYjyJcDOqRM+4sGKkd662+RLbrb2ceFW9IlIJqB7xyNnzxRTfBrBxxyciDnVVBKNlpPH3
+Qufc0HV/spbR04j9hLvgw4BWdo22JYsePiisC2GY/8A4WsKeNC+LG6p8B7o67PtWTM3YQsSVy25
Oxsi/lMssQO8iGASM0yOZCNsUbPtgunDKvvbv9eXZfhiIkCv8RjV6U+OWuwj/Wo7aUFnN6Zew6pZ
dJ4fsPU1PSimCh++6RE09bm3PNbZRW3hIf0j4+pSP9Pp5pynQDEb+naBV2nzBYSlItjZCA8+PVPG
Pp0f1EFTiXVcUqWemV6uf6wGgWbEY6f4tIkIZUzz4Do61dOYnu+VSVBbfhoHbNAm+gAYLWDxCuOE
gUptt4GQhlLhFbS2vGLtKpuL2xwdJtQ2mP4mJlAQvCaoT0zSjaoOF1uT0ikuy1RXHa3bEiOq3hE3
ekLdFpkdmE0qbfpgUycdjQMkVPB968Bcor2AqIUJUe/yJhg2hE056pPQ5nptoZWcBYPB9b3zx5Wn
RsLnXUzpomDgyRLd764PUEsdD3+quRIbmMH/jJtWr+fFa3BZuYmt2mFmIi3EuV2qaBXIlHjxgdCy
F1kxZvJY7e8KYfX1KxqNON6oOK1er9jQNV8YK/thqf5JA7o7jvH0wfKdeLQg3K3OaRSdosiQg2Ov
Z1xkSQX7qWaf35ZOFZTJ5boakiC1guDs/5JzuZ8kq2IpJkzag/VQcYXLAU1Y1VBA2nejYscKPrvb
qf+rj3mWk5UJ5ZQFkK68/Bz6WpCdEHq+xUCNjlg+sINrnL8yrdD2V4C8R0wnZ8PgVk6ALNzF7usE
Zo6qs1JtmyDtAIW8tqe7qCs8vKkHVdOzxomk4xcP/fJ77BQffKAc3BJ7PeJBMQPSOc/Ibl8CZEfu
8DaU2NbxoWn6HofMLt927Yx3mN8W2LZeunUWGeV5G0DAURIj8c3hf4isWqurS3CDvJqJvfryOwUO
ELWxJjarTKx6cdD+Qd59Rxf3aGlxGKryezgOrP5hG9Lgsi52P3+n2a+g0qq77q9pDRs4jtoI9jxq
C1Znb3dU3zRhlmfTwxZrGl+ZOkubr2QnX7cJdEGRpbITpYVRiBdBanH2IDc5HYNkB2pdhr9D19ZS
b37glWxUnK0cM9ahBjpIoabAMnEEb1Id7zvWfySiUlPDEX6yDLcUhMlbyqzHxcdm7SpldB5YVgG3
i4SNTmGzSK/TPKHNbRpcpbsaqCkySfUrgNr7lnyFUIOUTP0fXpHgz4SEvtsV7prXb0QoVusBdfPI
F+oLh7DqPtq1BhnDTFEi8m+JMORU0bDh9M/jbnCtf/O8sQhm5Ft/nmUOky979JbwZGd57/bsXy0C
purR8WBG6HB7rVKli35O5pVQlCoHJrQU8UTxhxtyuSXbLLs5QNOrK2lX9+YbsvCZcnCMqtkT2kus
jQK0qM4jQbIdiHY0FYAHJ29hP1KCoCWRWR6YxkZDRcut0IXmT2ly4WKJLiOF9ahd2RvUr5azh7Fg
nZDsZLGqBTELPmmozu4aXI3TCIm+gaCLwlnjNsc4k6iX/8D1YCoYJkuCf+p2MOw85NEAkFyf8KwT
Xu+j7tqw/VxKNAqtSRzIQZCmlHcaAb0rkoOa57/JwYt4EeMEt/DUobURehBfw1tz5UTaSt5BOZ1e
CYPUmeR0Qt7iEc9OiMLL8m6Xk+j34CRrWmhSkHOdIxq0i6S5AcywGhbIm4nQ8y9N+wFSSM62KzNO
Tis96RblOd6tRPIsWhi6Qq+lSxD1PPGUHXd0co03pc1mTSVtSpfMT+ClXFXRPwnBzKPd6sqwufov
U5urbfsa8GqGDe6Mr/tAHjrgTk/RDSqrk+NdlXt8+KKwj3X7hUkhph7WZOIoxnq5/uz80edO3Qvd
9xVUHfV+n9uSm4Z7uRRUmWr0qBJFDKi9cPht1GfiHZCLe4aMhRVgAv9uE6solLxAbVVnSpdaGP7F
pMznMgDOlNeRU52s88Ti9Yt1AIt0yhP5VGk61sd09M6PeFPKTwRt9+o/tLnz/SCBSF4xoT3tPy0D
fctamFzuZRaonGCKlJU+GNV8HzWK/ZYIMV7N0DAYcdWK07T3i2MCc+nDhDiEXcfvpemKYL5tZB2J
cU9XeAyqynbbCu+Lpxx6LCkLA2Xhlh/aCTnsEU7ZFNkNli5rFnWz4r+xOW3vvzDQBnT7hc1mrDVx
AGymefh7wtOTgkQqiauOtEdu2VwHMyAY6mYXTwj3/swKYaEe7bdIZzyDsrV49l+L+UE5FjH+8IzX
r9i8ZneKV/PkpKFCYnNa1g2MSOS6HoddQOU/I5IOkc08sgP1suSCUmR2Ic0SUP0DojLt2DihSW2s
razzkb7VAj81JdNEjD7MQp7ay6h2ZzvnxN4J3/MvsZKRwqlVYm3rgvMRxbUSl4eynH9jugbyki99
ftbEuvaxOSbwdhdE3wkWGaK8PvQLH9yVrBoDitPBGUkRDMPaV+XyGfs2KdhUumF1RbBbtLEIGIYH
4rS+Xcr6AWts/inDDy2NPWf52fmatg1lpLp+9Ss9rpYvQnh3/6ijCkvsQfhr6zPVFWShfq7J5Wmt
6DdTi4VPr7zlEaulkO9RSHyrHwKkcabNhqn67jI4rSqM0wSN6rAhzilGHhlXY0K/uTQgmG1QcrG1
eKCu0rGzrnOrN2WYYqFPX0xKv0xKf/7/rkEPjGWehVQHzNEoPv5J/+UyzlkBn6trBOXN5pWqOAZa
8iIU2k6Iyq8Ri2a/MZotW/B38USh2U3EfeW2r5RQcc1FYVybOhkHQmNmYD2UvyLK5m4mTFdcGwlY
GMaagfJSp22aJMKmz+hK8ATcu4JB/2krlsO7zT+f0MzaYarGJlHpLsBwH9oFwGpbbR3qCS59Glp9
6WMV7IvgFHkYsB7cNWD17tV/jib4AAWw3stPeZiP30+fBnw6drOjkTP8gaLkXjx/N21//itjSu7a
lC97p7jY0Sm31r7/cTgUhsObLLtXLtSOLEMNP6zyuxppRrKtBEyG27Rq6nbNBjrkLJpr6Zc2ninb
06WYhrsgFvf2tR0yuwgrE8G8zvad1PvHDCpQFCdNuCTbEMdEFWPhC8fsMrmMnXHSBZuSS/rCHhk+
qRxMIj2MWmdPcLk3Pm8DvP8zGX3FCZgcLzVcFE+LX5DKboDLXR/NFoXyDbaWmD+Gb6QbYmFxd8xs
5v+STGY2IYDkZdeGmimCJ0XVwEiNeRaFtMidigLj121tpB63SD7ZMLJ8UYAdLpSp6BUshJFlAw7H
a7Lhw8Jh5UA6AzpejT2bChFxuamWBzjSy8XVNZtP+dT9Z5u5jyo/WXzxgSkbHjQIsMJI3y5hgeup
eBn5DP5YIEGkSuZlDweqUtShkKTOemDVFNXn20/2Ax7YVSTUieqBDJehV5TnNrGTWW0ZafrFot38
U8z6fmy9X63Kj0PNxzZapQe4BeRsDXE7THQpN3dg/17zgoS9wuAgaZP7ZLd3BlKblB7Qhqx7RIMb
qxvsUMGFY6QUwP8o9MNk6VcXyaNfx/BjkRv5Kf/+lEzKYmLnxaPW8HaJPyrcuVWqyrj6whsT6Tx9
BwAhQFu10ccW/u0s8zmOe4qT9o/BIUPoP8yqP6+RCw+QRbugeEyCfgP7zINGAi3ohkpCjOsNL/kZ
Z4d+oKhGIhQQNKZy6g2FeFltgGSWsULTF5UBSYPkHHJVDbtWGYU3wjHsJ0j5NgcYtsBAoL/TJaN9
skb3VnAMmRe/rXEEcUgCsy9wtJPqA1E16yDJE07+xAAN5HVa3Nw/xuY25ZCAoHYbk5c6sGhV4RbX
sV4bvy0kFPe4Icvfu41GGblLmvRxAqNdl8XSGdKkWFr31n2GbmSLakNBCzOAewB4W/K/VKxKDsqa
OLDdnmvOyUYDNFl9CX5hvi3yevygDLnmYmIeIGxY5pZYuDmqd6Z+EElI92i0mNJ2sW4fetbVxwjR
fEALFue9RcS4f0tFnGPrs6wnli1eurtNZPp4db5V2LIrylJgedVFnNXR7AOmJyfQjyu8xSP+XHaU
W0k/4lGDi+B7ss3YrFGZ12u78KvbZlwZPuFT7MdQVON3gEmV/HcjD5yUqj20iwRFBxDpDLQ06MO6
TxBWkJxrUqhVxyzxI6Q+qtggSCi3V1ALHDrVCTComCKGB3asTTHPh5zd3CJVPNAgM9sYWxQf1/nl
Jy5I6+rLgSC7UsGYB3rcvcvW/NsJVLByZMERwNSDJxfBQEZ7yhVd9ZwQrLUdM1/tPB86+gn6wtl7
LWFixpZvFXEqmyhMBvThoromLetOcnGVAk8taScpqGx6BakC5ci3v3Neur8EJ/+92bGvxEid15K1
3+Ynv7dComMK+brokYdfnMsMT7/xxOHH0JlXxtme6KgQJGH6THw5zwv6IPnxx/lMpaj3TT6nMi2L
b9Hg3zLazkzBF0IL/w8nLYY5oXXkIuupgUI+V5SKpHahwvw2J8qWI+MAeyG2WOUyByt/cPQ8bpHS
tLP8dJdFJsbf/Z8rF2N2Xg+bmkxX2TQ6iHcXNxmYm4rrfsELsVAawqWseWtBuzSoYiGPTasDxqlh
AITBJnTJNNeigq32LoQurtSv3p0BmC7FIhq1oQeXGrqH0JNd8Rx0+4R1e2kZ2OMgNxKNhIq6Zc7x
BmCFk4ND7/SkIMbpJYehI/5voqyXOi3kS4RtSwBD3olopwFCznfedCd/3wx0foddFN/qWrgUAupR
nIi4u0uyZUwJ7Uo40v5J/M4y09xTGFhFp0wlvbT3Lo7r1/9NXtRQgJrMy0kwq8NiJ5M/lve2/sDH
feNWeTB1LYIzuLJ7qAQeHWFe+kXy9n66whQsjOWKXIcxn/YVnQW6MK1tXdZ1p+VJqj0BQj0u+Yo3
9Ns0mQmbSR3AaURwwXGsIU9nePD5l7UVPzDQKLUn0rZmOf73qCXt4FZAYqc4ch5H22cO9c1BiQ4t
PVjcFBw79DQqR7tRIejoqayCUB+y9harje4QC7cMqg057DGJwWVsZ6/MzBMSd2iHGkmTwJMoxHNU
0iKOSLvcGIWSzRvxteYOAj994f8EHALTMzdloSA4HPNXOJWw1o9kPCxLrBJRaJU7pLW0Fm8R5T6v
7Rx9BR3rHQkG4Pl5/XqmSjcQPvApMwgNyxBkt66FG6a4C0r3UgQxpLf6yRBRX1dC9BiCsCK8mDKE
kjcMmki2chNMEpmnNIZRPEX5y8++b1C09YPNIdSYgyv2/y3UO8ruB0COWd8eam3lg+KQ9S9bW7ZE
mz2tiw6XRODVh2bVi8/mrWMyxMsARvrwc8a9YOvmmg4o1ebFlpjrK7wlDacfg5ywY80sNIMAxUA5
zFhu1z6wzg04yfffPopU5VdY+TCb+UaExQy0QWbp1VTCUCrUuZEr3QiAholB7dun8iYKDTR+Wc8v
zAPCaM/GT87rAsR/lqGKSzlNHHgwgSlZuEGpYYL9cpwh4cTKvv4j1Q0yH8d3hwyAh3hFwQLumEfH
IWhVo5GLcMqJGeQRU8KFqUDo7Bqw38BE9hnhsG/FrxkOsyXi10uoCIeX5qE0WmeX+CtIwJDxdagU
ebpQmRqVzFd0hdiUw/nbRdqv9TEhMx/zAO0lQgfUbDuJFNTQCkw/n6YmLXuoj6fxb0ARGC9wg8H7
5noS3VfEES5OBCZUqIuKCqq+FAYNJ9sHrsCg6QyDxsCWBkLRP3uW5ZaGH0+oxR/c6kEcJ6B572ti
l9rDW3zg7eHzUlhocM45xYKuQu7kL0jxuWuby1+s4+qj5HKbhJNs8lJfoTqqxOx1w2rEhQVTjO37
ENx2Q43A9QXJbs7Gba1BXsJuIkPqJ8D5e5Z9pQlUzNMv/C7kFJ7AKohSXSbRyKnM3IlUgxqCc0Cw
WdTPciAC85PAIVf9g91AGcrfL88mzQM8i4O6vWOt12qHFmOlnc1nZmlOtMSs6eW9Kb+zRl7PH4DW
uTiv5nWlYgo6Nm3Mn8CfAvOk7DFFIyNcjQLxSCVViLzVCMNQjPcRHOw3DAzVvJPi/wb+CoAVh/F4
FeQ+BDw3hr9Q42207Y7CH5fouv2U/Su082OuK8wAZPgHWh2lP6uTaL5G6h1PTpOQpajG+YZ6FWz6
5e/7w0A2CTJGNkEG4psF6gHN4a2RPopbPYWctljqaQ+Jzit4mb/QiJFLrFYkDqFmjWYMo5WiEsmN
9VBrw94vHi4N5f7v1Wbet2ALjFIueQ3Xov5wuzxdepQom7VGEAtZotba1x4sCV07pUxmfUdaruw6
s2dltEHGMVzo55XeO6uJo/O4XxS/NZBMd1iN+NwxC31fFB23ZQvhh3TYzzCEcXmnkq+KRLebVCQB
sRQV2m7Et+kVROmWHyrUV2mTez8FOQgpg/UV2zx+Ib6IOkMNu5CqXtNF5inbG0rMPv7HJ0qeSaM9
KVA/EpESpRGoI7u8z9V6EquMjy6uG9KNx+VL0ArFga97/UZ0rnTTIas9ojtMD5ihBZdhZ+jr/EHk
yVRXruD4qI9ae/Ibd8gbrc3Yw+Qepm+JQ1QqEVzwqS5ifIoUPxicr5haGpSIWLWOh6DyOlzZOP/s
Dcoa4C3ReUEf/gUJCUm/Y5mSep3XAjPzcLGb2ipJJxzEv+b96amqxa+putAYiIZSLNijNVcFQ/Pz
HRzG9Agyo5b9rzdNgrU5MOiaOdG8UBfXrp1t8LQ+VHfXwuHLeSFzP/9qgXZRnGaisudq622hJkIC
T6X/tZ3h76bvrmvWUbnqWMwbxVvWNTlKgIgPUK3Tq+EM0zH8o7XgIG0Oj76Q1VFNuvRjzkVU58Jw
eoiOFFCLDhAFsqVRzx8L65LxBt1YRHzMi52qhxUuLumayoWQfk0agNkIm8EmDqT31CbqjImn3zpN
G3Bksbxs18dg8KsJjjtq4hsXS88eTbQROxObweLjuFe8SjSZsNKHbY2IUbzojtzxRpB9+tQy7y+/
8CmqIJ9qKp2iktyC1CKNAWvCSg1fRMewrRUiVBCEuE/Vsd9qfoEvtwR6Ad6NlOgjLRShmb2bgLfN
2+wBVttw1yABdL+4MIQSNOOojy7cP/BflSCZ5HNmRDGpiK+AFHu4BvJJzAo0WvNmf6DlSr69w/Iq
GAdShwaQJN9DiuQ0CbM4lJ6tMfFrWbboOxF/DZDzn+xYJdqDtonuBzEEwEKUv5IQxIi/DPAJ+1WO
IVzmZhYTyS37ychKxosChweKcbvMIZuPjuFIBzEwRa+o03o2uFABFBsixPtieB7Ywzn2ejONrha9
QZ3x1YFjQL/+AbY4XFfuUP0giPuraYEzRJ6C11g3Cjlx2coMMQybZaoDj4Qd92nVftE00cbqnlF+
UGvztMFdjAwHyoHKw1fWhd1xdbsQjyWJP+Ef3hjnprpj1DB3mJbaILiFE0MJEUYtx/xS9zumWu41
69kVdptcxg0sWeFxoAgI8Cml+5AmiVIdF37f0PjHStdTbhzEwfgJTbik5gJaExuQ6VTzPtI5QJJb
rqXowBPSNbrTal0Nkh/1J/0+UYFxVHXLHZteEkg6q+FnTDPA5oiNkJii/D9UPKAeIkLOdsCzGFI9
7cij3qQYxgxnlquDQCgwc9chQvjlWSLoQkj5P/OZXkGOTHiCbJ3eELl/fjPp+olv18G+vXPaH1Dc
7DP9Que1OP8C8mEX0UFbizvlmNQWGPzNoZva4GPPNjmFjiW4MTwGsIzXBWgbUTzOxVjM+2znE8PH
UhWV82XvsLTfOzhqMQVPMUvi1nVFpxEGGPMyC2IPatFuLmWm3nZfp76pC7NLvLTU/ngZcHRzMnq6
99jBzrGVCEpLLinlZpRI/ZPsrKPKIbhJXfv9Yr+NK3MulV57ERjvhbuzxUg2nhVVTdLnKspBbXuo
qK0ktRKEnTJbcGj0SfS5gj1Om4cX5yowla8txPAPVnqe3eHHzhn92NHmmGfmPqzYTejykMkgY+NN
w/wOnU9gQbKnJhIIo56sx4MspurU09Hlx9vJDESEKOeRVAesgZ7dQNTvALoa4MG+v3OumJWrRFDW
eByMzlhTvYGlwwiCIMCxFiB4VcGvxvr+SZrnRAPlFS/1ndPLEq2ixbfmXco+1/ExNZ/4uyaXDLBw
NtJPUi8hipag4DBshdUVHTxV2xl5Dlqej7/SJLEyk7SztCbozdEnzsxs9xmleiQFtqNmTTJN5Xw+
ZOBTQT2DUA6ijX9m+s7mkbrsmmXN2HLpU7WAUsOczwHBmB1+D8npUZ6Eai899q1t/gahB35D0Qpy
u/oHO9xQRyI8ySLf0WOKcxrL/yAJLXJ8eZEcA9ipGZ4yMm0YDaCD61AKUxuAsvhAQWrGOlc6a6yb
qvwLorC/PGUdjx8iEz4HbMBSOr8ZEXpU3/Dd5QaMNXcmWxmiuWrGDHHJrsoVZNUqwHnKbBwdpZAf
pMQ3oxrRjcLtltcxaCuXzeLNE0e/jTzupKmRKJsqDo9e58ap67E12VJkG/doYSSvqaN2b49NnclK
tvuF0NSMLD6rr1DS7+xujH2ovTPTztAb516fBZ87VGWLLS18W0F0m1aOoW1090dn4DQY9Jw4sCZO
m813MxZ+oJ77SP0YK0kltYcLU4MBerTe+e/kiYeIdm9zZSiHU1P70/6KydlX4bTzCWrRVrSRL5Wy
Pa8V9jDxo7iNXlUxqPFwlsDRSp7SzVW/SiLqmrpY0SXZN69ni2RY+Ua7iGQH6cz7K6whYnk63jTC
vwM2TrGhd9lXPgwcXweoUoK1GKN50Kv04SIvaVCq/VS55v9Twv0G9w+Cjs1EEHyA3PkPN4lZ9bSL
SxxSJdL+BJo3+W1X77cjDSzuWCUnWRETEG1sSiCW0NfhGOro6Q2e4WRpRTWFIS3dYG/jfgrs0ESg
ilnfy/kKgiYXx02kgxzx5O1Gptb1rsfc9BULBwWFg36iP1ma/q1eW+vTrcdcVywPzpoJlQhuwD1B
SkudW4cdzJyN4/MbXHSIzqtIT/wQUlpA6rLmufu3+qLXNNJSAN4eOiv16bJkNZPlY6dls5SdMHL/
u4vAHCO2Xg9AfNOEBOyd1KnPoLsq7fdgfNh1tM61kF8Uxdn68JtzNpbzfxyCram8scSS5x7ZNkWK
yaWIevFkOjRIauyZhsq4cAL7wGw6LLgrdLyYVApHtVnkQ8zs/MP293jS7LL1EOLTQLhpXQ5HM8Yb
yrVbQL6Kklfcu0jFsVzNp5c+0ZUbKHtjZonij9+M3GJJKLqna/Nh65VvsgJn9+QIL53iyHO105O0
RXHNInBhHaedqgjyMVKN/bXiXm0535Ogf3uSxv3D8v5i/yrk+hwHqtuOL0Lz0xTUQElvRmVMSVRf
CtB6rD9vNdhjNDmhuPmFlE8cvYXiOGxQ5Zk0IGqt25ufTODzkD5FDQCXIiwQyALXVWq3bwJZAYJ6
Tp7dhQRlPNLUTTSeFFJbrJDkuuLCJR5o/W+BKDAEiXilGN9oNXbMeWN/JIUvNnpV1xn9n3xp+QJ8
OHBXk+IlT+OXfjUMAJHZQixIInQRoTR0vXfZvI0jUprqpdmBmTaz052z8AbdQ5j1/dcaPjlHrZOA
LLzyBcs0JbZG15PffKNXI+ZIl6BROjMCDI8brQj49eDkl562yoKUjFjLsgmF8bYKW/mRG300kEeD
F9ecMeP9QajeWnFh/16hnj8mKy1w8diThhgohf4K97nSq54dMUNSUaDZowOPIE13a4WJCAHx8STi
p+/rlAEX4Uc8m/ILwo6IF4fwP1CxxJBRylOCnpOOOMZ0Y2ABs8XiePFRWyARsqpa0WEiZoT4gSai
EV8fIK93HLKI4R2Fs337zZsNE6I74xONKivyZqbTEZ0XIm0CtsEsYMFrldw26RQid2abg4kPoers
l4NyrYQqecCPYss/1FC0E574FVR1nQsiN8Lqq8WhV7U7JAb23Uzs2cywGQp2ceS8dk3+7Wee96ZD
I6+ON3qASs+3etWXajUlQ/PBAJ+mggdHqCtYuOXoFyaBVYPnbt6kXnoZoagLCn8HQR/VH1t8/xQ9
hD0w3Gobdu+N+CSyEPy+Ewi+qyYZK0SHGbwUFm0ReLtglCAYA1CPttV+e8hMSTcRnmCvbe8hl+n6
IS5KBd3/oM70EeZRVsCgNNhn4h6lIqV3lBelDKvT1MSrzuSjD5DEz4vdjOa9JWI2K6vJBDxiRn8F
Mz7NyEtxzJGOd77aTWoDVJ/dUkczMs52XzshkoD7g+CILELhknd0tDkYUf1MCUQ+Dfz/JP81kztK
zmTNCkMAMnR6OF4bHXYLpAPDii2XkiDgxmgvpbquNQS58KkVSH9Puld8Klm+AQGXfOP453B8otww
GOfuImnLhpdknIMqSU5A7KL878WF0NIGeBFDeQv47K9wPeS4KH5qHgXTfxJPQZT5yuW68S80D0g4
0yz3RmbHCrecAVgIQm8/exOSg0S7P9+B1TFOeqaEV4k9oy1X8rsXL0q82dyWP8X+6JhQotMw4F76
yHebbLaLlive3ESGjKDFfr3T+E6nFD6eWtckMy+oxWEap0XV5mwwrC3Ar85KvJn3nuLgEJemnCgE
0tEKdgH6jzVnWlexNWnGzK4soGe5Sg2IRRlZy3O9mKwkBoX52WG/NkSMgiGiPg6/NVQX7URpU7Hb
szc1rfmUkFQXX7m7+vHdj6FOAf7ckIFZwX4aY/0mNHY7OeUj+nI4o9k/KVk8aL639lHJk9XJGIAB
LatzvLhjkwAFBpg/Y8x+LPVkx1S/H1uBK669t9FzBUcxZF5FfB1JpFFShcQJSoaF6SwM4b0u9lE0
miOGO8DTNQJL1P4BQZ4a9hf888SBuuh5Hulr55Odzz9p06bhSbpZg+7YVyalHZ1J9hlxaw7rV/FY
xI4g8IPu/pO5NoH5augOFTYByPIC89gxGXaJGgaT1QxlknT/0p4UwbdphNQiWKmnmNoe+QFWsMph
VZNIpUPYzOwlZIR+v78tEsZV/aqg7Bg1rM3pvUnnH1qfOwUG5QWCUdfebuIkG0dobp0AFUXGDKed
MtnSblgvbUEqdg5qm+AHBa0AIypuLM4NXNQ+4X/Fn0yACEI2+VQdAbSWCcQCCPg6UgEzycp+VtVw
tEN+Ai6Q/zeK6d3cSutkgF11wEz/M28iU/8WZ6eFwEdmKjoazJWCQlfVyuIy8zcW5acO4XeGZjHL
H7ToIjOsEcehZMhABAFk2Ke4eWNP6takGZ24aql0yjyraGYE1VkVlciO54oe4Oz3yrtAdmHEzZNA
DMleR8gxXXtWgRDFOs3I7IriFUapCLs5luVzvRi9SodIgtgL1EqknpagZt54qB33uCYHU56jTSab
pHw/gX0jF/LR9yfL/jkogpNfvttKQqci8yHd4CwrkJw8w7nKf5zAXEFgcwJ/eI93ZeufTeCtQdDU
etrpkw+H5N+npfxMqqhhOCXnQUie0KTJsyA86UbBZB8hnd88t3iHkE9jrUhSKFyBXApSphRTbL2v
YlX9f7jOnOX+Pw0TLfKO3HjUlTO6dHglTKK/16NLIjdMyJI9Jlg+drht+IYrTG7rEoe5RMtuQv6D
uZNUq0eXO63gk+FABrK1AM7UP29NP8MG0C2JMyBn8Crkugy26FWC5GrtUOxujOpu8YCfx5QPEBtt
3XCqz0Htu/gnsqU+V26gFopUXKlB5sC8iUrCa+rXjSR0/klr619Riq9bblcYUaRx8jrVaMLnyOOK
uTIoNcqkBBEWmhUoE4MqM2eAmqFbH5217txhgyG4iEVPBUWqfllQIoiUr5T8r5SbUgxLOheuNl6C
dtdm9AqQy7mzViFtTeYZogen6+rvNc+0yjrkGSh+UEmMB1Y0JgJMYc4G33737ygIzW/oeL2O0h1x
7KFykW/nl0rngPwhJY0TYyYX6uTzK7/7OmOqUPWyTiTrc/oCrCyzy4eWETPeOTuXQWY+BDxjwNtd
c7iusEzUIOfmZoCN6pRiGeARKoHmGMH4scKfs3PYI0EBO/1QOnzaJMIiBHziBmaiJfumwlW90jwQ
ZxIwEecVJn9JpAzGX3BCv+1iK+Zzev9Dyhrq8ti9/9OBCbITNlANR8lwc5724gji2mH4Hu+4R8Oq
lLVicTjJJDkjqeISpxn5YcWbSob7E15foGACtDVdPv/ri9IpvQdweKErkF5bv++5obQRyMxwLO8P
dJoNoExUyUn5iXfTx9Hg+3ZzvEJcE2sI/9yPUwCFeYDEh8QDrpT2b+PUG2gn6+mwGHrfio6kk8Bz
8NoYIa+GSlZEfLxY93vxyNWCUMLfovhbLI52CHHPGhFpoxpRtkD/QSTaBZ+nBuuh/JmT7MbuyCdc
ky9zL4Bi1WWyrNTtRRsXYPjHzYqqsr2Pfzd5OqAy+dvS/HWZsyG695BDkd3CnRP7vp9yd+W8gv/m
5OvpEBQq4HuZRFrbl5M4Y5F8QvhJ9eJPgrlkDCZ44eYIvxtXb+YK4aiA4wDjLEQS+cpvZdB+OXTR
QJz36hisfm3i2p/H4aTxBnUSBTXyQxBVMDO9kkVsN7TNZ5v/351HaS39Ii+djrpAIucjz5dJRVOa
Ep1xyfiGC2e1fTRPMTFDr7vxZGHJGBLoMNolTqm1j+EHCHn7LtMJj/ymmfB0Z3KPD93H/k8uC4oU
0wrMpfQzg7iUVJuqlldfo/ZD62BZ3d9tk1LsZ6HMRb168Z+2OKjt34yxVQ28jjqENRnImLct9Py5
tF4yf+tAzOYkKFQbBBR2BoZFV1lz3yOIxqcT0nqQSmcXHT5oh3682IBNlwwzknugKB750OxWMeQ3
0P0MV0UWsJA3ijUsGm9pCH/YYxrkojI8NB1RzWgpgeG7sQ21KkY1sQllkoVSAH4U2HsiVNyoAr9Y
j2oSWKwZIO9wc7W501uaYrF0Ho0rrWI5Yu9bP/Dxmd2524f+jboZHPEXDkUneWJ91WDiF5P0tkmn
fTUIUED613Nlm0mBQ1HEK33nXpKMj7ZHKlg6AsjJX4qpX7o7/YlSpOWfo6Zx00tmOBGWdCo9QaEb
DN2J2lBG3UfLE93IiL4iPwmiLxzACPv2dCEdI1xIpp1ELX43z7WBH/Xt0J9jTRNSkkBgp2pHMrt6
CWoAKBiuhHCOUzVAu6fmU8RtP8TyjzKM9w6jUyXWBrXGO9E+WSaYtL3cK6asazD9eNW+EhY2lXP3
JB6q/rgPyE/Hz8AQrGV7DF6RfGzXR5lncmJCBf1pSEI5LoZAOkWkSM2Blp5phk0cGSEaFCkAf9Ov
AXbGG3b9cD/vDrK3ezbKJ0klzchDzueZ1Ue22PS3ZouwzQ5Dc3IF5+BMKNQPgwVUWxNZyTdQwivc
FjUM4OJy6f8r1cqso6gC1G5OTLpoBNg5LylwHflQCM6c0V63oy5M83gGuXEhbs1QXJJZAXHbvEgo
U857QfuliakvYworCSsArhK5vKNONTzlXx/ZJ0fgW/h+osad/7saU/lP0U+QEtc4ow54NYCBPViP
NoFIcf35m7PVv5FDXCMOMfzecg/t/8jZnH6TCKUulHhRdeQATjmeUEERZr/dylQiqC49NHIzMU7c
+Pk/xte8VL5ZtQq1Q9eyGA3RMZ606Qv2TAT8HTSqsLz8sh0U1PRAuOB92VTJPTrVLJDg/tSfv6bf
jPJVMJcQRuSTim4n2bj0gTndfJx4OQEB4I8Q58dYw6+U9aqpqXHkWMWa2I4inqVJApQ0aU5pvHga
c2Qws5NhnqlneoxCoLAijtXf+IW1UKFC+APSTFTeSjw/jRtcjFvJxRc1QFHHkaNZWUlv6OUWMjxN
atCaLfUPR9dP2d1CSNv64uZ2g15+ecKNypA/9oYF8MI8UjVgugTQalabQ0+BqhCOosVrAK1m373X
lB0l8clLO3VPus9TY8/+Cnz3lQp3zpQHiaVNpFSAldEDTFKdJBls48aTNHF1/JjIxW4p/0AqLkpE
z5QqnXO9m07VANm7UoANSSlZC7y0/snplzcNvg60FadK9sUzRJhPtz88r27THvKtBi7vSpVugGsS
mmepjqG7O+Sj5neOGQJrbQWdB0RswSv8iNBvKlOEhmOBgnypDtcm6qVqvdJBh7tbqFJuxcUvbVWB
XRoNEPO05ahAaRDtkfsAWSSJGM8BfBD5/jCsKLaKXfm6PQwQ6zjYIkVWAd4hyk4eOTAH5CJPr5UI
4y6EloMkvXIfH0ek60M2ZVkcJEEeY8Fpyckk7vg4jEi0zgiPAZrS1C7lWSm3y7vuxJ+pbaeo3CeR
sviWKi04iwi6YWlZ9/McVTAOK7eeAXyQ4ozSOzJQdhEpQj1AaP+tLCXwIDiy0fYoKSqdlnU8g5mg
6eB9QdVFhxT4/X8b+Hx+Tc2pAsd9xTHcGNzBsDbyQqW/DK2/dSvjiO7uGRcSegTcIgraOr6VI5zA
68y6lfOhYxw7pNpy+7X4tNSD7saPvY/hl0sk+06S/XVbjzxhak5wLm83FPZS7aMqKmIHtr47PVZg
0H6UD4OIIC+XHSSoTcFE0HYGpDes2TCaSIPOblVza6SktY3LljnJQ3VZPqu0ivtLl28ys+FD2OqF
n/fYmYMnrqdSZvcDQQr6eBmfBADE9OeOBqIuPeVu/mD4kZ9eZCHS5nq32OahudzXWfHB4H9afAeV
OkS/9XnE42cGq0t1N97dBIKOCkhGjPwzFpDkRnJdB3pquswMQ4BcycI1IkPivTg+1KCmym0WpUE4
o29qOsaaI75GHO7vGJso2ipM2tHQOYdJcMU0mZAM9az7iurFkoZ/kv9rFv48nrdtjblwOZHR2480
OyccXzTfPddRaeGvjvQVV64K77MwZxdMxR0dVdn0Shgw4JdZBAVDSVq+d6xVXZoKAgoBLoAgxb/1
ytOQ51uk3J6vOJzmBtjXiY1sxhD9ST/1o1HqOGr9qTxX+BpbA2/AMxnp3tyB7EQd668guyzGOc4F
9vMjnB7ukH0ehH3K5kfBDv6IffhNnIzXwDdQzF1Jt0cqo+F5OnZ5qUj/LWXCa3XxpjLzW4K4xbyI
dmrgetJr2sm+GZDQXOoF+lUt3r4SDErKEEveFdGCVD4/73KUr47YIJ9OYFrdRjhaYpE9IhR1GO1+
NTj00nlumEhDIF3RNAHgU8cRDe0ZevsE0OKVtR2PHkHcmgxt8G89tfAg4P0/HoTKinJu9AdD11sD
U+uDsA1a5x7wwcH8PuhY5YMlFywu/VWSxttXeONoo7VHSl5MJXJp5pQetbolRYjeeV+zPN2MifcU
humkSE53iWnL48kCrpjuKVRxEZzT9rUi09rg4iCq6UdBLfHDc+KvZOzn4z+B1qNB5KS66oIvE5Ez
COd7bmOlWjK2FpsweZctFTojyaDA6OpFRcsppHEk7obe+kci+wa9Dd1cE/rWyezf2VLV3qjqOEG9
XDuXI6At4CnE+FAaT2c/yVNu2Q2r4FLoXi5qVwdxTpYpxqogO+nJJXgz9FllFRIumq9VokAEnNE9
CrIcglm2f/nbyAifrlVK5Rea4ssUMwvWzp9U5oI2pkxTxwKVXXffQ/wBBKHirfVkVdOipeL95Ezi
cD17idTwo5Pt0UVS24IheA6FDDpMbFXkyUlmB3k8GfW8QfsGhIEll9gx+9N/eTS9Mhc+J2C29mjV
YZEzE4jnPmvir5MiBCW9cwbYeF5z+FZn4yzwUdKCpxKWYQ2Z5HgJbfFi8G+f5sxa4/r31Impbet0
uTBACMlDmy9+GrEIPj8jFxao8407GSv9AxIqizDIVFw6e+aRic1nG4iHihtKWIU26i5sR2b1uq9C
kzOmOUGJN6ocC72EoAtRdOnvwJSutEb25qQxj0CTmd6KnUyQpG0dAOGLksLBGlUgtcz30dHPRuBo
RO401bUt8O0OoHKQQBr8fSUoF7tRZGDxOTAGLquM0oWCcJ5dC/dqucBsiBVpRItEEHKapM3ZjMkv
OX49Jjoa4Ks87VKj/6K+mAC75mnIY6ecKe/0i7TZxDDnd+RkWXLDLPemZvWtwHy0ynswA7ZQ2HXs
km0L9wQKY6KSFlo52H/YO2e6aqRaQzDZVYpNTMgh6mwRr06As5bF237224HxGSqWA4sMMd0E8WEY
yZUTTQ5crV3dQqVFjhm/m6OVjYCXaeMw3uAkg01j5ww1eA6fMLjT2d1DUFUPg1qEUkzODMRrP1Zr
cr0KhwBmeUdMlpSqIT+6cdDXPIl1ScL2vA8KjxngrIhZMAQH/MosrM9WriUzp1i49OutBF7ppYoU
4TVvN7bSW1hPBk1XDqsdIY1Ejz+oC7xdGP2rpUnEOX+OmcjqOpaUKXAjHYGKyVJWBmdO+S37wmm0
dd08v4biJEhCR9E5zHAAvpkOXKNvfdBOqUkS1gzFa/nv5bL+wX3/jSR52A0FJoJpPKS76Ihztosf
26Pl08hqBxGXK8e86A89qskDQVyGOvBpfK9jMRDuLbBxNHZ+Vt4sFRx4mE+TkXV3bH0JjvuYyaqR
ftqb37VQNoW0kYfTT04BoCWl2UqBIba3t63DLdT7d7QPQbLraoofxBznsfOeg9kz9moSjw8QccoW
ILb/ESpBg/ZN6dnUbiA75eI6jN2/+W9XgvGTSPOU0W/Hm4iM7ASnc1FkEAGS48rOyDjxeOuevMGD
NmRxpHmqsjROgYfLc+6FzXev4ytdDjD7pq1Jl6mlrYa6PlkUwOtb5MRALMH2kuv5l8498qA2tXzI
HfULJY4eyZAu+J/VBrn0xGbYr554Gfq4bbPOJK7rz+1zOcbo2XRgwL2YQujfPK7rUGkNgKMyFUo3
eGXvW+0kaB/zSl0BJRWKOmJfsHMI+vYjITflFSRX+SoPClW9jiXUCTzLs85qVHyDBcJMeeJBjvI9
0gBen612fKe4wx0HVUpTCG807EstXLz/jaLu63kt8Bbb0XZnOX2zDa3W7vcOgPT1H5zeMsvII6u7
nznqPBbv41W41vbnVg8wn80/An5UZZWGef58HtYZ6+w1ZMops5FGNM1ssUKrryc4VOuPs35Yt5fX
T86Vd2T2a2WjUr2M0fqAmby8DXpiFgqCIW/xlYmWutvtg3AmVbMQo/CVwQC4OWMwI9s9tnlXhJfo
sXzVdlEPw/ydQL/e6KRyGNCHRzgb8x379RAQn8xqs1MfYbwdM7n6egaQ6qjgcYl5dVfRv1lOhume
v5/y7Idfvb5PYF4wma7kXTApcC1YV8u3mvQ6gsCyNmXmtfZIZwM7P2C4v1JAw550QXTsAwgrA0og
fP/Uka/S42kjq6K5XzMoY9AUM6qIygYV/CeCzN8Uk8T6KG3WtWKYhCFG9tOoqVmCtI3UhwsRk+ks
SJax9cnXQmFBE+G0gWV1XUGa0vzA4AOJxQtzU5L1KfXO12THfQSI8qpLCefoqxnBRGhfzy9mo+Kh
jZWMzXKJfUuVsUA/boHNYIAUsOZJS4FRlubF6zpZ369kgldpWuUySv8Dvn6vNnv6OQD9uDpQcoLZ
duaBsCNYShVmSi6fKFxGcC4TXW3bKdxQdngjmbZp57am7sfX51Ky/+MsyLEjDYah1JYWqe2/rwro
NbirkfZoKtbJcHHd8tUdsy6r8+mwdoKxksznQyJDKjrzqblNlsyYYXGOzFFmpel1srqk7jn2zRNV
l3Ojm+npkB6JY5Jr5NSyCh4I8Hj70T6vXO2yj1K4P1GOLQQLkYDm8Y7go+mCSFZKGlmb1JYyYmxj
x1OmPofPmUhABp/UHOcIP6SwoikKvLG54y+DsarbSYsel2Kkp2uPGAj9OmsF6s+pYLG8f7TZr8OR
SF3uUudBUTuP7oGKAMiRoQf98Eda1uZSuAX/ugulJni2q1TYuhxeG6w9pCx1BBChG8550gEntORR
rKdvtUGi4OeuIi6DqEhmuEZA50DNEA9D+sSqrqVINkiVmkAozIMrmKyCJdrptC+WkZvq8n89omfD
8O0gVQjvp7YPrhDgZy7kjYvJwdYIkzDffxY3QXsdRSu75eGskrwyUWEqxYtvTKKovubAxDA8uPIc
pPk+0XbLmtzSL6SCpBtc9cKkrXK8V8lEXoby4McQkM7B3UOQ7aZOeYivQx14MTQBPeigKmLp8ECM
NRZdN7nwJKN2ButxF9eCwtdRS1OGzed7eD3l3AYco2aSinccwuqeNkqwTaIiupaK7K0js/xa/3t8
BIwTCDwnfUPKHeOMyzQEoa4tGTxf5p69twV9aJntt6vVpAMo5fJaWjK8XxU6fWmQdcyjdzqjWtfm
O1apVBHg2hstwd4KkycP++XhP3TTG/lQawi2dYUKD9nGG63t6nETFEJ0QxnBUmcJj0SaJLG2IirK
6FNSDzhFPEItlzCYFKL5wmVuEq9Zg6AtsOBljTwhrh02AJrCwqkRsUfJ0DPpDVV5S+LGkvldOc3v
3Yt9elaOhCznjHoDr2betVShJBESwekbO8EPCYERDWkuReQ+KaLnS5fOxzkk2gOrKyYZUEOJjM5u
NL/j8x4QT3DTXUn67CitsjtXNmYs6ooQ0r9sSuM9v29cfHrVZ+Htg59s5CS7DZcO5bJAhTCYFZcs
vdzbYi9wmU5+GonEVk5eVjm3svCq/hso9HkIWQXFj4a//Kwb3HDl9pk1uGfIJEXF4/l8vaI8x7s8
vRY7WlRjYWCc9XoW4+MmiFxuCahJmlFOciXkGt2Shk9NeW3f9UsqYPXhNgsn9sVbvhPX4X5giZAL
/+X7EJ+/FBMmNqNPP10j21OBD6PjLkZAb0Nk4mnOEQ2q8Ng2hPJfAMQ7DVrKRZ6+ySgdYBw3Ekyl
H40ck9DIVy6xzVvrxZaKToU09BftsGLaDgpehcQkalhgqRC74PRSJwoRjikiQhZE3K9ribwogWCw
lP/kAwWUZNKYKRjYFPVxXGWv15nkIzLgHJEyXpQPShB25hrZDR1Iqk+vS7z330qNNSNu0BgCWPuP
nHbXTx78BitSorZCEosn4xOIuWppFtJ4RUV1FNSbr6INP3s5q1AFAAzrfYG0376q4QeBYRQTjWkX
NzfBD4De7aB8xsbfco2Ku100ujKdlDI5fMmqkLkFBLko9wn1Ge3n8CNVwEliQEv7w1l1qa5/725E
jbjwTcENux5wma7gxIC1JfTvK6c4zfU7RZXwJg8S5gDEk2HtxLwXcfg6rt7QR9+MtdPSwMIZHEfY
GltFZvqY9gxHj0yVIs6bgwqK7QbXDR94X1lvYs9i15nyOLouVFc8ubXMTMILg/oNm2X5F14ts56+
Enld06WRFF8GUdg0+U53fXcFJmK9tsgKN4FxGc3KiJPqF+g+yegCKW+2eXMq9OQrRC0q9xUZx4Hw
vtnWaT7dxgOj4UE6PTP5MwEyFsdOItXLn+RcmVJsBQoe77xl3H0whuxnpl/LnUd+St+Dt6p57orz
l2UOFh9vP7pg2OKTFMQuqtUG5KjQAIHXRlUIahwM+G7PE4sSLi4XQd961IQ3uwZV3IHFOxs67e3Q
TUKfp30l4XjyWy86mTgpime7HQ/oqv7UX+wGdZmDovua7IGgrW7EZprBflcz1d6wkesGl/Jz+7/P
xlOMZWCylxz0PI9hxzQbj0ZlZkcCf81rWNWh27pVTe0cazveYB6+Myg3C67IwXudSSJtGv8sDGGX
w7OTUSJCbUhTyUrTWZuylDbQHGFfVW09Luy/W6I9kqymoInmpLrwt2dcJ4up6hsA54f+vvXyf8IB
eIcbtxg2E+Jer/Q4p8jxuONdrJkNbLSW27wOd21XoFL4vpJZP2lfn1YzVw3E8IeqIIg/OVDNCeGu
q91Sm+I2MAfHY7iVuKDUJHPZ5N5WppHW0oMQZTrkVq+cMJb2T465shmynzYaTEIwFaSLbdkccRjs
0tcSK1VMWcmMoX757nYz4ontF23th+cR9jb4WUz4WK7Nwld9ELAmCCONlWGyi5SgFtN49luqgwB8
T0vBj5xwbcaW05Y9F7M02SNH+E168VVy6PwxVqqibxrXDZG4a0CfS9KF4CIim50Q5aouQ9eHqKoI
GBZIpFHFzI3fUWx+EgQLcRoPzK8LsRPcP3ahJfGmbK06bB0RWBozk3lV0Rflf6GcxiPtj4Ggn6Vd
GBWpSZIzcfKORgGORNophmRhYgTP6/hfgbwS0bZk2kKx7+GVbZR35KRqRfvC4Q2sES6OfxXqRKTt
dC8eZxcSLskY/nNVsM2b4On7k/oa7LVfFk3cbH3/4ed3N5zFEDHvnIkOn1bwRQ6Bzjt6u+WXc34P
MIrvA2PInIpob+Cs/TJueUeKHQKIVPlclMEvTE1FAncWd8LVYIo1hKPxXUpEvQgukGUm/7cHsGZs
cb578FQnGqK9YOTWB8zsPAvKjsI/vhnaMiIhzq3nBVO57mYkGrigtg7eyy9PYvnDIM2n7nFhTjCM
hUnYyGUGPElXO+/71vfw/Ccb9iukswDNYVkMjB5kWfSaP/ZzqelLTZQ8ExJec/Xucsm8nBj6906t
EbGXDkPpwtSzbuVTxTMvuOzPk5f3XTdE5zDtrIQnmzoG/I05aQi4MfAgRpFmYsKYN5l7ymrSmVp/
yHl89XfT/FzTnbvl4/NEwr1F/s6THD5sHK2ehQneaVMURahuVqehCZUFpIyW3ft9lCjKRLQtSJkA
AtErwB30PWaU7V8LXq2S27nD4dMgphGZIDfJXQOHQtrPR3rfOhZ3f/09YIRsbAUnOsIIF94ixK6r
xQd1wNTyx+OAKXbdoHrwcgjH/ji9jva+CmjqriyJysaZKlA3x/Cn4OyV5lAfFbwUs2DbOqtyeBaj
bUzCZ9WVF13FVFUiejci6zRe7jnMslDdf05Soe+oao5WnzYwr39Yt0BmVtuSQCofnawCu1hyM+hL
5ZywCsynfLmZ8EOvsDciUUb571xxcatvPeHTp7N5jVzlvWz4OZmjpHhtLlF2nQV4813mlEEyahTe
AnIqwq2FG1gYG0T/cuQW7tU3RFxPXGIFGj8SLdBaMSC/I3yJ8eKUU4zxlCXr/Uv0fJ/GsB7pMGfr
uArbPE/jUw9i18uzI31QkrLIfNYGZwPNH4cQUcb5dCheQLvM2pCbXBNgjIT/34nLTJUYFGXzRRKa
7LyDifrHBPO+Anas6qipaGoU+vqt0SwCBoMgZSK7h2HCTFmFfGXbrUhZq316cpj5ncsDbv92OHb3
hTt5Zh6JU3JMOu0NLaHgDWD4fuic1LJ8sNI27j36cxhCY/YXRairX7wyEbWzsj5e7E1Q9lHhVFDn
IQZXZjYtnqKDr0gN0McnvrwJ7I14moCbqxgrQKIOljN1Tn73KwIayM/zMDmD7YtuTV4iVErxm5Bz
ang9Bj3AdDB0BBuHnIpVvZIoSHTwxPBEpK/z0T+MDONdo36EU5iKJMqzpAuiM31R8kYWTlOxB98d
4fxHeOUr7sd6v1wdDv9QQv2Xjs9hUEb/WIsPiGPbSOmXg782re9zwUZSnz37k3eGkLvz17oHafet
NTOFN4CxwrshdwrWS2o1Y5seFT/LJZp5envRo34PfLbSTnNYO8hcbh0GZmnNZ9mmBT8RuW+vHIlb
yFNXAgLfmbbyaYDJ+Gk0GG/0noarly9vzHQDljBz5sUO5EdtF0krrQAvGuXneen1g6GXzxACSkgW
N3JQ6uCH5y4kNc2sNWQ2OmF3KWMX5IgNacbZkLRiWW7otYKkeHW1695byChxHMD6ataool5eGfSI
ojLxkQ2+TUeZOUG7XQgBRKQAqYNwUNt9L9/D8D1o8IlsOBZrbtOhOEa1nf0flHjrqS7oPEaEGpsz
WIm0rjlWjlnMNet4ycvNiMSX21eEH87flZ8RTnnoBQ8JLq6+bYFdH4K6ntiUgS+LFAIk+9I6pNLa
iPKXEYYxrnleaEK28RhvE4P+aaYAInMSFlEWHLqdb45HZ/+HjIMszuuYaYOa4FOnuvmUqBznXXUx
XxNhAEiokJu0sdLjrkC0grpubNlUaypkkaVazFbLGGg+zNDTh6CT+CxbQHS5FhX2EAUZyYmexHQz
uwSFXdL6+c8GIq/W7IAUDppkw75z7FajPJQB+pzhXkbcP3cerDN8XBU1kBxrZQvHNZIYepMB2jxG
J/iXD/l/bUX6kvwkJqn1f8l6PjQAqoUMCTI0SazhjeNb5QknE4KebBl/JEhjDZy7U8tgeexOYiAB
ekdRzook1RR/2pb8Yqrfs9AZ/sZmYPUBiERo2Gx9wqgOtJGjjYLmaKR7BojCvX/mJGlgjVCmsucf
oH7QAM9lQhbaCS0d/shGRo/iNETg4upi5Q/bKAHwM/OQdY2Ve0CkTkSgF/g2j3vwu3znmBctI3Tx
gd836zMRbreTVCp3Z/GVx41yWh3IStSlN/Tq5wPok58CvXiqmmpMBn/dLI9ltpLZW7crcZKsSIU5
4aArOyK2SKHRjAHMNusNiM4ClraYNbHjTn0jfiXXljg+CV2kAZdIOfyDgypkj4C5l3WuzJdGm2h8
ecX0jbMjCKnRpJquXrSEvBPHFaAQeyP5Pu/wYPwePIRH3yyIoYR5cNJTpFePxWyxRGZEvFwQ/9Fk
IZn1ILTc81p/VY253s4/1O1zRlnW+dPgg1HUGP0PmplIeYKUsUBKPcTX+YGJJJ5GVxcLBJxH54pa
T8ZnSGkMW94gkQyRh3TEhIeWk6uP55oxiyokJ32z5KtiehP9RJwyPKoyVrXewZyo6MtJAOWW2smV
oSbao5Fe6D0FUhhGE/7YDV10DRlIceczZ6z3VBRN4mOrC+sgtboCVEcBDtcPBrGMb22D6glQScSV
wPEuV1jNjCtr6XzUtNvrOYW2bO9gDofr4p/WytyikPld4sYxXm33QrONZEPawF4Vvhszw7WsV9Ud
1ZCQ9ZbiPuEkmsCUPp7S9LWYMdEy9sD/RES4l1TSyPbJ/JjFIXVygCXegHVXshUqQk0Xmv2KJfBJ
L3kZwKGHUo7bXhVGcij91YLajwhyWVI/Bxeq0phVnqQZRncWll4wWCytk+Dau1SBmdPxNOEarXsb
IB8LxFg8QSQ340vHLfuyru6/MP4oas9L0oIuDQjx6BCMNAqHuhxJVCHSTWhVPi7BimIB+6kku6yu
zRZ3BLyMXt9Og54pHe3vjOw4cVLNbmcCNttTfjNWox3vQMXiGIJre/j4HxPkFq33Nhlq4I//SXNz
lV8Nt/DQS4eZdFNygNNxyjfguaQgGXfYSQa2VXL8+2t10Ea19pY4H+ou3JpwlzhUW2dY9nSblkFH
Qksf2X01cCa32pUNAUaoKSS9Up0m5N0L4oy//WQ3UJkWc4S7tiUL0jXvye9aHmDR2tyOOCzxFfR/
sStPnzB9fZnAWTQu0Ttah9vLJvZSA73AcuwQJyT2NP4A/eyKLNhdh5R4VmmvQpdpsCo1dBjEA6x8
NWwmaTVWtB4ygpyYWQaOYLAMUVDPy3TCJcEpXlzDPImuVgPS8rThcqNfWhTMyDJT6S1rLByLNvSR
t6wtYvI9R18qjQB9uY/gJ/kVlDuL/+igo+RWZjFda1DVzVgU0zAqsKzHcC4PXDuLRC59yyjhW+yt
4+tVskrmp7C74VMllsmkUBB5UIWe+xU4Bht0xtiTGkL/kgHXFRU8JH4plhCbggLagYjHI9Qa+ToF
csQGECWSN8Zv+XZk/pFJu4H1Gu9DjaSnbl+ls0KTYCanczxtp90TkDR37/JFBlk4yzaLY3oXr7Hj
a5smccSeB8+9GJph6MMJyRHVpDHlxDxPGQ7JvMXU0/rE8pYpjrmBA72HZdr3W+lDkqn2wVqSUhLH
SDKU41k8JQ6VGC69l49zZkG5a7jggbY00ITthVbY7r8yLG1WNX5mJN9AygwVBJmUPx648BzROQ8e
k/2mVaHF+6B2XgYcVthLp4B7Q9seJ6/Vq0BChacDxjE4vE60jWFlJ/lGcPTd+pUB20yGOh49tYL2
XMVeuTODwGqw0SQ6eO4qQTs1yDuXNrb9AWRWZ3fdfEU743rgcukPF+I2rkD2rwxoOEWTGApewpm/
CR6gcQ/uYp5V+GRIiloA27ztoEPjtSwOl22gu9wt7PXnl5zG3qQOyXsslLCwf0omhD2c+hT6AOG7
7WXGpeR6RDq3PijFwHC0s1ne5D1JquAqqS7MBudD8LcGCp6W2XzqD/0Gqg+5nmkrizg4F5lSeR2M
IzphEKexJIJn3usSphAjf+l5M3PQxdGFf2JtUVNMW0VjC6b6Pn6c4RROkvc2UpgNoN4mVn6AMELM
cehsyjFkWxIpDFdfKA2+z0u9S2NGqHKTKXv0ZvN7nQy2F5vgiXzf3HKtE1YOh+L32QWnGB0wbrKs
646e/a4rz59md7hEJ4bIdIoVdnseiJiVBYxf/qiiGTLL9WMdT7Jb1GghPmNC/bXL9YDFYuPl/6QB
MWtZNLH8k3v/ucfNlCFhKJU1uj7P8rFNMo+n2ogu/ScQFv08JQq+c5UFG2bj1Tms/4+oDEL8Xsi2
pfYFGJj9i8K8Hc9oJL+9JjF9ky+OabjCgj+fPbfxq0gPuF4YterP7y2dM3RW6J/D1IRU6ipryGK2
DzJbwCXZPoRyMyOvOESMFgLtF2bGOcOB/QAdztI/v92kJokWKrbfMuXFM2MrKcAEXfYY1RCh3T0z
iiyu2NCjC7Rv22LuC59pe+AvgnZOSgUkX2oPgifqLoTTx4XHhpkrOzurNpGr02TjNZPcIFurTEiZ
DfLmlvE+A+Lne4EICfsbhfgNOhZjtgO4UL5l69PTjAH6DBSQ4lTXfkRr3y1Guoh/z1YPFwWEFWG3
f5X3jocldiW8OMHmkJHVDTe4rn86dq0r1SdmCZkyyr3Wt1JtVVyHyd1KA77dyIne3wbls4Hnfp+r
QluFqPme0FBrNBnKzOqiIpYC+uZMQY7sjHrCoCHgk3IXUX5AkhGYPeTrp1tf7xvE4fPgjlg6vlVn
5JBni7iNBb/WzLKVC6dM9UFJnKuKJE0zImKLd39ppRRsVoE22z2iJdMtBt7zZU+tYkGImW3R5wHz
ga5ahVnkZRrgciJExhq4yQ+4ip+nRFyXLoCSKhj9u9OScjRLuK37loNeVHLnhfxHGZ+tf7gTJVGh
bz0I9n2KCUpC7ZSyjWQXe7t7lhsBsNHzYgG6yTiRIPvm3GdsACzNMKQOQGupoH6y8WeH4ROlferz
eOiiN0/IXWryPQMGyjQgea356eK0gaWblSA+gkvtoXCCcKqZ2ManDB2QewJefPG2dipkO8oHvobT
u8oG/dWPXGlfAPxPxwj5Jd1h7gcwFHzZF9FtQFgXGgQT44wl0G2PTM4trz5XBRrPyq1eKg95AlQt
p97FBq65Pz82sLp8tZihC8+edkm4Sbtx9kOljWpy5Z4My3EVZYNa6vsGmTKbUnqqq9Y+lI38A0nj
IWs7VVuXBmHvO2G2zMww3lKtqSMuZYyg+DI2SMw706CQHUlsxDOGLQxdoxDKcE6JBQGSkRZoqbzR
TpXbNGwZ3sDUVs9tXE6xbMAF40sCZzB+AFC60rjJC5dwYOouvfJiZ0iOTEUCASQYzKUy5gf8BddR
Ygt0THrqgVnxMmVDnONlQVxOnjqWPmb96LqdW5a/kq3XC+xeo7YLk4c7N1Z39257vrzDfAWeSpwK
tioi5crAIFjqmfmA3xK0ANkzfkt6cdzd+/56yrlYZiD9L6BqQseAkFWlRAXNtymsLgZJ0HC3Pkv4
wECH+JmJPH7pdeisnTBsttfSoD7ct98NZoHbPkIkhYAxcOYuZxrcqcbMEC1Okhgy4qbbV/xFA8tS
i3x5TdzrB5XA9uNMM4u6FbXf3/RKw5enZV3ruLUaE5s5qAADSMG5IL7WXN3Sy2kQyU1oLqprO0da
Yp0PzHFF9j3okR4rxRqF18SslM1uTSxZyiouZXNH87PpSl71hBM5uXwu4capKAEEEaevTupKpK0U
GMasuTr1iKLcfjixTlFVDuZrjwwoqODEeChgMXH8KyXqcINCKzMxZiMghcmx9bvGD3GpE0SxcS96
9+D3v/CdHFFp5hBCXRVu7crbjH2PuWI3zKZ7ETmCBXXcu8CtSdHtS2lZVg2xSKsMSW/hCO+kg6mF
8chRo1/EJv+bIYClmsb/8AInO+geQjv3hQcf3oxe+sU48j7EgRemO1PWP/RcoI7dvtHqE9MS11k6
ATJpcCwQGJic1hif18KPmzaJBBgmthAhua9RXZDhXjKbyPGwSCU1+hz68Sae46C14G3gRsNlx24R
DgbdNHzujH6n6K7h88gRIhFlrY5E/sffruAzMhUf4cWpfHmgYkIXsOq4L2t1lEFl1N7+V2Rd1xVF
aka7rhdbOb2rah7x4SUPItYgRBg07NWkyfCbR2QreUeo1PE0o9xcoBgjAXF06JbK4wguWEyHmpid
qLKR5flDmoUx7VcfwYOW2HvB7+NDGg+GY3Q/c/GEqud+ORfPAlCrgORI1VRE2Cj+iwL48WWOVhgi
DyMD09TaPtmX21dalHjYdnwHH0HAz/no5evx0Q+tRd0G4UksJSJXdtoJxTWJcFuQOoR27trgU48o
f5DH3LKnj58Kk5snxvkYc2jfFxc2HQyrr37B9mQ4inHKPAmIkBx82SyIR65d524D0VDlxPvXfFQD
l62Qka/isDEr7F7B7bjDNJRmhJQ9ZEdxGXQBoK/+e5HsQsBu1QMfo67DudmLaCjMbIh+l4iu7gA3
MX4B/zpeDT5/A9foPCeP+ODczVWxK3kctBaChvhF4DlhK+GBoG8jj9hiiNuaRF+Y31Fy0M0tbDja
H1nFuNSQi8A7LJ8H3RFredvx+VOOhZWlD0rbdm9IbncuPIMrXB1zTtW10QcvTLJ3Cs3P87RWcYuW
HP8G54w50Zw8Y9OKH17yvw1T6vVVnrJTqt8CH7HV0iz3+X13bnFAN8ZwPRQX1IB1JhK6heNxYHDp
Jd+NCS2+hVvxoJGW+sUa+30tSZOZEXgBgUhOWg9RAHq7PGTnB3J87qvvVC3Z9htC9mDP8v/xAAQe
e/omtGncbZj95A2zI9O/Qi4r+9qvoSUbifZhbca6/RMjvY7Vlfq6tP9HLFfOY18PC99Fbzlc4pWj
L5M+UWs0CtW0yr51XmeFSmuvbVbXl8eOEIyL0os++aysGJFKlvWUBHYGAgv6Lxx9g88ud/u9KhgK
ZkkT3ZABJC2OQtcN+ZwQ3CwO8tyqlYKrMSfOL4kIg3sq6NFIuXFn/OrocK1YbapYG0hVkL/aSfWo
uPfX9+ivIIopJ0UXyskWjqZoiyrZHTO4io//IG8GUB/Gsdfp8NJwmLaGLkWHREnxeAbiyiKtQYgc
TOpIDZe6GLEUL1h0RywEcS1gLjqH8R9tpREaJ2B56Wj1iuZy746VCDgbsStPexgk3dDxghRpBUEh
rEYP8DdWJOJxS/KvDRnkYuhI07tHpNspaDtIdS1/O0IfQDOfMKD40YjHu1LoZdB45MqiOxF2BloR
PDPqBjrAIZPG+BRFD9H3t6VVgW07CDs+xqymyULhTg/JXoqAP2TM8q5heBFC7+ZNBreq1P3uMkn9
mrpQP7XyobQHrA6QlgotD2jkvXybNFkixtpko7E6FoSB+SXIZujvb0PPA5YTyNOXr3N66QJUpnPr
GBT8/gBCYvENyGhHIK6BXWnxofVqB5+OyJji24+6hKPOcu058U8iqf03sfjTGJct583bDdN2BWtM
mb92ChOBvbi7x8eo0NRHqlpKdoakNaRV5YmnBFjIP2UYxHWSLa55gGzX70ErWYVOBUYs6UU7FFON
IkTgMa/BpP57gW7Ks386RWiSMFPX2y+5yO8ibejImbclDl0DA5C1DHW4uj9H2e0ALUxUXmyIsAXK
oip7o3QNZ/IO2b0XMLrp2hjCoKxnCHC6wnvK5MAMu1NZDGEvEHgT2S8OaFMKkpsADXcfOLkrIdLx
/XWIQUyIMPnkw9QevXvxVshGGcQSVZE8yPeK2t2Ad9qdgTatKltIChs48EGm1wSfyuKr88breUPO
DmitNXebKIFFtpBFBfzgKHjMw/8kaPLJdB3tLDEw3t4X0wYh4AwqF5uj+QuiyDzQo1olVgbFjDEf
EjNU46oFmCJeFBnQcQLN7fnUk3TU5+F51BKXVqI7oH4RfUG9CMasv+r7Fm+KnSZzGM23PgilKgXu
7jAC9sC+9FUmHIJVpXC/sbGJLkKaQTKSKeIqUEL8mu6clBUFVUboMzkVNfB4EIv2cMtaCC7gQEhF
G3+1fucVFRPUpkhy9gwwpoeKcMKs6uQoj0VqJ5aIj/D+U6+M/Yq8n8gcH6qosDoLo87uBXvd1Xtr
adNQ4m9umJ/GrSSU6tMyoCFg1vhiIlt7oDjKCCqsOrNjwlN4lRGz/1LwXPhREiSL5J+dGWg/NR+j
ThVXJ+W7Dkg1IRcfvWfxkGCk7G0Yy+9bA5cSV03qIztSx2aVjkTX15AQzFnpNVFpaTPCa5I5x2/B
knWg0Kod93jqVXhzbt9h1y25A6KdQNsJSK5QnIvx9NpnscuyR3JMlyjj7GGpkZQmnrHXqFf2B/ZN
HEZT6sMAydddus+yL3zMn6h5L7NAe8MCFNyQuNl6V2elBZ0a59WhchI8xD/RSkMkNA3ndl50WoDi
YbJu1ioGpIjnwgAFR4Lcv/81ki1nmTQUuc8G7l01vpvF9z6LFCfYHFKYnycWxrLnbPTJHKowr2R0
7Xp3dmSxGXUSdOj1oZWWQBGoIL2Y61hpfqKvT56OFcBk0r3Re42RmH/cJh9DoI2YB0hFkZliJlg6
oWW0OSg1JbRSofTyLpUI1mRBOA+NUP74n7meCMzP9Mt8bTHzSb2jqHfrMETMEL/n1JxTM7ZpKsZ9
HzpJWiRZMtdN18Rp/VGMuSd0+XXa8k4i/p9GWpO0Wc2WkuRWZeX3n8syr3z0pwGWChHG5vGL0vGA
e7qUHWVZ3b1Ijhle2gSjwMttiONFyK4NX8gSd2mRDLt5LHW6msfxuaXoVcteXMKyeKfxT7c4V0FU
si45rFFGTjWuC4ooQnrX2ny0WCY3xDZ8YumfMrIEmZt4kdaGlQy0EwTRIhuxC/OveDFXnV5wn82X
m7DIZjJOU/SAC5NG9Ec43V7ElXYC8aEr8ftETD7GXH6mYFYRrUchQrJl0207yAP50YundnGfrbRq
wuXfBqaBq83on74ajD3SrxA1Q7dOypgJm0VKoygdTDC4+6283zL08zv6Jz+lEJ7IT1u5Sn7nj4QS
mjQUHcTxVo0iv85nrFF3MA9WQAxfB7eVa0VWD3ZRlvngMa7ejp4/VUXBz9cbEWgfia7qD7bRRvQw
aaKzOOPHSpWL7GaflJXxQr63ad5t0q2wbR8LtjaZ1Hca0jHjvpL0H0pQf+eUWJeMoa4Oqe2eakYq
JfosS+q7oQpmlz77Z8mGsAenrrRWpvBB2XURloEJ68QrwC1Ird+c/omiy4AoGP47YswOZBOIy8TU
bnAnKKuXGrOBrH2TChW8YRcd+rBMn30R6lWt0cUz+Cnl8EXh4Zq1jS6Qo9QfNGX4zpSdtpPT4amM
pEiXMG1Wt+fAsSSLlw6Eb7qBVUD0bPKTjDqvu+hjLd2nhCBuKNQxVLNdbLkcjLJXZy6uXPZhlzE7
ZLWdD79mdcyU2x/HJqiY0R3JjV0iwTXdqtI0g29I43mROZ60PqjwMloJMsLgQpvKGaEZH3DfcTxd
+3RTx3HXy7MxAKucIlgDKxbw7h8G8V+naoXx91wZ4R6kgfvnP7KNSGTP104BL0hVa1tzOzi7olZL
yuUlEvqRAbo+F6kml5zkFMg7kZLOlPBtEt1F0yIZwmUx3SSUBDBoMIgvlvmWuCzfHndYCbcyzpff
VJj62IcxcFTtIJQnmUgbIk556qzRhy4UCe23KHo8bTkXjPLcMvfpa+OW8W2tzbxPvES8g8KbsWUf
pZisumS3k8yRCKeV3lvtyAZXfx1rn0uIAl0Lskhd0n8ev1szX94C8+SAwo9gnV6ZY7u/NpoLBwIu
8N7uuBBcVLvTfY9hYCZ9/nngf94et8GCIfSd7nlriig+uPslJsJyL0TxAumnZDc9yPXe5hl9nHBz
RTp9cogjwFaKRCgH6t00AZtfpwlim7Xihiv5RL7BNmsvPNekCY4scBZsb/iu/+CXh5LSryZpT0sU
Dgpm8iokZfjRV5e68e6l4fZH9uVXK1/S7G4kdy3nahRvE0RfwBZMXLW79BDW2je6HoW45lLOJmwj
0EHnEu+SyvY0esdvgx0Yex8rKgXlSsuTvRX8SBfw5Pu0+TMpptVlEBgsMDiRqWxYTKAxeomMYdBG
ZTYj47G4orxV4o56Ixlc5SNIWFKJnijSlUPN3uymX67VnweppXOijHX7lF4fkK5jQkyPBY9hskm3
tJEAwFkzEkg+/Ss7jzgB6pcu8enJlXiafxXPz/T5n2WmOntflmKN20+oev8bLz8u0SOgNqYf9Gyf
4l/Jz5aqsVRhhG5lSZgPVDP+VKcK9CRppI1dMo6qbWUPW9BoPMocvoUv/7LH8Pg/+DDjOoikejYF
bWVx0WGlmgUenTptbeXpTi3vzmsMP74AvNKyfWY8rmErSBqKnc0N/w6LUqA0noe2tDnHUrEbFjEx
yAROwbFORbKsKT5olhThNR1oHSENlHQWoM2V0Hbyfx4eXA0vkZzDYgYvkm78Sz4BP61PfRh2rfIv
N14pEr539LTBohXgY4XexW1R+UCZL+9o2q+EUZZpx0/Y2twR3ltYKFNFKVXFUwNzaFIm9HYYbtZk
i2/uhs/Wdz6hf4p1JLjsPFEtLGXC2L0GkjrTH48rQc/TNoXP1gAWE0x+fvQHsgP4Y2zRvyYWkGQz
aL8c/tkEhUCf5Mw90m4hUwryRaevnqqZVDBDOa2QM3E5NIYF1mkGPgTxu1mYhIks31ksqnDmixv2
i/Lm/lUqW/Oq3QFTEIDHZhKMYXxzHkbFA2peHI5uDZYqmWsKuc/oPfL0GcxvOdZGpDgBzpQEHeKM
tC7bJAwEpQ4BIe2qM3czv5gLizl4PiKfD+6MaMYtHTb81PjMmgaHakG6Bt4wcz7tOk4BbPAAfqS+
jNtLaIu+3QjB0VngFEpvSy5YsD4ucLh1i2q+uI0ltf3C6+jW9IY+cXwy/5l+HWWQvqocBvtHH4Nq
R4Q+uLSUokNCw/KbOdg/F41/xybM8g4eNvfOAp8iO4a9MK4sFknNOgXpZQ+1vgu20POWlIjhFDLj
fupoodXxoaGUELx0R12g9zvhib+DVLdd+uxlbm+iwq+/706rj/NUd7fNJg4MUkfSElZ4GIEQhrh+
Vzo+OYz8WyQjz00mrnjU3NEoiBOdlcW8nUWioinjpJK4EAGJHRQiblcvYjneN/5TpsrNArDEvs7D
/hz0TLyH6XxefGZAMhLaS9olxL20xSGn9vNtElj/Rwz5BBaRoepGWLZywMMWa+iui5vvYjk8iC19
Xd+CdIKufpCYL3trPsigtDAoGkicMHmK8uZZEkUiptAjO4MbwnJofeQD6r0smvMdCHasDlhPLki+
EKBgnMvNMpM9i9UsokqJYusuKWzmdiiQsHyQf2klIbmpIadbxkI43glvsQew1GppOXDsRmDFoCqm
KSm5r+4I7ClMl/IyQyEiumriVWKyvgy7nrJy+e5KunnEZUUtruyWXSSfueHz98u7lGTQsYA+c9n9
kX8ydkMIV66fb6x0dkp6OxLJmJ7Ac61dcYz79s2hbF0Bcl3PuTr+XkCrXhyaPTj/ty+L/QVwb7W7
ZxBuVz9AqXHCE8gf0LHSv+z5QBMtz/VyXPF/VXHELMZ9LTZPKrSeq3SBv00lbexqeSVU70h+rLd1
14rIkRNqz+CtIZxjTC9esGWM5DEIZq7sqALqCBiC9snaZ2VKpMw0D02Ok0oDfATMEuoixqhmkW7h
tGP32jpAXRsejI9/fll6Sh5yQ4Sl3+6AgO97buRFk+3A0GBGQdHA3RBAnT6lkz/ArmOjj6hP/6b/
nBx503gKqtr5m9xVzp50SbYIrIQNBWblHWaTnpATqLhboIKZ3enFs9EjDi0dUxfFJgFnfNzIpaB7
EOhInh6SErDuJZ0+CO6Ved5Me4LHhNS6sDAa09hfxyhPuiL4QhcCw57YIMs2dIHBkhom3CeYuguN
GZCJGR5ZMpi/g+VX1SZADJHBqZyWTNtBGy9h4sIbeeh9l0+kxnDyHcCSzq/bd/UF6FKkghMVEB3x
VSpLeRTGSFWmXeSj0lhkMSRZo9DeipHjQX7vM7ytZMdedxH0vwAAdMfV0pVN60RRB5UzeFSNPpy8
EFDhu21AmvqQZvxG0naIfgT2bNp87643FuD2Xd2yW3Jb72bKyLVBWfELCwQTVVu8Ez+cJ6igVqKC
DIS2VQbRe64eY85o18EPv0X6xgptSlj7eP9ze7epH9TZnGkHr4BGx3X0PaHjBMNGFIKmT++m8S/X
4xDA5vN/Wz+DGyOf0D9aFzppbaxWoJbmxRuYV0pjZlDTuowkZJY3fhwGRuKusSuMYpo1U+/kXqon
fMSsb9yPCfOyR03P7oVV/Gmwhl6VlH+/3qNH2GodKQu7GvXukDTytVDyEhKT1aGFQ+63bcQ/wnSo
UleP7ND17EUBMNa1Cc0skUXIE/Z73fCZmvJKCG4EpBtjbbm9l2WeyOxmFRo89eSArwcnobOlsDzQ
4t+LNWHmGY1g+kE1AEC3QlYkJnxGGG01vHoz/t0KWg1qKuAvnbu/Uq6BNtySNosZ4I8sjng9DgT7
J4IEov753qgJQgcY+dzXG51LtOJKCUMBYqk7KkHn554EtgAhsNYtk84Z6Is0CMVxvDH5Cj8XwRCS
dHJtssnN4FxUqjr4C6CwoWykFckIbmuA42L3U2vXBp22s4ddiF9eqfZfUjdwAeDjBFS88kHuII/k
LrVlZlbXGm5AiwSX86EyuJv1lQ+k+MuYMiWjE0MpsTSjAvtgshj0+nTczSxgD8SV2kTgDaJrtEQK
gt/0tk2recRTzTeDZ0QRSDOHJ5OaPbWvERdI3GnljRXhAdGw2SZzuIO7AlF9aOMW3NtQZM06/8h/
8yNiyNfRNjEgiLgpJIxRidecF5xC66fCFUUIcHPT2UDww2VvEiVfqA+8pX4LiMER47lMthAL7AuV
WYcVzPbOOeeEGpxmBYAK8ooyL3pytvav/ATmyF0/kEep4OXblcwuz7N2HYcNq5yf3dh64yJNmkw/
+ReugN3LMbtYhX/2gfR5wqgFoAgZ/uKqlbhQuQlOHdf137FLr9e+Hhq1pYn2KtrnimBnhkkkG9mY
5OH3mW1poJcB+57uzUN+ts0D2A5P2tgxFijGo+5USHi3Hythd1duMI05MtA1Ctq4fUst+7Zobd0r
7zXKZ6QkryEqmh0KktGjCxatHETfm5Eqi9bnlcN0cNse/mun2swNxiZKwMw357SQMqRZlRkAd2oC
+7431gU8UT6tTDWmu39mhmHxZlwIKwuHT/y8dXZ6iNmyjANvGVvfqnMNt7TPEZs2fyYYRhMeXFFM
CdNy5dl2NF/klyDCv6awBzkUAPULpcBLJVC5v9gg7fVTmzn5fLjwHjxYFhlvQ7am016zIfqldXqr
tSBN13MPM80LFN3nugApN6LU5/hrql2DHFDmhQ34eH9EsYrGez7Q7MDK37QVbkV1wnHmbbe3G4nk
TaNFYz8DfQ7H0+HtzLYo76e0K1C2vwKlbO6OeGb+eL5Bhy2+4secXX/GeDcrQFKUs8UTDEJ3DWy0
WYGp8gnXt5YxPcEEqIhRguCf0l3naZbj4/ht21C0LJyRFIFYIqdmGOTWbwTCX1Gl63SxTflMGIcc
QeOkkjDdByNU/EfA9CSH1F3j26McjbB8vYnuRbVEvGJeDVHK5MMjMVzX/Bd+XA5dlldWrVLxd8yF
DUrzncXJxMTlncPygPe4vhLZWZ8Bq8dAnnZeWvZ/tk7n6rl/rHRETWYKesSMzDLCHgkItxhoCtIE
fRTlrbBTsicpBx9qWvlb6aMNSVeqFdzleLQhqLlHEycqm3PVlvkLhwdd2HVcaXBioJLAZDBwA5OT
SflOA/0Ynb6qeH98bnIs/rltx4YqcCtAAe9ojVd7pJQzEcFO3BMuvd+O44xV/w/5MO1vw/92eviN
A+ne2Ew+dVywHqMcXaAO9Khgj2CyxNF+AQJ4kbCKfJiOnSyK/ukP1BqT0uO7nLSNUtiSsb8snOX2
KSrB9g+u0kcgeaFVX379AlHuYbwdw4O+G7ToFkQUwU/hZ9v2QpYF4TTszqVSgcRPpinOlprEYOvT
sX3EnO4bc71rUviTXlCWa5I8v0ooJDMz/bNq1yp1s6bSLGK4eWtEGWIo/sFbhMkwwSSHw/rKK5lr
YFO5sAy7ZvTBbC0L2kXzQzGUBgHc5EQaee92IBF9bPKW/Jww9r+ayZfynbYHqUzqR7CGGNyULndj
aHa69QzDfjvC0iqkDKtOm1sMIC1+7rBXaa34gAl5GyMGfaKaYNXgCroKcdAOMh6r4DX6IPnG8psz
mNKdCxi72rNJ6g83Lgui3vHBVyKtykOfeTEWazHkQ8I7IHwFFEn2C3AERiUyGOQ2ZRUi0wj+ozFM
oJvnywDrI6dYhzadgYimRHTJ2n6mSdF9LM8V3QFX2+HoABuGVHW9/UfE2hMOppYxay1kzq22Xj7u
ak/YuvxFeHUbasHBTeiaYQg0HMbQWSdLqwbqNsd1XafmupNXg0vhCncI80bLWh7EeeXIgFPQZDda
cg4laffIdtGFZ/c+lDss5es8gWyG40+UYhttIXUe5H2mQQGS7OKL0r5g48vlgYMvTRT/7AaYhHUO
68ykFH7RBuX459H017L/UCVMvPPMGyn4TgWkGSAF7DsA5gfXF9xhaxQFgHrNlI5itgb7pxhCy6p9
xVc79+8cNeAy1pwOGmQtjhwpWHK2OX7EXcBkEJ7YP3ZCKuauqc3aezo/LmO4OtISYYjmukl8S29i
CGCbWN/Sy4RhwQlxuOg/wF2z+QwpNky1YaR2AC9Q9r1eLR/JN+Bi0clkbCISXcQvki0PCGuwO7+u
lfn0HqjB69J2a2P8Og+/dyRWULDPjjqgWJH3UgZMMu2k+Tj+Xj8AG1qk+WkY5Ygr8IpvgahnvZDA
iqEVkXNgwjN3zb0AlS4j3ef1X+zAnsw+9s4tegiMqdaYmHvSfmFn55mDka2WzPp8AnmBNowilVdC
6JydYZFrPjQWkP5N3OYUrHT+78l4r1DDoir+74QpW5/RkGh6QznP8j6BAePBSKSQAgbmv62fJgQI
mxpp2w14fDsrefGWOxjMI7BPPwBVckmM5PYE4KXyUM9/VNRUrEOck1LzXGGtsgD3IADb1GFY1KYG
rRp07VM/KAoqRK/e3UXDdyKC18grCenFNzTSMEnsadB+qfdX+8k1yAE1LX6m42UWWTMMkzHbMYry
GZ1Dby2ny/QGv8svJ//N+Uo2+9rhKwBf3QilJ34PCOIWm3s5xYfVIPVra1alnUDBDA1XXgDzBkC+
F+O2Woj3HWwlbz3CuYGcAEn4nhDnnNXJ8WXW3MOG/l/w+DRC/icgnqKdgN57iCJEMx26bNsPSoRW
LsWtphqafgmr1deCPmzSSug+fuODvFJJhxFXkWUtM1KofIZOOk04aQmcA244gO/yAulniBdgT0bo
iiklTyVXSD5XG/IEYJQtqDsGdqeX1LJjkls0iPuI3P09Ii8m8R0NT+C9KGStqRkwuDN1PFQM3zfF
c068GhM+a+QyhKYm56x2UhGHtR8VwUZscl94q1Alz4moV27ZYmLDADXn26J4G3MCcIEOldPenJci
13o1SymVNa5Jn1wpzdodTxA3/6mPjghFv0KWzD4VRzuYbciuzQqRwt0qN2K2DV2FvmDvgt6Re4PE
1EH2yo2A9/19mIVNHD6niom2jUpmHyYhMxc1gpuJIAZthUyY57aMwvfz6Q9Feu3ugvpRGoxqlte4
rsfEL+TlDeBfAICHAU57NtVT9g84CJrG2Al6fKaVnIGjv9LWOgdvcsb3aWJwzuaP8uY6qPhUvZ18
D8IhHHwFqHGMpXCnO+r4yl7ylrZFdFNDVKk1K/rhKplowVjh0Px8sFf3SpInyazn6pPcPr93DRXx
p7N/J9lXcrXVdxfomfg6/q/qHVNJc6jG9NugpDJuIFicyBvhdFK4M0BNH16i3EuWNd1hZGWCL4QP
2wX83xKLli9x8oVdAuZy7iyY2eBkBf7E54q2IQyoA9tNffk3cu3kB7d7FJ53LH7JZIbNtp+xFNzU
dF9b1Cr6ehSsq1DHv6EVYNDTnLQlvRkPY3J8+GSUd9uka1j/gcL44B8d1TBMksS7b1XuuNyjCB92
CxrbYLCIu0+pXpq5jAvSxSJb5GksTBWiA2z1NOudsGK/FjTnHTp2Bjq7itzZ8ilZNHqgUDopwuW1
X8azfTcWlZmxoXjlcdn6glSY6bWKUk8jY77fOQjCKfk+OKbLv9cfBPdsYh7QYNnNLMcTu/D2eSw4
iXb8xAy5dvdGPMl6WXsioOFiLDQiowVVX4MbirlfXAsZoVjv2LDirZOw464dPPNMu2k5Lu8DdVw+
zTRBgNzF15eNaTSSonyuUSTgphTFn9SbFt79Qm9rOdt22KNOxKuEdMC4eoDg/SauCNJ2FBYM2p3P
FK1QRXzg1AVumtzYNglK20zIK6vwIS1Zqc0YW51iFDmRs1Bu4bitkQiwpdBv7oq1/RxbFtP1e/UG
ZNDfM4HXiDC664gr30MNVPc0O94sQTDQQbIXNvJ9k3+K2m5+f15U41dj22dsLNQwcGFNOY0mbf1z
sriiDGufl/wzmnnJ2c+/dD5irJKtY6xMXM6E7rCawEl2P5AZ2AaXuSk+hct55uKX38eDE9ItNjzu
4BL+eLT6535nwArBTG3uqFQLNSftc0lLaKyfsuuP2qRtfUl5tOQzVdQnLRxFK709kBXkoo+galHC
BvW4GQkIRo1CEQY1qQROwImzRlL7P51d6UhhGuIw2rXfnSdqq4dlBpRiA3ZUTgJaIbRmLNQ0k/+7
mNULrRg89w2Cl7Ql1rzpngc6FAC73/70+fgjA0DUtOYUn62EIUOhr8fDRerUVLAUAAB5UG0m2oG2
Pj1okL/N2m5doCy4r1Bwu2iFA+WB6Eyv5kxLb1+lzqbFQ3P9EpXePoBmib8wTyeOgqdJ7EBtsqtt
wISJiCPP6IRvQg6mtJrrmjUu/EBlsPyGO5wBFkgFJVHyl1sj4ZkIt1jv2LvKmNoCuwSFCNUhRqWa
vgkn+35CHywWblNv3ytWZmLOVRkZQkLK4puWMtyB2slxCUA/Yub2hL4eW2Qjdl6JCpK2iKWRRr4o
51vosMr3i3HRrxXtj1NNTSqkPhTmAJYoUkwuhghm2+PgutypPkxdqAwRiIkLJ7ZLQvIkGxmR/aqI
YShCbCRiWg/m+QM5uLjrecPAiHyEFWhnfgU4UswLdXfUNqMZJWnCxzfPugMIw7WnW1S/ZU5RsFfa
Vl7Xj7SEM6KiOu/S3RIfR09vNlLhZn7rrQ5L/akeutzkmsvvTM6G8vLcK/MO90Jn8HdSVQOuBisQ
yrmQl86ZSCUwyOx7kzO5V0uPXcUvaQn6SbKneyGTBDku/yJBlVgCRfVEOMX+r47VzdLVXtnY1d9H
Dvx0qcld1hJiNhKn1fXCe0Ou1M/8c4TXVyzKmo8uvQpKbTdwrqqJpNT0jbxIdUOwLgehvv4J7F2C
yVMkfuoWezbH76skFmmJuTNEwjSvF8gyDKGYlG8+SC5ehWCPaHlK9vurnSB4nCaggZypBr2gUEJb
u2lbYKSVJnWLmY16mnywmLv7WAczA0vsYo+2Apxsu77fMS/v2k3Vwx7eMf46A3CW2X/NpO1ioKdM
BNqgd4Ou330oHMXkV+OjAcrtMHUnAeQrhrmpYvqHr5W7QUGb0F2w0QzcWevegvqx6gWhHDpPQ4Jr
I79gMI4/gNif9AGhN01tnjVAgcRMW4aidsy1TWLWvdsfxdVVMcQdjmSEeA/UOPOWXbZfkmOoAJsB
MVD4TCxywWSU+69KviYBXy0gMgowvGJ4AtejWmXkh/JQc3JyLc803BFwnCFRVHoGh0aAIVGSt8Oy
8rnJtnlgj4tD/URk7ieP3oiRkUECrJeU9jnmyLZ2dkIWXqfk4VlHup1qqThS5EjVlxx7tArklTX1
KuZaSnWIn8HQUuP4tUiA+HbPHZBakHVHM2zb40HMeGt/xSyPKwfQfyKYjooyT85LOgxGr3sd3/xt
hSvI/UznO2zsaM+z0yOCdyGmKCuP5ledMelCzjTRPp347qnH38G+jSRy0VoPUZHqTZrmX96pJpLU
ALdQOloTlVxBKdsocrPXfPP3gdEttcJ7aAGG7PuED6u/4hDfc8mnkmBZMlvq0K/GGFGINt+oyb4e
I6vLU/OiUxqqxHisw2J+u0kgqtIH223/OAa4AhYykNWBhXaQNsCCibeKauz0seAKg+pbNT2vmv9G
8rQ+/2oeg5RZZsBt6wPJcYSVgtqStz87renrFlg9NAiPh9AFYDpLxl7Or7lfJtf3Khcsj/oc5QQ+
1b3jqSOE+7pIBl3YQPXKg3srNkAQeFXq24bg1eOycYatpfsKWLE4IuEO+ZsbRCEo3AB2qc+HAfYi
kjrbkNIYHZFpsKlKurORuJ/A5Hk22j5UwNWySxHA3Tz3LKzEqqkCGcT6gCt+4n2OR5v+uIi6ytmS
sBLDT4ZfYhbhaeG7aGxTdSulqdF6eBCO6hjPxtntQ4OkY0QqBct7MgJC1tI79gV4jz/ws38hHvsv
J4TklBM7itSnuFauvoGLcuPXWdkQv8vTXplkjLZuc1GzOCYsbqdpAEPV/y0yKWQecq7nV4Mn7GWb
xM33qwFFWxoXJiL/VuzQEsDgSqPmqfC6u1svs9oEKxRGfJOBVWa0yjOCaIn+OjC3INekmYTlX6OC
VEVkefJBwcMQzGcfqxT7zNtrDZqNMSXQfvvTWPqUIX8mE+9UGqQFqlRukRlZ40ngjoX8XKrFIKpl
7n5ijG9ob1K9VyIFQN/iJVrT6/8KYW0wVwGfVvyoYqxpMIsdvrD6XK/bBWMiHxvwVRXMwOyiuC7e
ssb2Ac81RmuVg8hDRqQKlXHK1tQo89WigAmbvz8iNU02c3RJpUg2T37MBAUiry6DIyoPNtZzo8ui
KLpC7HHEkoVOMeLcdoE+B+uCYANDSO9fhayE+j6w+I1LoCqkdAY1W7VUbhtRp6h5zEV0opwDtYaz
25jNh2aZXMTq60GlmYiIZhMHLLg+7cz8EIc3o92lSnM931a2Qo1rbbit8SMrB49TwXrbge6fuwhL
0x6YyfLVfyR22G8Rn2Sdy1+uu5zmd90MZoYt68RK0I4viHSqm9VEC91npwZ47nboY+sZSwpTvKck
O6sXh45EG/kMTUok16aIKaJvFAlOB+McFi+mbcI2SpTUC4gJ4+irBtCgUjtv17Fnj1tKoNztJ/kC
APNeUjx+LwGyfvG9GeJ8YU8Aq32Wv9dRLK12T34qpmrsOxKpVnqFo0an9nlVEICAfr/p3AKQm5Wq
7wiKGiuo4iNxtvCVT7zvqV30kZhhVOkY9G0nLDOFXpIxL1Yl/dQx1ppNfe4qqEUv8i7KJD5keopX
fUhZ8P0Xe5cRnUBdrDWjt9y7IKEiGYR6I++OpOllZuLrNIabPUyJ3KEaZkB2EuurwQDuF7zl74zz
MUrlu6WPNaoL5lRBRMGbWtDlhoFO2RYw3sGVkypYIe92LVSFatfFQhsnmRQktXdK/zL8Pmx3tfMO
TiDbwzUdCfPHQoIVMqMB0I6synOSANZxI9brQecoc+vo/17JrXF83PK91zpXE+pGkEIxlkcDKUy+
/64QaS0gELqnxebZZZkKQqhnXQJfhrtso2IZK9NiwCeLc7ekVGGGY0TXLE9iZYmbj4vQry62t8LV
NgVBeDz27utr7uRSnHJlW9/UIRbqs2cX2wDuHAQH6FkG1JGLpK8xYTru69GGx9P2lhBv7vcPYZbw
r5fngrBx1xGZp1jDkdSpRGZaKLz466JGuwq/xYXgiV93jIj/QLx30eRCV39EaUeEq7lQebnbRDgH
fWLXU7lXIZEpHZ1x3ClMP7GoJCLuRj1gnuahsGUIeMQy+ny7xHAYV2clMm4G1a7v+UZLGkbI5Vn3
iekQQI2T5CrhSv0zOxappooJkcwIO82rBn3jBZCvxyVroIH1cDNeAJ5BUz80Kd75GGeuxACYurHS
+S35o0uT0nN6Dh0n1+WG+6B0tmTbTOruPGRrmJ2fGMyy2hr488ww7qPI23cwEuOmancgraeTatjz
CZ8ZvqmsPVbvDM1CFsHv+Z5V65EKIkP7f0p76oQKbaVG/8v39zUgYXdCx7qbEcyDxVGaRS2agX8N
GwP/HxSDp5CmlWNcKkFDG8CD/IFiDOIdSHWEOq2kHxjn4m3hdC+hvvG3xVynLScT2E8s44pg/cdI
rwA6DxqhfBof+1fd/oTWWDZKRWDN8Q0I7RX0A/xKlWKwEiADDXPJ2I8o6mU+V5XMG9N+GkZMTFL5
JrU8mrjT/AZV/g7AA3YCN4dRd6t+LOXs5K2ecOtDfvDMXtiboGrglYaCaAcn62KfxbBg48C0MRq1
Eq8+xL4UX/LKo/Gv701ivrmhV/8mH0RCMojMs+rlkmbtqnm67wxoTjiDRkiwfqlQOWSebRMCXFtD
aBksBtJ+KuYIoXb0zonXxVxh4+hlwUYKU9VmNau8PDzHKmv1qhFcIAUbu/wAsTg1j8/BpukOtQQc
CmD7lhZOEJ3k3GhJF5dhL9GJdA4p3P54KpEBYNBs/BkOlNWcGIUu6jr34zhPrY6pSyicPuLdeKEI
IBzpVvPO+EJzfrwwc/86jcVcmW6gOEd+0lsutV1USAE41W6Jo2SOiZWrele80NnsnMUKi8xSjudm
WlshLzdlL3mooX+6a6ITtCObNCT535aiFJvbiU1c/a/Rr0x8tkINC/zq9jaavfVfLbnaYU6pWpDV
rVwZPNXfxyBos+Oc0TyvRSV9fFM+EhEIxBia7qp1UtuXpeIoc6oP1CBpNa3W7EDMJHbG5oIMChr7
E9qc+MOm9lG/dRRj8SnXrC/CZQLMBgmJ/GNXJBvgGOv3y/8rY+4fVyfAf+4mQFBkMwCeFHy9iDxI
wur3yl3aTeC4ImbRD+5obrG9p5NjHPwOMTFBqCqJnFEJAIqOb9jWrB7i1JgrTTc+KMliMoPOI0up
7otBSBye7xcW4Hs7yU4xGTYzTAAbGm4r3OtjHNowWwhR27X7O1+YyCIxTCcfAKjS6wFniX/T54y8
oWuiYl9pkaOljPtJJXnWXHs0QOMgwpHafuJCK14+/dI9icNsQ5SkarCWrg+XdibiQrHUt518pjtQ
PYlHIanDPoeDN1rpFjIq21AAT+QBily7h154IN+WVj4/g6rQBGw6Nd0RKVMex1vyMVM0/7eodnWF
yUJogieWjRfejAZFat5EC9LhNAmkwxawiAzW2BlzFgGAI9tjSgk0Q5+9cGliinb371nJFv83FlBh
Fs3uUaLP74IpbdnLY8pgWXoFDpJ9ESmEnrYaYHeRB7d+fn6TDH+vQ1rG61fRye+EUajBBS1T8qms
RZo1iINRhRMs7vSba9Cn4owWe4C8+JZRbSmme2p3Gz61Vrm3oI6sZ+gm6r7R8AU/41fJoWbHEUC9
MKHTyMrs1TU4/RrvzCQeHhIng7cm65nTka4uLL8dNSr8IzZHQIeFDK7okxkGjTeWyTg8DWPbJaDC
TjLOZs5G0s1YFpunF8M+4jGaUBfd3TXcBrvB0nMpMYjeqvqmauKsdUkzf97/47ddmxEWyVhApTzf
lgbZHGIgnB35uB8FPtGvwWRC3LaztufUJLQpWK5yBEh9hYCI8S1xq1ZjA+yR6v4Ib9h+RKwSPg7i
vMhgc8RwkQRmlzWxHzLih485/ePu8Et1jdNjUpgM5Jn4kb45eb3yxxXuw2ND+j5mbqLjZ88G87TH
HSAaLEEIf0AXgtEyJe+Qk82JRa1uZn4xRkl1yapy/NZCXlmoHESXdR6OnRmap9RW8bBpM35DbktW
GsbpOH4mC8ZIp/lTXfZK28YOa5UVfiQlicWi3tpirJ3pzvY88595MoA3mb3IpAbjgYmvc9cn6mK6
+oaiaz9U10IwZkwwwtORD9Gscl4PXRwumy+ndbjDZZkkfDPpZ9HGZY4fgs18qvGGqNUfd549xKG3
oEemYkfS3u6x/SeI4VFo+e6kNfb1a8J3dV+Sb/U61xhbh7IbgWpoYKyVCAnLZTtAqPjv5UN/dFgf
Nu70Qi9MvVrY0lfgnCGhuzNu+pmQCeNb8Wojnee1N1SKjlN7LaafT+I564G3EGxOG/shOc7Ua8mB
4J+GbrO1wmglH1sVq0JKqmrUwmK2rZgiS3CTcw8yMwyaLCIiV8LoXRUTSpZDSl7fjJtLNf8no/+j
pgT/prqpq2uQHo/FYKdlxXq+Nle2W099YvdSMfG/CkROGrnJyBBUhuy606RpnVUs/1AuFgD9xE9F
kyM0GpB8olQZltScNjYyMlI9zyk28WG++k2UTBGb7tr1nbibOaZwjkHI1ZELYEg1wWjPOCK0UfPP
8k/qmyMyhDqZkAojmQodqV0zU2rbV+Jbi064dqf8zi9m/c3umeP3N+7VExhWaAnZfBqEGsO5RxbK
aE/4v7EfL81E5DhoDHEC22DWzmrKMwrZqkTCs/8X4Wa2THsjEFx5Jo9LSiyQrMb3x10hiNbgMhdg
dTx/5BxY9ycFJKyQP3ePJyOli3vAUuz/FD9ttitPGb3ajWeDl8HFvkQRvqNMt3DExAH+Eu1Kyi4z
jT6SzvoqMHnkIv1xbaL5Q/ZMLYw/hPWGTvPgIE+sRbA4fu7IWtmtnPQxeyKcheNygUohZPgenotJ
EX1Afgv0sn/yLRbbfsoRJxzi193p4x30VZNKkRmVvXAzYWMD8b7AS15F75F9NxOzyCdSF5CCxC/1
yhAXE5THWQn0Js+Wgqf3LcbYPDL9HHAs9OKa8IGExpmKFj5vjo9+6+xXr69DL9h5G8sNQrQ8mmOD
egyM+a9VBk5a+6g/hFjgoN6HRNnZgGSeMigYGUeYHMkbJ+yIQ6jDz8TU/GElVH+6XvHxgYbMYLmz
U798Bv5wtKImZWUIUcH85uU7ce5IqXgY5YdUwyJSwILRXyomNG6i96Gkzu+2CtJ1gSBy5vfHM6kI
Q+dMjuzNckDf+w9+WT0p13IevGesmayjD+RbKS/flALp/No4d9j+ZHA4YDWYDa3j7Ul0LPyZ1Sew
UyJFJR4Y1Fvpw9uIGh9YrfSQsVOZ3X+kEOpC3p1z1S/G3I2Uy+f0ZK4C7OibZc2ZEpiRbyZmwUZN
oijKHi1mnIR4Ucv2ERWy7YWGoh3zi5smiCUueG8SbLfDiXKBbadMq7ydR2eJ4OvKHGBs0xDIqCD5
AznfLdoeN4m42gyRVOv6F1MTopnGMlYUrDroD+uDof5E5eMYuxfTVpMaDFVtLd1Qx/zyveFWMlAp
luwa1EujNek4uhAJUoNXqqdAL9XX/orUH9LqU8OYLrprx1dcq/V2GHujh48iFm8N/g3tGJwwdZCG
Bx59VUH3aq6dLdeDaYoOf0JHwhUW5k62Z51vXYultdlMPikJcR5rHzFaubSrNgsJNLUFgXi3JN3x
yFKWFYFL9iyHcRbxHkSqpAFCWYwbSvSfhLpFex7Nh0JvSJeH2gep2w43glwlWOOJJNRQX+Hw3aTL
EoI6WqrDrNB4qDrtwzJxQhu7kXz/QrhVe5M/+VdDwhwacEuPjCefNopGebIf3WaCtIfmDUTbkQSV
ulHIgCzJHkj/Q38FAhUUVqgXBRZE5J1oSkEMHQXFgCYie4Ytpu75yBZZPN61agQaKe17gFL/gJDc
DzsG+4ufDzTikSjDOL9f2BSA39vnUJ55bcz3gnBdpp4hzkmWE6ZTCv2EuLBzWkz+l9YMBWrluNu0
yEDb/es0lsmX3aRp3L4C4gS+4g6oXsVM1+3bHx5euO4OWLEyiRAjeK5yKm5sfo6Pdie3WQzHmbRr
GFDhDDy+S/rFm0bsZ93JHl4X4zZiSH3/sOZOOxl4h08Y90cmOORx5U0sIhqStM+F0geWiHnmjxdo
8m6I4TE1S1wpOj6ER86EHXj3MQjh5C5sFEBwvNk1O+PIz9inFPI/3c+oLnY6ga1uDA1WYA1horbo
qoxRCIkm+Ad/alauZE1SLcqeHaZh3cqUC+LJXidMTnu8FW/bX0g08RRbgyQdCZxGx0i+zzBPrWJj
TZs8AywiWAEtysTOKyeJkXXwb9MbisFyQMkYe+y3EuVsBLel1ayxZO/dKXXno9nwxC5JJ2A1DZmr
gBO7oPYS2Vf9sieHpZq+uiC1ASN/1P0DRrpFB0um8tJdr9NZBvT0GFGhkvH90Bmodu89WgHwAS+f
reoAGAi3KW4Gz+jvmjP5bAhGjeu3q+mo8hsSRDacxrcdYOLQi6XQ4F79qPBKoLXdSYBVAnfABdAd
inf+2Knhvn/M4klwKQgwMBi3d+tHvkGu01ul2k/rDQB0XZGEc0RWzGufArEcV3Yhls532HJHFpPA
qy5gdjmzJkTDBWtDcEWXuOjFJEjkpHNNl18P4aScq7pJLUxh72e12n7eR062L5XJeOGVvjmmo5eI
sbIAw8hU89Ich4kqy1OOxOa0s9kU9bFj8ZtftroKoUh4FHePAHLjstGFUjypwjWlWCDhK+XuH+Ia
v3RWUNwkRbQ3fbwl2XtWjv/nwiat/isK6jHbwbPqd5fNCuoa1+HnzLfHKXsI0T8t6Ec7qa8VcOvm
N0SJOzbuivkJUin3dNe9JYO2H8/E+zH89IzAxaqDF8gIVPA+5ZGQwZrAWSEckuYT0SW65A5JRbM/
AHt6tovkUW04krIGWfQ2fDNI4CqLup3Zp3TlvoVNygnOOb0eZwoGMt/hT838sdbFsNSfv0LTovNY
yboIC+nRtV5us9gyk0Rs8qTPMTqo+K5Q187wv8jEm8KnzBU+j5qtsqUACHcP9NyaK1jC4gvECu4z
47IfZ+1a9tl7LRfNUmYoE8YSAp3ZR8E8lxEmsW7D14guBw93BBB8BHT94TbMcsfIOhhp6ew1Wlc9
ORhENmrXEf6DVnk5VK9S3/MP64R5cCOuAfcxe+MtJ7440Pcgemp8fLFZuflklHZ9uQHkHzH1XSy+
nYBOgrOGZfnjSIVqG0IyLuN/9ziZAF44y03SSV79TRZel9lexjLQwclLz4GAxsJZmqXc4kzOLmKz
Bje27eOFed0ZsgwJLK+MlD/5RbVx6njwWefutvKjZouWaLx7PCpz35ofHXhI0QvtDpac3YXgMO8b
d9/NDp5Sk/hKhnGcns3rj15sYVjXz/3zZJR+XUP8KpY/xUr4uoEAbmPcXK7SZw2mTMvEI50rBIz9
i9NLJLkUEp7Nu+iqmVBJy8F/hT9qCVVh1CNB0vuZLQWVJy25LSYss0NJmrs6k+GQmoKyhUZSlsKk
SO1Cn2VTdY+ACJOwgNOhZ84R+ShRQSxAAtPVehXFSGczIw2pwDkere91gx42FyNL9a8qtd2hORt+
TqnXceyXzmUb5Cpogu1tgjSrPU+FXOngpNp+lLp0ZqGMtrWRxQthmlTegyS60bpZ0gStzHWAG3xk
wImM/LnWhwuIJddijwlZeGf26NYIEGkTzmHTdvts5/6SRLkiy1qHr91GAk8mgEizJU2/qf2R8sOo
QfYrLQk6UFtwaSEZAEatDfq/WhOVzrU1H6kBSQnDZvKWE6B6xU7BNBO8npPLIJ+EfK/bm5/9/IGX
3HfbhqD4KOatmBHkYUTvsXBP19Bfr5RyTrzfJXthCIFtQH2oWPm/At+XRireZDCILcFPdtl/hwhK
4dAoH3LqvR0D9PE0FMDKaQhcZXEVCR7rBS+rsUR/x5J6JA/ZwcXEf7zwHRwSvyoBOz+d4Q8WEQzL
0Cd3P+C3BHeXNkEjBEAgETNg/o4BuUl/0dndi2Ia9Li61FCWbfTKmFEp3PShyhTHE7S7lsIBVUFI
7kF7vujCWj8ldZ//dnHZg3D/DrNo5y9Gq53L1Tg4q1gIHODGdpIHY6ypVOW5QC0ZJAim+ieF338u
kYDIeS0YqRV6y8cbkaULGLnGNkkGwE0gJO8g/6YIHd7/fjECBtkRZAJYUIRDcHAhTHuFyrRnCImr
pJLz8YzvrcGb9ilXUnBda4YQWXavrSOQOUYvh2dNzJtYF7anZ8pAGrd9RQoUqSlMX+Ji0B5D4P9s
9gdqdYAaDAMJVbCH2ihRQ4zbkU3/CnXsLxFWHh+FXkFVq0AFjHEzG0Nh83gGBrc11h94Qzu8Jjfm
BF1iuX3PUM7fXewHI/THrYqj2q3GVpLRGHNa9Zyt0Qt5Jwjk9uTCxmrwl7Rw04aNwNifcuJAvwW/
gCnC1MAhpt+aybFq+o9/sQJb5lUCVLfnVlvIPh6Rgcfp4IBGN0a9hQi8wKUTpA2R5nbShQ+mhZdQ
kv7dAUZF32XijDX0tLW4vKtaHvlKqnZsZX+lPBXNhgViHwtBSe4EIsQoOEmijI9x/gAfXSgmCIGa
9SNKc55gDYOPXUOMNjwC4PHC+ZDrMtwo1ewj6oKyosNu10VGt4llMENOHC04AKrTxyztc0nz+upS
golTZZ/J92HQWwM8cpUfnIKH8OI1mQx/NXvL3nnainjY+a9gan+AXwWGHUC88ktOTbQawR5IIEju
KrFsHLojmwMzsnvFgYRSqftxvoZZVxzs+1KMKXP2LYxWpoLnrkfyVAg7RH8jM1JtcG7Uw/2wWgr1
ObyFPHe/TpT8aR8clPTinxip3P+N4FER+pklaOQ4cL8M0gHVWRTaK7gR2VUZ+KwRTJwAB/rPMW3H
EkBJdrBBpxVwFQ/f32SqhZg9m0a+j+ZkfhfeQ8m6e+vSkD9zPyipuQxJqxjW+46nZDU78oodciUK
MZKwEZ8jB8dkl1K8/VR8Jm6F947u0XkfHfA91UC2fmqbUNh8wG1asObpXRdhQZPn00fE722QCUdb
Lj++0hLpFj/Sa2e1ujJaLoQ6sqHuIqY2lZxQoeM2FABef7ycD+AwLqfAKtmfuXrEoj+Vg+PVt83T
aO6SZExjrGmOXMlWDC3v0YokDGUZGgRrajUMNGA4IAc9n/zLEZghIKPjgcurxB86DjjbUCklAZWp
tBzWhHxPiuRgjw5Pc9rsQ6xkHDU8Mr0dM7a0QFDsJieu4RpMBF5bwMiK1tC8Ghd/Kr1CWczFXNAD
XCAoTEMcw12YFHxMFUvJsaR40WjNhJMzC/+ojMBlgOzD67B2aH0wDRZf66AxTJAQXUVEFNAPz1qD
UAz4c8JiGLYmsK9iyavW487ByCwxBRQNZcClkqfYDM0PlPrnyHvWnNYS4Rlz4nNZmCSiJuBLl/2Z
8m8xX0I8cXieahI99FF7PGSJ+Mo+LEOrjy15BSWIBGe17s0d/HB9rEyUBGFEUtbBCH/KR5Y1wsBH
vKDuCF+LCVUzYJtuxFlUe2GaldMfBoU9mVDchGCuJyXMQ0vISVRlt4FhO8C+89IWfjctPTm5KpEd
Gf/eDHSGtTWEb0roodWyQR/oNk3ZedB4kyMJilqKeeVGvesu66bD9KF/1uXfRBu55LtsmrslUP1w
55+Tvs9mhiBLaS0mFQ1lcKeFHvxT+MS2w9wAGI8ggl+ccNf9xbDgdEjc2gdDDwUadpF3t0edJoSa
llvh8behO0g90xbciAatx5oIIV0wcpwWbpMQeFaHpt2gthgsfWc3WENKBRX3ikr0C5U+2oNsHtdW
vlpq5mxUxycV4ahuEHZnZqRdzMpHbMcIYg+mFCZNkHXMKkVEMtzAvk4l84UVIf1gZJd5jmk3L/bG
UnadwBsT+e1KwfafzhUTv7NuGVuXDy1zPkICQOojYNr+n/G7F6jSKo5p38Mv8Trmjx9HkfKTeag0
HAxmqaFRq6blMpEsnfCNHPTnpQPPLlYNiX6M/spdg3eOsoie0HTwgWZWUrGgVwzIveYcSA/W7BfB
97HMc8p/fIXW7gavDFIpbk3SCmuUpUDbyQE5arn/wHONL8H9tvu7ZSqV0hFnemBPG6ZTtL8P5p1b
oFJRb/d2UPpv2YN+3ZS6HDEp9ywqyzdU8/EWfE4kdA4i8UbD6zAiU5UvnqO/ElSCbFgDVISmhcZG
Jv68vLsp/cwcVSSyZ3FL3m109xxFu0x/lSwuSZBTRpSxogmOfHfJa80GEVn5wUZoRqMu1x/cNJt3
t7fSXdq19JPlEO4aUsQT1l0MVYTvB+KbnLyMuI6Z2JiirhIUs9HIqK+1qdERDGkr71HlXqd9tVRO
DzRV92SQEb1bte0EKQXH2iPXvYZN2MCaoQ8KoKmT1niJ9diqYnMbc/Qs9RA5B5d5fg4mKlZHlGmH
EsXUorA7kJlOgGgMRG4tSZ9UVC0i1hSVU8Am9t+afLxeBY2frx12Zqwz5qxXC1VZ6CNelvVOxlxb
yRe82o3FzlXtBU7ul9D9Ez9jGzY2f9LzvpKHQW3RvySqG/eZqiywkMRKohLUWgOc/hI6isn63/2F
AtA2JlCYz/qIO0qlO1iBobeRaBoWxR5xXWLQhK/GFQzfq13O/BHXTSRMq/OPl5DehxbxyaRjgRuI
/YIDuqVkvKBDZ519YSXWV3szcEdO+rr5c/8PdQ6OGNc6rxkyl4ICsY4CNwy1Fq0pE0/Qg3RCAHIP
oMC8tdWtKbkKQZPm4HcrXjog7NZrPMygMvgLQtthuKE0mXPjxnR1UHTg9GBIZFZ1h9p7eSF3oIVB
IR4xRi6Qr8srvAEuivl29eDW+mCCHiL1PTRxp/ssrROKOCgPgEKx3OtJtlMW5rH60MzENcesTQNY
mhPWc5AU6CcZxR7OiL3+5NkTzw6MqRZ6O1Qb/G4DcgLV39uNmo72NmjjWvhgS+6Ph0vrgVwIG4f7
mf8RqlzA+qeFMUyKFISlxXw/QT/s2w7vDRpo1gsYr8dGgrjd2vRHBppMUh+IuGGEhoZ6KSoo/0V/
iVFT6+XSBsZcCi7huYaSVOa+AavMb8SyqO3iQbK/4LgN4tmBoEAbhiRJZ5OUDqjrMLrrgmeiDrS+
IlcDVGxIqYpd1TWzunpgOzqLYBmu/Za+rte7RFgIrN0w5RY4vGGEZ60mBdAYv/GKQVuW55iz99I7
VpAC/o+IVs9XJDD9nuzSO68KhJpUYsC3YlV4vG/tTrCbHbAxzLWyg3M09LL5V6yH18+RQanhn1nc
UTODN2c9hJOWczx3onBEI3NoXeH40odXqoWumOJAZIaVNnHGoGTYvQlbXCOt8MuxL37OLP6xtVkr
dSH8gOB75eFRtUl5vSss2+TRfqs7KmAOI8JZrmlwC5OgW3RxpsYDbjtVLgiEzj//oayEoMMq73KY
FZMPlh7zFGyrRy6IRYY5t10gvPG5K3aDC4DpgqRdNXUp08L7YM0/0IUZz5ds9my3j9F/gE3hvN5k
WGSIA1nuOjpVmMamwiqwWDe2lIeLqIoZFo8sOUHqlY4muyK38XZA2kExxEMVWyssKywhIHkA5w2v
x/jAaDewASIvhpnYYKxvPt7J+vauCZfLpYarvX1TTmU0So5QOLrQk24zaxce7mivLk/HYZZVqpFH
/ZIVFTnyK31YluTc5yoj5j03QuJwUPsfIbAI5+y68BtiW4qzcesvf/T8XhWaXLxEiJUMZyRDzHOh
RrfhLlJomoDKfYJb1YRmCOfYU779dAsYFmYJJEyI5MIBcFGh4bSWY/zCv68EkSJMMOyNe/cubb5c
IN+1eXmOY4pA6897V9JyJorFfsIy7IgL3vivAIvaQHa82XkJeUgUkK4yPnH7RgwXQZIvtC7p0r72
DhC/nE6i8wGSmrHsWRj4fZwodtAwuNuz8prFt8ajbiB8tbEAPBb6QvnRBRMOJdtxjlbE+ZhzSVlR
txcOcjeUrIiOvyHqae+RwsEdelcZY+5KSQ/xIXRwG9V2pHAHDJrgQ285Y2+pyNC0DkaMH3gYtL+c
75Tnv8Rup0qhg6ECnaGfJ0CE7KWK8DEjH/r9QQMgSGL5vqKzFTw1qrASPL7zdl/sRrzrmWDGvLRF
7TX1WJcH44OjixH6ykwVtugkjFPdzS+VmXQIEGMly0+ucAKDSrdgqoP1wZ8375locWL4fltTNr24
AbcXAfffLb72gM3zZtaLPeB34B5CY1bN8sDogIJtEz00g2ujjAIJfDsVstJbmKkjFMsFXiHPpEY0
On+cnlY433ZaC6+J68cBzg8YQbzjwq2vKQKM/4vT5E0JVrD88dFpbGudUH1f6ZWYj1wDvdVLpobN
89WsXcGpP2l1VzSJWUvBmsGLKGuoNaoyywhVLnUvL4DvHuPHPJ9zv4NM3f0hOchYpjh8sFE6RPCW
mo5aC5u78eNzqXFy5ATSOdv7X60udJVt6af1zZiN+MdAEI7tkRYTzPAi7h5Nyz+0phrgFZIv0DOm
VNqTDe/c7lTVC4sWkkQIVPw4wwIVytjsN0PS9TQPLIyoUjbzEso1RZt2Mr53vQPN6qSt8kktiCkZ
GMFl2KkvJBcxua/qggYQcgMCxbUtW31gDqBArDncsPLe49Tw5+tM1+YgWkFGhVGFk6s2b+Y1DBXb
JeKQzPyEWhRk9y2EddbHnWa/xw7N0E9iWZhboRWkhJaSTxtwPAsTmmXOOwiJn1Hcbxm44rYhgoh/
kvatlAaQw+ybGPr6jWVq03EVKhOUdtvBpLTE3ifeO5q4LV4ApGisbqlR1NpJ7SusO4aW0rlG7GcY
XSvGUhEZWg/mPG6HPFuTRs3YjFiKEo4TKgK1nNcYLY9bbpgCgQnd1jxBPg7xBrC2cIy7KN/vJ8Q8
9HgOVHsgWpTbaDteUQkywGuWjK+gfFjKdTW11TJSKj2Dey5kh8i7FKJFJdJEjB8qxmmErcRSM9cg
lITA873E7eqKmwpV57mN9LNw9LB5Mu7dXje3OyOgDsYc5wXmMZjqtDoaX2cgJuEounH2Ae/tVeJl
1kN5f5xvNcUhjdZYxPpQps6ERdUFkcmJwB7EFMoZ9/XjSmiPwjRayP09oQE1d0pfTVprBkHZO2yc
RI7CySKpWTlLMzuKl4u+a+IDMhnqaX8ddMfUebfntnnp1LO8YvKVeiif7+xDJhqPCqfcWNf3ZD3e
PPmJ1qmxkxeDoxcjGUaRvexpkXGYSvR3igb8jqmKdCGOndGyYNrs6Nm/ZcU2TDtG53XCRKqImwgN
T8y6zuYMcDJ4PAIEYI0VqSobbX7B05cDeGbI9EcXKri6HBaTGELUsJY3VbtJ12Sc1AVeO+5V9LCb
soVRw7E2fT25KutAX+ROJUQlzhu5CfnfUkqm2N63rY/VJ8YsBNhrCXdcEX5mevWNLw49yx/nE4sb
dU+ZLaYoXhewj5c/A6BrH6AQCliN2hCiNV2/9qFBQ2pd+WbYjeLK7Iebye43REu7mlFa54xiZ82w
FTBWzC4zsUzuOLgeSvvrcX+G+JghhjLtDiRCNEEle0sxdLlmlQk5ysa2naVs8xjj4oc9TdQFlshg
9NY10+e2MrvKUazVuuVrJHzdSmUkE9VSMaoSVUia/0xfCl5jSqUWnd7+wnocFgG5QReIeKinaf1T
M7AYbVa6fBkdpqFhYxFdvoT4udwi4R046OgOxAOz+a92QZKES1RyNYexVTWnSWY1B0RMjsuXzvdf
SonSm4x35PbIsp66eKDLYciJE8o0Tm8VogY/Thzm6dIng3tzOnXTxv9bEaLvkjkCGm/RwqqxSX79
mdCDPT96sO4QVMhHAsYwHWVY9bGNAR6UnWNayx8eHCR/vJ05ESojZp0UlYc1SfF5xs6WSHxQ6vmD
7dUsPnM2pHUVuNS6tOzNC6UymQcnEmeXZvN4/Ou9YgF+vDfeANNwi8fEYTCO0cI2x2xc3k/dWn+u
oaNQ9lod2MerwWgur0pGEJT/HGwi2JlbEXpSq121pWXCqSrE9fsR6NAKvOls9YUsYuiEgTL+wU3i
Qcwv3Vew4rUGqmSQkp71EbZVg1CTEmbcvMhdsI/Bc9URqkzn/9G9j2Cy4BCEy4jp4VYyyf5tCFjZ
g7FJ2kKZtTI017RzSAnBbugG0zv6qHTE5Rvu9j7a3+6EOfFx/TXpIqwQcTxDVdufC1+EFxjeiyWx
fWgtHXvPCG6xK4u7bzNpcXrdxn3BknG7keeWXx3uqpzxEUHibeTpCKfdJnLnFSZpVydQvUOSWh7R
VZE0sK1ioDOWKzqgCfLAvi3xabkGWWwBzUs2d1zeDWIchV6ownrGFrw+oiwXXSNcz3Zmwuf8xuCm
gXyE1sO1A6jwn9942cWbm/uBa1Tlaif+jQU0bveYb+upmQvvAu6Yyi4f+/0D49hBek8yh37tCyK+
dqGcW/Hla/jIsK6qyls/GQAwWYqt7/DG/wRMpti2clxv2EJMJ2kxeEBvRdFf/vulzJH8Q1asC5P8
4tWN38Sr4sI0Oy4j38doaSYuUUbxsLAKF2J3AkSlvjzV0zPzeSinmaIOG2dVh8Mzy0hErapP9YfP
bNBmviUjuTJJEUq4ITzPywVCM+xk7k87FddknuETVNIetqEgGepdePhyAQygRfu1UqhGkyKvRoCA
x4Rr8Zn8J4h2LHjETtCtc2Ihg8QY0bGthS1K2Z0rK2RyGSfxvwDK8BNpQwzDHwLundC6lhtBKdZi
pcSlRKXQ8CZA47m6EQWA9Km4wnRdI1f3AFnH6NNjBWIEUfi9BbxmUhMYcwJ9gizWWdh5Frrg8ljE
vvFMVT2qDFXmK/zGUIkHAvcDKM8A8dkzxpWXQA1OW8GVkH/0Sr4m6Ko6DS4dUh8IFGAqfwQILmmU
+Ipmh3Jkcl9f0QGwSlIHad5ITHs6+pM38PTKubZY5xhQvdzQtDnoQWOh2NQyNf3y4wA10PaTgvUW
TsL8KdP8692UTzufWC/CeaWJrsG09oMv19RRXrS+5Q0SawPRJJESQl2lncO8gVbdkSnX6erYQql9
jLGtxpiU2z2HCaknXHyEFTTS+eXBL7dinLqOCZ9M5A27+TU2t+roKZJLGxnSnqMv0zQjHxc6LXRx
hKn/uvdg2ckSFIvC2gGBtBBQiGd+oVcvS5eyYDC8NXSuyREhHJLB/g1tG9qoEDoboz0T3jRJGCge
vgVGKyRwzfkY5nw3cLS6355h49UoWxn+Z4lEuZqd6mkujEPYTlMunXagpeHDdQzrK2mlEyZTj3tp
/zgkSjhcHSAlp+BciKzJIGBO58A39nqAvhCDSgwxdJBZlVx+YPg+9TBIt+lRoe90n9NQF2lbnH2c
/b6mLfRSkJxpdcImK6tswoSE3ixrX9kfZi4hJ4dVBMaL2CrezXgvzYzVK6d9jkDUpo3hKMf3Lfs2
CAfb+eZHlAfVUbsJGH5WA5OHaifyBDlbnVg2X6CJ61gtVujHsujvmofgP+oS4l2qTOpSWxbu7Yc/
TfT+SqcSi0n2a+QPjgdPlIR2h0cFUu9WMdWeQDri+gcxxSgyJIK7sURAUnLNw/u72jMF/1srgCQl
wTT6uuqbg/u3u4zAYpdb93qosEFL/F51g88A2jqCRoQ4y8Vx8Ep3EMFQVu9FT70xFyvUB0ujZ4Al
E7I9dTPO8Z52S7blA4iU/Pxqe/1k5b8+r8avOIvKLZkWGj/EFjKx8u+xzmhPrNu281PscN8FTtg0
JVeaHDKu2nMlzBSbMSPcHEsIkOPj2Ei69EgpdoDZeYq97pHVvZ3AM5LSPV8kn3xGtsD9mAVIxBel
wmLtDXyebKwiubowjaxLyC+2A8Pja8RacN601iJgTueZlOJjfcgZFcHgTcmWnif1uoD5+8rzhqOi
SVbi0D6UfFC8RLpKi0wNCfel2J9q2QwDhZO6tS06LUHT+CbCmXTkdxDJofqU+uic0kk+oDR8rp+R
gyjBhelUZTDPsFb/sJrOhlkPTxmBtY9hzHEzqX0qO1MN2nLFtSDFIRBXzcTA/YsSbTbWkefDL4kO
ihsR4PCOGH+Q0moHOAI3mSiJs9x0lRahqkrLi+NydjgpOg9NXFRDjfMAuhv6jTLG9HIP2WbKO1xn
GYbXlMKvkuIUKWQB724lyYHmB/Fl4UzknoJpOCLyq96JmJVsz8/9BRrg0TKXOZzoYbLnLBQYqHOY
BBgC+JvBE0YvARtAVmLsaT3shEl+yYQAoUDU6GVNHYcFVfqIfb1QGPHooAFIlRMnZvnelSi4N7sk
Z1grpLR6P0LGagb+Y34NeqpEy5ZnsFz2CIk7e0p+QnGXaB5IJ5c/xZrHZEEPYKPhcjV93Qv4LG7T
Su6y5YUmSObx+dUCxl8iJudNVl0f0BtNnEtNJCxhuVntcpPxQaSpA5Jw4urNyC33a7LFZEjq8QNN
R/EvKmy7xySovr/5WrY5sIEGn6OYGm0RwM9H0EwpaeyQt5Iw+432yXbCKevNQnztlSSb1bBNGkNE
NGfqoeL93V0Alv/z+s36myz8udMl4shZ4clI2ja9VRzoyRHqScpqDA4W4f3gQaG6nUZvirD9TFx2
A8xFUMLghVACC6FlvRL7gtr4R30i0/r7/5p3+N3h5/v7LyV/epab0Z8H+a3AixQ0/5hj7Pl068Xf
puAYjgm5MKkrcBO/wKkBPvyhCl+obc9hE+eDC4qoJp1nnYO0TixdIYV8IwEdWuY1eMdAnRjhcYVs
G9EwDUWxc5gnoEYCM3KQI7KCQAMzZzu8RzJSukS1NzWaf0zzoKWX6RPr0SR3CdSBYkuouSdjWvTR
rNWf/6wwj69qHf+4Xi4Lr0L9lkFUgfKSkxx/XBcNX2yyGBFBWOtvX+TQI4k9pQ7Z6K6DM6l9aQ5o
mPgsLy/zBDPxNMy0Qn1gqecS/wk4AB5j44IQ8H2oE+bXmi3bU7A6AY6cQItcgdYhUDTJSxJlixLx
Hf5sHKS67dsVAQqzCm9SyGY/MdsYjXH95kWweRCdQOCFWO7EdeKnBHCdBxEzwusWarIw17cbzCWM
k6EQzJ+h6UeZ80Ah0zcQZTigb0ZTHdfC84uacloA4xPD2RCrRqDHO0MzO41ao/+7xBrNpobLAwpA
rakDnskCdtOTIKHO3kiXw/Ex0IW3h2aWCfpc3kBbPHWtuKvwO5l5IrrXDbcs8M1atU4kb1d+EOTy
JwFZMGy/qdX83qho7t+Sm/PIbDhXdNX0IFaZPCpXGUsq4jAEt3P7n4dTdJfWxXF5+P5oytI80g/M
AEU1ZM6ZjbwQPJpaAxvwGwd4g5oU4wN0wwykUE8JTxZAhYFLsaSjZbyXTDeKiP2U1Q9PKfwGMKb6
dugtbswyxseQ8HJZWvzIvKgpv31gkHPantaPMbHimya2AqIJAWUjDdJyw4+Svn1guw0xg5RFRXjM
eJoD20ZCHGnkEw7R5aueGKq04uWZSDju4ewkh9eTw8hAEOt9ED5DmKMwWe/wnG4OOozvz3CuHbwu
9a+r4qwTjGU7R9JprF7OBIKCD87Jd/qFHR1qCqng2pC6RNpTWW7QDtLR8R6I93CETc7n6qrLLzgz
z6bJaQrJnaj7qJBdFtAf9KEYXiFHMMhAo9v9Ly/GItOtSKV+GN7JKwLqY1qokWtZUZSjKjVsm5sS
mP/CmDmT2Jq+juK3AHfAABrqg2fe+DvUKvrREFDdGfZaW5Z2Ccm4ztbimOIHoMoTit5kN/Uf75L5
q//cwIzsKoxFci7vl2dva84MAXV/Na3LhjsdjFC1J3y82HsPWOEbOiWR+X0zuMJWL4ZJM70jbqWF
6QpyqLbaf8wM1IZ8JPhZg/hHT9kLzz93oVv7+MLE0tUCulyymbyU9t5EiX66CIXYH0U/Cawjyg9I
izjNR8ztY8BguLjtcIKsUeu8Gq28FTXxyvXpFAn0G0lPiiHmgXQfscitM8vkZSOVPg+bQT11TP5V
AiLOFqrJHOrpeOSRmQ8XpWBkUgxW+p4jzsYDU93FNuQWyKE7swC6/A0xZwO1OM7Yib1N7uYL95t6
zG9lOaCCXLNrtbMNy8wD3W6IkrJxrxdxyiQwOg07yLsbGdH6o/4cBUwSy+JXP7L+sIB7D+b7UjCM
aMf+QkA6VhEGARgfXC05E9pu0rUedWw4ymFSvyclxzPcLsK9SA7fAEkQoRsUjk5skVqvXJ8eoVTk
Q8F+9zaSg/d/Gn4btPusWGtKDKp5GenWxjUdaI+syUC/YVUIMp+YyAVRu1GmTTAfO5ljHeaXpkim
AjGXCnzKKqxKuEgbQ1/getZt08j5Z4j6qTAwThHE/Fj0ccYwdyWrx9CBZ1s0pR1G0dYkUP2wOeE2
4EmsuhKmuuCA433eD+gfx6qfxQ0Iq+Jx9ma+itFTsK3L5L9mOINq6V6wqKnMBkBPWSgh7mXqf1y4
02LNd5wgB3QuVFgbNCOX6GGx8lL23D2YvBSEYwaiv2A4XQbY4yoEOmE77623h0fF5HP36WQ1+GtP
p0vpUByuLydkpZlogZwGof+uSxSc77imJDmFIEVqZSqDrYHYaZClV9VvfIR1IX79UvmiZoJ8dy5h
1gXVLqFhgi9tshCut0LzwxgfivTy8tSpKs2KNsZPcY+xpR1LAuTCpyToMY3RfoYCKAn9xcq1MdiM
of+Jcyw/fH+Quk0TkJW+CyppaceJ5xgLv1Ocj47puPZHmHZG3PCkSem/XKnWIuum+UjPPGN+o44/
ko1BivfLbrnIpfBdsM2X3FFnJnHDsDBKlTBxA6sxxbOWMS0L62oPsQkPNDKvO2Mb9r+C6KW+WQ6X
pKQbsOrlVgpPKwSJ8d3bvl54vjbP+5yWrcj76h4WkgSkMrTJGAY+KpmYHd8YnKxJPZROv28xehas
cahWELxVMWtUc8MVPrrJk9wGAXDwclDl/CwfC9SxHegQ5zqGoBm2v8iN/6wjtaJyndjCdvVkn125
SQ41CLy70vAeTz7svmVxpN5kGN9I583tp/93fHMI8zVXr8tWP/QuVG4iBKa2jsf1fpGVHKAO2qxQ
hElwMwWhKRI80K1ZgB6ORWZwipcJmJ+TgzFsWDDhg8ajrqMIOX1h+DeZgVjpbZwWn6TrblwRznlv
nndqrbIvfwZFt36K561t53K3tD83gKkOmppNfmiR0zJrQtMdQyK9Now+qOr9f3bhWdZK/6wXBjIH
CEs/xJ8pfFI5Fdjnk11dzOVIZLQn/ZJ2l31allcF18RALwW/BaZZcn7JCfyZ2IZJnNvfhBrd5IaQ
eMq1YXrD1o5cOishuRJlf5ggxsI1jLBqTCrpfNovwX0ucVuCCeU+75vnTWpk26Qu9VGEa5Lbg3Ls
6BsTjexirpJ8KWB5vmbmmOIrMXWgsoavyNuY5LlH4r+12C9/k+6aZNyQg/YDBsJTCgtdpwdNcfDN
LklrvY69kcOoN/+n+kERNJZ+y6QNefZ+elaZboIVO73KK66Wmb/Ss1Hyfj/z0b/Dwfa0/HgpY8dj
RsrgzKndP3XL9EDD+IPvaQ12Tnc22CL/biI3QOieIrrzTA+qT7kdaMVqRUt34FX9fWj7gvz48i9Z
UM6pP4VhT6nfPE2XMHDDtxmV5lIfGXGEdS/fYbRgCrHXdw/3YG8xxHL92xYjFsYlaBPM7WZRwPQI
S8Y9Z15vDHUY08opRm3tbMCu0JN1YqhNk5eY0MHkfT6dyAmHLh1ub9GboD4gS6UjJieSz4zq3HQV
yhZE1Z/yTeKlih846H3AHm8VjOa5v9dKnG4cVspchmTZhYgotdmDR/a3FBxrb39oIOmmRgmc2God
KYFyCEo18z+JXQBAPEzZUTF9bWA+u1TEhp+h4/YqwaJXFay8domca97AC8mp+35SNkq8WNxiqNOl
yRDGomMIsTm7BIUxrgPpADkFtJdnHcdVeSSajoHULStWGvbTRgUwfD70Myl0GL4jljH1jn7aqdPk
jvE609mAPtsVGIIrBEo48tqUqt0iSRFFm6rWlWvf5WjNnjkqKxDQd0+ADrOHxzrbkhBieh/k41kD
M7Sp4xc8Zu0ivamzEtaMtHOnnGo/pINGRxvMjvrfhpr6qe/vo9OwIGpbp0r/S3MESQiV0ZYblADG
QB5GWtaFK8DqOO9JCwfbpn5hWSWTrO+Ex4Uhdt6kNEBf69sBYaUZI3FZ/QwGzjxYUHv88uBWB8tq
ZlX2GJSGgXrRMh9JeHlOeIhDlN8buCQNj1ZRzP/BfgJodSYVBrtUC3vceiIlonnwdLJKZkAvbabh
qBxnhKb+/8z6jB0Qup3wsrTTCTFVxGKvZQTLYrWfKIVn/0CH4P7A6Wxy7bPrZOM1iidrpJIjX4mF
gaBfErYuduV9UnEWL+Z8HlwbL0EkNdftizSPhIQcgzEd+mx2OtVT4TjNkaVH7jAcGNZOgI8/rwab
n5axHzukmNqJEhK6ceZt1ID1wXkrnuZODEFso90USNB8a5QVEPq5OqQed1ryYVMiJ62AQJ5ySLQb
R3cn86z602atxnDz7Y2vy1yhUbdvc+A0s4Vor1E597YSdY5/UjkgupYafnG+rIOBfVzE2HT8QFhB
qr/pV7b+zIZIfb2EdKSmOYYN/q3asctwaFcVGFAVegH751Zi4mf12sVszmaGyc6tnK7jIlgxOmAS
O23acfGGszEdxCf60eltBGHRwzxbvJRxxlT8krbavoHPzwk9wNhpkjBTjtFyw/HU8skhp5cqFBt6
z1vx1WzoUoOAg0mCEfZu2wVoef7rX4AVB1hMihgONTVEI/ZSr1GStQlTPxCU3BYLlJNzhdmC/Tcq
7TCLu36q0kh93h1BMTN/F4P2RNikxXc+DwO8gWtpcY6ZPwYAZhww6vIi3IbDbMR/z6fdPTZuIu5E
407lAT7R7rz5jKIkSLf8dUh9SdQlRCdmoxKSB9kjYIba9k/OWsueFnfEEYhRIs5LFFfwVGXRHKjd
y5yAdDIRiyH4eCWVuRv/pnj5Ogt8GJ8bt2oKZUT1qkDuBJVbw68mMEE7btzDf3iAAKyW6ygRakqR
FrTw8qRHguSqbJMoWsyfnVdfKfLbg4Qr9/j8j9XLfovqTfPpMg/7evjDir4zvZqEuZKquyOBre6+
bhzccoGi0Bs+vh/f9X3ci4HYWc26JlU7t1uQXYzrDLQV1fBZ0X2XZExGJl1FBnJxBmg/drKGlt1o
X85lfaZy9KhOW6UgUljdMHVKbljOXSXJbAWxGC+juBqXRxiQ7Agks6D7igu+wRRBhxEkQ5aKLTCA
FAoBigq0qb1+ms3GQ4tJnOSVITe2H0mYYcrOZ6fNffdl40qVSRoqXeeEXd7vnIrpFN/25fp6jSMD
aHMvj9tugbKRBJmf1yJmZ7O2JLNBSCZcytUesJrOHC+h4VJquTrqS0J+yn4r71N5QksV42MMDmYx
r9TMmNhb3WZtL1u7rfHyZjDbsvs6gUpMEURjtPgUKTeBcxRQgXrhV7zxWug+Gimjx5oUbNk9meRn
NISvW8UuRLSQNw0zYtgkSo5+TceVEF2GzkrQ++MoP+fKpfK9iu6GsXdZFY+B90BqpOZX+E+8NLf+
fGq/sFPuK/7scVO75gWzFN5pFi00CJa1Ihh51k/aw+/uBy19d6Wac6D5+naIiFUNVHO5PDdj2Obi
hHfs8dg/HBlqHsG2Gpd4ycLUMfqT/kuDRi4T4FNJTJb1OxvM54kxJ8GbcpYmHAYEf0sMBNfN9/cO
yxHVWDmNAddkvUAr0BI6UC/zQCggzxEjOfWgLtxHH+Jye15Q2JC3KJVlp9phNy31IqeTPInw3YAr
mbi9KSm4NfxnQijzBt0+etWk/JmJDRkVFDDKQOnLrjw+0RZGdcaMY9E0HTNq8liuFriMUbc9jd3I
gK0V4n/aLSNA8WZRPCZS/8rUmn9QHhOte2PVStzYKNLoHfThJYa51hrp1lndFcSwuCJ8XaNa9LQT
oBtsyn8MIfdUUfnu3fYGzhiUi5llxjqYIqA5V/0iNX1Q0KJoxYehLo5PpJAfApSaOovmMFqjNkTQ
i/QuQrK2FkDm7u/VxlNv6xwQdN3rdNXWtWtNysRO1sEZCGqycJA7TpBZVdZuoV5a00ZBGnaraTpq
Qj54/TpQXqUaJc5AlMj6YgcgrbtkBcGFCjBaHGE7y7csYwjL6AdMf69pE0M7RORy3R6RVM8Dy5nF
seDeUkS8YONNOa1x58X1ZIfN9okTgGvtEhfHOSsUFOc1wv1kSI/UBxT7E0uT7Zrqy7RCE1lonR8H
k2vnjQR558IYDtPMMiK0cSV4Hf8qUHHZw2nS0xkCeMf6Hh3vaXn5h99YwLuDMDGQsfzyGdG9KSB4
BMRfkpYZQSUIfi6pJKmLH0JIJLlTtcCFWVpJVA7kPC+QqkJ88WBWFukJaGWJtOu8iqx6Wyn/cf6C
Rlwy+c+dfFX0y3JhZYT6c1ZFYNDMbQIkaOX1963KwYpAMIo4hHNschipCO8ykNREoM2GgHjRBzgp
h9ZgWb8Lvr2u82TAVoE4Uc9mD4Iqm1XbZVLoj83Pboz2JR/oxKPXRMYsTR/3X2+bYW4pcIrqLN8h
gSo1mc2GqIfnfVc/qe7Y6bj4neBH/GMFbrAyi5fyEW+hGMCgA1VwwQ9H+UovMW1Wx8V6xHhk2pY8
GUPfj6YhJOVbf+BpBB6GRIlRhhsUICBdCpQjxGUInEABTQ1hKx2ECro66A/KjStKWOMjA6A0VD8A
+hxcAMSC9//9Toom0SiQyNCiyYZ14SCI17z/to1HE5MhQH/va+MGBhBBzO1yP+V52XkIPpnWqjG+
XVBTZOUHPe1q3JOTf7NYftyu1UzDXoiu/4WGHrbbIDJROB1PwIirf2jkI1IqEQfvlVhZDHvoZfol
YBn/djd4YwQ6nsBAYyBiF23NrHmWzcmn1ajufv99l4Hvdyanh6vw2qM8kIMdGjZBDRTSc3ul/bXm
12WvsG01n9efuyGgt9+5Gvfo9GnrTUxXbo23EsUakszdGODY9cdFZ/I8MPVCEYNSPL6nh7AvEyyi
axLrcdjHzCv2gI8P8asgUve/pXUgtQH+1XeYVhnhLgAmpd3BTrYyxWxwMElK3E0Jl69i8ikK6Jj3
oKlX0BJpxx568xNQt/vIqw2QVzMcY+iP6qvZdlS1if9iHwwGob4XalU4HcybFWRNjLgFGwkOxxuf
Lwr4wBr98hxXd+txnrIyMBoX8Z2sqQ/WFJQlOkC2iwsq27bR8BKCWvFZPYOgYyj2Hvddh6ohTScy
1oJo1fpQ4f7wNp8k1OeXQnLKe147simyuwfGk7eZM6WVum9gMBSa7NBXCfOAH//3wNb15eBSW+lD
AqSGzZeMUskemWFoRb5CnEtQcRm4Q7dbbaA6Lr03RRRu8Bh1FKXng2y7hLC7yqRSXtcT5DvSeOCo
7IVQAFyfz+/HlJ9U4PXTH63bmndjwKZctihJTkVGt7fZb3TiTbXonU4y0JVhjMIocEo1Txb4lKK7
ST+ZY/KSHzP377Y1kx3h+qeFRF/fg/hzHmbqU/UtHnul97ijzgqZN2N0xfF6VcW0o/LEA81HWl8K
XD/ois5RMbg4IWiYqEABuQXo/8Pmf2kEsyROKJCuLHZ2Nut75fTOTf0E/rcayRZy9WVOY4PTA6Gl
pqN6Ezm3/PZUzABgLkgVXmGj+mjUAQ+kGqmuyKQn9CJwjzzvygbjqwOdCXZwQjhaVc5TkAU3aC4d
9MVDQEHuiG1F4TVKcg44e9R2oPlMqFd5RQ5RDIDPEnW8M58e2XTJGzz+L7tsXhsau70pEtbqn0Cs
oLkyCxD612En2C1Po5E5HfouwR890DgBhZyiMH/3djtt6TPvwqA470Z+No/4cvW2VmMzNMd1jzgV
EuoADMTn+b9NlhOqZNEuRLERNUTjzA14RmBkIT7xMqo3ZoTfmR5/OZ2Rmw0EZk0eU/v9uD/4KNuq
aBDW5GUPObPvSfIE4Uxk5oe4zBsLNf/Wm83nOb8TCFq/STKfwkuJZAKUXjcVvhRm1SXQMtBEOEJg
qBAkCzVxkE320TeXhAZmgoKbk4zndRkpOaPbBQbkk8RtttkqadYcBX75+zF3WS4RPynT1D/uc/c+
s+Ei4iPswW9KSfGGOR0qu1G+ZN3XNyhbin9XmmlYSdAhqLAHuxvuFJtDchUxuKAAgZUb2QKCcLeK
ZDavEibO0lj8dqvdJ1LEy8OmfqR+24YNWNkV4nLUDSBoRi1YjvPWL5A+C4SerupUKhRphDmvN7Mg
zNsf7hBaOB1SxLrITkzYQK4gHLwGGNXdmZ/lY7+ThCYl41MtlIaKcpo/ojcSqAxlv7kH/bmB3v/l
ywnFodoLYQKNThfOS2GKQVsJtVouQcHp1HgFlvbZcwdZ4MwHK1FGzzHWkDr74X78RA9AcRMqUBv7
9ZobLtlAZtqyYHwA7DrUhVZrAvHxVWiI/vOj3DNTM/WX9O2oHLEpAUib9iWVINXe5oIpfh0lqtgO
lWtDZ4wsUAVCm5wQ6F57LuPu85mSWvpX8d15738a3VasJnkj7yW4HeK/svXlbhF6lc/Rh/uWRwIp
inSsJTbddPHd01DVWpPJWesxb8ZEodPIoG4oRtJFeZoynGSMTPF3bzdPLl80mnMsn6L9EoQGcU0f
xQ/LKLvxaZXmRbjNqsfY6RpNxSdgfovimQCKsc+GG/6tIbr18K73j+uktmJ1AQYGW7sfB8LiRHIa
SW5WNU0cTayXgOcr1x0Zfc7zBYTBZA2ZjgtUoBZGd7c07UE4f32RWDTF70Uvu4yQ3Cl/sUdoUJ6m
uJptT8/d4Q6u/FDRWuQN/DgcETFrbDBtiiY/rChBCmuETzNt1O3rQBi8O/BTwvlEMG5T6HOCJYm2
6EYq8W9PQGB4hWAt7iKNpUS2ESS6Z/jvZjvBij0MLEMDi74xkoqgEkabm6RLLnVQulOr9Eeffmxc
RjqWc9rUalhTYDr2gRIuSPmN/4h5sgyd/lPTQXuJ87IATfNnMiQvvsT8iPo5FulxNy7GvpYFl9ff
akAv6rgAu3aIm/fgZAesypWmSp3IkC4SXYebbI/ae2i5098DSuCo31IOc3Twv1f72djbYDYV9rQR
F8XTnBqGdjvZTkU+tzB+EcSh2viBl6qibUX8re7CrPui0A22jcqOaln3FkL29VRplC8CxDae2IeY
Y+Z3zTtSiTGhJiPmkiG8dJ28TXgalNVfal/X85mHjaNv4LRK5GSVs9OC4fGkP+HvNbRBcT2p0hNC
Vf4+0NyyNxFdc2ky2PPkwPaYPD2d3fWLvCe00qr0ngYskSYf3fa2Vhbj+yPiZFVUxhaim7P+W1Dr
C4Hd2ci09FKm7cgu6hq1xQOCVKG33HZJvLAER5Zifuu6hXuWppgejm1G2IwJGXueo0SdjXjV5jgP
WUKq3u0UviC+NqG8FlRvnkYyhc8ptYCT/IW/kxYUDRTRGXT8Rhpi3KyRnBmWiY6GPezbf/MEnK6e
l8cK23OGycg+V/xweMom2cqQe1kyvVh6z3byPYEcxVHlvBDXCWuvqewIeCtjesuayQLDfau3194o
zwVdYsqOldogN1J+exQC0SONhHHbL/ftHeHnRzlY6qG0BUkQrNlt/NayH/DVEQObn489hOfYgHug
myr0nXFiJuJkKcOQGMzZsynI9Vn1AIgfly+NEBlq4o0XFoYEL4btinA9wnS9YzwHdzKFoEcdsZUG
13b3zAfmcp1T2BNtyI0w2+qeswl9yxJPW0Vk49QPI9VAaDUaWGJ5E8HYVqmtDMf7RRX8txo3Wze6
MrgrpdJ1sJUmepdb+ojRZHb11Y7uIbLvS1oDjymD0lj1jlhh+aerMObmV7Ukq1iCDpIE0Zk0qu9D
ljU1ibpiyRQTQhLOYvIuJ/6v3wtMe7KhZeXPYETeHCK2Yry5kNYklrLHDvk2BjrpAHIc1uHw2MmJ
AFotU6VtTADxXKTT6Bzeo3B3cDhM/jk/W3MOuOWuOVLIMnEnGdZhvSZB6IdGIiF36ue/68gnQjTn
P52RNQKcPjsgaiJK8LJtPfJJ3Srjg9aVUleqkZfM1R8c9m1TNPzprjRStqJCSSGfmYXuyFwgyp3k
4jpcyWnZTLPLmCaTKBwtTqS1qQBrftkKil8ILn3q2gCkzDsttpBddDGT0b7MDIUUz5q1DAzTjQDG
+xZ876kWeNdFsCaxZMPqFwe6sZL97BFNNtMXLMPnp9e+YLo8KT9rbYfxLrsLstY/CSxj0xX+Aa9T
nyou6pQGm30YUnPgyaaOghcH2tbYyVJ9U2U9yryx8OfIM9NmjGwb37Ah2jvgv77iV/Y6LxwNGVCK
G+9aNzdEyJrvF+v2diD4g0ajey1E9/FGWSde1m3XZSRrRnwmFh2Yh3qH4RjtwwW+x1oFQrwtKag3
CXdoj2VVnbSYc5tmux70pkfV8N9pbkVpnHFKpyNhKbalLXdQMzkzovFYANAJ/4JkA2FAQTqGxv1w
ykdXQcxRDrzB6NTIp71LKTy+r09WXcxWx0uQfYXg8zQlP0dYl/EE9poUXOzO/8dHGnC0R/y5mAGB
3mC5jEjne0Kf51oTlCtZTijzMUrfRTcY6TC/Xy2ZwXyhsT+Cm6Zt7OMaq59ZDcIQBgbE9Lc/OQ6t
i2zIXyLtKcXaTBXX36rjoiKKaaXxLJNrwsHxDVWD0eJGfqLqlT0cOEZ9o5nt5qVhjrdvLtFCJ0Uz
WTVZz+qCLh/PTOgoupJbr1C35TEiANzYN8k67xzSVFV6CK8UAD8NEx2OHj7Zkwde7cWheOKjwYFw
KNDHOy4fNap6baV4QNeLqeKP+5VrBTEJAJbmW/pcLcztYuHCXeszFH1q1eKePt/ItSwE0UilNdG3
GDhvpXrhyAM/YeAoWGdrBeLwm5knB6anqbTe0TN/vuLXWMRy+V1SHzvzTBnG6YXlr/GI+fN4pRtq
wD9gZvC8CteGMKwvhIDfBhT5sprd9mGkSNcCn8/7UweM3muLaaJYNiV3fn4XxJkxjNacz+duY8j4
O2nCLVokbSeob/dkpRiWc8OKRft10RysHwIbAX3lP8MkRbpGzfkrLycJWpO/irjSpM+KGTdThMn7
Rej37hS063R5dR/cTG+GSH3A+Rqqo9dc4un+x68TgN9/VaaLMq9HwQGLjrIOnikYdf8yb8xoKkLu
lio3N+siwkOAjzifJSoOUWJcUz1f6VSLyvmTJ3HWy3B+bb2fhQA9K5XQ/5jhmcy7WITacSQIHAkt
gm41cG10D40eyalMVjtaDHQ7FuyitvXndONd/3L8nofuPRq+dYkgbWpJwwf+iGMl7bxGVgEU4OcA
x6xz1VgyhEOaerpruQQsiu8voRa0vUcx5QHLvnEZZnW95fZkJ+s8gnfa7iwXqdfdS7lKtC+TD2P0
yhrClgQslTRFwBSmYCCiYgUPpaKIUwE2y3Se9Ns3/8qIII9f+mEc+yHDN62By5LO43wxzf8UOnvm
lvXJ+SoWolR850w2AGeprQTlwEPNQqdNSDi7t0WpuCCG2NyeEjgT5VjOp1f8xxGR5L2dAh7sDod4
VvXwVGzDmf3uBz5e1C/baX5VMcbSA5UcC2WA2EFlb6CEeYv/2grjzr7HnURbIUBjVY8pa+zzPueV
sXonmHhUiqxFGeNqAcrlJTafQvIH9YksdqtM0hIkWz0Iw0rL/WhyY5pVztmv7zOAvBSWiN8PrXpJ
D0ygapaW6ki5X6ZQvO+s1BaUinF8rna19c2kV15ocsQjvERP862a0ZXG7exGuizLIIeU7F+MvgIT
Qmf79IyxjcZY4UZ+O6410aIeLXzOgRC9NU4+EMXCc21jSJHgsWnfnaHRPnRGAORBbSnMQfXefn6N
hONa/AxwW6AYeRw8GVZc49jlOoGkurIdcEeG1121Wb0JGg0GtBhNfVApr2AVlgRKSrcD0HRcry0d
8b5D6BPQigKBA/M8RwWj0s0NWMcfrBs0zg/GZcdfMzHBScwwsgxqM6LjXiLPfvpQr731VZZv8ABp
GfthL8BqpYfzYMc/FnnxXBcnj/LupPNkRWPTehg/icvXLK3lb4w5KzslgJ9Ot98T/zFfbG9cD+ki
gKfLbfcC20uH+kf2JAJXsAPZ0FrMW04V/rYXeqeJH0L063tisb/+AU9PY/menB84cE192fxP52pN
QkehbwnBPBmJA4AuGIEU0FjvrMZkm4cS6PI9PFDgfRlO35Itq4MceKQumr1ss1WE6hveGzF63ZVV
Cm7I1Bmf0hiQzT7ZAhjLbnQBlxdg3mp7ACNB6IcrY9Q7L1eOihDdVpr/ReHBTuF0zWhvYQTSC+fg
FAmK+3aw5ehY9E1i+/N6o5UoUMZ7HXyRpZ0i79yvtVht/YRGsQp+KFZE3dHy/iFiVGNWTNHayxS3
uNUsK1xX4JkfYyRBPJn+VkUa1L9gfDD3OIRTj3QhnpWsdCi4bCQ8JW4fcnC/6vdwZfBC6TFFy6cE
4DAwx+SppSzdq5kbx0eia5Z4ay6+/o5E3P1s1RUIYRzhBHWTls2rX45ZZezwzU15eACNEb669H7Q
+ccwu9bt0fDa+13PBTX1+t4DHqoN6YCTWRE2Ha3UThc0MobctZ/FrChERE9Ao/5WSCm0FcxI/CQI
g6d9RPjsd27cLflbpR6/++Jm5FGqqAxCHU9R06PChE8noO8J4H3BmNldDmNvrP+8HQj1mJ8Vgtku
hGUyfuUgniZbQ+A//hjOeh3u7uvZHR9Y6kY+tyb2zk38UDY3+WRxWbJb+pDK9nyRJTt7nsNSFtiZ
PRvT3zCWZNBGi4fPa4gp/Xg5WtHv93bvNg1t0h0pYLCNMzt69IIEsQhN75frtNhKMEbEITNUy9Yu
5uZjn6P1nFE+2FTmQgE4GodQLGeTzRvQqnxUGs9khbKHsODz2kGVHZgdDlNDo/LdAww1T8pJZ30t
Mi7PRwcS2mw8nFIiAoLRpEsJYPWvnP8I4MzFucSFbKafI1hY4Z60DvN6FbvUw5urIR811k1D2EaP
p9VJ6jYn63neqEV772It35ShxIH2uBH+/1kPYd74bdb1yon2lwZ0LK/DDokPV9izqGc12WXZ24jh
MZ7Re6dZcS8lZ3X/yUbGhS1FCcuy9N7xvzndgqgoXnAIvbku3kMrTsa8c90so+/eTLjtqfTcXuOv
mw7pvYD0wH8Im9XVQkEz+AiHYozETwYUUH0GV/IM51Lw9nMcncDJeyoI7yao7nL6b/uN1/ureJCF
Pu+3GQoztq8h7lBfhwCGOv5jvoLoeuKyRTQD4zd0qI15FwdSZqLMVHvrLFUmJtemnEDADF11hXeE
+OduqeS31UvVhNsDLPlq+sKTp63dvAPHA9bqM6XcCKN5d0sXQLZnF/BF7UNTNN2bllOe1egUlIX4
wwA1sVcN+tdJ65YLCAZ6J7wVO9QARZss6RMg0j/Lr2WoqSSB3asv9LLlsNcQrBgDX0ol+ry1WUfZ
2VjwcDn1gaVca3ykX0DjnKRjNRN4cfV00LqaG+bQ/FSZCZfKg6Rxz+zRSAc4YPJK6b9OdpyI9IVe
CA6CGjtDDvRJrxajx82S/ZPvOmRgCFOvq4zo17/Rd6bOloDZwxa3BdayrIHaUMXssy0LYtTqQu1j
GWyUMyNYu4BrdoCt0sGvtna3NE9nqX5yZyE0ioCxcvgbhFZzkt8N8jV16YWBq4s7NSMVa1RoKqbY
podlC2ZYtqfeFC80GKA2rP4m3uHO6WdxLkE8LAEmMzHRViI/6BeaOAdRPt07u3Qssr3qFcSaHiJP
2mkPZFWOuRcO/wyFIP9Y3c2HK8F5b2tC8mYTgDS0vQ3A3Qt+9KF1PM/K1D8kFycpQT3yXgkCXhTD
ysAma9jIliLPSOgZXy1KVtgJ0QjqgxpnvSIfDDRiQLxbwLF1mn1tc3OGjpVXMq8jWOzfNf2TPgxI
jv+Prw7Zcag7M9dw/w6jqje0wS6RFOLi4ptVedY3Oibg7U02N6Q7PBd0QPjPiRvHgql08+gwQGDM
tTJJsBaSF49rXU9HfmD+9D2PFcl+66pLTCa4KKWeJ5F3TEZkW/76EzG/hNHNXU3DPi73GY6gzhMx
39JfsGgjie1WgLoNfJy7HPrh7emPHYuXt8UG0/uXyoTjNCHU7gNvfkaoaX+799v2R3tbWVtIkdk3
Syyabi1lzpJdOrrqtUsfUOnKVuzudHP7av8FHLJjZr7yIxek/r5ried23vQQvPAJgDKTe9t8FJ1i
4PIo5aUx+uZV6aY09IW+cpWqraujHUo5bFQy0y9/nc92Acq+2mrLyDi5zEyglhnHiPi3uD3a6oSN
MWrhWexeyGWTmLXEek1xlamr+01qzjYayzu6gyTnWONU4zwCUrDmasV4i0m5r82MOUM+8Fvr3LFJ
o2xya9G6PsISyShJCM37c8ec/l9RNhyfAZYHrzkTyw/CTp2aBH9Rm9HSfLL46mpA8xnw7xMkTGJN
FWer4uDFodvjWQxA4CyuaBB9+OpyRohmqmtBK01WOLj3OlkpTA3fOlMZ5btCq6tHsyclPl3NJplj
EE0wnI+ul1QUrpxLmaDL/wOByzqyitn7OhYdRBuG4Ry3Alz3Y8dV+BgDZvXY/Oxc7obBZmZ3nPmG
GO/Cd0YUc2Vqej3YR3RPzDoZTcq4CzKALEkNh0s6rEcfbl9lwxm+I/MKnZxOVuMg/U3wu717Un5B
nGG0ZXWhcuxfhnA8Ct/nrnGNdKzGoqQjrLvdY9mXDsTT0YFpxP+xzSMAQc+k/xD5mx/S7j+xL99u
jvDy5bnD0EmoL0S2iElHSgQhq7gsjDWAxCop/DIsILrICbfZ8KjXNBWECMfkoJZT0cQy09Np6mD8
ehuvdFhHrbm1GvPKIXt8uYxV1oPub1yH5KUcqhgdJRkgQq3Kk2VqeL3lcjtj1C5wKTRX+ZLhe5xE
Od5DXy50N7qrX2MrmbEbXnaR2XPwHc8CPFa9K3ugOHkkjVxoRDrxdmJ86ML4gHA01B42D/xGtaPw
rLqceos9cI5Bhe9mKkhWfCEvGVaPeXUFLQoh2/x12LKWaJLpRwlg/JO9/zgFxjr3oWg1pjaSIkMq
DllUvsnPjJ4Re253zMfkOtVtCVJUaC07JF8pywtWzrm59Vgq5lqQtTiCxwgTqnFR1JCdpoXbIazg
HJ+/6JX1iveAPEYv7g0h7oggz+hP7L5aDT85Eja0eRIlVfKY5tDj2dIwat9CcZCJD6yXFuBaZdo1
eNkH+vvvnWWARNkM/HQsPI95Y9R+Ae4m7BqpyezCOF9XF0YO4HDkEg7RvU6Qj7V7wEsu47nBuOgs
g9j/MYlrcp9spnWkyKRcdUjG2qngd7gytV1MHxvPaLgQkxaU/hVbFvHbGRsplEV0g5/U+7dXD5/4
BP1jIiBNRra85JCDyCjufbfbI5dfP+TKdPhgvwocVOfSQK+Mmara3e6myHmFBc0lhIY0J4kzsPvB
dErmRfq+VyyXIRTFERMN0aJ8hJVobakTdAZC93NTdaYxQ6dlQNl82e/A6vS4kntpDQPNBIIt6QVD
ggC8wc/mwwyR0WOiE86qUqHLl6hjAZEMZy6v/V3a9GbSPaNECF7U0Ya/CIw6IDbH3flLdvE4qYH5
c8KCv4fcYmUtvWMqaDxHwT9RIq5YRofLYc1bls9z3H7Yo/SdoiS7AAUowdHz9/pwkbQq0pOpW6o6
QWY3uiG7Hbcp1xBKtJE5Vf2m4rz7B8+x5jdW/g4zFf5EcwX/9v+Gi0fsqvKaKf7WKu4dLFwoaL95
SFkaAccWUS8KBVN9yrBVDS/Fl0zw7bBjbJ9/LJ2o9FiXNuqB//9CWMrm4kfLV2qJEhfUK47DKN7F
qse20dq8IzN8CDqOlHPqNpDvwIfdZexUe5bpNQYotIAuC7JbndqIbhNyvd511svn1SmXQYZ3ZUh1
ASC+F2KAGddf96Bm9/75Qb5GVHvFBgbGXFhf7H689orOLBoJcUwK0+NLQHYA41tJexUV26yiw7xS
3yAJoRMTGetnKvnfrWxVmA6RzC3NUNcaq2RrHwva20cqPpIz0I3ZHoNLhrUmEGulP+y68R3DVFMB
TobP+8HMnvS1JjBfrpPabq6OMCzjXezRx07rFe++Znmdrl9w8ZZhp0LzepsxcxXYjByJHguM/mVR
p5RFTrUH7BZnIMkB8eRb9QBzJom1kP0hl6DJp5XArTNdabnNVpC69QLXNFACV1kNSwNm9aDchGCg
SMqWYMd+ZizWZ4315UtiRSFwgJeIx94qqtJYK0qFpB1/JqLlCLeoPQVgn1b39BFpigLPedMH65/B
IvZdnIzKw8jgDv25NsM5/NdUcyIygirUd68oFGD+AQ11br2p/8JuluqvjtJGRyTStsMHYRCKIBGD
cvPzizKSszZu+UMykcuPZyF8RvS/43OKcjr+0SC5rcxTKObvZrIRIuiD7JA8ZhloQ+ZXw3UicGtg
VN+dA23iCrlTOgfnzoYkR4jDUUO61yOp+w0g4LNW9wTo+JQaarOIcdoeGADfqZL6+IWVicaHKmzI
kdBTi8jJp3i2AgZzFjqjdy/amAaQOCOI3EjT0dVaqwSG8EwNvfEw9dARJ8LcBbtkCWerbt3NHE+D
tGS15cIqrux0omF6PSj1K7Xil4D8YgEvrYtNnHyfmas1OsvvfJeCDcOnH+EDlQAXCiT5Tj6D0dCt
Urp/yKVHMHyF6UB3XZM2dkn1E/yuH1wEmZ2IMcst5S+N82c3Ss0j3bGEGSmskDHdbqFAGi5GDsKz
UdplPUeBIgdzkrmN99ApMISpj+3VmBOs6dQCRcWu2ILGoHMTszKMXt4l70vSMYh865QElWhwVMOY
CdFvlaMnqXcqKtbUV1taaiHb0TuBpj15Yp9vp/s58/bhC+rlWmnC15ndOh3jwvueQjjUvzfc3ZPP
DBlyKD0BT1+CdxYVbgQodcQe3eAomfaJ8T8QFXQ/BRjfVxiOAM+DOixuQ35pHsW1PSa0Tq87lPqM
pblgtEcTUohrAVJZexALyI/ntH/Ov4df8w3hFuW79gx2qLcgjzLgUEtIVQAIZ98PDN4+IwJc2AJq
d+iKVeBu12SrRkniIXSLCA8qhXChoxfRlq7ZIwzy/mBwgot69V/S5EUv+leAI9rXjLNIQoJIKEqu
dqaJVxrzmKG8FvGPG69l14oP+M6uuqt0GAPNL+6Ka2pfk4CSTUmoddGvib7MITW5Hsw2kKYa4UTo
PeR7bE79n7WgVlcDMvw/SuAqyrO7WWBJ/zYBd3C0mAbnh/3Zw7ao62pubVC10D5b8uGth9Xvc3Mr
4zHB3fw8PXbfzgE09UbjrzjUlDikRbBZF1ZBvozoSxzcF13FE0flvEjgqoxcAR7xNRA2G9tIQMqE
Jk6M0vlc4e7EMwtYUasaj8qFRyD6QcHWC+nudl+R5e/o1oY7/ryRBieunRulAiH3r/q5iYr2wqnC
NibfSvEZOu+N+rFIL+vm7oba/35cSeOHJtfUB0jlENmm8nTESqTVqxpREAbEAj8B1FskRf9xCzfo
OM5s3AD++Cw4J9FTNjaxTGTn63j2kqL7hGBZkMY7nzmPlHaLD8/h2rizQhnKZqoBALGVoBQPr2Yg
GWH9Mbd/MgvMXJQx2LkBwOkfc78u6w7SJeZzMUNkOsRzzAQYtFMbYjB3yuijsqg9MQrdRP1B2Ip+
pKcYa1mzuX6V62l/nWmGEyHutCTE9VO2WPAvlJn0c2Cy+BWxPzkyCimxDezj3tvC4MSPlbXgjf+2
sUB4NcYrH6l1fSEYd0G2W4lSSLNJiJ4eGvkjR3YRa2qOVjEA+DkHoLMGRWnummF19DLgEhVinoFZ
YVZ9rmJwDaMhT2Kn7lH7JLCznyRmJItKpfSH1aVgfcpMQVCMO3xsBQUJZyNhbd0VJ+9UjHObFspC
nQp+W76bNdjPaWCO1mb/RjHn6GXpcqAsRlCclvxWy5jGiO5IKSRBmDeXWVWqHjIVlUad4oAk7wdQ
rfhFM2095NQOr+h6xyHQjsN+WEgRRQHLT34RHMFsfQXfWy8Bsv1J4QhdY+GiSWjtLwI21H3JeGbe
K86aekQtWVeqZakqmUN9gRFBA6L7JSJy7QCfEGhdp4I/N+VFMmtDqAXoHJNYCLBhIGa7YXoEN9jM
8SilesclBk1DWpWpEGfnuPR18LZUX6PIDTmTxJoR0jkSQVogfqajkyPz2kfOdmpV5nNziMJO+8Q7
8339z58g1rWicFhySxlfHXaobWFma9vvm1cONFNk+0GtnN4Lne6BwcB4X0ut3FCTYxA0MRtCXZVW
DH2VO4lgXh04OUb9nmbU5sRQI5Y21CCqt+zO8ju5eokVIHCr4TjX4If55df0ihq5MUrIGYdnvLPt
VGM8A29naWo3lLOfgnMt/2V61Q7G00ZsdrS9OQTT3mf2XvT06m0Xp00g0hrRVfs3oI2UYw949AR8
pweoRep2XqDizUM4QMD/gd+e11D4Uef+UUFjKBTIHI+JqX4jGHTb1ys6mtiWplacPmDnHIUrFQut
2pBbhzAFlP08m730Gkp2sTfxKJN0Wu6m2f+HsMii+yonGHwHBezaRxTJXuconLHZw4B8k8JTUBuk
ic0qyJ6mG9+s2FttrTVLCH0gLmuvaWvtzD/LyWCZ3UYmAkYS9dqvXvBxpFjDKsa7d6fhdQrVqcWk
Mja0eF83cNGWi5GHCA5TiwXGuArkfsaxmRohsHD+5h5n/KvhcemYnSvxMSYPqGfw3tPkeJ/BybnV
kVspzcxVJJSZEfn+MLN3wEwz/FcFCeIJ76NKBxoUBNZ7b1sDgOHQMd8z/XBQYuYGKZUy0Q0vHOfE
Gx6f0wVFAwvP5pFMxe90QKVRp+Y8tu6rYqexxfDEq5Noon62GodNhlW3tJkh8+2o75TZ9Nc/InkW
LpkJhKK3wxqHkYFdJyV4IdpNLFSvr/byZ8S7vcAIQPoe5o0L1CvRojEjn8oe9BdfyVadvDTTq931
b24UDPXFcCnxFVuHuJHRe8LPKk602iraH8lYUFYjkL2YxiwsylF6W/5rvZ+nOFrpVmnlj8BBuhmJ
urHcLOUe3P7zPWj0HjlhhnzWnnApAtgOdXlQWRoUaL+Bua9AT0ZE6CqRxqob43AXy6edyVE56vAF
w71ykisVfFPOeQzqxh7mZxdAw2dAIhclTzVI7WPu8PbLmoFyFFm05PP73Wz8sJRJ0hP+4sJ/KrKT
wRUjQOv+YJ0luLhonWlP0NG+rlJbh1dZiRoCJMBzY44/Q/judWjBzvjM2JXGvOi47QA4w9kfcYYV
9Ak+rgw2OKpafQ0JLysL01QLAen7JuaOn2Cu2A3eW+6r/35X0GQK7QFyuKMe3PANm8RJ3rFmWK7D
6FL+kgan7IPFh+RywiNi/rWAUkztDpH0JAb8u1HbZ1z9M8wzO7LKLfgBESSpiIE8JHXBVA6PXGtw
92yCGsDulC7rL9tWtcjVGi7k6wZd36RDk8dak/WvHSV7PKzgj9UwVfKS0IO+e/i8TfWXoQQX+hKn
3CL+mlAFw8ox4Lu9Ml8vpXRIuvSP8A+DucXPlBX6rc+xZ1jeKJpVjg4SUhOm/IUwOUFnb1uv5oq9
1cFE70n9BaWCteqrcqNAVLKuts/14hV6WTyzbVuFaCcNYqSI8fbHgXt6ez8E4rUkLV+1B8p51P20
dEvYE7KfpiRd6JqPJbI0S2bIwa64Y+2OpUN81diErWtJYki9nmva78qB3x2dh2V53TfmtDAaI6wV
osbtHYbA3sqxOgsqeTqTLQdwMT15FtpHkUqZ0ndBevWSQ7v810WCMQUFJoIKy6/vpsDCAwaP/G2K
CUJG0Shmu6DSRiKSvLCyCdwiSSYQN+3mh9fLjs5KnTsQJz8RUcEaz7cHrsfw/g8Oj0wmmLtEU+Tl
rOaUupsosUjIrbFSVZ9R6+/SnP74ERIxkxGSaLbbgU+iau0OjmhFZPVo7QN2srTOmgrz7TjI49xo
128oBXygNru3xbmUPu2oOA1/m2eApB5mrOyHRGjrzrmOMSx9VoI3F+6voeItp0J171l5KFpqfhEE
F06wtObwPUDEj5gTX/dQ1D060aD7LUdksbW+m6ekZSu3vi4B4BaVmZSArNN+BMNA5+O/Kmj5E034
ehL447QjAJpOmPtL9lXM34mlNFkob1KlNgb59jbMGSyn8DMIsum3nQBXLYi+PayvsIhkwd2WDRWr
am+Xxq5ofAsfrrNabRFX02NNR6SncmHpPTUPjGHnRBOXztwD+aMVJF9DrOiL8KOhMjoJY3EgQe0n
pBQJbWOwt37HsOsSdIsPJK8Zhq2FMQVKhOl4QyOcfRvbexl5ClmB1x7Nl8TZc2ODd6T89zrqbhaF
m4COjo1Ew9U2eU+mxMIipTXaaO2oXYxheV+yMqMg1sc9u4bS0mwqgdBsJSlcnbLmWRvjZNd9Thb1
j57ys/wdIQPB3enaU4T3zT7U7BJcA5n1TmuWOIi14D9OYTFWKQYXfuscCvsAkJRncXIg6FIqrF02
72IZAaRch2Cy1nhzTTAxCFJOqfe84+YdXAb+Wurhrd1p8FY2mu/atFQBi8eAzBty2ElFVgmgRqhz
Ckla2ameYwL3haBGIx2vTQBRVh3bYMdJb1G32Kk2U2VOITHIKmD4E9ny9KCFG+DNHL4hSAluDI+B
NbozW95BWWG6mTm3SFnKXvvHNsnIhnqdc2cdWCiP4bLXQmvdpdF4F28Rsc/Maw58+Akl0HHathcZ
iwXEilusWFrJekAAScjR//o/8mVvSqRIhgaNUkk33Qg6Y+Ft2xGqsiou0N9nAgjb1CfKa+rRRvRC
lNr5PkCVJOkNLHRyKNeScoURRb90fAS6JaWTSx3uJh+KQq+sCAmNXIqnkeYUa2nc5rcJvvnajVXB
qHGrO9zETxVrqii70lVVcuH3j2CLTYWZXNibz/ylhR9i7WFJRczubWkqoyNYMDfc+7uOqxNajN5n
4Yju8RfIoevp3ZqnPPz8nkrgp1/Tb3QZIsGiFdj8OmcqD0F/JEWodHB61ipbuZRXMb4WDlAkUfDl
WtiBCjwYf6YbSgf1VOtA6ByckdXdajWLW0ojgpHG4P/TPe4tLfFfwuFVT3H3kwscN8zDAFajN5Bt
zLnkHvvYRM3GaReDS6hq+ejip8rZq6tJ+HxhLOnYP9VL1w7y9AlgfaRS7n1s+rMcVJQw5Q0gWOiH
vwjqN8fihYFZT4EXMEvN0GCbjI0JyPYhGioMV5r9G15WPbUOuwpSRGabLNf1nWV9G4zs57w9EIYU
dfJzHP6P4s/HL0h4jWCo4kp44aXAefG+X2K5Al3JKD8e45e12Mx+vRoqrofgB0gkEgDbyNSyiIY1
aMMC75jVpIUssjX0w9LgeuvT0kcbH97DytZmyN+kWeDZzUVexCPkZQNkWNHtFqyTyDhn5kRCKodS
gGU6eGk3Lrf4DgSNs9OWrnqLP4LN8hNj/wJO1hNJbWEZmG1ZxwALX2JmZRQPQvSPw8QZm5i9AjEb
akPVRDBXYOss9slzgl82bIYisZ2DcfafvewjXu8d4Y7rjK/RqFZJ3requgeWh4ait7r6kHZq3yb1
CSmXGysyo1AZULxGZoYz/FiZQQhF50KpuJLnHCxrB9Dy/haktBcLS3I3VYPxaibzeNhPFK1FezYe
Ma/KuE4rcWW2AqVR29yGfoEojNA3hxGsL6Pxbl63M6UNLEECGrzGoRady5r6cuZ3OyP56cs2qiL3
TaxfbjD/PeCLH11FcVi/+M0VoXPAkHf4k6T/vYOUu+HkEyIHp8ySGMPl22x+qF9Z+wAc8xYllXTb
dK/RcVb9gzWua0GXBM+tnJRD2DhKeyAOadfLB0uWtsgGlHW1+Kc7tEfMvcG72bOJtzz/i4dqoLiM
WpOn+uhMR1N0RaCU3/kqo/db4nueUItr0cpheotEgWLTH6OFYfCk7ZyRfjAdk+ykLxaqKkcmSxBG
YAugDh1a6kxK+5v9ClJKghWRS1D3dj1yMvw1ZJ//Vtp+oM785GGfTA4ech6h2NFVAtXomJoZQNca
ylpOYSgk9DaBzxqBzpCcuB6WBSRqUWcY+trlNy7JyKpT2kXUcakOFJmgF7/WxTyy+FjCJZF7pvdZ
rBq9NT+R7IoNJIcIQzdg+9gcawFyigPza1bOqhXAD2PQAY2gHJXdIGxEekmuiUn72RV1q/0Gc9pt
l6kwhMXQ28gn62TMTf9fyJCn8D2h1CIQ7Vt5PPwZEenbxP8PAne4uTtaxl5SXIPriCbDVONp/ISg
0vxAV0cp8PpdKyPCN2LyNxrh/sSg5Usrdf2ULAJw8CTZanRgRmZ9edB69as9iNA+3qKjOpHsI2C8
UcRB0pvdMSjBikvjukisRA5kfPmnq+oZX2BsHcON37ikn7IthqFqlJVDi17aAZ3GF/HU+997ieYH
CHsjXudKsyQReFzIPAVonkL9zF384EMFr8aq49jq41f5KKwVGdJXCcriorKe4Oym7SWznqkFejiv
rCEHoCups6MaZVxNd6bMaAyFwICPQKKNIIKCAQOHv1aekjHS7keZXD65miFi2SUV0XDZXWCTJDVw
zcylXIBn3MrOHjFK+nF1IKcZD55io2/kijOaDA7qLxA1jMRbMLU7/uWCeVhpmEbU8453JvOH75Pw
yg3KiDlwHRXaQiao5vdmycw/acVr4JLIybFvpfh/vkUmErzTC+oKInpk3JTOlcvAlFuyLcLU/hC1
v3y58L50fuq37R1+YFUhyGLSO/DycwJ0wlJ5JalaVXWGaYLHlNU9OBcLOTnSaUyprKBJOnY0qzK+
aAfgq1Bc8hRoWEOXrwDMfcA7+3jZNDOcA0CFd9Z96niFwOPruNRhMLyiDdaBzjrem81sXSRbHZiV
UkHyxIWl4GGzgcNKzi6rcZnHYVhgyxA53dbo1wukjjlMCcKC+KP7Fr4A2NcvD1l1cQHI2DDCg4iZ
oKAkd2K3HI+mPpa9TBETOLAFQzIksA6H3d+UHBU9pTCpFVfvbeTcyTZrtIgVR/gAFGyB5nX51dY4
GKLP4GeRq/6iG1/BKKdZVfBhJVjyGnzctydCG19PTCPJ34NeDg5XIoxT6TlzX8FM0L0e8MIiAFU7
HdgfQyrAXhWYgaIRDYv6bISOp2+vZCJFryPQZe69prJZApxo+Z2d/HVz352fccMIv82kNff0bb8J
KTv9Lv/xjqZaj81ABkBBhk/WZWhKYesErHxih1yjcFpFk+kU/4SlEa5WoyI4gCgxAam+AYjTlfhf
Lr+sDczyXJOPnmx562qyGgPYgYbZptbqSJEQmZAp82Yzy43/xccpFZ8ONRF/mW5tSrGc61KvFyex
XQWj+CMonIxe35vBP28VxDLK1QwgbzuiyZihZHvFReJDoP8pEh19ykRHvI216DaUed24XgMyoYWb
gQs8CwXe5bxThIhae2wZ8AXNIDdwxsReifnkDA3XH3q8PgQfgfR6Xho9Wd3lO7n4RZvvBn1R06OF
9rw+M3lQsF3Oy5Qagvik6FQujQ+uZoslTQfOt62t4l9wII5s2frSdxIVogFqgncIXSqAYj9GuJYO
MT3/C9VwsGoEPE03UP5QHDMDGW5NKv6hwAKz7lu9kwkuCA92xM38yRDeYXKO2AP+qXIz6X4bOqoj
RYPjIItaeQ6VOknwGL8UXfCYK7u+gKe3C3DPM/nkdUgrqznTjYM9uYBgdsRUt60mcay2mo/i3WP6
VMhBCve1sGIwm4iqzAlzQvnWnKWp31Yiw6m9xc2ae7NOWPbNtkuPZdzrvuL38V8UQgRYam/u7zEv
Nj3efyDPOvsMXaPS4rO7LcuiEYa/rtsY+WrsyaU9xMqTkm5cXSuMRAxfSj7qlBENBdi6tvJMvl9J
LLWulWrnuWEKEqudi3lP/qpFY4W3qXDAYOas6X/tTsNIl6AmHUwxS6vSxmvMfSciS2ryOoHTrnxa
ruRbUa0APGjpkJ+ysrABt8tuhJtPLAwJxRZmHsSbK1kBhhHJS3s/pkqeppWh3fL0WbcVzo97hPKo
Lz9AgZA3EvOcwcumpFqwBewTYgKYfuxOunJIEbt18EVmyQCihncMbIUeRVu2803sgJM0m+btmt5T
EjKKB1jQymwLD6pm5F8X+PfQVhbk+nkIFDiEf/1CPmlUBgNlehmp7ejtTRPVYALjUt/p67rjt+Gx
dZzzXY134U+1J2ff/q0ZUCvMznmAk+uK8ZwiNTM2Mpj5ksxEw/8DomZvmJ5mwbPcneg359gNmS1p
sxT+HFXhsvFhVxrcX//7aW79+j8Hw+vCkDyPSf9jAG0mWdcmxBsS4YwjmfM06C7YOZbrGRe9cOX6
C1O/+GwdweE79qhZcTOmfUyqpT5peRSm5bK0gyQ2ihGLxUbPiBrpCTRBOFXAWYSVuGTuvuqG+A9k
7t/EqVYkj8FPEGLmmV8XWMlHL/Se752r8pGwDanLGFCxUJ9bjRJhsvzTE3A0iHgOoS81ohju1fyM
lew+2xFaFtgmulaDe3uquFEYxa1GQnL2TZmRXWLNa2ykRST3uBWwpCCTeiqEJ9KL5ybTlED+rIGB
DdSh5Z3K5dCkMvIT8kYxxrGO36B8uV1tjM8wZFdBluQ3v/06yL8Nd/8ImWWSNsHulaDhgd/UuYVQ
uV9mNZCIcKAkg6q6vH+/0MYPl1O82zle4b2SfyTX7nJcufMkv3CZYTAZzscWf9qt9WqahtFksH90
7pifbkH0heC92TZmfpb21AlG4ONxsa0lSI9cIrSFzdWTzZb4LAS7YN9OAStUG2h/mjWsnRHx2oGF
wHR+BeAo6dpSL9B6ExqgNnm/Prl5HpC5OGsFkzNwYoxUwKHxy2RfYUSdVNn1os54Ln3RsrP/LycN
r8Yo4Ojjw1ryrD9aiQMyD3r1Dou+UTJYE0aFJe8Z8hJiQ0T4CB0+m9XoEOQuYS3JQbVwCpkqolqN
evmOGZsLGAcOP+IuBK53VG4RxDsYmWsJ+6vt0ChEM1rhCrlW/LnFhcPUkSSD4Pdruh49AWdFrXLP
937hONSO6fOAuZmIQBXe0Ve3VWxIXBLoMs9gVa0zC7VlLTZZg8tc4DgMkHpr/EFwS7R49CLdRyrB
almSUVv/5ewr7Efnda/xhC+dopFz1JJ0wLR/m9UBApRR55n2SEZoTrybupwVQLgS40rOFGVtA/QK
GsElEz+uFS9YWFU/CIpp6LUFZ5r0IsNFOnKh4fBZL/PAGT0x0pQoi4/cUOO9wWVmlDz9ESUyf06U
soDAo9UeuBusxACGXSx846fo18noAfQOrACFCVvD/wyVwyISmXMDZ4k0sHsyyngOhoMtDmEEJweX
zQfsYzQ7Osd5it0aK1EshNjLufeqMGmIndb++AA8bw3ZwH6eRr9Jre4Ff/2wvjqbCTtrd5LnMzuK
HI3AipmsDXCAXDMagvykLv5p6URxWxWS6GVpVHGbEMaDLU3Mcf4XkWv2cfh4eNZMQuH7AWLKD2q0
t0weaf24GIwjP3LMKrnoftEgqFV2L2P2egTSblQPW3rUmkY+Gq3r1HPTxbeSF0/hBKhUhBE1XAZz
CQJStJhu561e+PvS3sthAB/WGArgkaFgP2x7f5qar8Y2TohfcuX/rmafZa2YlUkNyiTzjY9e14UL
kfRr3RH8lvhNZUwfWVLmGyO2RuGREclu57XTkvKgDvzOd34my0V/ObEXMy2k7jtkkgGcZcCsuMBl
slhBijfBCbflSW89yGgrzuD5GyqFgNnWqBRmxtgoQlriqpSqhOGkQtU9ZVHYUhfjq03wDGd2gF99
kY8314js/eCJxw4M/skOn2ZXllRRnoK5Le2ya8YbVZIzaHvgnBh3x7iWYr089U3hptJFf6cMjuyu
h5SdDJHEorEpL2oNqjiMcc0hleClNym4jtpK27yV+nLgNp2RKKvsIXDcxpZ/536/YEbP/jVybt+J
LorO0LWorlEqRkWou4ayuNmSAVu1QyOD4Cvv4g64Q4QB9AFz1NgpMn1cqQxX2UZuZnYcEj0D4k4y
8hidTc1hkINV9BgKKXOZxa5+7e1U3skPB2PDZIxnAKM2Xlk530t/8Gt3vqoO/AVBJlxUBrkOxmQZ
9YJ4rnlj5uxVH7kLTFrTZIDhhIMJCQlD1WprZgh8JXc6SiaTNip8BduM0sLZCTdj3HS7baoVmNRz
6sOnMgYpPUQgltMMa3HL79Fy/hBnglknzgu93qiUcTLRkjQ3bPsZT46pO5ZneBP2yoSvtKIkB0JK
6IFdHM+byN1tT+ug2cvjdqZ8GGP+H+LD52/AjeGKbXQ/+1aPKmIfMyRQtt5/HSXXBGX1ytBjmWkQ
8leIXeogrWfHj4R4wClDB+6zAQlczjRKtBZFs20T6Cf8Wm6JakDUWWr+q9BDC4PuAmgkQPsdnJgt
CY+VMMaRYW+ZzW4KQmS6indDV8MTM3GxSouezGJjfX6ecfLSOKclboPkeLI0/SELsHOtU/mTqnhQ
TMJhTarDaqHt7T/k+yQy57QbSW0DGxJDEotpgTcQHcfYnFFtOUJKtgl0nNNm6lcgrvMCJ8ZBlryN
IZ5D2x16QgPcqY5Spru+gcFfrnAPbeq0OkdHzD9fiYfME4eXn4/nnK9fDLr1yhW5RJcCyr4v4mex
5c2G9VQsLjOcJ0lSDDwsL/p8Ssv5FiWzYENugNs2GLvUi8NXjcpkbUriCgqz8Gy4IQGEgNN6nd3N
/hyhc39PYlgzj6sU6/s2S2hulDMGRBJkfpHkBnRMB4XA4QjKn4SJxs+7bTUiTjfinwre9z3SNyrU
2jNmq2wg5Pe+HOLFvZ1nb5TRz3muRfyNbl36XQPpIMstrA7GEgTxBiFzLmhYFlYOZXltRXjUMqCF
SejateqbvB8ld539tIy5w3DC6juK5SGcrdOIegS6kFHHSx4798P8r0j4x2dLP/nSZDQNbvRX9FCs
fdBccnz0YHfisjVw6RrV/vWqJ05smeCq6p+lb8jM3Qmj/xytRSFPvJe9t1a/mOhIKqYhCU50Eb9p
TLqL6Aib1YEz0k17dvf3X+/d1GbM9EQquYwS88vX9tDCyHVAjplICTtV5SemxW0wvyjKLTaOT25G
P3C9SVlPPnwmNTqeuykKA+E5VzBVkKlERg4+Ib5FSTEwWEtS2axQdXV9jKwW9/+u4W0rj1xyoP+7
7QApbHrf1JKMwZwGzKU6PTPoYSIi2nFZJT67aZveNLLxwbYP0XA39n3G3AiV/NibrB8MYayOQE0l
y7XRyL4hiUL4j4Vv9sLtwdXg3gMGkTUKFB1bQMQVWqz1tPPVdma8+9CxwQ/XmJDb0ysHypXibwy8
Rt7zZ1J8shw1o7rsKsWHI3DnhIerqEml595oAbEst5mHh1hcPT3QExy2CKxG/dtp95GlHzkndOcF
rgHBe3JAdL0Nt3MK1g4f+IDK7nc4DGZH8dYa8WbkzsBOtFaQbjb5I1hfMFqbk8jQevSP+Si7U152
f/HKt5kUECz6Z1EYIuD3Fme5vJVKFW6+BdyhJuF5D6T0ruBvoA1DiycGwNLFzNvd/jK2yi3MjKEW
1bwLGaB8Cs6+ILD+s+zxsc1Krk9IMRVk+ApkIaph2QzK8+9O6m8pOWw+zTn7PvC40KypIzvieJsy
MzmWAwAmXMQ2B9zBDf/sdA+NqGRQ5JmcKZxyhitraQqUItEZ1m87SI3kwHcGkaDpgBlzfhRfCap6
q+XrX7hSxfR5vB0xCeVOoAuJwk9+OR9xqRIeqdWe30IC5IEfVvXXTLtFYYSPFOtKmBTTC+3cWRb2
Kf74LgWpCcyGfkFnGAvihdyRl8HbHCap6yTrVoZsLUYJ1j/o+rWTCFX/X/F6bZ2jW43Sc98X1qGj
zpY6t/iSBF3ky43iL4yVY24ID0ZX2eyBOJlR/MXTN/sp2gRspyNRu9mMeh4S5vhFWRKdVjqKfJ2r
XtVmQOCNispKVIR+wPxbr0Bq1S/gLwZuCdMtb05AvCJRTpZAH3YU5VYJTzg3nIEtqigWzCUgcBU5
CzEoFQ9piijf0+ngYq7KENBPl5vQLioE1iP3pjfH1HpggZpzKqCKv8s/ZAStCVTOG6VmBHWk+YSC
UL0owMSiAy5+z8tI9SJCgsRRn+XC7nXk96raMfMlcbRuPHbkj2SWJRP9u55ePhmf28HOBWu4U3K5
SL2ZDlNSAJPDBB5HzUfyPpadiLFNz/q/Hzrd5b4NNyKC7mWi9aoLslVm2WYL80H+5lRjVtVsSTPU
vWyCCYOYSfJ+ccZazPOm7xWdbU3ksLp/kYGsWOaSipR6dPlnzO2ADnvLLZz+LJRki+xCRCFBE2F/
xUKYpb+7dFIh+9QnjVrSP4buwj7va3Vyc+SkDM5ckuPQmiBey3+7eOyfsW3DPpdfK7o7edlbr4mO
GPB1H4uF6eQUapJbEvSOj8VD5UrojJ9ah48FGVh1Wb5uvEFF+1LmLbs41+pptqlTPNe8uOtc41GH
3xiEWO+AhtCWfNr6f1YjNfHb6it57QQEa0MUDTOdsXYey9RB+Mr1QfINAussTIldFSDCANW8+GCW
j1D4bwrkxe2LkfVBXCunNX/9+0r1/mDcDGZTJEsprzLT98fU50RO/BtZQ4OxPCydZcnLuKA7isbr
qpYy4zMHcPHh7fga7nwO9PWfOFhMS7x8UI3+FmPDUCBy005cIskJPozvt/pDSB0AE5pR+XXGPHi9
iNBN3IpUJtbyE6TDq6tCtkzbLEfTpDEfljQzrhcvG3DRWUAg0zkQjPA14RX9IXQ8uVTfXJ05EMIv
mMJ2rmGdS7+7UHT7bwTGc5SyLtl+Kyy2Zc/nU37rzY6yye//pxMBaoC8hIL4WjtYJriLMIbLhNOF
N8U+ITOS4Tz/Umx2MtA3kqO8ORdM7RCbtCKvzssvqvvgRPNuuPa84dtA7/cgL8xwQHDxQuQlC/La
pNT6JwEgyAssIX58K0jS8TGFAv542Xo23CEPCSz5ucdxGNZO+VM4q7LNswsJXxMePf54BDX4gxiw
keRpaLTtsPYWNMNYVVo8fhB//37ZQNgdntfxTt0kWHxGEXMsP3Q7nvb38eJQISuRF2LciC/QX798
5RtTsm5tDFwiZ7s2BHoxjA7anIEOZmFysUdUNHdJKGujdlep1dW6ce5m+lypAewaaCjURdmzpGPK
NiNB5OQ3eHXXXeeB0MN5dE23FHYzzJOSsTC1WYRcrSUa2wX+oEG6nf1teDrzuEKAuWMGZ3sRgFMe
rPIU324HPT2mZsVDlMaiRif5aMWs/TSKT7toYIL07nP5RAVHuUMufpI/cQmDixeyH6/R3W9Zvj5k
TdhUvySwmBSPq0vs9DO40X9Kj0fwEK9lZ+REinLOHJEuulGrNQsuYSKuHmEqBfq8gIV9urVKCkhA
Dl+Ta9DDkIB/7SJmCMGCo9hJ8YJLdcEaXrAtRR2Lv+AV+8KxYwFyw0PBEYh8p0wD4ZTD5hhSrrI9
SdPNqfUgusV1RMLLiu0odv9g0wMEfBksTT/lBVlHpPWvsJKoHUS8cOzGNKYPnLrMU6092jiY+x3W
RAysdyU90TqUejsATtcdOpAQCd1jfHMkrfm0dtEa5JWidj2fBiU4vWfgnWydyzL57zSkk09Mp4OQ
j/wIkQLkNeJZPpTSgchGv33QgYVrcFNEowGZ+9AOH9OS9Ndc69f5+qfr7ReAFGtvYoDu+UR/nMth
nfuUNHpcyVRvFw4EMoYsOtrGZRZdXCDp2Ubux7TsglZsZ9vpyEMTYgjPTBVkfWGpJklpli44vJ7f
tqWkLsg6BAKyWD5oQsaCtkq+/HkWgoaIX/SOTwp5VVFIHItgpZai+IssQSssjV84l5lbJ9VG2gFB
92fDRZKREmQXVlhxpw9Kf0BHZKwH3EJQYSxkMdSGxvTSOYmTztgrpr+wkMlXXUZfU+JBe8eNF4QI
+146rvFzrVmG0G8KR849ZqHyMHUmyOtJULASyVWHn7T1bOF8AIC7qTPdwHNvGilGpk5dA1sYe7ZY
lYwcx1WwTippOTPbJKV8JDAtacyIt19IukJiUGTKmnXXbQBGe4pmSVSZ70V/pTwUZM0NFmdFplQM
zyjQSuefE9fBUwsHXPnihCeI27ZbhtbS3ET+jZq/dJyaEVkbn3KJyvbygVQ0oVzppXHrbasgGKMG
+SSZEoRfk4KZETe3JnwJLsVx88FGyfskC6CNNn9Qsv743IBwH0PRi8gv1FgNzslzn8Nk3mBHzYLC
8NRwT228/b2Z/nN6g+JmTAdi6KOtEhJiCxRgpwdE8+8uxVyqhQ55QF2Z7DCtpTb4aBWqxcvLnsrP
YdAkzzZPd8EDOh2n12qR0W1kUMBYoqSSOgh72KWY3NHEcScOkUt7VGLaaWRXXgap6eU6AiLwYGjh
DPet1cmz18Q5HCTMbFJLlfj2fF1RLkzbdsoFWxFoeTKgRrx6U2XKsKwWgevdK5zmaND8/Bu2I3Kj
XGuzE6cDdnjgUgFNK6f/H3U0v6qn2g/cUJH4ybDis9v5C9QRdmSgq/uz42RHXfkX6ceP/lloHSCe
B5r1Gh5pRoAcLXxOSEBniddR/CUQs+JVchyt3wem/umkD7LFzy4+qVKqhwtJzd644b93oOqfHmi4
XKmZGAcqsi9ukNBpAf9kI6DEzP522SVE1BlU6Kau/dI/vi5CowsxDfxJzqrMaZQ6p250KtEIfb0I
vML8QRntOGR45QSpsvKqkcDUyW/9H2kcrc3R3M7aUEYlKB9M5eZ6ockgf4Ct0rI2s6uAeaEN6L4v
vyHMBfL1b7VWrfJkNlhZpC+zOhBm/xThkdSnDCakxODXI5Vv3NWaZ6/L+rG0gVUl9CWSyXue7gnf
OhjwmB9qhFf+HNSUpAAockYRmUmJtaS/lL7CVCqzNJH6XzbhMf7s4Wych/FzFjweil5xbgX8TROe
UwFFODDeIxvIdLd1m2/sry9YMIsN4aVT+iHlHIwUScfZIw0Hbb6QiGQ9/LVcHXwbtbcAPI5iBOPg
/XykmOk9vu2icphj9MUki39uB36+lJTPXsZkSInCEQpR2JHrdaARm96OOf+n01mK6uuY1EZfCTOt
31Itzz33j9sd9czFX15/1YoA5UNAPyohu+/dV67Tnq/WL9eUeUQa3TUpIuvUc2ERciWr89TRRtF5
2P24B9C1TMvETNJUjZT8TAF8ojW4osTzoklaq0zA0bXxBIa0Q6RJMcCxZqPQdODfxf46uHXtOZc8
DHjsVyXEcQurEBDUE1XvP/mIYzyAvTpxc5Ps9JVoCDMdz8sQxz2BJedFGh7fyvqHGFA8Ruzl7E1j
Xq6a5YuGNsGOz2CQl8rCrovl0mesBAlRMKH9QZ/9fIzixNBYGbglblIYVpn1PidJarCm4DHVkr5t
yFP0j1WL38GKoY8Sh5ZUZgIzWuF91Qia0IJhVg2amEiytagUy5dELhcyHtsIVN/36BTZW+Of0WKx
xBP15iS/qnbX11E0mGitEZ7mOR5u8W7Zw+ZYsEEhpGIm9b07E0SXYXV9ehODvWO6Snx8V1nyKE3E
CYYJNnwfHgdoyPFH9dkebFgoB+rLFRlKW91stTPVmrugtRnY0u4ofHUe685stx46URvsYvfXLUdn
4887QSmNT2Wxu1VfBIFmQMk1PS74rMpNwlTfQhnOlYSJnoJ8SBAnwbNGwab2D9QdyqMyjpWW8g9e
dhgP9LrHNtJCmS6960KVAjO+Qypgd2LCXOUYtSL5qLElQAgYa9VoCBeE2ngutVkswIQ+edDGGG9k
wHVkhHoKE2L40HlEp8wtQXqfDQtl4RwhahB0he8m1AKcsnTVsyUS5bJ+UdiwMDN5tH7+sqDnq196
RhgDOkJmwlbCy7rz14JRku/wela6O0KuZyMpVqkBVUFz6lJ4J8AjX4g2V2Ivhzgb09uuaVXaLecc
j5P9QUINHmcEbrAYdnE4NZzA3aJgJSBTu6bcYepcPnrRnEtvCzOvWuJA+49DT/xvpYTtG51K/TXL
MlAbr0ByNmC2iWgEa6Zhk/NJ4siTGusSPrUQT9pdxKJT+Omjxzu4zTyH+8iCX42MP7Ruu6++zU2X
1eR4VD9PvjSD0ySV8Pkie6F3J604o65bmq+tZEoH02Dor1faW5rPI5rFOFPefR0BG+vl6senY3iW
uI5XbN0BXbe+NVw6wBzvFn3YB3Qq7Uuq/GxpWKA9JCbbGQRo4GMSYYuknsFMMFNXPrkbShyh8YSj
+l27N+hShJin1ZW/mfYKkPM9A4AUfSzFFQJ3JsaCDXdVN4Gmi/ZXCnDvz/qkfucSsSmMflHqlCDH
3JnpGvzQU3ga8hZbbTuy03VifDbd1DB8Yzj8Ma6oXFLCyJkRH0Q91sivovKzWdvoGl5CTNgx+qc3
jEDLmgp8YOIbqsYdmcc20OrYPqMDuIIDrr8A0bg4r5fSLqU6WHudiK6bRpvdYZepfHgBzm1U16Jo
61rND6CHGglTBuDb1XwQE0Y76hIwA9e32Ylj3ZUYyPEwZTlriZGdlZS6zmJMXWpxb8ewYSZwM/A6
GwCGyUdPYyAQS3pRLnEsLoq5OCjPclxFcDNBLMaLX7YiGKBFOr0FHy7OBHVl6TieQMpfIn3lKcag
zgkWdyeHwEYg6g8rzHyC4dtQX5deVM8YKyUqGQuE/rU0rvreJ5O8iP7/CrajLHZjvpY/juAQrbEf
qElt0ESc5g4Cn2+C/HxWpAy98V/pUkDT4INAOR6Wfvhx0K3492rfm8RqBV1ZAxHx2QgV8DgNZDGQ
3ICVKpdbmKqK0/AfCMK6y+qgQiS/W0h4GGiaxUc36nHbkyzkykSKet5oI8TeSVLW/2V0iamdB7qX
O+IWUvvkeexhus8Az9BS7gbbT9xIbQaJEI75yBs6OJHes2MXEElYzB76egjxFnUQxJ2COxQ4yE4S
M3Qc0HAWC8qSlg97ylEnRIa+0NfLtgjNb7fy1rGHQ66kJYz+a+fnYjre8djupN22ND3fBTF95vBD
IIoaZ1aZKDzaOKo6t29h8X3y5pNi93X4DGEQgG/ZNW+72zXIomYoW/vzz9Snp2jAsZJrFVwdosdD
NiqY5RfoI2+ml1nok6SsqgnownuIgXMzf3H7W8MCsD4vHePh9nhgiSloa0DGvi9/v7idwdG7WGsg
w1kkDfA3eoDLhweAKlb0tu14rY9DhewA8hjQzgg20sQ1hbj5eNTMq28I16fzYBkSEwg0P9adPvhJ
nslz7L8xoc9cFN+TKBi0SVuAlpoQwb7lsR41PwjBR5LQMRQCvdOm4F7dk8a3YY7ADmv3vXVlcn2m
RIkuAUuSe8yvY6Gkyn+85YXO5+Hi47yuLryjgU1w+mVZxZ3PC9MilvLESLPKCkaNXdh0zk4XnLS5
Bho3JxR8fkYlHjhwN3rqwGbQP0PGxAmrQmavEZAgtrsxtfGOPUaYqmgP5S00M6t77cBNIcih6kzg
KY738AS7ajjx+SpBJ8c3Ch2Lylnb1uSQb2wiB5F65NvlncRv2F7VjZyGLAapKynCU0okyooddOIQ
yTWz7ciihrpQKigJjTY3zkQXJNK8uUNJeqIuPFVuMl6QcM+/d7g+XsV3r5JLfpLggyvAZ5ysCeCe
x3uogpDQWfISzK5TDiqWRx+13YT1ru6oLqVkNhZrXoi/U2lEv/oD783spQYFmYHR7ZMfX8MFjscY
wQCxDMdRsKcrsLI9hKGutGWqjpKudRRwi+Zc6+P+nGFkY1Fc3KX+Np6r6cDoaVZZEGU9sTZdxvyA
rUBRuIwBsqqWLdKgnOgcsl1EYn4vhaWCmWslWEf5ORjQn8shrTpVQKOjHF/X+0CkgL/qn/37/NvT
wUYSmKax3LCYosIk8/ysW9rh62auxdVOI8QmkYur3xghnnfJ7Cw15pZT6s4PaFbz7Sb1s6+oXWAZ
5dNm6CLjzaM7fo/EjzhlcXLnz5ipGs2NWHlmh+y3hXcwb6MoB+0OfpWHJcb0dobzdsCVC8gv1GUb
QXqvS/eekhPlCO6SEEPoZd6S5aWEY4EXVIWzh5kK3K0bZXufP1rIOgKRQitmnU/2aAnmR2PL2wgM
DH8gkYg/jQOzDgMaaJuKI57onKlGxV8Iv1Dw0+fDjK6IaQt7bIFBdb5abpdchPxvHRvhU+eib6MG
Mrcl56y6V8PnArqroI1VQ6XtPwLzdVV/gnKeoyxENjD6D5TmFUHPJRlRyLBZh/Z4d8EdkFziCbNu
aDxTJu+zYz2b0990vBKMtGb1e5zDLR6SWml3y0H+HjsNBQybkPsYo1fqrnSFy4y9Bzs1vRimCFde
/hOQBi9jqvnJm53qeKJHIOvbNDl1XjZhUKeTh/RjFColmNC7TPy8umB/EoVqN0Yf0D+T2Yt0whXw
m0iPgbFaLZnqL2p8cgmOgrwWjBLlB+iGol515vA/SE8TNeehy+nRKzVY4VMJRbpG8xJWXKxRqVzi
vYW2QTXcRQt8syhqAYjTGPuTwUQKoJkTefj3nROihHMRW9tErjAocO5JJLHyXDKHsNp7h2iYHskd
chI99le6i/omhvgYNqXVXM2F2sSLSa4641GiIdH0Cz2OpZtDukcodtiyV8CMWuhr5rvDlyVfO4lL
poLdtGu1FQw40yF7du49WsoJDcYtoo7Mzm8Bf0AkcUDzPiW0yn9UNFUvyEAAreKtfHTnWM6Wrlqn
kL77sFpc93UVtohdVhu7Z0Nbqou4tCQXvH9y3JrUw2MDEktARsSmf7FWO1VubCm7/v+QLqSyCX91
CJCjqhZ30FUXEZnQkDuRp8hnqiBsrsgkUpVyG3or6LbhJbUKxCSnZQ5B5q+KZ40sZhJwRijybEOG
S1aEjKEyXz18O97toMnbyBTajFbuA4OvzzFmAOGQYQkdH/2BQK52A+RbKuUFw6k2fSEOjQMJ/vJw
wJzGUgbVBx1VWd9rSOirxm3ceiMwoGhHmgUG0Y9cGm77Y5U2X9WoDcMkyRvWjy+jUD4RVsK/O91n
3L0kR6HD6ZoA7otgBwURmdaHtIt9JMazLIJPqcKSzL8LVTakV4QOwmkIUhE2qkuxtmOWqhtSl/fL
jkp8ppGLjuZz8s8xTFLZYONyjayrPChrAiKGKSgalsgDrCe89wkNrDPQ3XKYxNLbMj1iTIkP1EWm
KVAJ2noBGohgzXRtgurNPtd/AS7yhoKdPeU43D2fy2kEdRfO++wVqxEfxqfJ9J2bphI+SRxRigKm
EtEz92v0WwTv+sN/nTP/xDBc6OQ3kxCJIrhl4X3rh/6hLXE5fkamAiPsT3h17Z7tzhybq7XNGt1E
asVOPHu0Zkfxz0m67FQ6p/oxXB/tMWgmnO1MJVc7G7Yc3fd42EEum4YJClUyf9/6M+4yMoKnA+hX
tiAfPIrM4fnE8Y2GQeKTOAjkxDe8aHRQ/iEoHD8NYYh0Yr2FvVn6q7+y1V8/2F7D4WASPKAR6bUP
rMEEnbuErOlVZ2vIf8sUy9chfu6ihgoOn80cGODojHqiLaAPURgfa82waFRweySSkcKdQ0O3b0CV
nCLDez37F6D9Ap4dcBx+hCGoR3mfna8JZcYgzyTeCLTfKPLNlk2u59kz/s2VsfU5MXLLcSoq/NZD
uyzEOo0xgRahBWe0Tlmt71CKkCzrROQMjWolVonrO/N8OHm5nzZr5XtxKM+YXIwGn5OBxrtIsT4o
kf9J6FPmhsdDnVZ1RQ6zDQLzej8eYFOOAAt6sQw3AUwKn8aOYA+LTe9TbK/blBwh2kcTcuDZYJPU
FmctUFXZp2o6nTvCzosgHfPyo3Ep2yN6E/JSzm5qnAgKrKXABv1Vz91Ox+Fn6cPjCaxxH/LwXqBP
cd0MNG4E/3c0f3h2YtjpF1FtNVZam06E8ybC6mrIxy/nMYSGPe5lLRv+ociCnzazCm/8Odxn4IVr
8qnkU1de1Ig60GsidKrdOf025IGnJ50EZwV13IeTMGa9raNhqDJDtoLrPdfw0JTIIhak9D/sy54/
QirXrgZ/ivyQfQ0kXqa7Y9Glh8tY42drY/hCshRJ33+Uo7ezs++Dk4kThxzC1b2AemG6x8/zu2KH
qEKgSrh4mKR2ARciznFyuNKmGvL1LDicfQ1d4rbOYstHqoJAa8HpI8ZA314i4F+xQ5Dzfs1LsX8m
IGCC7q4rn9ubUouSokRdt0WCwCL3Vwwm0TaUn+ZAznpOIFjvTkDtT286a5Xe8jn8Z9kFqMReU4Vg
/Ig5G61eL5kh+n+2X9Yw4yJeWoTBhzQz1fqhuDsHcsCa7lXJM3BRuspF/lN7nnk2wdx+wQOP1i97
RkocM66VpKAkwBVGDyTxiEz94/LgR6rZU2efj0GvvHT1AgQeC5/BADwTPhXLxFEIojtGpiz4GAuK
H8Nh7/JEwPRoI4cyjS63EqRmmJ/+GKFcTBNETeNYu2+dbbONuxovDDPXqvxAtiaSW5SdCRQx8FYV
A1ajnRcUK+iqda/8nvLjy4Q0LpIRbi1XeFpOl/R+afiBkfhvb1Znj+yKo65YLrL4nuwq96vp05xG
3oKMP+8UZTvMiOeYS63fsybZUP7UwCgrMVqxzUGhFZspqv6985z5PziEe2kSnv8Ob9Ru1ecfNJN3
7/1HHQQRKJSPG82idC78xCNDmIEkL6RGEXZQFAjWmkIzK6+0AEbOQB5XQxv8uGTFLkFwROkZVO+V
zNbTsPPPci/OrcyoMEZi4PIGOKJGNnFsYiLDEjQw5VUftIdBVxCTG20Jix2m6dhXzB1hpzNIKZfd
oNvrTVi861IjUWM4fcUBFTpdLl0UfbMykjC02/hlrS1l6sFExCuUm4J37cOCvSRJgHJ+ZNA7QIAk
fXHpEEIiEU1DBX+8GvQgcWGIL7LJ3dr/BW6NSU1L3tqZ8hfrDgH9jo6fbr5LLfETlYK5P1eiOFay
zqNE57NbKZX+7Ow6sb0uAy4NfhO97/PBdBMKaQjDyuGkeDB7Wv65jXNLovifpJZIMvnlmef+hJgU
g5xzV1ARc2kIGaKYJTgL9rval6ZYEQLoJIZZfsl1t0FS85zZbI885z5lP/PlxbB2THiWnRTNp077
t1AEA/rMbxaM0I3qoPUhRgR9KElAAe99T7hnu+2uJEGUnb+atAxzzA+n7JE9F/Gjh9vXFWV/r7ST
7F5eEZ+wN72yuCskOOM6zD06pLMQY8VD4glR8+XIZkrVlCAAM3jdjcdJmoJPnKNf/2csd1ToPdxY
UauayPV/82SLA6NE639U6rkHI2uNFZygwTxYlFxHJU8tDGgCjV45wIq/OEQb6FFtReC6xt76W5Cj
TR5Q2DKLeSoHMh/y7TKn3tmXUTRjV1VnoowQtqCrKfqSolPd29Dt74IxidNQLil0DkjpTsF8wVal
KBeGev/jnWByj7AzuOHx62cGoGIn755HN7/T0VghGclrcVtlZqiCgeQ/s70m5/7H7N5rdflMwMhV
GK34O0SA35Vh1cuaFCWsPib1XiyRafMsWbrkd0ePqMHT/tbKRA1Z9RCwdrFKQx3LagGbbR9520Zt
7XhscGaGZLDYXiL7OhPVM8OUcHQJ9sUdRP+UI5C5sshkYaG61JqH/Ey8jXyKG1Mv5ZEfRTz4pZH/
hz2aTQVWcEQ3Isq2P7ahcY4buVlnFd8Xh+TyZ3k7dNfca0z1Vr0nguMDtOdGWa5r7eyHmKM1K2Py
7YkFIcN+qjPXf8P6rStQvMBHxSAdPfOFLqsjTB47se94yXlW3ja5J6PVETAxQUdAeDjSmHACUsLQ
r2Wfn4osefFSoF0IQO4Sptmn7z1yi0vpC6IjIWjVZiaUI31v74rHdblikO3YTUth12YSLzW5ZJ/B
mpyY28V4X0miLZLGiWGa9RGrA1F8wNg53wYmBVzHrc8FVczF9Gmi67pQBQ33HRdfn5g1l0HvDFBM
XLOvZfTy9Bn8zgJJva6D1Xf5itWsbcW3HB/EBqx1OhuA1BCJSu9jJ/SnjoDCrIwtBJ5cMfCiHTon
Yk6CRP/i60L7hkWynpp33ouTjgbTLtQ87xvKiyqZ57wrtB3Y5oz3ugNh/mJE8y3GwitfwFV3QzQn
Ixu18mEUwvZQ6BcB014mAEjeHsNshUDYzncOm9zkUjR/JmXTo0xnGTq9LfJVObRajDh4jcTuSPlF
dTSGWvR7IBJcH4FCiGYHYKqLa4J7Nqn46NJD7DS8SBOMcuvj5nfyljYHCu6Uxm3XxnrZ2X1gKENb
rm26pcJAr9Oe80fOLoe5bTlpPOTr6UFLgzX4wJOJ3Ifamw8oZoYklwWpIDaFCjFgLT3FdyvxegON
HhbsuhJ/PoM1vYN97MKCN2f0iTRaWFjrHtw4D/KRihTm0PVMqG9TG5SnlccT4cWsLdL8bUZ1PyBN
cSkoiAurydzTlPfOr9hidnk/WJ8A0dPTP+GZJFJdHHT/7uJ81swOtWGGbxb2tUHtSFtJKZKcHTXa
jxhIbUuVKKEADRvbCCoMfZnE2elZ+OPkXYJZIMg38Mlj39KvI9a3HgArs6le55nMPPC4wiCI796n
Pm7/cDYqTVV63vWQ5hgETCK0RdN0h9s80w2HFuwdl6xzcMMLr5oPDLyrmlKug1xafN0Qjpdpm3kU
+T4pY/NIbyEuP+4GfNfal6znRPBsE/jb6RHHJZKOgAlfwjodItS65FSsqAy4LO5uZMuaafDabo4U
j1GcPnlF4C+MY3JLnDSGb3TMJ4mP3FXkO/+ctq1k50vLX7ShQgS8L1ekHjGGPzCgaAYQZJinJVNt
FZBnxa4miBm6+7HQUwJbcAAXj8nd1w0+3NMpwpNFuhZrSKvjfMTUDMFRWld34A7DWiNtAN0ry2+n
LRqCEl4vhcImgRirURw2MQHxBUue3qeXAP0LAOohUnvlYJ2/eSzu0e2bLL7Fz8eqY8sRJFexzY8O
HO4SKeHfKzo4Xyj8DAaWcsA9+IDe4obhbyMiJOoZ6/xFzDBIdsrmmMLiBM93WosEmvNe0UTh1Q0g
6fZnTrTYxHXWOkwtxyjSHs1vLriZ5LXAmcCp7D68W4Qd+hk7nkifV/CqzWUcl8QUEJz2zOw8evpH
8OHNRI5sgoWFsnC70ema8SPbf7NkImheV6uLwBc2wLfQe/gzcgZ3XicfApPAA8yXMcb9xjwr5fY9
99LDMs2ktEhp4eW5TO566Wrt6He7x1CKou5CPa6kutTHWpPUn/wn5xkwqHC3vQVhDGB/ZqVwnzYm
e2gjK/7+AWNAzcM54a4a4nda8wsjioqItVp536xId9Kn+HA7sgQEBH1ZG7eaqF7qbEAoN9wSNsf+
lvOIgMIYBklXBb5rBjJeISrMW3YoCSiPMmGqfHkMGu8G7X5Bagg5mVTQBpKDJ1SNF9MxZbzEEPhZ
YWoCjZgU5Pn2KHk4fpCCa9RUNngNhGmfLYWY6AKtLl1vo6zmg7VsZbGKal+rYTPvywk3x6re9osm
flLbMcTGWXRty6Sx3c6/A9HlOq4ufFigPyq5Icg0ofF4lo3lOXiXM8xzlw++m6QnGbzezuX4rvQw
NJi1WvZ2NRY7tF/1ydU28A//TBVZnPKQ1cUlXDKEPRaz7F0omDFy9gT3H7cxUlWXEWi1eT3Mec9x
K78JUjULbAN4ODNg8wt7t3wQAScieyD+CxEvrXJcCeBVwGhUJWUFsSHk1HYOV94pNPOYQqM1qDG9
DDMhm5PLjPixNSKToDJJpb2eq0abH/IrGbOeUHemGw7QYQs0HD97UIl1uwn13IvwMwNJ8K+AJ6iR
xzCqgE+i2gGwzSjhMy+BLO1ARufZzc4CghKcwVi6UKmojFbAYE8qAuOddU1HwMoVGgwMQt60jNr7
d6VlbC6RatimBuMNAJ3OY8bACh8ZMKTknTHCaTwbrRoPA439tqQxgjK9kFagHSuOiXd7V7IwyX59
76JuULxfxyr3MLWKCmSJP583mlzdyoFbRa2SWPQVKVJiJePWmb6cCRiyKRQRyHf2X+KETGKbh8eX
c/Q+40/s+ohf+x/YL6JPsvpyJmSDTFziiZ0SMrcu8JYs9zXrcdqKqYLL2tF2mcKeWLcdXXPSYYkp
r/isOuGHOyhqKJmv17L5537FEWRirHZoOCArzqhVNxZEnmQQetUevb52fhyCV5N4UQObrFmazJIK
PcITwUr/HzOZZhDDc4xR8VESu8Wf7k2op9LsJMNPOoLdtN4/ORjUvlS1sELbVs6FYMkTJW9T5PVv
hyYSxhEaFmMOHRquJdWgann3hyHMWpK3DnqvQVHljx3pXZ+8uP94DsTFKKEb+HzLO9wPz4EK/ixK
lQes0NmNXimOlBEF1QkZu6I2WcJBan07Mo37x3gzoZ1jGIIK9CTZfVdqXtowqRNQHdGpU2+ioIx6
5QwShL17bTs+ETFZYRSF7HSazbOw1LNRmQMT1x4klo1IbeLUoY3sXu4j4kyeZqCxfoHdhLgR8WF4
FdX00pNP+VwUmX46SvZ+mXUF+4lq1NhmaBSFmyFHIrPiXmvgYpuE2OCCe4XGGiPrBCWu7xEk6HJM
+k7bTYiDJJtlIaifho4LbAH6m8L0FFO8rSo4Xh9KSqWLmmWSCR4SDQCJjpBQASwpvviCTlN7ZUAZ
7fFbMm9tBjHUw/+DGHM4nyHqKnivyxqAh4kAG5rw1RESoL2bKHcTa/KpYzXkld3MLGzozWAe/a4I
v1uvJEXqQaojDqGgo2cgiQlA8XZTkBujLPboEUbuCDxEl8A6+Hc+EFMpsuQACrSbovrA+4UPksG2
reQxjYQR28fiCetL0fMCZWt7j7fWJU3tPR0asGT/yZTXljO2ezdlM4kgdckJQRO+S8Ydu1kuv50f
YAUa3Yv2NOE2zouVy0MAkCmHg5D8wTHigiTlZDE0RH8NTQ7X6rYKR0xYAZ3569SMK8oInVg8O+eI
asd/T8exOsnIFVX7a5JXg+IGcD7z4pdNb31aVAqW0pMlkOJiTFx0TgQvv11AoLSC4RCK+e6s5ZgB
vQmE+NY68HhGrP0TJNEMv+CZb+RSZ7iH8oUgDouIKSodCYkzRKW6wC+rutr5HhYVC27i2Uxy446n
yB2cCs2FOa0rwqTviHUY31dE1csXawyLTdO/OemMElTDKImS2WyHyNfvJiSXlKCQbhTbRPNktbsS
fFrRF1bOCt9Fs2TvkayUiqEaZbBtBemI9ATPJ/KypdpED7xR5rPW14fijDnqsYKGKC/LCIrV5aEr
/MXFGSbLa5fy3rVqBgLMOskPWElaUaBlY96Wf40MnQ2AKvpFPZ5xb3Y2dz9B7ioxKmspyA0rGn1F
xB3abMlxsfvzzEBuVSl647J8yi3C8aAN/GKGn/yV2V2wMiUiMYV6RcMz647pv+tJgvgoZFYJ3aQj
OKv1YOAMMiZQrT5olwbKkChRiWXLskFZf6shMdPYlsvhofTcr3uBxMa6iYqPQh9fbhOc2RytvICD
uz+0dvGA8ylguHEs4PjuPtNaH7u3q4u2dokVW8s4wUKEsffWff2QQ4aEImH0MLRlipWLeDn7yjUg
tSbbx0D/lf//RBct6MDVHD5a00+lwxvxZglRuauexZjMYr9YoX7YILls50M2xCo8ezduO9TSM2sE
kQ60yUPDm5pC6H+PohSvJDYN8c4X328BYXcVGllv7ReXzeR/dCOMwygVWigIKZ7ThBHZuxBURhaK
2ZZRLyGWcS30uxMZjqT2fhEQlZQix2RdEzptylcikXj0d9KS55loL1NDXcyBAWYXvlf4xtG4q3vQ
N+WjWzHwsR7EuZR+/LtNFYluqO5o3UKDfC7tUNRyH2pmmVmpprFkjHyPAUox44CNuxcZjTlfnn4c
zPi44qcqVOx2h/QoK/AnCmObg5lcnLBjoX5Rw8orHmt6WpCYYwYsjM9nb9lL7NydDgioO/zcxUg+
XG8B/z3c8ryZR25QgCf1zikPBTAsxrdC8ybtKOGhl2lbdDy6jO4UKqa0g4tdPvT7CRngIAQLqRro
hq7yf55FKMT6PYDpIQjezVOJ1RljTqi7rv4/Esh1EF47RRWoYktTNRH2hPmlcc8wDrDdkhV3USl5
h2SitqGS2Bo7qKrcgmMfu3s6YVz7Zmno/6sZuVb3RmsGZWndJSNfHEBCLNgEhoYolpCBaTRp2lKr
1w6xf3cCgVD8dvSc6bFzWJsizTIvAmlHaYAeqJl4AzeAlVPgfNVkRmNlRSNctXMH0iSGdu7JRdHj
Zfg2/CV8yYRpSzqQ9rG8domm7Ln6SamFp+sxK5bw0DeI5Cohq+PgkvVF6tArJ02EBaQAquI8v/Vx
xMu8JrwWosuo/hL+GKyzNCOxLNpWxvqQt2K/yOFh8S9RnKyYdcFBDjUmGgoNk6D1+CLe2b5IQglf
NAP4Z8jmYB1XQy9//arFd6CMjNzZadGhye5BGLBfxILWk401zKLWkx/gvOEb0jiljWH68ajwprYt
2FYbU4H+Pw0mjMsVXiNk9dXyvkDhT63EeVzVacB03JyJt2g/ynAuRVz3cst9dlaQ92tvUofbMvMF
pitZ8XKSvUkkZmuKSK5NYfRd7P9euC5ag/qhBuViAG8N4DqPa5/2/L0vzOP8Ot6ZVcqJokwa74I2
7YEHC8illSizciLRDBN/qq0N5OriahhkdPi7OAUdmWZrHlClaB4+ZSr3y/zE9ymPwIipzK1MFKvA
sPPwvwQP9QuhpWMXOWGJmXnYNIaJ6gykSdF/Mgte0DYitgJ3yuIbZtmQR/1N6WAqePAI4gQ30X0j
7eyo60ks9yQhuD/wFM6lsrPL0FFaP91jRLmOMtWqrwDZohWf6HPhSsKtfJDC2XwQB1fpU62+foW+
Qrg23+X2/6W5eeLx8aEGX4686TCZYXqYSHJpmKBG6FZVpbhxtGMbPOh73wM2vC1D3VcIlb6bI1jD
tiKmc/ifuZjuR2Hh2/3d6U8Fxwv5H9/YuRRe6aH1Q4JjNG750yvbOMiFctVp6M7udhKYA6oC0m7v
Sjm1Y8d0AM47M+AJz0QHM4B8yf8Fbi8w9MkybCqYr04v7Mk0+VSEEINPFyA12I5UQFGkh74jHonS
+YqAWHuT0MDY8wgeDSeFl+8brbdPU9xf38WoLvOGTW/G+uL067uLIPKGxYiU7771prfpEh0x23fm
pjQ2I504gKym8/bNv11av3bH00fLZmzK3x/JZnI7rl+Z7Va6FY5fIBFSB/C8pfY8/tSM/+VI6COk
Iw87JmV0XWJ+InoXN7hnYNUr6VeIkA4FIdmxTfRwjJzIoUPX0Cted2uVCQgGJIYQ+eqDUQYlymn3
eDvCC0FnkJXPVM+zyUWlHmyLasFOii6YikyLhkL+z8ginVAjP81oBOC/TgFfVk2l2QEzskZivMVI
M/zxKfdFrsT9GOb2nVB6sqbFoyNwzzmIij++QxUWBy0aZC9tFfERtgZMhHyArv1FPlWAz7zXWyKc
QTvJG2t0GY7YveCrJXPxZGCpWuE/L1lFJnf4x7/kDtmV+8PJSgneRPoy89gWnExe6o2JMAv/zOnI
0nC4leHAYf6z4pBMWv0+z0WvMr35Upgacxa9nXgvo8HY5LO5lgZldUr4YRVhL2qorgJnUkgnzzdD
BAGW/c1TJXOELdie8hEdgCChRZNwLgzSRWjBJ4zA0WW4orzYNmoayHjahgsnWZQlZvQ7LlXebU6C
NuL94VeQ7vScanscxAx6LC4kd8PRb2+sz9bkm3Gged2ZI/nJD+C4ZUITSATzVKqoteKANI3kIKvl
kyOAozw0v2xr9H3fKI9GnqM4ghrOUIommmW8wlo7AbVIVfYwJ5iSpmQYxeZbfzQPGmGwyAnOQ5tZ
RU7fzQa5HQNZFYEsVqd+qXofxj/eJjW6ePFPEWta30FE9gjYCKGzZKPLjUmLYhSJFvrqkbh6rwiF
Ka5E3TiLKBbGKeh13nT+oh63GZgmxPmuSuk7xni+dmI7EoVMiy2wumm8yAP1SKZBwV4XrxUgt8wQ
hHzGua/wE2CXmJmfZR3yU978c1gE2C+2c0YtuFHLlg7U8LnKtS61VnGvaSkhJunnZL/hD2TNE/Qw
nsZUiZeL3HxBP6wvhaEYO6a7HE1gUnSbQkOMjUzmQIHULb6scF2rDGy0Eo7nBkA0/+aGSJfaNfSn
Ub064QiCDurbbYWQylbe7JyE8wMpNgRwhPvkoOucF8bNGPevA/VjrabGYYQVvfasr5HJe8EJ9+3W
9hKwm2IoL1byQYbhVIEx/gVn++ihJT9HrCj9M9Z75kdzyE/q+0srFdvH4sfZh29uBeFCAGlBCkNa
PeUOGa2IykHo76YtK4Y175jB4gTMYxFAtTIdV+hPY5wOY1qEX8IZr7A8TtJw7eNiK0JFNHVhESw0
pq3KS6r1gtcbDDocBKBDipNFNGugz4MNFAY7a4KSDaaKIJ0pF2WQ4BBJhouEUhfCPQlNOHRuSTgD
9Ef8prbElPZGAo/A4UyBLj2qOPg81hPlb4k4HN18SQV/hBlF98fc39vB1Zh4hTbNyAlnaA6kwybs
GsRwxVF3sPqDsWdxOKe7xYzQvRQz4NhNFt0/FwwAdX9iaN8lTFLxaMqQ3B3XwtXu/6ODu15QMLdz
rxVLBehquRf3uyacqBKskXxYKJFBtlvdEnyEZF53nQLM0YH7JMKLMPn7U606xQRTdgEMyoe9Q8r8
45cNCSqiVb1aQDTOcHh4kHeikhNN80akw8wnOGEOXmMngf1y89WeQrgum/PVUdOgJbCCe9EE0qk0
ClIg1l/lfaEe4tMmDQRIx7XJsR/wc7RHPbIy3QQlQvNkU4nYe9UnfjEzY46LyBSicZbIo8UTbb2m
qtDx9plxe9mGqoDeGVX3j2i31R9exOXoottMObVeQfLQYf2wayqoqGr4S9aaP7t5BLQBjQVuQRmM
6vpYwMZjNI46DZZUsbg2d9r3KvRaHTC1idw9I+mTyDHVBo9UKEhSAMLOkPuplmYfMTI81cyRF3uH
wCXn5wLdGDJZ6hL9dcuKvYk7uBV9LOUW46mDwFjKwNTo1obm5hMq53UBFWGfMPZUeQ5/fuVYluOs
2qFPv/isr3J5JZ7ZPwfLPxzGIS/v4jvfwqz4Q50tww3/oZ2G+Tcu7219RralXlxFPPFFL8eyfTxb
fVCu81wk9lnOyZSxYjx2T/KmSgFy9IqXgBV4uAe1ZBZHorENDEL3rinPtBJbJ+V8oDLcf3JDSd8n
463D6jn2ruA77KpUgS0EflqJ+BWEXaabGwjrX7h42eC7SfSWMUFq7F3ex771yMHSFZWkK2h9gEdd
kObxuwSdaWl7K7b43tzbGAXimS6YTjq+t6emWthFzwW79mEf+wZ9R5FYDx/WdF5Ljek7q1kHl/BG
ZpmDllVaQl+i7LyZp7H0G+BNs1KXBYtTxIML2qxvGJN12nHMJ9uQ5el9+Lk6KzipGRcREXvvHCFC
ifatlr7gQAPONPRx3AJjpmtzl+FXJzPxaxo7nVL04t/7k+9IlnRMVP4opJD8qa2/F9q/GtW/c8yU
71HF3w//qcRcR0DPEjtvSswTgpPyk9cBvWVIfb/2FTKqEYWonnnj6jc9bpEHUGROYoVJXVwCgcM4
ZQCULNTQwRXXBF5GgFNg5xQM9eFk+dtK5gTKDiJJD94SSRvrIV+8apXj1Tlgb6XaFuLNg4IV8Ycn
MtyG0KVWAwubCzlf7KtJR64ZIqqsZVBW+eG3YgN+qW283hyJC185dPUE31z/mvo7M30PDOTM3f2i
l3M8MlbU1LwJplX4tC1OYJHSbLPBPAydZ23fbUBg0NObo03rF74Y+VNxRWJyjnc8haQeJi8qnuYE
5Nwi1r26wKrJ5peuMl+tCZFcxebbDjFBcBZNADzmPUMlqGGiW39EFifJc+w98mPOT0gZ2h61KaCe
k3ZdVBb7YDG0YljK5x9b2F7+0RsDGVfMQhbo+lsVcPtfAH4L9GmaieUJqjaFCkDkj//py6aeGhDc
raGCOtOTkm824hpMDHSc+tfDAvLzlpJP7hSrapFB0GSYAMf9PWPfazkogJZkSzq5JrA0KysdXa2o
btMq1AYkj1eFUDhCbrkCP3tgRvHUWKA4mAlpRKLHAB793oQutt5HEO7xH1kyt9yuDMov75nUj4U1
w/YXb6gdYrIkriDkRKzYr9PCf8eEctTrm/mqZufwJ0ijId6idmYSQHqw205SbnDy6ROUkIRR4/Kd
/KUd9+/Mr/i1l5MdkAzQxOxeCbkrq3KX8kGtYB3vicJwue1qcKlJOoSXQdZJKZ6qsBMLp+y3GwNR
QhLZnH9ieEMZgdDFYGOcjlrKDgaRycxjG+PUgEsGN3/W9IQtKkTmbIPyqbpwJ/icXjMcrgtGc1MU
SAjb/fZ5Qblp9Y05YJwNp4k0Rz+dzzczJFtYfarfnJeahsZIXWTwohgp0VmzkmW0X3DqZFp7VsA8
j1Rk5KUXmZ8YHI7Ju6ZuDMXp6Qm7eh08Iwp2A3bs9Rtt/87febhSwQlOO4it+RhqjluaULMyASTA
6Roin9rSv+d++cDbT1Scr2cVSuq/fL1cXDl+4g13GlddGvY1qLs9S7reSx7qNmjKAG0x15gPVb+C
cNkjjBsGksKuSiWK+IvnHlbNg44lm5FpIqFDPSzoS8q7NtJEavGunlQVgyEYWf+eAb3XYWK9I5rd
b+Lf3RT3DvYbLc9RqcMWP/aXb+ctefkC7oPfbq/lfqITMG5qi5HlqA2Zl6msea1D3HXMFHtol07o
Rq/YZcK1JqG0c5UWIPJq7ruyDRpyQLPhInI+iRYJ1LZB08ajqQgfDPsO5mXCKy+NEwd5vbRKYe7F
yAMb4YQpxJv6IwYdKq1sGhvaJ0MkfEzjhqqmI+QqJC0qeHT1ErHk6BVgTiZOodbRro5xx52yA6tF
n9vSVhVZ5yCwby+fS+7TKXub39hD2vVgqHVILCAnI/n5dMwpoERsN60WW1Hi0N3yiLYn+iWDES+y
do7pUcY0EL1f7qf6nsB/19rvYMcuP+tm1Ne+974jdtP33CmfT0p33ayPxBv2rcfF9eJPahnrfIcw
SdjaRhBoRA2D9CEjorauk1aGori82sa0/zdd8SR9EbNP6Y4CZ2lbI/7pDmM/v++qDyQlpzG9kRlA
k/nPjZCllXwk0LnDh76R+0XDnMd9PnVOJY3ZqYUVBP49gjE/FNM2xsrPQ0efbX74PC7Ys3y+/RiH
+zfTL+9kJ8YGOO8kdS5GsX2sdhdbMLkzqgig7c13FnoWRQIcgs2hajilp5shsga0yRCWc2hKPCUs
OUSHnSmNGguJSOyNeyGgLli5ILjJfEBX9Yy14XIRyHiU59e4RrcwHYBCIQECK8cQeuP4LVwT3hCi
SpXU6RP/bYg3PNPBWYIT1vEXTRYDnDf6rwmds58ValzZdZp3lQmL5yIFFTrXZHuOs6tgYVj0iDq+
hXcCdp4uxEqkjvRS+9qcgRSg9PQcvgge9UvHXQwTc+kiK2xahi9C87MobAccmgu19ro8w3LgIZDr
lfIAC4H/5eri9CKBNMGynpX8V6554219HvIFdraovZM0aceZAJdTZJMCJWzoB7MnAOiHooQEui+y
POaVZ8QSQNVvFjfJQ0KR3J7d97sG3/7MKkolPfAnsmDgDLtoDNSA9h7aEJfenlQ0YekjcCAkob3W
E58I//iLAyfz8YSyv6vo9rC2JIEnZrjzdDdtqTfh8EdEYdnpzyP8PqSXml+92hlnl1rONor22Kaz
wMdZALLGX3VPj4TsKoUeq5n4WcAQK3SNlvlbqMD78+5r0M1SJwVnlXQQKrlUBP1u8S5OvO/Q+Ofw
wOHh6JncPZNVgrGofAYk/8VOdM6/W9ZmBmQlDPinIBsUHpppOAQpPxTcdsVxSe0rh5n8AW/7Cc26
ND3eydPBR/V/YJx9fd35OWoODefokb1Uh6/Ty8zea6jjdPAJ1C8siPXXLddDk4BwweWNnZ6dph+J
nnhm94b88eVWnyUuojgCQrn2rIsPiB6+I8yzAQKqHcy/blQ7goXeO2qQc8NCQi6gJjDQBF2e/Ia2
9HvaBvmzHVmoZE5xra6jPhLCr+wHG4TyU02IfXAZEAl2zlD9oHx3aEbK2ftGbvtSifUZjIA++/26
yMAto0cNxH4Ut+Hd6VDF+M7TL9Hu7YPnGt7jHPX6YdGh+3deJLntbYPdYemNIX+yt8pBR5m71ZKX
OLdccA4CchPKRWEfn4srMNDnNnJw8r0i5pdiqJBNTl8vK4rBrfLhmfDJAQaktL/7MnmZNcwuVxb3
p0keKn3qN1wUOaZaCp4Ot7F1hyuyBNjS8uoxYhi/Vi88UUeIUSg2Fl2LFtczCU5mcxxJq7Rvwc6W
W5LtXkgPtWA9croubY+FgUauy3kbGoEaMB8BBXonMWGFkEsBwRbEhMdJBrnuFczKUxEd0rW2VTE5
AWkGCd6lcD0A162CrvuEF1BUaDAM4VFXwJwFL0f2kLunF8DpmnCdWSr588gC8cMJKIOl8Dh7muYw
sFxP9OCvwbMLrm1SuxT3YU6USyMYMu5STBypitBW/hEvrH2fz3XThlXi2lsdHNw4J4weG8ipJTI/
1vGtTyus42OozmxZ07gMy7ASiQfwieoSLnhYKoe83z8Mjp0+6vth7m60UEep8mK9LGxd5lakYrqp
cS9CmPvcA0u41ltTwfRM821CHkfkuhyXkgnws7/pTJz9wuNF46ahPxcuEMklnd1OGYlQ/ZAAvOTR
z1kuFTWXfVv6JQ96YGqRojN7swKSqpm+D3lf/XC0TXAd5QygjSGmd5eewAoT0Vh9rZSHG5Kddie8
j48spKoalWtR7oWDZms+80pUNNKAnanL6wPY58sJVw4mcDBdMVwINYnNbOVGHHf3qQLyYjZLSWSQ
X9qgyXbPCcLFqn36Bqpv3vzLN+5J8lIOtlpS2wfaQyEpfg9qsjUkUeGlCtbbvBTOMUk6sFl50MQc
OZBWUiG54PyegoR7lwrPMu8lOodcPu59jQ6k8ZIwvgipz6Jk0uaQzSzdrJb4PDd3sVJiXBGSIUjY
tE2F7MIT3LKrYL6LWCWFdR8hR2J8J8ukRvs75fYhYaFMYTwtRpi5T4jquqRJt5zv1b0pfaVcj3pM
aZd1fTZQr6ejn+QbXAIjyzZeECTYuLnRjQQ6Ol4pHb4dprOl+lPYNm9nuVDkIOVgn7PIj+PEWeA7
tC4mxamtQ0yJKPH+C24IKj6qfml/sdeJVc9PuTORcXg9zuEwqyGFNqRZu0Bxjueb1WHUwvdpKk9J
mvqh+XLrK0/dpmtZbekTAceMaAnI5sSIhV3k0poi9GmzSkpgFdWoi4Cg8LHQGu3dP9lbruhx1qRO
rGjIrdMkJ+loGQrNXoYePpik6tvp/dflkYqsmv+NJCJ+DIeVuEyv9i1zrLH14i0BytODeGykBH01
As4zP9h1qrp8Uuq81Muvff+RD/1x1HUoOsOKVK6HwkG8cR7N1ZKB2Tc6fZugf9KPreUX5ajeIHCQ
tyTKx3F2zV2f9fZc5g0SGEY+DB9SD3wjppEll8pMqW/MPSGCeF1dLoZlnT77F83M2FKLpK2q3jJH
Y4xmGMlWpliW9L3/fT2TlUUeofZaaFKRafYuRFYyCQuoBPz79KkfMObqkuBayxadXABHpPoWgNe8
ylby/927+tbYrs8v5gGWPuqEU8DL89mild1xWopLtO5Hwb+mJMx6pWfV/bdfgS/GF40V/GR4J9Cx
JuPNQmt1zcqw8F+VwLbATxhg9XlKHle8wvEaEXWhP0UZxnOytZ8GyltTE8IKEC6K/h50Yx7gWPsZ
IdzCzOKMjKS+b9Z19iO1pfaZhYzXDBhtr1z3ld4R2s2Qq+3WY2gESFSkwlNKFb9kZbF1RAmWAoz3
gcv3AxLDN8ZNQJW3irNDABHjoMvVdAJxzKPSzN5ERE13YuTr6pxj/oeUdYAprqwh1tMcSsNUgw6Q
2Z9PDZ3m+ED9lTotzI6jMI0uK/r9MP8ie9MQAgA/6TEl9HcMSxA0Fg7sR+ZmPRLHgG3Gi4dHtajh
m3tR4hJJkVRC14gl3CtUDINCZwn4LbOyybAYyhxm3i9CEnd2L/sraPHfafaPZxRJlFNGWSv8YIfq
bpahUhYpbLNEwbqqjMUJikTEvhuKIIw4gElQVMug+E4xxJruXVKdiNdop2DGCN1THn4nBfmyR9Ak
ReaPe9wLA/gWdyr+iDaANeYMFHc8HHIDiZSwaQ1hBx0c/4dVq2WJ7HSH/n95TQKTuZ4s21vlTc4O
rl24nuM7DF+jf/PRTxdZAMezxm6CH8XQ3nevA2kf2SDvKw34ZM/PZyXfPUfDkUBMzR5VOjOduZKg
lTBKBjBnE2MxCaDjEvFZhhcAAqTZd2z2tSQJbX1xH8IcsH+F1UFXWx92MDiZvHCiMZJp9JzSzSkQ
unk9nPBt/cAp2/gJXdCuhqOg+56zLLdHCiG5G9wtyxHpJ3AoIjKyniG2zBfWrNo8Ntd6Ih+60VPT
y6dkxLUBHYH+zEAcQZxJT1m1CdkcnHTT3/F0r+2o7L75gADPWMGjlJhl4L4tlx5xR4oT5bOaikhi
Fhyo3UJK5LjCTb+dT8r5RY3IPTczvWSyHh0xFTMQTTxKvp76OQnJuSa4NMHpp6ff2tWBy5PzaWrJ
n+BD9cZOQSwjLPeWw59tTqofL8ycCQyaY72hKKadOkfsX1r5lMT3psB8JjkvRINGgehKiTUiBkwQ
34YowpBLA2fwRu4gxvtkXNMIIWxVF3RXqpZ+OTX94r8xjZVtfW7syxMXn8JlhQEdbg2Dgp6dYOR6
LXMLnFkB/BNLDmA1JxtNr13NHE8UnIYFKFBFfsa9YjHpNkJHothu97A7bkJSDowA56052eciDOLq
2t248Vir7BrU7Xic/WSkwJ4X6zEYGmbC9rd9CsWxv4OLbaCDzFF6MF2uBdjabEcX9REypRSiOhKr
j84X0Hr+plU+RwJX7gOdBsP3//nOx/2N/mbPHancxEX0yYoAEoOrSMVm32DwiRv3xUru6AXZUi7d
8flalrS2UkB2fSrvyUWZYJPDVJaPwjEFfxW41QVUIY4KNi81rEpwnDEHVvow5ijYDNeJOw81Umda
XSzoLTpJ5PwLy3Z2XhgXzR0ceOkvmMxTNWKsTW+2Rcm604sWRWtNt2VKv0ebKG+eErz1WjQZYgAT
QkI0r+10cO66OAlRgirfu2Ajbwx/iOYcMG28vAfHE5k+1jDh1tad6FPkgA2znVNZs/7MGXDs1kjt
s6s25rd282Y8WC/VhDL/GD0a5ZLCYEJ0BnXXM9bJhz30djTF6fm9rOIT3gn5n2zZkZLQQw5bVNRN
sDgLahwP1TrXVN95JSsNlTJssRB8MjW2mUKZf/CIhqV2HCFv9A9yTYH7rlcde1FGi+vuFX9B0cXN
hfreEcom1RW51AObJIkMMY1xOdqvu/CDnchPDVc05sAtxGoqJXU0g/Y3hNzNnNFkoVineWePOXbe
LJfBBEUFrVs1GBGkNB2zuhbF3CsRoCnMjyjh9drugn3eEZOLZTsxk9z1bpX1gjItPsRaLDifdY5R
u79XQo0nguuOUbqtl1JlMBika4wqjb4h7uvNgJwT3/y6VCFbYdzEDVDr5PsurgskJ85DWwuNy8e/
Tqq3YcWG8xayeCzoQIddhtFFLYnTX+4XgiaBnE1QgYHumrW++wO74J8i6LlZcKEgD7nfarFcGfQG
1DHI97JsZwojEjoc1WfL7cRkq+4D70nfUEAalhjz3iFw0JntBIAEWkP5XbDVhtZF9Jm8h2L2nZVy
olryFbv3I8Pr44CUJECYSySEh5/yFHNHHvg5Uz40pDLGtD1ylAXljC3pFqXXNXi9IDmYsWTS1z2L
75KRjtkaFrsnfQwt15K2AZGW+m52YqXFA5XvnIo0goHHOvTwOFTJMB/91gWvFYvdKzwhFY6LOlfB
jE8n+WhQynkQ/Wq7mBdbDQmcYCUuMpJcC8ZfBf+D60StLqqKNhfw07KKEaHFKFXJnxq76UWG/GKh
oL5Yu7il+QffNq2B9aZDwN62vDn4EVMgf/NKV1TAH17qF0TVGUTgBCJjSvUVxIoWtX3+UdMf860G
youIWls63ykZYgZCcemh0nWpO1Lwol/5gVbmHLTYWserRfw7YTnDDn/SdXbTrq7/8KNhR1S9LzkQ
JYuM6qhdkJd1mPzClBb/bfQwFdaJnZecQz2Wo6A27jRFzt+OdnarNQo98k6jime/3AYOssdidzpO
KHjr+Cht19a1KQixcYmQK9zeQ/0b4KKLA1ih/ki1BroPy3uWla1K3LYqlNK/oU62RchKcXV1vnfQ
CdU9D+eUFrjc0CMTvxTjeAI/zG8ZTl10gy92VuFE24XGFzE5Inn3cT4OEDHTOTWbjJi+JZu1Khdb
km/mFlBL9eC05qoLpL7bozmT78CzG79d4ob86SjEFCZO/TyHJLfGV76AGPRykGELXuH/fWK+LplO
DyzniLyzZ8WItwNPdUI+Ea1xSEZA99SBBKw0wci/L3YcWMgK/cu0kjR9iZazukw5wEcyAtd7eJp0
gtUT0oDCXaN5CFm7DG5OK09KsussFNDUDctaYXllP5vn4pc/RtN2SIZRhQTxB11OZV2F9F7dvOoD
2YcL/h78d0v1HhknSm+mw8+PBBLY7eRwpLTjVn803sYDkSCtrD8z6spxs4KZ7PfiEPlaVYmJUIKu
hEyqqI2uVjOeqWzUrsZt2UzYFEb5IV7XXgX701Sp+pguEaToctvrtb7uRP5yTMF+M9u16b/oCU1S
1S0GQEkA+okM0lEm62TrlWAeDqTk0JzbI6fwYYl87bfBdFabV39Q28rSGOWI00UajBv7EwhMW335
8Y8CMg8F5oTYR3CV54u9CGatjO2lMA63/b7n6RxPyBIPu18F7DJNzOPseTbFup+yHDKOEpgci44d
4FbqLEOqPDqWRfyiyxsSTKFb74hR3F/03JggrYkkUpyLYDyCzuKSLJl11/mL5M/7KgIhPuXCgPTG
080oCaoIT42WHXtt1MuDfFMO0VHJMy5iU9/jMVgsfJ3KlmG6sujn0xoNIKQCuzQRtz/V5jo9WXuk
7ABpQmGmySXOhH6yOFXe2GhvKotCylZelr0EV7dTaQiOnieLk+DIfJszMaLKNLlXQUWimAL9MXq1
Y5zqR1NcPjM2+S4f3Gr4RrNhFVsIBX8S9lF55V/MPRHO2gFiFxKU7f0opeKg16gZG3KEOJWuY2S0
CloE2vGtRVUPaxmB5EEWccuxcjS38DCe4crKFb5FGRBQxRH8wg8pA9F0qITZZ/5hUWnHCXUA2a24
ndxq+s+AK5tEZCQBXJhSkRTzefbCuBk4sj/5LcdWsGR07AkbzRqLRFS9MsyAb1WDzwqKWAG/Hgdg
fvWA3Fei3iSjgrzdI+5wRfmyD2EqF2lnpn+hYqaX7e4+dwwDzWZU4cJGL4enIfu3OlAXBTLY1Syt
K6Ek+t48X4p/XbnosppMET+HDJRCFFvPSCZYc1cWsL+wWQGxkXFx/Vc4XugmyGYZQAmc4FmWcpVK
00ABXGNjnos627HF+I+/hlQNHpARSz9sogeK4R6ZQYw8sMrHR42B7EsotDDzsGQvFb20BzEnMDFr
hInZ1/kIzK1eH7GNinUJhd0BtodvOgBqAzig+pAJD77+rLsZl4XJN/0tpJOGtzdEm4PUFOOzDInF
j7IQ4z29JneWQx9sme2sjPQHMxLfH9mXvE6RyXBs6+XFjO2nsBL4OlVgH2A3isF027JFdnZ+eyVa
RQihJXd4tIJo6W1NJ4ezX/z/J20j2UfPL8P1C0jg9jUPUdViundYsLQjy3y1puPPmHA54dtYxsxk
llAOtWerZ+tR9Yan0Dr+FlWUGE/BsqJys3EPTrXx/QptFKZb9IpFsYSKISEvXtTjacqGFKIV31QT
dWsR1eVD3J29AeTzEw1RhG7BD1JlNc3SMseCZvrQ9pF3Q53CO0p60h16tWw6l618auVPbljfwgXH
6KT66zvmllTF/jQ6KuuH8KvewvZJLlwCa8WabhV7dabefageX1IZ5X0XEycbrcS60fluOHEVnL0F
52ZO48BRToyEpyYO5AliAdNbqWy8VbJzZfs8ULpqEH+yUxpLS54ZXh2LGTKcM3oGdBW8Kve1qkpF
zlYoRSp3dqUh7tjvf6jGB73sAlErH/F1PY6nIhmD7CQh39LyqL9vZKMDdn0yx8rmUYE34n0UgEoC
htuOXLyiVC1AJMzblitHebSDrDuOUYPhAfH6xw1qyOxE+jTF1qiWu9ss/hyzqFgCm6gX924Y7GEA
JW4e166LrQka/4tj2TbpjiGjSh4XTvuvtFjR7QZ5pCieus5VARhDwVYUpDMB8Ry45cqtvdy4rqyP
jvAcwfsNS/wYSJrwMezXmIy62w3HWKnFAzEJ7wQvibJd1CSjRYHRKLVJUpOyUtj8qCfDhBS5ZkFb
Lte/QQsQO3awdeGVcMddnc40a0FV2Z1ow9rsOWNYqsHLbt/wgkzBJfM/9P83DADYwe+KVizjkKAj
4P4agd8MA858I5IwgEFpovp9tE9zDWdGX+AQXUefJBPyWzXbBPXUDPxZvw5x9oJ5DN3Iz48w3bg+
4W9SWDqDjIgaqlIbuGdBPkDtS7YKjG/52EftBKa+gJ1X88zbAuHc1kpEj65VOSuXGtmoyiP8oMxE
2x+9bwL8HqYwp6bLKWEm2vYliRPoeAyoD8kBY6ZHvmZpupOrLPecbDUOq7Iqq5nP+BfrBD0j6R91
V+rJRvdoFDRCjD3S8sNMn+TMnQU8xR1ceTFPlmr2hM4XvER0H9ChtacilZIeE3DVEjg+r2SyCs1u
1r+lrc6o1h2EminrD6RSN8gx3aUmB+zs/HIcnDxzBfQVJg7ldC62tAlRFRfqpzBdjkFM0APBmHyS
1BFpjH10MRmGQzTv0KmHa3kvYKPymmZo108ZHyOG3cyMgYOsr6J1+AkZy0umdcABY++rhhonLEU3
vLQ5RMKLNzOixQMT5Qo1lhIhgUQ984bD4FAC9HQX+OU7Gmv/j0d3F74khGqcp7rPIOQ/DydQ6SAE
UcmtPP6Qjd7Ik51XUGriSyhQA8km0Wv2P6VgkoG5uqSCFenRLeF5zhWhHswuI0n/SfaNLosP4M1O
JkWvLCW4z/HsE6d1LR7kTbSk5OaaoumB5gNVTA6nNIm71GR2NSM24wEUrTQcyvQ9hAFrXATdpHC0
8y3Prya6SgkKvf8U6T8t5WtmgKCzTbigzytFWcfQcTvdtNojrMA8t41iH9duz52dNZTjxpiD7Eqv
9D/JUavF4ZmfddxsigCh5C13szbuGejdAqa+3+N4B0Metdjnh1OVhjo/YND2lbtsd5ft9AmfIbYv
UYwLpGFtb6xpl6KyccaxcJgrHjdMO5CAB1Klc4Y2KxMLKj/y7IY42Za2LeRJKVWZ+D3fbczL4MRy
SMGig8w80Kc4T3lEAvR1DsTTnVFHQ6BpqUzInFKUAwGF4kwBj6IRyF0x88AbyO7kml5IoWaJGcqg
OsvA84+ybCrBftUAcRy2YDcedveTmPgHda/Cnb3is8lJWprTOiSKahSm2WN6VpFkXEPk6955Mhfc
HMaRrjCgT3zYWpuRyHIbkE4qoelX07jTPj9eNJGGu9MYf9C/owKKXlwcjdm6lalCAbhvIUmXDVtO
L0C0H7uPLVYLMf0B1KIg2gvxlys9CN045LA7KCbI1xf5ZtAKvp2J0ti4uF5M4xQAd6kpbJuw4gZa
YMwiXQ4yiQ8WrOBTX8mtZPZoD6Eagf/lz88/igRJduyl0Xc5KArKEV0iQjT68vgFuBIyW+pchi8U
NOSUS69cOSUVgQEO6y3jQ4g+CCfuI/TI16qP0/wl9DzPPZA1Bk9kLP0UBC5Zfq0d3NlShkZ0iCW9
A6JWpjTts3x5o71+GJi0mFYBPquB7omhAl4ZHINDRWLDEyVQirVaRCtsHWDoMDiry6tbMChTB5pT
mb6Dhoi3VWAl7qu6S9I2NPpnhhx181Vro9+cwR9WVXD7mJ2yxLeqdNWImGb2f55uMMeTD6nEgoTW
8kDqsAU+DiLjFb27yFZX8vpFh2wnrzUU+zDptEHBz4HEvumrY4A6L+nvgRd96Rgw1tBPmYMZ+XUy
TJvQIpIN5aAnPX3yGKtEMzBbxz/Ks12GZY62vQq3LXHtVnNRLX3ajUcTR/vRhW0IFDKRyDzjx+Nd
8X51kZAFBtdSPnHdvpX3JFaOaKFkDkpKqe/7Hqntw7nqLq01nuGz2RPyNjeHx51yaT/T/mVGMAEO
crVaLz4Q1yb1mB1Xv0cGJFPHuK4esMjIyjbpUjOkzZBz/Z+A1Eg3bZASakVzz9gf83oRzOQn6wX8
/T48riEkgJsTPkxUYA+ntScG6Opaa5tppXkZzhgEx5ogMaf2ObC/YXCGw178JEL4iHF+vjW/DPKM
6wuJ+2kvArE4hqHqZKjk8kfPrK6EAfFmKDcWFdNfA9B7TiXcmVQi+TWZqSTK229WCEGzJ8KnPFli
a2p/I8y/+kYqKb1voFljZScU3GFkfjGWa2mwuf6Nm78bkjg6mMDAzp8E/+OnS2mAFkSPdVOuISZr
DPdGn4AVQNB13aCsJ732JLaA2VIsqFEyeIJ+JzWo532wUIv9b/AAvirc0ngB/Z2NgrRptde03Yfy
ooiW7FTAuy7G8hBftOHavqhtZpKnNlfbMclFQ/8pifAyZrmTl+bk2HxAMZJqXRA+IoMxCSYEtlKp
qz3f2OClFmA1YWA9O1QKFrr3youw4aT2wp/2RfGyYlc7FoKSBLpefz+xlNlrQRL9t8araO4VfiK/
9q6oItBBLm21//rX4XhTK4ZbhF98MSIWa/q6YDk8jF/BbjdVsJdQo6ZHdCGO8SeybL8LhoviQn5f
PGuuA18DZsZfP36YQV25swY5gMEt9BLxlTbFe2xVWmRjz1EzR2jQZ207jknC3t/1Cj+sjySOVhgV
BD3K4db5JYKdXdl9g8hJlO5B/oBSzzB7Ogqqmr6TuxB1wfwaSwIika8JcXB62sIzUO4pV/du8aCE
HN6JjnzAz7Yxdi+room4odIkT+6yzx9d/0yiHZq8G66hKkjlOJ9VpEakuiHetD1SRgnXXgdM/3Bq
kjboStlrP+PQfHFgCP7GFMVmY5IqoRJXdlYtlbQizqWts06cUhyTq3oD0UwtmRhogXV+qN3MqimP
IovS45B85Z/+F2tqtBiHbMeIlD/R09mv+0UYZwhyGJs458Y2Tj3Rkv5FjoRfJAEzszveq+zXkqyA
VidLTWjJXH0+/bO+kMDRsRslzu7x3+IQyJaND4usjo2vH3s2IN1A8YcquOweM61k50nm6jbil+fB
RZaOHm/MW/rVkDHl8DXgD0L0vVBE+LTVR2mP+FxSopUSMRRqZsRcnKlNiwQ3qCWWhK2KkpePWf8g
kpyUUkQYBCeTOgX4y2YEGUqtJCT+C2XTIlEx4Bghbt/SRGnuxSJP5lQwJlTaGKoXKhojOAO6+Lq1
efrI0uL3Kn1MRf3729l61UK+LxjQpMBwS+zjME4D6xxCqzRkw2+rZHHjtx8m7ozfvGPzMRRFINir
A/pvH2vHMjaBjxblTnAiMOemNF5who4XttOjUTkeSXA/bYdKvGNoT6dLT8VIpF+9X2EKAZ5KeTzr
MsQbe7GR2nQPRFEq1YFzDOeI9uGePxTtGBgh2zh1k7FVGr5K2M4GfdfuAHyRRPQ05VE6M8GId6Ct
tZs+B18i2U/X+J5v7qpNUgYLoj2eVyv3d2X4wFWzt5PrWxw5Dcdt9UWN4mh+kV5XZnXQXWQwugX4
X5Tcm9fYgtV8gys3fSXPMNMdunT0Z1dTO57xKPpqY/dKw1Uchb3XU4vuEw6RO+VJ1RK4aYPw7wZ5
anWSDEQSxL94a92ioCNxhD7fmjNkBU+uybqhEhvVh7BfdTe2cFUJIRaY3FQ8yDk8399IoibewYhv
Klwr7deipAr/bpQn6K1xz1luDZHCDZXeTd0v3LqQ0+CT4rIoDPmJuAG3qkdBDz11TucUxBH+trmk
8zbpkdWdKSsNDG/qGxgqtQtKwY3fkMEvlbR1yWRJp+BBt8LJZTMTGLEEUWL/j9PXu+W1pNYj7f9s
0WYkWb2gQ8YBEvJo82/sLWZyW4m+Y5ZqGXOkTuc/o93Hv7h1ymc1gNT/JorwEEcoJ7nAhie83AMj
O+k5/etQXTrPYa462holjIOww8iWGuejgrWU161gH4dWbZp5J45+F06wQE07zLutqWgNfQWIK5rW
19qC6HZNwhIIZQXkS91shkpArZTNCucLgyCP+cn6smlJkfiCogXW+pwKm6B9B6EWeK5NDsUAbjk3
/We0rzOxCVusbx4S39xiD3ryRsc12MFUoa8Ov+zVD1ewEs/wdf+Jefelvz91rVXu6zmkMjvn/CRn
rnUZOT9vDdWMyax3JZcMYLXAWTrdqh7SiMM3xHCDq7tkAEYN+mljTxFZbEJu2cOkhScWN6uM/4JC
bJ5X/j4P6k5ITMzLN18+P7KcpXGF8MVZVCNdjACKE5LxFHls5rbw2fLpkgOMnr4uHiqrMqhocv45
0jB5Y6/7CvwE78K8mCoJk9eUe9zaBFihACDXeUzi/rjOeLiTCzbr9q0JIk2kA9txcXdmIfbqpB19
UyaKxSq9QeFvGsCYyQHonIhlKDRLxDuLgiG7MV+0Y6+h4mBk12rNzbhTSLLGLsQBirIgrDo3pRpf
RqTN340H/2LhIwrbFFI+rTW/GP1cVhedQGC5K4PNLVTGPEjTosCr00yFvQ4JCoUGc1YrOmvgMnmh
W4IkTebDG9VR/INrWw/YyBiPorBP5S++sbs96/lK7txvMtaOByAaNv69vC3zxEO8TXDVmgxKNbZ2
s+ZHMLxPILwx/RZtR8RMy6G2lef4TIeGMZ9cvoHrVrBJKfRUaLERW/V2il3/7YPi++4aR5C9a8SF
XiE4Qa9udI+yVjKgLrdsyl7Hs5bzUicWnNN0Obrot7i3c8wY34vk/z+D2dStLBf41uQVJX7Udiy7
E+Zw7ZR/Itg+aQikEcDISjLVW3dHiVZ5i4tiawRAgRaKeS76zuivO7YMgIm3c/QdeigScxLdlZsN
+Fuf78EXfNqt+0HrVE6h/73ENcF5EctrDDhatwf2odzxsHrgoSfnt3aQg6Uhso2nLE5D58FojT3W
iAl9X7OligZEzPU+EArXhB7i2sjsrrwRJZLzB7J4cYlKAXJFbRCmrc4gZayKPHc1vXY0GAIXGKCm
Fo8l62tH8mNPDbZ8SVv1x6N0k5q6W/9ymWqvPbr2NfSo7W3kllGjP6o4z73n2lGr/T0F3L+AOba3
NPpe9kG2iwXE7W9X9p1uOyqDHNrXzQPnHDPjx0T84ROOgauX4QGaq8ylCfm2QQ6IA/3OUol2VVBt
cdU9xoLD2rJuVIq13dCQL5c8SKpn1UCz7ZBlzrm3FaXGhMfFQ/hmfJhrFmxbeTbc0nN+ub3vyPAA
kx4NL3CNLa1Kgho4A2UHDmf/XOC/3zs5KHZqUYlaXP9YOl4vosnve0GDCFnvIC/QMNTaGWvLCqGx
CwIT2UqAnnMhGoFTbXZ9MfeK5GO6nO4D1akbYb4FxfBVWkEpLh0veIYopso+3qvd9ffeQsIwpRIV
NDvwfQCD/1NkdGqoIhonVmgTgYX0fNeU9u/xglGxd8ZUHZfGduAtJSEupouQvWYCWEfmZKfBd9BW
kpS1CRhS/yM7/hmbgFVRjfksJBS/Kqqc2AkhdbThrtl7EzuM2AZGMc+kCfsDqCCJiUTCbE1UDNxt
HJNAyQhpKJpMYkQ7jN0T/CJZS9H0+ZkdrL9+cCQ4ltxLyFPRovj0cWdh1ecIjPZH/5LuuvoGkih7
E3KJRDpnD33+pNu67Bid/w62NyLUXfD3vdYgBG/SKDCc9BnPAkutqy7Ccl4sCjqSZY04+P5s3QU8
j8QJbePwQobY7TT1gABsfmWi9aMQPxU56WLw3avmSDdfohHOFW2FmXfmcIUxQsPcd3o+vMTCi4vm
RyjkeyHwsh2Fa7a7+YhRB/QUTjwGE5rcdldSsVIaK8GDx8BmwA5tdH7IoktMH98UKqSvw8diDAcQ
2QvBXrSfPjYzf+WMWMKVvf9HCLTXEwyXfICZkcgnI05/JVJ2DAMYQo+q8Mu+1N2jVPH7Ik3NYPhL
vtrisx1EEEyor9+Q7Wid5CBT8e9TpyeUdJhMJdFIl9eBngEef9B1TijyxExSGDK5W3uNATNRDo/x
dMBiF4F6mzRckQQDNMo/Fx4HlWsujv5iWhY0VCZgPVjovixB8NdEb+GPGhvKIf85xE0vv4EIK/5K
H7i6kKcuj7JqhKBu/Gp4Ov1ordaHk9q8B6YSXEDFkJnX/HjRlAQ8tgaCEQQ8HzX8IVSanmBzirWS
L3QMTeYk9l1fH9yjU10vCOvk8IFkdIdO/cSbmNxtq28iiZVNZoTzxwB/uKFfsIuG9LNJxp9AmmUK
igPm+luZit3xcOLcy0KOrrMzbEArD+T7YVjCyupDyVqulUFgWY1Y8//VlWW0SOKnoQ9SgZaliQ5N
1zRS0B9SjVTgtbTV0DdYU7XNR0dLmU40uQ37FM0WtSbTRh9AQIMgtf+5EWRYC25QrkWzfXYs7S0y
18WBEbL435Rr26yVZCOHb//LfMOZOZjURpilm86QsTJL1rdME9adWZcdThlYEhzDuoOqV+/V8aVU
MZJZ6QN9rjfthxb2lvq3qzRbek6M97QzVoUSSTw9mzuB17q3DlApdTJQ8Tu87CR34A17rWwzjHIF
DpKWUo9ogOnoWtVwGyq7+p5Hbr2PQbB5j0H+R0r9HxMX8VK0UlDGjKSruJbAepI2yZQKebDIsQ1n
v0TdLqWXFvUnJZejRvOEOeaKjVZ/WIpj4zFtcCCVAplnGY7xboAmUSOVTdHi/drOR2/ke37NL6/q
l7kXIyXx84K3HcooarHTxVfUpe3r/snPSj8lxpUD+F7LArBXmZq6iJiAsOxMfnVrGCkcbRwyKbRy
3/GyOZYbPtYnZ7vvGQhm5dyqV2uHFc7ou6i9Q21VhKfz/cy7ISVEq7H/mRqDLLmqxWqAPqfDL7Wk
023pe20m3QoElIorDAWMpPiXxj6/thR7pCm2moXjM/XqdSrB6gufJUTy9EZAolo4qsyGgoDplhdd
UJ/8k8pCW5xtzx4HXNiyblaasLzpWJJaacbK9gbMHxCtOECpGSab3Qj6vLxR1EqCqEA+aIeSK72s
Cn5A7w2lD9OUuN1rRzSAUbRoVFxlWBG6Nd7RHi3oY4beDnHo7NGr6Cc4jxtU4urvhbqx6kVape30
Oqh+WduX0C+xXSGDljQtd0loInctsZFUpKV5u4VzbE3ww4hLFEqcqaEPICpaiGBfOb4hi4z8OXDa
9EI/9FLrcrIMqLIeJJCz1Behvapp4k+OUhACf3mzxQp77K8WmgdAK5U8IgYWbOHaRy+UQq209ujK
HUSKQyd+pn4rMxPQxlEIw56ClRd+MrrMj9RxF3UJ51/Pfvxuxr0X/Rj1LattSwSTff/7jFqV0yt2
pIcQ3+3QrqNf/UFf65l2lZzSnAtwCuUFCH+KiULzbW841H3CpfFyB/LyUmok6v7krMeXA0P7AHp4
lyhCLvRHhROkpsMrUyItIapi3SRS0HlaOVAy57cLanBGedhvXuXhhB72+PqzQUuGXuk6BRyfmKrZ
y6v7K1Xzfsc0DVNSguphMFGdKZjPqr4gPQRb9cj7kTJB4zoQ2NAQzGnYttVHzkuRKkGNJm4eF7kW
Jnh+AjKlRcnr9xB3QXpVTj74KMmmPgxjI8s7DSzJzeIUY4WX0Fsz1JmeTB7g5TpmZavXDByqj1YI
humBzysl2OtZnZo6ReDOCYElFsUiHvaR4mIe9l+Ac/wnPf1Fa7Q06+rNwC7SffQzOK5jRfxj1MkR
/esknPDTMXIemYwIstRCPOOIWKKCzOuXEupvY4Oy5XQhzEsIX0Tg1TsK3Ih0l7nwDjvufG+gdhMG
mKFZyF7O5lj8FypZBsXEMpRtAW0NXFzQm+YE/3qPOdcreDFtGCXTwSKxSpW2q5vrH4Y86GmUWV43
b3mo3fWCFVKH+5eACw7n3rdxqmfWPqjtLPs9k8jDmCSJGyxx6lEde+AnyNlsmDOROxKfDKbXwnlN
Ql6kIKe4BMulmTpXe2LqGjW1P59cpGCnn7ERfBQjBjIjeuEdm4ztyi/DYU0zW5SkMhSPWrRe2LqK
1iQ4fPeGwbTCT3cJOuAIFdECeJMqAVDtK/fToyv/M7cxT0rzgewY4crfaJ04yP6HYl9sOnQSaev6
OV8agNT3R5Xhy68bngpGi0xiDvO+pc/HDYz1YA/L7AYlSZ/MfA2Z0Yx4Tx2rGljHxEpMqj/eF/7t
u6IVdNoQfZDjAXDcMR8Jot0CroRIBZ9AThHhdHq7VQpFi0SayGxNyP9B6QjdfO5mog3yqVH1ozUl
y9dlEcKjn/sPFawuv0sFqn3+ywP4Y+0SOAaKjGWe702HIDKkQJwPXJQO9rVAT0lrG/RakdgO0FXc
NjDbu2vR3zunR2pkb0eOD5/kREYF4O5OtEnx+2dKDHyKdSrH9ko0NSnRTN7lDFJkDoXWjg5oieRb
U1STigF90qLh/X4aymd0zGQcoegN+zP6JPUAEgvPBizVCMe1++zix6wikGFQl1gR+7Jdnh6nPJfp
sWhzw3qAiHlvv65JarMhZLxmXFYcQjXm6WL1i4DBma75yOkwPoAALL9fkN28qmmAgoO3K46lN7KV
IReyzIY980sg9sDK8QYN4/tfcKYs/rlqBbcpB6IWzHvhn/z0SEvOqBP1Ws56DPHHz+qmSidSXlNN
r04TJWLn+MbR0Ub0ECuAWrHOFkaj3qcKDHVYUOEYcJGb27bDOVyBDK/FJoiNMgtvBb9t+cfOEf2T
syYGpITvgIr/xfE3p8QMQJtWeK57D3UURHLTTkB1oqIKIGYGS/nW3tSF/u/TRetwbVNaUoJOPwvy
n+N8mVXN3UWWrvvmo67zlTJIHG3giFBLZ4bbIrGg4f55gzGu+QM1paCGYdKLn9jC+p7kJ79d73iS
gMhSPRY+Vh4AW0XlYYpcI9dxe8r/hSsQW44tcpSTufP01k+bYfriMEX4f4DVegnNJXwXHwQHH+Y4
oT8JG1cW9LxPLMCIGIxxJ20Upub2WGNuipPN+LTBLNMA3HoELFGJXyKyhQueYpE4pWlSNI+yGGQe
O4nl9YNWq5GQD3oqfigDBYHHsVVfRFsNYy5W5LsatzRJaxJ9HKMQXaAp7cDKeG8811gZhIBVErDv
rxQK/d7+H6Cde2U36JayF5G69qB7TT0GEKFfKDQ8zv35Kd2GGu8vaj5VWnDsMiEC/hcuEl6qy6Nz
Qu6d6F7C2Qq9iHK2cvcP4ATsmu3bNl4WzLi6x4X6bEXxi7KnwPAb3ztXTbbcgawPn/vp8URKDVhz
4tQRDevdOHJWWroskZRlG0iR0S+29qyNnJ8Zmt05PaxWc1ARtqPCBANCOo2tvwa4ctGK4lEzd4yD
xFR1lU+nFNsYjbdYSEfGEHhDxHuPS5ET/NTe+XWbDYfnCEVznUXmqIyg+lafhsUSYwByry+wuYFv
8NJnxnfCY1nnV4CxTlG5r1/7yqhVAc/MiStdyvVsqPqDXU9ddseB6cq+LY6P2MNHfe+34YrqVh+B
SaMQQRI66HfDVEH6CFpumd5Wfdg+v21/jYDHOlt5Y4PM6hPH5s3gKR+4X07uFWCBpfq5/EmfBVlf
BTlAiTXg0aDoiU3IjxMSv0aLguY+hw1SfiAxdKFBpOYTQhsQF1b8gt6x1d7zBmYrVUIdsykM4Cg1
ikMyodN8NkOr/6creA1gboICn9DAwbkyk2fZ21Y7p9sEUZKq15cH6nDoKh6NzTcumVwiQO3w/SIe
9j+sGPZ8gEo1NIL5GxGwpYqHlUDVCJCCTnUWIxzUAjHOJ+XmtMEDheBpQJUT0xQXR7uiOWLk+Cfs
jzPvuF2R4GS4b4URJXDbeUrIMuIiG+ZLSs2gIG5SXaKD6NlmLjuza8u25CAzEpS94SfrS8Pd6ZaI
uFTUb4ngy+UMl1so6KHXtuHUY2Iu2XcH2Drr8/C0ROcbYUnpm0ro7NWkpCbyVvRKLu5Qlh0i2cy+
DhwGJ043ZMUgVEunfNWHf4G/5kb9Ahmpq+knCrp3G5i8R8Fxb4t3kDsePgNuucnP09W9vbGzpIoO
nUa5xUmZ77xYxKms3G4FZeYKML0WWZOzXvqI5ZQSNR4uQfKnWnrtei01xvtgZSFS3N3TUAvSbX/C
8Tkb+Rvt6944b0rFVawHv/yC2PFrhjz9mx985aXrIzLdpFPNRGG2gaO9Jyea9HHr99s6kvJggOMP
xf3N34kC45umUymFUYtb2/4Wf09p+lBD0myEHiMwwWZ8mLDvLr0LlxhEAqXwtdJFOAwwIuq2TlL7
/axk190q+wR2SOBJxB/D8Jhc1Kcq2etM2htukNhC/VkVF0+/UstLVUR+lF76R4SHCRXSG+5Ls9EQ
/jTWyyErEjhQyDRBjM5HtxFjmRMNPeEC40C4Kb16SX2YEfh6BW1uqTSp0L0lZQvmTZHgbskLtwwo
ttriw7lVWil9biAFc+sy5cehOH8s2XzOFfvaL+oPC39OmaMPIXjbv57Gds8v62laMQFgqcdTyiYJ
i1qYUVYfT+RbW49RbAz9YJTbvkacknkZzzMoKci6ncST3iXRxHGzk890o+W0AToVENU0TsVh9WCM
b/RYcCu1iTkiQ/ZQFs3A0/GAWsr2yLaUGtcD9a8jPAFCYjrxO7CAbbgiUIYSAqulduo6X7ulTPph
JCqiHS+xePmZ3hSpOuY8Uu+bi/sAGsdaoOGC+VLGN1s/UNqGYAh3PPzOSuEXz8mI62HEFJ+UqGsf
v+/+vYwBi1X+Wyw7LmOVpUwGM5tR934jTYrcepgpCT0wZTCjg53L9yKB0zdlY0eTayUAx7dRDCqX
Py3p/uS91WeUb7pTpn6m8TUsGmH3iY1nIU0mGmeinChsVw1n2kB9W8FDbsrI656wo4OIWbTVEhNd
WRcPMXQ3BIpufDEkVHwRQOStr/EPMrP+2L5lFlvli2VKDkwlZ17btT6Mcv+0+WPsZNU8+C0LTG/m
EEwuA2+SiBNm9aqwQTfRWJdRpgeaawMnY6pVmKaIgJqx0iEEaDBeWLd6Y0OI63AaP6lvBzzJLeZ4
S2Cou0KVBvpAzrj+xuThyZvvhwW2cvEgzGOQcmdpKMBHdHxugZc8mECyp2vDkVMPpu52Uhs8NdM+
LNoeZA92KtT+ABMyZpHpVyDTsHxz3fFxsMkcLV4sb0PW4Tecm8D1u1RaiNSmbDnodv6zuv+au+wP
hfluJ2PGJqSsJnycwdpAIy8y3bxuJYsRx6JTaHaql+IluDPdS+KmStDFprjHHYG9JI+3pS6+kiSm
shMwjY8TqjfZbyh7tQL1wu/eON81Vtble8hm4635c+bnxt+uvlmcQ1291TtkeHRhJwj5fU5JEd4w
tqNciHts4oBR16vEkRk5ubwSvgCjIvvrNsU5Dnl4/t052OezTXel12AWQQv2zFTDZBwNBS4pwy0z
GBCecTYq7yDSvkpRM76vKroAwmU6luYi21lRzxirK1tDi84eypZfw0g2sVu/qYX1FIjPB7xgtZQm
FiXQ3dTdSXFS+IcpGgNLPnb+nB8dFGyMGM2iDjmwzErtdy8RlMHuHdJCT+Q+Uv03El09jTrx36su
mRHh7RqJQM2bcCEv+RMaEpmVoDlMOn1G+VzodQvy2mxp6EEo/4KYKETTaTGDTnh8KqFCTTVkXMe4
NMws2zyUbGkD5H+dY2BPBoro3n80qa5tNYyFfNPq6GElizgqloIJanjfz/pu49ZoyrnhQtINGLw7
KE1FEzKSyvkfz04tiekbJr1+KPSoH8eSnmdl3ebh4VSM0cB6UdeOYgl8b19zVwVCEzGz7PHm8mRk
w7Fc4qjuC7BlzMAZ3HPwppdgKqckZDJGiMWyJKTK5qcZRa2vU0mfGdfJso+DoLc6boenf3fNqfAB
/OlMp2iHlx+m6yzGzb+pLae+E5RlN5mrF+jyCw3SFixiPpM+6tKp6kYzWNC0snyr6KIBgILc76tP
IjRfYMYnkghmMRiUPfnJ8YDSWVa/jROZcfXg14teWWtMpiL6AWlY80pos8JK+dhd8XnXbsfwBxuN
Li96WDHvyqIQL/hhoFYUrmd+bEz76QzV9gUNkTuJtY+ofVfE0ONmnNTjVcR/szaYJeNulD5bmCUH
5r1iSw08Y1T9jOVLcCxSwKohm865xD0jIwjvE6jZoQxVsnoPzgjJeMn7Ethec7kioPCBIiqCHo7g
HvyHeQ4aMLkMzw+xr6GjGC5in+HrttJjGGro1s5mjs0Tnly9eT2kSqZQYBSVPzwUiDQrYT5WUfRr
58itMqEeZ2E5rf5ByggVOnK8TNk0WHkBlp5Eg+gCS4tWmB5I0FmxVh4CQHPjjlihrtMkmW6krDil
v7pCeuO1T8x7CUu/23SNehVhx3E647WrCFKBo5934auOvg2W/5TiwuLL65JpRNhDxD7woHpKF3++
yBIL11po2WZEzlTUJMCPUrtnEyS7Ynv37HSTfPNixG6F4WwFTr0PDATKYA2P57WnXEeoeW7RU8td
dGIpkvwNmU+zaIsC7WBXn2ODnxa6pNOZIS7LN3u607FKgCz/pITpEojGCFKAUI5wQqRRtTof66K5
WnML8QTnT3YTWsHpXCtswDdS+PuG6ueVqETnDT1xNzBa/BNcie9JaWfNg3l404MP5LWZ1SciQ0Kt
JQmARPm5PPukveoVmnmdPmurSJPUO88KInS9PDFNGw6x11vybiqi2qMtZBKX0R09bEUF1a8aZD40
QOYN5Gd3dhDczn7ZokZMZvliFbtc72g/OU3UYWwMEJa4RdNvzSaDS/b82q+ccZT3BiGyjmCMkx+1
cGWENq1x/FVaW4+EBP09aLFycYOgXOWpVZiwrwJBU9RadXoPa1HSbPWdv6Tp/2crgB0XtsTzCvuy
6+GRKFfJ2i8SlbpJF36QbSR/y+CPPLOyIn8gjVxlfaqr1KtOa5Bs/aiKujZHXobzKe7sQM8mLeC7
IylPBnM8HzNypS+cmSkyFDKiCz3m758z9Im6DrjeV1B6r68OR2BS3QianpG3mEQpx9CnFqFXatW6
NKp/lyTQoKaTeLDrjxmO+Q3hKShh5ecr0Qjlq5jS1SctdXWXSfCrdoxGOenVJ2gDjXAHYjlacCZR
ZkwoXNMGn5hmDLpW5zkFnsHbCBemplhlVaRSIlySM72WRT14DPHQSQBmAN5uFHPl0nXHt4QHOvCK
r0e6aaQA3IC+IFERqSAZsBqJTxnPWgVf38LyEwETh9Hjvfm2P/Ym1DFLjLtu/I5A1auKpBydCLaw
pt2GWHPPtb1VDxhF/hNiERPpH/SwbUgTt/URVi9uajm6cYZQAwli2FuVJHMWOR/pkxFjEg1tKxkj
kqkBwrh9qQDXmtJxdHzezQSEu7WsmHYLnncgfGs4vYl0CUiG4azlA2z/oNMYrkLe1QmZAsa+sZUv
mS8oTiKGkHKITD2PV3XNM+HWNRRZyLUjP+3ZEI1QArvgBSSZcaB3NN39ytrlUmZ9CHwHkscPsfMt
kFXAv/0wukVSnFTtsxY3ht+hd0Lh4dgx/BJnuYe2lSvcmkxGvIHCc7a5m3+ndGh5kwveUgYK77hc
wip1sV2d8IMKQxBXJRDrCKOutU1jdm2fgiP0ZFdzyk1hv7Gv1uf8Sn5Vt1T/vzdAyswi1atEryu9
NviscXjdiglHxjeK9uqFRGKKPucN95/E3KtC09bDiGp6TapaR1QsijR3cfV1UxIoTGE/Mq+kBWwQ
Ic44gWlZKf7X9vy/0TpfPEMfmSJ03GJ8JvKyz/l2em5GeQUg1sWr0talcgiOrsmPeFq0882qb153
3YvySqIPAEu9e2DOU4pogmZp5d84SSryTuXO3MagQ2CDZxZK60PL+C7TqiLy6ixqhndRyhYp/MFm
QQlYB2G2noLMQJ57s7l6J57/yfq5V7ECstiQYqL6Xcq5KFwwEYt/LlJjPcX17R2M83+mminjgDMv
5gWzk273iyJVnJQPhISKv7q2vDjJLywvbLQxQpWwn92yIJ0OHnHLVVMOpZwLG2MSrw6VvDwenKcP
cl6b2q8IRZffEfgmjSEaPvckottBSmUTHwGBUG8y6TQkaETBGKLHZ3aS57T4smUDamIXHfREp3U4
6SvCTU//dfqtv7cWfU8UtuNMS86FmMMyBcMfDm4+9YmR3lhX3DFbuD7Bfipry6vFgjYoWPS6ECjj
HfjbLTal6eLVmvwozlASJkphGHLO8SPbjUF3EKidTV+G9oxc5cdDXTMWlfXmwWPUVyFiPB8YnGfW
3ls9h+6Oh10F/yquVD/70GDhkn6x1nAXbtBo0v9M1cnZV554WFm88SsBzBy45pKQeoMDGHHglr+B
XJ0Ds93DxtqqM4ib8BqRBXGN9U/esIi/ALxGFT1MfW7T5JQ40ZnVjm7KHNwp6PxvOlyyQqUrpw+9
cyHTEOLHE0ZZlcgwi7uuACVZrcuTap38lI8yqazYdqnieYxRhGhVIidXv33/J9CqHeDg8wwlsaMs
pXauBEzwkZ1vtgi0hJrHAcg0m8GQsPw8uAbE2AniNiCeaF26XjJHVewvPeT7MBTDkiRfmja1sbWg
j+fcfep3MzPE1hLk8xH95UovQ+OydzGMIjVNIEDSIb1QnzC/ALHoftNDWAPV6B0/mXhQ7B3lipPN
aTM+yb+Zp0fgkZ0RE1/1I7TWnUPCDXCIkrXYyBaSy5ppPvpQXjMBMeTEEp4moCdCxH8zK2ohpPXe
GpyQr0xwkB5iTV8hMPVHPMl3A5bWhGCUpG8Q8pghuASWeH/UWQ0uysiw0xARaCrBDG+q1M7l2zaG
NzVj9uQ/QFmph4hhTdShSJfMJxWo11jyT0ZhqKaQbv4AGDXDRXGUx5FNn5m55ctztXV3Xh35HvQe
jACyQY08WjRPmpjmcN3TtzcoF4hjvYIhlN810fcqB4HP6x5kwVKGTwZGXNEe+9es6y7t/caT5JtR
tl2m9XHon01f+G2PT6FpTr5w/9ZIQqBGamjWBIlBWO1CIXxO3J7oT/cPBFNzEutVeh1ZlZBS8uoa
GrY4IqDf/RqNwICbExhnH/0r12MTuSyullURfSE4+0uHMoRAM77ik/Bp1CELGYfp0CQizF6Qcu2o
PhdWH85GLToRIBlcHgJm/QmRgCdEp5qLfWBYw86MHminTWhcDY6cK48X2zFC34q4uF9OFSlsaXf9
2ltyEIzWvznHKBzn+36jZxlZ1RDPpqj5otcCJ5Lj1b8WswlvwuyY7GzcWh6p4qqc6a7QyyJPrxcM
D+LhOpfuWRhFn7iSnF4PZeOzxcpSIAobIN2E96owYMdK6EJpvXiPp3qTQO+T2jaHby2pGs33ugOp
ZRR+QzSiW5CorkgCHujF2CWXi5c33l3+3Zl5xwL3iNCHFZ7FnWvpTbSYHoqwjgSsULtXGa23jinf
wIlde/BLY+4s4VLVu7tE7mECXy1kFHq4KdZc/i12cSPWtNzE5nM5dXGktRKrerdlbw4u2HX0D2Th
yC+ATs/kiQeHOvtu7QeIFErxqcc78OxL2KEUq8zCX0mmvIHsLXNTAKyJO4BHxPaR62uThZjZSgzR
wH2+KLUA1sKCX4J5U4crYnKre/pIe6Jtx+54anc4DJbjJgd3njAaGBy19Glzz99ipelWjAmNfl8V
SN9DCixomwyKzcUJ2rgm+aVBt3dlDI8N3ShqS9swFEdGXX3AJ6vNwfwvRL2VUoQkl2SYxoJ56FZ4
CJgOUiKkzI/QPdTpu9bn/aHpNbgD88xNrzG8PZT0ug9GIcDjVkS22GVAQ3hSDGvYNxzY1auFjoNV
fGcuwA7/YqrG4b9ktdOu8BVHDSZMhcZ1YZLHXNLlU/rEJGQbXr3xPxtJcXiEXaH/mPsLMlFKCcLI
Xn80Kk4ToC0A04iEywsdQgtTjFtPPUYGduIaT7HiReO8Z0HF5fqT8WsinWUZdpAjv5sugJjeyHsC
9/SGXKZ8A84cby2LJDpNUyxdiTwITPgEgbI/f54pciKEuwOFecHy1NITfFmsweIjzXyTSyjs1q1M
Df2x4eZb9KkGo3kVgXZyeumzj4Pan0bGY+4cLWt8MkSlFvWxbD9f5WbmYXp94wAnTgl8lVobdqhD
kZVPAIp6h+0fl70dN9mbKBctPE0ifLfJ//XE37kdAMUOLeF7eDtWWjO/ocQ+5j8/3A9HyBlpQfU3
DD70Vt4djLxxoZuNxJd04dcGSv8iAO3YF2yJfqWfVMzozEIeocCuC66fh8SoBxW64gsbWUIsrL1Z
GZ1vYbeHYo5IjAG40PFNsa6ofNY7hO0z8W2eI/8cL9xDCfn9wnW8NhFpXMKhgob+oPBpS6XjAM2X
9yh1w08lRZw2ph2J+DH6hmrLBpVVy/5RPXCod5aj6IxRjAAbb8ZIIgg68trkuOC0bmiUkGBOn5f+
zHtcuYhtT8w7HVj3TewjTWu8KbPXgtx9p/lVGzVErEcOWl7/9dcP+3SfVdC2v+MQWrbSktLkcIu8
BUy3CZkcA7UgQMpjPAIvqXYWP5E/Yw2M+cW0FOAGjNkU9KSiFpv3c0MbVTOvOtG7m5Y9NFfyj2Aj
dfDc5dbBdNPuwbjLS0oIKpIhG4e3sM6849p+0+Mm0Wpt6sf8JVSgzFszZNyah9F9q5CVNwq1MjaD
CzaFzbUjl98YPkFUG8V/PUMYq64LOHN3NSAeET4UT7z7GAVznVbcq0dKQTcmXvlmgfeekB2eHoOw
tkCoVaxzTS85uoHo/KNAS4vgdad+G7tgFnGmxsMnS2o3OaUtd+7u9iHOln5Dtd2hks/MR6U7Dh2h
upyfdyvHEAJ4WiTxBFKxZpz+j2tfVP+bYk2WELf9EDHq8IOVFJaXlVMZC7chXYmmW2wDfz4lSaVq
pi5xVGWRQ62VeVE7Pqg+H65Yk7t3oGmfKjLHU2793ErLwUfO5lfrQ8cL+c+72T3TszRwTsvkKPSZ
jMdCLvf476TT9Ra7m9qZjBKvo53dBrK0epsj5+Hj8LBbnXnabE+X1maAv3XmLTearMzBWwpqKLMm
lzJ2Go8VB/ls5KKLEHL3ym3f243ttwWA6zUE9kugoOCbGUV79wV8bZrPMS2l86tEveTMQTU5tVu5
QgkP2NqshSUPbCUPcKXsuogwSn9bzbr8WsBIfLZqOLKrHlEvAeFvo8R4UqsRXN3BBYcZT2p794wY
91q5bhuZCR6hKSYWXwO8UjurmzoH5FHuXU20GauXqhzprF3AKm9RYNccaj1tN1O+fTKqkvUEJhFC
pAr66E+aAYjh+oMU6Jx1Empn8Q5UlzC32yKG2uTk4l9K/ZPuIUtfogzPBd69R4MFR+46XPaPAUA7
CSciDyCHjjfhQ9wtFWlnIm1T70MGPYVFYZEcyp2MOY5eCodK8Efp8hZEZPBRm9YJzGnaDzyKFaAb
+SN3kf9a71gjKRa3gZN6ctQhAlM2gXtye6Lv2s55hMM0p8OW1ewgTKEH4Ub30L8pGAuI2g6HXQ2r
fADpj1lRNuGB88C+ZLZv6k9ii2gKBYo8Z4jy74Gcqmpct2e4Izkg9efn1Qz/q3eAPswTsghFIcH4
yVJLZtZQzTqm0AQLwAMycJMrwem5Pc5VpTgNkNnjhL8ETbhoV8lRP5Vxy21d8GisesVzNx/9q2zh
+9QlJMlxTvefreZRPNCOqG7P3RlLT1mxH/IoMjhsnWRfC1Qfni1E938UoHbc9Un/9zZwDbcAabit
uRR3hseXhzLWT3XMg9LmbQXiyUvhQMWMFeRz9LN5VqShdbMlQBU9BqWQwPEnQbq6ZkjTqFER2Xl8
DQQdVqn+jO7u8pqdRH9MRmZHZhH6mG9485cSQKRs8LekqsUE1+s6o0DIlqO0/GrtDwOO5nx4ChnJ
cu/VFni8toENnYDYm3koczx20OdIRQ6a6vTu/6kbWvOZMzfLZ4veQr/u9onREuAfuhmfUqTmSRcb
nuB0Zp1P6ZFNGBWFoEk9MLraMvd9AHVjg5Y1qxhbdGnMhgsW2eUarX69qz20OeOHYZi9bFUmtAGA
0SwS5qtMRvCHXrAns5F90Ai4ReErxoUdTcRjc9+3dIy9+N1tgnSwqUVjeLYNo52LR9djlZ3MAxbV
QCSW7CMBC9QObm39kLKUcV8gWzaE1nQqO3XkLhhzpNDp1J/ib2pbI915ZieL9ZXhFdtLgk8qeIdZ
Sw1++l7WpwLqojobbtGDO3IQEl97/WTIlAe7iRAENdiLS0F4Mfml+My35BGX4EvVrGSS4z61nJce
5HfkQHGQATr/5QDE/eEnaHNWioMe0pw/j+sbHFky+f+ecCT92/5XA6bFnO6/eY+16eKh4rxkTk5K
8WJ0BHI90aNWyIG+gXSwkIWBHc3bdJ3Zt0d+YuciW5SXabZj4hXkZdUToLjzUyEnBkL+BlZbsX4S
4oQybaHw7YlsYjGQqMezmlmDOdsGDzsKlJ1I8HlEaYLXsqLeMeJY9Hve51EhSIXuepfD4PQfhdWx
756se+Bg5koNWFKkEeugi1c/ksczrDoch48CfX9qO6ERx/++dtC0XuIW8v9ajl0BEvmjsrWIieCy
h2JDCLymNnosXD0VHCvfpaWhnBtqYIOIcZTG8sO8WU1KWS1HAsQ4GoK1EFHenDAUdhAV3CKdNxnu
v+yhIrq3mkmJAfC97tZmBJGIKRVYt1ls6EMfEisxtNKM5a+Lt3L1mDj83XZX9KUFZ7K3mDAgdtuW
r0CSsbXuiaSGXDbe7qtxqAwMwRuQQT8CII6HBVD7+9aeIJ66x7xIPy8VUE2iAaMqhZxWII9CifUr
1dZQ+PQ8F2KBkyHOhZcWlObw4zZ3fJx2J/+rr+ZQi4vnFHPSMdykz1KslID8ugevCxA+G87Faw0j
Eamicaz8zP3odr/dcCioBRN16jTnerd/H3Pt1AfMSJIWL2iZk76e9Li5GglPts69jyX1QbLYfa9d
pIOp63UpGg+AwVlOSYOU8bwGmmYkQxzGvhfMf4claS596jeqPmY+W4LwGXQNfxMWlpClufcpmT5H
QqugfLdy9dgxN/0RXUHxjydOg7TNT9FiFxmyXeMVH+27K0+mJhTxcxTa1oxBy7ZLt+VCazPAdZNM
fdM/ryDvqBivwyg2YJFUmvR2NVoTIYV67QXXnnAlfTTLIcwudG80buYO7PA0+emFOwJfO4oDczwo
FtQNMjXPVV1ShAjRQRPeD2hKnAgW353Avs6Ghup4vffPB+HHDVFjU+yUHYKdiJgpxLotlBdin1Sd
J+yb0ttT0ttJk7sDlWOOsp+pgO9gVN1IBbdzg7DfU+4w5hNMMYvc8NpOUBrc0ASo2FCzJ/VmEbvQ
qjXYjNC7KN/PqldtluNDuHYihgHozZmSZ77HIayILIomRlQOOJSmZYIeWftxntVkDff1bx13AxD9
I8phsfDA4nMtLsiRGxk1p7s2vsXTbq19zzPn3GTXys46pZd1xecMih69d/w/kqKiXtDDM3EG+EcB
HodJgnR1FJoceGmYOU6p62NlIHOV3CN9Bht7yIk2n5oUWgLBIjZh9DvwWOLY4XBvCqVcKyh6Ct0r
XYNLzFt/oAmywmgb6nkqpTggE/k8IG/ykUI6pQX4ZfGGSQt64KBPNKaps65w2jDb0LR/rEMDXQBf
5mFsTqoifGafSg+DFJVm4QM9D1PNEEQAWLNyWLL3s05JOFfaxoRPA/Zq94sWnxgNMQlON+0uioiC
XhbudWcv8eP3C/ny9U3ovTvL2Uu9HN0m36p7sXF9xyFk2k7eloTdp95KVTcnYZBo/UrXuA6dKUUy
6yh39sFgMIoUxLPBC+GXVEQOQ8dKYGY2C+mwjAcsk6AmB0Eb7541O5kFHb9Z6gP9c+TApLGCobgz
siWt0wtgierO8HoLptm8Ad8oWLGDtzSYJtd95HvzAwMtbaXpXgdHsM5bG7qnDKYYv6yJmRbuRonN
hgLftkcDcMx7pfDM+Eg1k0RJEsHc2XG6X79iXiUvjyu8qCoBf5DVlSGLZBDjDAv6JB2LNott9iwt
Vyg5YNkNDDLVuPDzlWciU2ok4jjMkaoTW5+jgsB0FYKSiDVYUuVS6MdxAKQ0UPKilbeSKCDYZNjF
Yd/lFSItvqcE4uzfVzog/MNznUqW1f8BmRBWPGytEAgWR1yUMBNL9ZybpZwv5WEFpiLoZdKuyhC2
wMnVBoh54Vif22LowRvfJ82G8AVG3ULAdG06SkyM0p8ltph5eNxsnZzSPdRW2Ob1NtXAg0GTykoE
foJDRsU8ojGdDahnS8tVdvqieExLJDr0yFUxWh6V8yMYoYBVMhrRDFmYWXHfi72R07XCGwR10iki
TKjdwvNMQUakGG3nRa4EgOwSvfJ1x5FODCfF89HhkcDUUj2op40Sq/0c58S1v7QPSNb2l0VjrjTY
U5jbfQJ3hKyEbH59LE6Mp0PqXO4Tjq5bZ0FPrgeWZpv+Mco0Zx7hO7+75sd0NCexej9K0nO46AxQ
ZIT1vbSj9OMdt5yNtz68cuh7sOl7pg3zFE6q0bKOvLtZD+Fpy3FJIXGl0fH7AFJzFAumlY5uE4WV
Fcdk6tmeLbUrODaAmCvLQNOJW8cyRlkEugUNTJ72Mi93GL8x0zyNsJba/ruryCkSUM+/bDvJpEA/
tRTyTmpo1/MUKESLttc85pdboNHuQUXEA1wZQGyNB+KA+BNXogcAqAsvyVw0Os1ButztrMxIkB7u
Rahc6pLYEw6bFfcwOAbvPaWF06HGVjrc3tmn9PtfkkjYZIHfwEdG9dQZRQXJgr5DDnD3Y7NUwE5/
HpiROkC9qwQeYXi6Qp6iN0kHpKEAaGexIVFSrWDOVITx75nsox8yHYcflhiK8v37CWAqbZ6O+oHX
yqjINLJiGsn0bHgxA5iapdGp7iT1okSCaT8mhuBMbaNNZYHPVnKDqm0sX55p1hHDB0hmcXWnyocN
757cveXRTMuhfqf2cn4JlRHxWIzg7g0z6iCwocauXsCY+A3ewkEZ/Al15qRIlvkiWx9548UdqmiR
Hnbhw1AOVBaD22h1vKejD8pMZhRjmx7wjszB/1I9+9ygvr5+IFSgO0cK62Ic+wnn4n/ctyt4OxzW
W6Wp6QyJqJ213okAO90oGhmxuYi3BCx8GqKF+XnO+u8IPsVuMGmEykULzsnu+N+8gKMpNVwH8trO
wqBJOAjuZBLJgXyd+u31CN0xrZunFLKsPblMz3Q2TZq4nft9AqHkzoI/1tAPiV67HT+2sR9eEll1
MhpZXNeH+TBCq6xcjphgVNHmMT4LOLtuazj0l0hR/LMjA3HgsMcalqbgxD8krWzpi2ncuceirfE6
QPS4I70DGQi9D7MmmIUw6u1PMRzDs7e3MvDhJqDEsf1THcaaZMz+s/H7PA5mmyJJJYLCe0PiFfzx
b6WBZlhm6iWmMeMnyNxNn/CSxjUzkhYaCxREvSXd7su0xUBazymZcyklYH2/b8hze0xcyS6PJef4
eU7mieE5FxdSe8DZ8QdrzgeUKrHON9HA7VoBQQkffln3VIAvaFcGhd8XFysy8RsCgTpoWiNaMC3V
gIpy7cAjN0q18ehB/X2Am49QYvE81XycYsvpyEki3+lSvqJ1PYYgvaOd5mSz/UYpNmEMuCKacR5j
mBB3aWFk5mxWmPKPl4hNvhnNvIxPna02RYnCgxqzUJvf7M3CDUJ8FyVT2xPF+9RlgPcXsg0Mlz/u
HTEdW9GhLoEEHK6f3XUVYlsz3UvqRv0GJG+RmpodFPyWgepEMMfUfhqhaqocwTCBMvfVpAfrnb1O
Ou565EZ9DYdJB+0OyID2xWA+azLjpOcncZlhi6+YoEJ5AjZesvHwNXxtDzVvqRdx1uirmyzYt73E
CoHrFklfD4O3g62KbfP2TxkJK2T8ZSS+fzuV0XsRCq5Ivmxge8YuoLliUIzTKNZVepnfwxQXAZYU
fqU5CzGo0nR7CNW9dk68Hp+ey/QkBI5fMx4QBi4XqSnay4RXVULTaU3vZ34Vk3672x8OcjKuvjML
aogPEC0rciNulNm3e/KXiCnlHxkXvFiU2Kh5jZiHWJQwIyj4GTF9//ncdcPSJ00N0T5yOV3TAuNn
7v86aW2qsNtFMBPvIHGNghgNdBWcWWTsSGzkcEzLUjY4NmhF5QyVBChWl+JUoMD24GI7drjISswf
+qk3qQwmeP0eFrLIDEfknU2q2rDJHAjlypOzCUOoP6JofB6BVcM0SER9zaIAwYrLb8ZUkvPuGHyj
CbKt13R9st2eH1NxCyn8x42HPULhZKYyoDksJ6Skn4bboIsuwfmG/1KkR9PlAsf26CbeWsfws2LE
b57KwgSpiVa52/LK9KwyfDWmEehuRKs2VpoJc8bjUhvTqEnskg1kefrTf4R779H+DXhU2ugF8bqy
PwvtVevv5xCcaj1CiSGiazNoBRTFbp7aQ+nDFGS3kkwZHBjdY0LdYvDF121InsDjZyz5+/SXadTf
Nsl7R/mwmRFq1rSc79r8XueeylsvwtVHycd8DcBhBNgL19aF++IZVFbBNBBYWjaTdhKENlMziJVU
yZYaUqU/4Si6DsihlTHfqYMKM0YhIZAz4xkFKyknvaYexx98bXr6qI3rxNuwcGXM44cgQPRf1ZFN
bN2d5aWscmSOBe5+v1j0POdAM2aoc8CA78MQI8Bl6EHe9lIKVEiKqZT5oKgC4o3Ohe8FNRILsmPv
uXhOBjjkTJCYvC0EFVeDZFQG9Ju5L3QT5inBIAU2wsqDXsv1tJFcT8zE4DOwQEcqkTrfOHnSTc2F
nxkpxCl5YA8w8fqA8gKECqNNcAnUte1xum98Y0q+7NC819+nnVYbYjePJiiembzonc4tG4f36mUT
P14jV+haQIJH7bAiBJLl2/oQCBESgfUjnTjYwVTniGpUpf3m5cD9TIcFT5qSRyAXL0zgc6i4Yxu1
hje1nb+0n5gxWRotETqrc+i6dMqlFT8FyqCQZeEr6yvfgxJRVt1ncbeg+pvnY3UI+pb3+Q8QwHER
KhypYxS+q1Vuoku6Mv6VqE1tBFduUYf0iVPM9fTmy4D4JNcUVuTa6b94bR71NqKCtrz4ExnjBka0
JY6yuMThohrJViM7nxtVSfU/JYjZAUI5mC4q6EwHTmcRdFlIGHUrSNSrzA+515+jbpfJZ93utqmr
ErMkmr3oUMsSr6VMv/u3DzjaQDduSbdOTcn3fyl0RVe203+b+qsswrLjuSaub2s/CfGYt1SsYHYd
PgaOrzkU5K0J+w/odzNQ+zDPry4TIew9yjMz/6hsUKXF6idNB9Ut1f5EjJJR4/ILuU21G+IWFjK+
U6Ew5bjOy4cmL0XBH+qVdLxT+ZJ51TThy6VvHkRhh123iP0F+5evgSWkBi9g1QmNSJeUoedZNbxu
vPfcVmCKZ17rLcMYnB35BJQKpxEGsJjlsQZgW+OfjxpKGbDuqArAVaxBpmNeu9M4idLeajV5LDU5
16NM0w8HTwPwvloLHnEZenYP0nnlc1oPo6rN2ozdBDrWqs3H9Gu56p14mpj41ynhC6IZxwQZjuch
pbMl/ez94Ptoz6cuBVcU98FHv0LWp/XnNk6NBO/Rg5ltdN93Dao3CSBfOTn32y6kXsTOhmcIesiq
XWkNvS2RV/bNt1kYaRPZ3uNT2eri2Wthbt5U6SbID/NYTaxZSmgBsa330mF1SaR4evzdFwlEqe6V
grqkwy/yJ3kd/+XqY92F/qy3b6te42m8bSV0YksueLh9/ONNFTVIMBs9JqBY3YFq28VFUYVk0y8K
cvdY1w25ptwcqv2ogsaSIv1/ZEFtjeWbGusy51yFxH6DZGEsieaytOzLgaENBQ82lRtQmx3rB5OP
BMhc/85K0f174aiAXg7XaPKtNZATJ9TXjUHnkRPhN08+3GUQXuaDCrcxNC0FY/viIXhJxAh3LFMG
fCmwUp8jrybM/zvY2/3C1k5UN5PbnrdF35h5qkWUAnzl3kZQndXgvtcfDk4gdbtfQmW+y12u137T
ol2N9pD7MIrHBMj12TYdF9DHgVHzKm4s28LzBgNJue8wgwobQS7jZbcX31hKoykjNVjLLkTlFgmt
/4pSAKlKIFNduxJSeAUD9n5+ubbh7f6LdC06AJlk98BH1S3aTpxd6fHve5ZsUP6CJkjft3Kw3IvY
QvfoOM3H+6P5fwynel465P/QLidCOj1s9nY9CMs46lU9gSs0/TJu8raQs+LkEZL6RCutsCzuCpix
jUuVX77vWr2OW7dRYM6COaBBXN2W731MbNxmPF+y3om5G7X6yZQZrPGu/ewt6QfBpJ0PGyH8VrYs
Rmz2paJcP8P0OPmoq14ucS38DJsJGCNEZAnUjdz8XO5O1t77w1cR5RA20FgScAH6SA3zvMudCPG+
1pj7XeThYB7S8Q4H+3dMzXoS5RFRjVCoqlE4njCJANkY3V3LOyHeufNWbom4wSbOZ7QaUwRrg9h3
jMLMAyoMcmS/tpIcjugQl9Z8BLdORqggQRnbNX+NQ6jY6EzUMaQ63o17lzggLzukXgNjvPYORbOg
KntI9HsMagbLF33JjaUsXZmp0LmM0cX/vmNvPV4eFwub4nIQB4rMV64KgHhvZAWfYY7wUSa2Ofk4
P0v9A46GZggEdHKM0KWnnbOOCyK920A+fTBjvPFXXwN6VxMxkWYTNnQrteMbtJJCg3h5ZscrFuZv
8L1KLNOUzTdppor4q5FOhSjirgtY5IJ/UaCE2x7C2rkqnqgXhpAf122JdVPSl8zFnOwG66yVX28c
46To8dee4UhrcvziDZsa9xvGPaKu3Bd8KyewQAqNxePy/WngEmS8SzRZUANOxyQk4K+bOfx0DL2w
YPh/pbsCBEK2Go+yFVeiu9DXD5Se+Uwg3zBsiqcKQXCPB9qASadxKpyVR2DQBxHoa7701tvl84vc
SI4ycueNtVFC31F0n24BFuglqvlyUoG2bH+Rhz3Tq/7q8tuLerRunZR7JkD9CYiBmXmcnxhdhQNW
GROIZoD7SEnlc1oQX4LpRcodAF7rEwSMbLx7eQZ4YtllpAjtc2oq9/tYXtZGSsnJ4eFi+EBEBrLa
cv7kPDHVK/6d+J/CLWs3FANrQZs9vv/CPtB2YdIH+swyVqBIAUjmzG0QKFBhD3FGi2rxe+2LPvYz
PC5sDMArOpLFeJgm2rw4H948Up3gPSEZAWUvPjtlBsOG4GDsZEbSWsU+2qaeGGJ7OX787zDc0GVn
tY529sopCa3r8C2ig/hPpzhsjEhkHAIXxxB6gtN6uv4WFdLx6WzX726GReukDNngCfgdVw01kcTV
VeIVdcxy1VyKr4TkdnF05PRT1xEzkyYjH6bvW7YkvWjeM19UEk//dRdYDUdWtbw78oE17YzWePc4
xiYJvNTotlQj/jc8x8JSb+UJRm+HXQ6eC5sT4ck7X6Gf9ZsHN6caZr+UM0ustKT5TMcr8NOo1k4h
obLJSDunakR3hBO7csnU8uJYo1B6jgwYN9cqkIWV4QG/xWOJuOyimD3llr9K7/GDbYTthc1/y8gC
IRtuB6IEoZ4Cx/oqIT2xBDsKNCjHY0X6KTtXgJZINcEyQMcEbCGq8O2zDhOghJr/fHtvoct9j4mC
ui4FKj2lRuX49OPM7+AUeOViXvA+10/fa63mO24p7SSek1qG4bXaIhlwJsZM7swPKtqMmdDs3ky8
I5Je3t08g5Qik+t7zd0+PXcnJrLchNR9OFxIWe2IJX/liNRIOVkWdLciHihhk+LgwAEdJcMmNE8f
S60/J2zSWZkBoiB0dLJKL1CtXb5SjXCENwKRcfYaBrmJqGU9nkRWQfaQNjsBLefTQLRatAgguvZL
7sG8pAvHbyWE6+yaK+qVrXAPbfDrBqPLPmb8TeHroxiekgw27BZfPa2S1wewN65QJIiyOAVtcBOG
6OIvXrJzoLtFfeWYY2eVnNxCm/WQ2W/Ec+19lR2qlzRq1uc9oKPX6D+n5nFjB3cgt6//QZCXQjPQ
Jz/6X+Qoy995k4B68VRnW6kyaGaoAIXARqxqIvueNXSGNooIyTCJL56k8QF1R31l7DeyeBjCjObn
CK4kmH5QVCYAJMdeGnBiygY2qFJenITxf3erLAgnLA5vuzk8NETeyeTCLYa/BcZdwD3CzjXEM1dZ
a+4+DG6ST1OE7CPjhob08GERyKFD8fVtlVhWzoCUA+wc2smcmBOepKvvSmQtwKNidJo0H9tuYRT1
LRoCXZlbhlC2AgCG0PhSWjbL6G4/68OVrVpLaJZYn8YKu56s8goIA6w3mDImqrYSaOXEZXj7ssOF
PxHIkDgRWytEvwAvVLqgnx/0XFj1wpXxpJ8079cgDtcjLoZ9Y0myrCZ8wpO3lSWGuNglClfY5Xo2
yJrCKO9XbaeMGp9ACebQ/BYpacey5hNvO2XOucSxeyp0XstMsMlwWZMS4tt7wm3MxrVhj2wcKg5n
ZH+lkAlX6ZBDxx2FVutzkJmGelxwVgKG1ByLxLhv06AL8o3JUGLunI8gm5uvHReZKLLiAoeVbgZO
8Bt3TBCBOzTQsLwuGrwHvKMEEhX4A9AzArs+vsnbVjmGNYGbZmprB401f1RQBN41z8T0Jh3uHmZF
1F2b4zUwK035QDd8/V1VLTet81TQ4AN90pUGnhjKeGWlHQDIZkWKXyixNcPj91s6YNets2vAPIFX
R7v9dmGPwgOq0tUWUvIbgSIXQ6VA9mS9zj3QtAwj/3HJl3k/tR5M1OOK+2lz6ito0RNqyH7J4QZM
N/MwBHRejx7bPfkLCfASwFVHWbNLO3xhR364y9e0zR5CQbybLqjPQHA18q6MjNF308jJufXpWshU
So44I1o7Dz2iBguQQKywWPxmpZfrjssOuttQMFx58ZUclzGKkE2PRSD9j8JzK61raQh8HzaCQ4z0
vHMRQtkZZtJyuDhAsGITKYqCvr7jzg+Kz5UsjMQULgG2d5ODh2EmcBHzB747MbfbBJ1+H3p8SAss
IAE4G4BRvrreH09tl5P0HCJOOeNtvRdymQJ/5f5fCHDjQpC/JyEGz/1cb613V8zZKRN26go7fWr7
UkQdC5v9XzdafbmYaTG7rWKdlirCEoLaBNJ+VNOOuvrAPaiUoitzStmLMvWeYOLVeqiuSal6AfOU
knN9aCEa3eaQ3kOjc8T/pYBiPyRBA7nTVQoLne98xI+v/KUMe2RU4T7rpbFnY+vSZZb/FtnCTvyK
vpAsS+FvgUUmBrq12TEWAT/S+NropghguLod1A0v7zX69rVBV9gef7624vgZTrOod3eg7jUMgJhK
U+PI9d0MqMtN5iOTEbaz2qSjIJol1B2m2y18Xpyifwup8tZKnXWKK8mQ65NhNSL9xlFCe5WnWYvH
yAr3c7lT137av8oEfyFdYmqDvzizTbMnC2bkV6wMcdV4IuMcwVnxmyc901lyYSP/Ld4v3BS5pKm9
iLCf876mR7Z2KrEcyiNEPy+sG63WVBJG1yukPYJFl7nJQ7rz161fWL3HMlZQD2ycKA+NRGvJEAm0
rWXqLCoQnT0GFxLB3N8em4u2KrE+DJsuANEGOvfdiXybvvwhQvoD3Ht+D2kalBljRkheeqplwE+R
wyrhPdYmY4PtxLIf6KiF1z784+XOuJxgL9G0EZZyqVg6fnhr2tbXyNbRyl3irztRkOvXRtmjd+5i
F91XpZlm2tbAS9DJwx99dL/a3gYBJkXNusHEYwYIs26gi/Wir8zKlCCNuCLUjI1LMMxgUSLHnYk5
Kx3uwIyHtIQTIl8PdMA6NEjLpuaHJiUPYT2vM403CtqxeLbKjnNV+IMwTSzi3pggQbjerMVByQse
7fLit8dIEPY+m/czxiuIrtLnZ8QY9nUBOBIePc6xGnhoK3h2Zh9jJcxYwgjzz1BBUFvEpONhF0at
VySnkItS30eXomxo/l8/aUEGQY4waHsAgHrZKDROzj8VLptHxP6ctN/m6Oml8vV1/fZIoPp5Mn09
Y7Mb19rP2k2QdrfY7pg2ELQw0s6gVm+HylWy+BMC0wnnE+Y+6MR6dVth0ACXh34xLv76ANh2EH8V
gf0HePClQ/FJXHz6oGAB2ce1jrMJNtRQ3n2SBWHdRE1Ju7TWqM2iaT5m2Eo+u2rBFqaBrdy9es5P
ZIMTrRDZGPFU2IhCZSKdjdjwsTyLDbGJ1WIyX0NNo7d+nwBC0MXVCXz6PXLGX4fsU5E44sndnVH+
aTJgb/3zV457UMggLoWD0M0JSX35ZXjiJaHMI0SQu8y9rc4I//aZJ4w4YJMSSL2cCiLKMbrgKAtR
GiubdY5NuUfbLZh8lxTeG73AjqDJw4W07Ka+aDZIhfbq2X90EDNOU43UG8HH6aRg7xbywXy5uBWO
Hj4TiIoW1g2M6xU+SQr6lfd622R14Uj9Obm2RNhJdvgkI2u2FjFKHR+DiNF5UrV4wDyPaq91/iDo
978I3P1D4iiDtCJLdR5etOqTElWIUyIqge7kl50gfmBZNrn+DUsKr3/TRfTth7yq1yw6x/uQoTkg
NpdwDjc14ivWfVuC/9dch0ov1gMvvMBEB/u53GK6GIuejME94v0R4wB+m+q3lO51cijpn+uSU/3I
t2TJvowhnBbH17H/A5wsk6V1G2cIXoTr2WxsRi0SAn8t1WkTYoI75rom6QdySE3PALpK+wttUIPF
K4BUVodldT2sJtYR/yfbW05D7SdrhAGj5dG6koUjNQEhUfx10WewovZaJSDD1tLAcIhKpV5amSf4
fw+0fNVqudx11H9ideCKM3INcnfZ4GfQqdZ4nGeHc25AgP59yl3rcTptFKDwzPgOZDJ9ae67hvOO
xLzFoKlzJQRbNIxes4t/lQxLtjcbOyFIQ3eKSPLLSX2ryUxeQnlmbF+RSlJs6ba5sriqPrLMst08
Jt/04MxkfRzMHN3wU8STm0GMhsCiXikEtCA4bmJDso++gO36aD75sG+D8KPLOPYqXEon8npLpogn
Jp4Ghlubhbv5fJesJoKSNo7DtagLHPQs+P8vJPNvUmnGvykpwv5IlZDIcn5HbWHB8k/CF4uohnn/
XUNLRaa6mJo2Umq1X/Ok0UVnjN2tOlg7h35bbXUFwMuMFtl/ZrS72vYxd+IeD8KDwKWNQfP7x8El
D5lZvX1OfPc6thbnpRtA/F5qgEj+X6g6fXspmP4gZwzS4uOUO+P5EvBLEl+W02oTdzITdOCuaV9D
2F0+x9uuzsCcx9+FFADRAUEelCOWpN/XRzfhWN8dua+wzPnGY1yxXF7HGVBkaM+ES84ClRO6SfXp
n9Vl/J+5MgyJOnsCoa0VsEqk+qPam1xjIW7jnTIngov1NI5kgC6ynn6DpYXeknW5KiMteMzpcSS0
9KDFPjofo5AZUShHaVV2DmZ4qAEK/khrz7h6AVky5i17N2tituZPnVtozMQ11CIAj2Ts48ALadTz
MQugVcHsQkCJM6ectjjVbjQSXlHDUvE6HFLBTn9L6BPaWp3gJD0l3Z454SEHkDFj6nPxaH+h2xQf
ndviP/Cw2TT/Zn1HQafzMKI0IkV+zqLDSdkTI0yiqb+n0F6AFrpCabxs04VY56yKdx7ndu1lLzPw
8Dix+ocs4hhia8JgZ9jVr0r+8lJNfsdF4m6qrA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
